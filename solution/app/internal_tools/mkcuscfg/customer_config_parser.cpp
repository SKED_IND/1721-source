/********************************************************************************************/
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/* Montage Proprietary and Confidential                                                     */
/* Copyright (c) 2014 Montage Technology Group Limited and its affiliated companies         */
/********************************************************************************************/
#include "sys_types.h"
#include "sys_define.h"
#include "drv_dev.h"
#include "common.h"
#include "tinystr.h"
#include "tinyxml.h"
#include "mtos_printk.h"
#include "hal_misc.h"
#include "hal_gpio.h"
#include "uio.h"
#include "vdec.h"
#include "charsto.h"

#include "customer_config.h"

typedef struct
{
  int value;
  const char *p_value_str;
}value_map_t;

#define DEFINE_VALUE_MAP(_v) {_v, #_v}
static value_map_t g_customer_ids[]=
{
  DEFINE_VALUE_MAP(CUSTOMER_DEFAULT),
  DEFINE_VALUE_MAP(CUSTOMER_YINHE),
  DEFINE_VALUE_MAP(CUSTOMER_YINHE_CANGZHOU),
  DEFINE_VALUE_MAP(CUSTOMER_YINHEPH_SMSX),
  DEFINE_VALUE_MAP(CUSTOMER_KINGVON),
  DEFINE_VALUE_MAP(CUSTOMER_JIZHONG),
  DEFINE_VALUE_MAP(CUSTOMER_NEWSTART),
  DEFINE_VALUE_MAP(CUSTOMER_AISAT),
  DEFINE_VALUE_MAP(CUSTOMER_ORRISDIGITAL),
  DEFINE_VALUE_MAP(CUSTOMER_MSTK),
  DEFINE_VALUE_MAP(CUSTOMER_CHANGHONG),
  DEFINE_VALUE_MAP(CUSTOMER_FANTONG),
  DEFINE_VALUE_MAP(CUSTOMER_TONGHUI),
  DEFINE_VALUE_MAP(CUSTOMER_JIULIAN),
  DEFINE_VALUE_MAP(CUSTOMER_BOYUAN),
  DEFINE_VALUE_MAP(CUSTOMER_KONKA),
  DEFINE_VALUE_MAP(CUSTOMER_HUAXIDA),
  DEFINE_VALUE_MAP(CUSTOMER_PLAAS),
  DEFINE_VALUE_MAP(CUSTOMER_NEWLAND),
  DEFINE_VALUE_MAP(CUSTOMER_NEXTBIT),
  DEFINE_VALUE_MAP(CUSTOMER_ZHONGDA),
  DEFINE_VALUE_MAP(CUSTOMER_JIESAI),
  DEFINE_VALUE_MAP(CUSTOMER_JIESAI_MINI),
  DEFINE_VALUE_MAP(CUSTOMER_JIESAI_NK),
  DEFINE_VALUE_MAP(CUSTOMER_TONGJIU),
  DEFINE_VALUE_MAP(CUSTOMER_DFGS),
  DEFINE_VALUE_MAP(CUSTOMER_GUIYANG),
  DEFINE_VALUE_MAP(CUSTOMER_AISAT_HOTEL),
  DEFINE_VALUE_MAP(CUSTOMER_AISAT_DIVI),
  DEFINE_VALUE_MAP(CUSTOMER_HUANGSHI),
  DEFINE_VALUE_MAP(CUSTOMER_ZHUMUDIAN_BY),
  DEFINE_VALUE_MAP(CUSTOMER_AISAT_DEMO),
  DEFINE_VALUE_MAP(CUSTOMER_JIZHONGMEX),
  DEFINE_VALUE_MAP(CUSTOMER_NEXTBITNETWORK),
  DEFINE_VALUE_MAP(CUSTOMER_TAIHUI_DIVI),
  DEFINE_VALUE_MAP(CUSTOMER_TAIHUI_ZHUANGHE),
  DEFINE_VALUE_MAP(CUSTOMER_TAIHUI_QUANZHI),
  DEFINE_VALUE_MAP(CUSTOMER_TAIHUI_WANFA),
  DEFINE_VALUE_MAP(CUSTOMER_TAIHUI_TENGRUI),
  DEFINE_VALUE_MAP(CUSTOMER_AISAT_CHINA),
  DEFINE_VALUE_MAP(CUSTOMER_JIZHONG_DTMB),
  DEFINE_VALUE_MAP(CUSTOMER_JINGHUICHENG_DIVI),
  DEFINE_VALUE_MAP(CUSTOMER_KONKA_SV),
  DEFINE_VALUE_MAP(CUSTOMER_MSTK_NXP),
  DEFINE_VALUE_MAP(CUSTOMER_AISAT_HDS),
  DEFINE_VALUE_MAP(CUSTOMER_AISAT_BHAVANI),
  DEFINE_VALUE_MAP(CUSTOMER_KONKA_MONGO),
  DEFINE_VALUE_MAP(CUSTOMER_KONKA_IND),
  DEFINE_VALUE_MAP(CUSTOMER_KONKA_VERIMATRIX),
  DEFINE_VALUE_MAP(CUSTOMER_MSTK_DS56),
  DEFINE_VALUE_MAP(CUSTOMER_JIZHONG_DS57),
  DEFINE_VALUE_MAP(CUSTOMER_JIZHONG_NB_DS57),
  DEFINE_VALUE_MAP(CUSTOMER_JIZHONG_SM),
  DEFINE_VALUE_MAP(CUSTOMER_JHCXSD_XY),
  DEFINE_VALUE_MAP(CUSTOMER_JHCXSD_DC),
  DEFINE_VALUE_MAP(CUSTOMER_XSDDS52),
  DEFINE_VALUE_MAP(CUSTOMER_XSDDS52_WM),
  DEFINE_VALUE_MAP(CUSTOMER_XINNIU),
  DEFINE_VALUE_MAP(CUSTOMER_ZHILING),
  DEFINE_VALUE_MAP(CUSTOMER_MCBS),
  DEFINE_VALUE_MAP(CUSTOMER_AISAT_DS57),
  DEFINE_VALUE_MAP(CUSTOMER_AISAT_PHP),
  DEFINE_VALUE_MAP(CUSTOMER_ABLEE),
  DEFINE_VALUE_MAP(CUSTOMER_TAILI_DTMB),
  DEFINE_VALUE_MAP(CUSTOMER_YINHE_GTPL),
  DEFINE_VALUE_MAP(CUSTOMER_NEWSTAR_DTMB),
  DEFINE_VALUE_MAP(CUSTOMER_HUANGSHI_DF),
  DEFINE_VALUE_MAP(CUSTOMER_JIZHONG_TL_DS57),
  DEFINE_VALUE_MAP(CUSTOMER_MAIKE),
  DEFINE_VALUE_MAP(CUSTOMER_JIZHONG_PAK_DS57),
  DEFINE_VALUE_MAP(CUSTOMER_GUANGMAO_DS57),
  DEFINE_VALUE_MAP(CUSTOMER_CATVWALKER_DTMB),
  DEFINE_VALUE_MAP(CUSTOMER_JIZHONGMEX_J83B),
  DEFINE_VALUE_MAP(CUSTOMER_DS52),
  DEFINE_VALUE_MAP(CUSTOMER_SAFEVIEW_DEMO),
  DEFINE_VALUE_MAP(CUSTOMER_NEWGLEE_SUANTONG),
  DEFINE_VALUE_MAP(CUSTOMER_HUAXIDA_TENGO_DS57),
  DEFINE_VALUE_MAP(CUSTOMER_CATVISION),
};

static value_map_t g_cas_ids[]=
{
  /*
  desai config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_DS),
  /*
  only_1 config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_ONLY_1),
  /*
  abv config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_ABV),
  /*
  yxsb config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_TF),
  /*
  advance  yxsb config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_ADV_TF),
  /*
  cryptoguard config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_CRYPG),
  /*
  quanzhi config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_GS),
  /*
  sanzhouxunchi config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_ADT_MG),
  /*
  topreal config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_TR),
  /*
  sumavision config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_SV),
  /*
  kingvon40 config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_KV40),
  /*
  by config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_BY),
  /*
  diviguard 2.1 config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_DIVI),
  /*
  Enigma CA config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_ENIGMA),
  /*
  uti config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_UNITEND),
  /*
  xsm config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_XSM),
  /*
  dmt config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_DMT),
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_CONAXK),
  /*
  wanfa ca
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_WF),
  /*
  quanzhi wuka ca config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_GS_NO_SMC),
  /*
  suantong ca config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_CTI),
  /*
  yadianna ca
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_YADIANNA),
  /*
  safeview cas  config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_SAFEVIEW),
  /*
  qilian cas  config id
  */
  DEFINE_VALUE_MAP(CONFIG_CAS_ID_QL),    
};

static value_map_t g_vdec_policy[]=
{
  DEFINE_VALUE_MAP(VDEC_QAULITY_AD),
  DEFINE_VALUE_MAP(VDEC_BUFFER_AD),
  DEFINE_VALUE_MAP(VDEC_MULTI_PIC_AD),
  DEFINE_VALUE_MAP(VDEC_QAULITY_AD_128M),
  DEFINE_VALUE_MAP(VDEC_SDINPUT_ONLY),
  DEFINE_VALUE_MAP(VDEC_OPENDI_64M),
  DEFINE_VALUE_MAP(VDEC_QAULITY_AD_UNUSEPRESCALE),
  DEFINE_VALUE_MAP(VDEC_BUFFER_AD_UNUSEPRESCALE),
  DEFINE_VALUE_MAP(VDEC_QAULITY_AD_128M_UNUSEPRESCALE),
  DEFINE_VALUE_MAP(VDEC_SDINPUT_ONLY_UNUSEPRESCALE),
  DEFINE_VALUE_MAP(VDEC_OPENDI_64M_UNUSEPRESCALE),
  DEFINE_VALUE_MAP(VDEC_QAULITY_AD_WITH_HEVC),
  DEFINE_VALUE_MAP(VDEC_BUFFER_AD_WITH_HEVC),
  DEFINE_VALUE_MAP(VDEC_QAULITY_AD_128M_WITH_HEVC),
  DEFINE_VALUE_MAP(VDEC_SDINPUT_ONLY_WITH_HEVC),
  DEFINE_VALUE_MAP(VDEC_OPENDI_64M_WITH_HEVC),
  DEFINE_VALUE_MAP(VDEC_SDINPUT_ONLY_31),
  DEFINE_VALUE_MAP(VDEC_SDINPUT_ONLY_41),
};

static value_map_t g_rec_pid_type[]=
{
  DEFINE_VALUE_MAP(REC_CAT),
  DEFINE_VALUE_MAP(REC_EMM),
  DEFINE_VALUE_MAP(REC_PMT),
  DEFINE_VALUE_MAP(REC_ECM),
  DEFINE_VALUE_MAP(REC_SUBT),
};

static value_map_t g_nim_type[]=
{
  DEFINE_VALUE_MAP(NIM_CS8K_CAB),
  DEFINE_VALUE_MAP(NIM_CS8K_S),
  DEFINE_VALUE_MAP(NIM_CS8K_CAB_S),
  DEFINE_VALUE_MAP(NIM_DM6K_T2),
  DEFINE_VALUE_MAP(NIM_DM6K_T2_S2),
  DEFINE_VALUE_MAP(NIM_DM6K_C_T2_S2),
};


static value_map_t g_countries[]=
{
  DEFINE_VALUE_MAP(COUNTRY_INDIA),
  DEFINE_VALUE_MAP(COUNTRY_CHINA),
  DEFINE_VALUE_MAP(COUNTRY_PAKIST)
};

static value_map_t g_flash_protection[] = 
{
  /*!
  block unprotection
  */
  DEFINE_VALUE_MAP(PRT_UNPROT_ALL),
  /*!
  block protection all
  */
  DEFINE_VALUE_MAP(PRT_PROT_ALL), 
  /*!
  block protection), up address 1/64
  */
  DEFINE_VALUE_MAP(PRT_UPPER_1_64),
  /*!
  block protection), up address 1/32
  */
  DEFINE_VALUE_MAP(PRT_UPPER_1_32),
  /*!
  block protection), up address 1/16
  */
  DEFINE_VALUE_MAP(PRT_UPPER_1_16),
  /*!
  block protection), up address 1/8
  */
  DEFINE_VALUE_MAP(PRT_UPPER_1_8),
  /*!
  block protection), up address 1/4
  */
  DEFINE_VALUE_MAP(PRT_UPPER_1_4),
  /*!
  block protection), up address 1/2
  */
  DEFINE_VALUE_MAP(PRT_UPPER_1_2),
  /*!
  block protection), up LOWER 1/64
  */
  DEFINE_VALUE_MAP(PRT_LOWER_1_64),
  /*!
  block protection), up LOWER 1/32
  */
  DEFINE_VALUE_MAP(PRT_LOWER_1_32),
  /*!
  block protection), up LOWER 1/16
  */
  DEFINE_VALUE_MAP(PRT_LOWER_1_16),
  /*!
  block protection), up LOWER 1/8
  */
  DEFINE_VALUE_MAP(PRT_LOWER_1_8),
  /*!
  block protection), up LOWER 1/4
  */
  DEFINE_VALUE_MAP(PRT_LOWER_1_4),
  /*!
  block protection), up LOWER 1/2
  */
  DEFINE_VALUE_MAP(PRT_LOWER_1_2),
  /*!
  block protection), up LOWER 3/4
  */
  DEFINE_VALUE_MAP(PRT_LOWER_3_4),
  /*!
  block protection), up LOWER 7/8
  */
  DEFINE_VALUE_MAP(PRT_LOWER_7_8),
  /*!
  block protection), up LOWER 15/16
  */
  DEFINE_VALUE_MAP(PRT_LOWER_15_16), 
  /*!
  block protection), up LOWER 31/32
  */
  DEFINE_VALUE_MAP(PRT_LOWER_31_32), 
  /*!
  block protection), up LOWER 63/64
  */
  DEFINE_VALUE_MAP(PRT_LOWER_63_64), 
};

static value_map_t g_lock_modes[] = 
{
  /*!
  DVBS
  */
  DEFINE_VALUE_MAP(SYS_DVBS),
  /*!
  DVBC
  */
  DEFINE_VALUE_MAP(SYS_DVBC),
  /*!
  DVBT2-DVBT
  */
  DEFINE_VALUE_MAP(SYS_DVBT2),
  /*!
  ABS-S
  */
  DEFINE_VALUE_MAP(SYS_ABSS),
  /*!
  DTMB
  */
  DEFINE_VALUE_MAP(SYS_DTMB),
  /*!
  support DVB-S),DVB-C),DVB-T all in one
  */
  DEFINE_VALUE_MAP(SYS_DVB_3IN1)
};

static value_map_t g_modulations[] = 
{
  /*!
  Auto modulation detection
  */
  DEFINE_VALUE_MAP(NIM_MODULA_AUTO),
  /*!
  BPSK
  */
  DEFINE_VALUE_MAP(NIM_MODULA_BPSK),
  /*!
  QPSK
  */
  DEFINE_VALUE_MAP(NIM_MODULA_QPSK),
  /*!
  8PSK
  */
  DEFINE_VALUE_MAP(NIM_MODULA_8PSK),
  /*!
  QAM 16
  */
  DEFINE_VALUE_MAP(NIM_MODULA_QAM16),
  /*!
  QAM 32
  */
  DEFINE_VALUE_MAP(NIM_MODULA_QAM32),
  /*!
  QAM 64
  */
  DEFINE_VALUE_MAP(NIM_MODULA_QAM64),
  /*!
  QAM 128
  */
  DEFINE_VALUE_MAP(NIM_MODULA_QAM128),
  /*!
  QAM 256
  */
  DEFINE_VALUE_MAP(NIM_MODULA_QAM256),
};

/*!
All supported virtual keys
*/
static value_map_t g_keys[] = 
{
  /*!
  Invalid key
  */
  DEFINE_VALUE_MAP(V_KEY_INVALID),
  /*!
  Power key
  */
  DEFINE_VALUE_MAP(V_KEY_POWER),
  /*!
  Mute key
  */
  DEFINE_VALUE_MAP(V_KEY_MUTE),
  /*!
  Recall key 
  */
  DEFINE_VALUE_MAP(V_KEY_RECALL),
  /*!
  TV/Radio switch
  */
  DEFINE_VALUE_MAP(V_KEY_TVRADIO),
  /*!
  Key 0
  */
  DEFINE_VALUE_MAP(V_KEY_0),
  /*!
  Key 1 
  */
  DEFINE_VALUE_MAP(V_KEY_1),
  /*!
  Key 2 
  */
  DEFINE_VALUE_MAP(V_KEY_2),
  /*!
  Key 3
  */
  DEFINE_VALUE_MAP(V_KEY_3),
  /*!
  Key 4
  */
  DEFINE_VALUE_MAP(V_KEY_4),
  /*!
  Key 5
  */
  DEFINE_VALUE_MAP(V_KEY_5),
  /*!
  Key 6
  */
  DEFINE_VALUE_MAP(V_KEY_6),
  /*!
  Key 7
  */
  DEFINE_VALUE_MAP(V_KEY_7),
  /*!
  Key 8
  */
  DEFINE_VALUE_MAP(V_KEY_8),
  /*!
  Key 9
  */
  DEFINE_VALUE_MAP(V_KEY_9),
  /*!
  Cancel key
  */
  DEFINE_VALUE_MAP(V_KEY_CANCEL),
  /*!
  OK key
  */
  DEFINE_VALUE_MAP(V_KEY_OK),
  /*!
  Up key
  */
  DEFINE_VALUE_MAP(V_KEY_UP),
  /*!
  Down key
  */
  DEFINE_VALUE_MAP(V_KEY_DOWN),
  /*!
  Left key
  */
  DEFINE_VALUE_MAP(V_KEY_LEFT),
  /*!
  Right key
  */
  DEFINE_VALUE_MAP(V_KEY_RIGHT),
  /*!
  Menu key
  */
  DEFINE_VALUE_MAP(V_KEY_MENU),
  /*!
  Porgram list key
  */
  DEFINE_VALUE_MAP(V_KEY_PROGLIST),
  /*!
  Audio key
  */
  DEFINE_VALUE_MAP(V_KEY_AUDIO),
  /*!
  Page up key
  */
  DEFINE_VALUE_MAP(V_KEY_PAGE_UP),
  /*!
  Page down key
  */
  DEFINE_VALUE_MAP(V_KEY_PAGE_DOWN),
  /*!
  Infor Key
  */
  DEFINE_VALUE_MAP(V_KEY_INFO),
  /*!
  Favorite key
  */
  DEFINE_VALUE_MAP(V_KEY_FAV),
  /*!
  Pause key
  */
  DEFINE_VALUE_MAP(V_KEY_PAUSE),
  /*!
  Play key
  */
  DEFINE_VALUE_MAP(V_KEY_PLAY),
  /*!
  9 Picture key
  */
  DEFINE_VALUE_MAP(V_KEY_9PIC),
  /*!
  EPG key
  */
  DEFINE_VALUE_MAP(V_KEY_EPG),
  /*!
  SI key
  */
  DEFINE_VALUE_MAP(V_KEY_SI),
  /*!
  Gray key
  */
  DEFINE_VALUE_MAP(V_KEY_GRAY),
  /*!
  Red key 
  */
  DEFINE_VALUE_MAP(V_KEY_RED),
  /*!
  Green key
  */
  DEFINE_VALUE_MAP(V_KEY_GREEN),
  /*!
  Yellow key
  */
  DEFINE_VALUE_MAP(V_KEY_YELLOW),
  /*!
  Blue key
  */
  DEFINE_VALUE_MAP(V_KEY_BLUE),
  /*!
  UPG key
  */
  DEFINE_VALUE_MAP(V_KEY_UPG),
  /*!
  TTX key
  */
  DEFINE_VALUE_MAP(V_KEY_TTX),
  /*!
  Test key
  */
  DEFINE_VALUE_MAP(V_KEY_TEST),
  /*!
  I2C key
  */
  DEFINE_VALUE_MAP(V_KEY_I2C),
  /*!
  F2 Key
  */
  DEFINE_VALUE_MAP(V_KEY_F1),
  /*!
  F2 Key
  */
  DEFINE_VALUE_MAP(V_KEY_F2),
  /*!
  F3 Key
  */
  DEFINE_VALUE_MAP(V_KEY_F3),
  /*!
  F4 Key
  */
  DEFINE_VALUE_MAP(V_KEY_F4),
  /*!
  F5 Key
  */
  DEFINE_VALUE_MAP(V_KEY_F5),
  /*!
  STOCK Key
  */
  DEFINE_VALUE_MAP(V_KEY_STOCK),
  /*!
  BOOK Key
  */
  DEFINE_VALUE_MAP(V_KEY_BOOK),
  /*!
  TV Key
  */
  DEFINE_VALUE_MAP(V_KEY_TV),
  /*!
  RADIO Key
  */
  DEFINE_VALUE_MAP(V_KEY_RADIO),
  /*!
  OTA force key
  */
  DEFINE_VALUE_MAP(V_KEY_OTA_FORCE),
  /*!
  SAT key
  */
  DEFINE_VALUE_MAP(V_KEY_SAT),
  /*!
  ENTER ucas
  */
  DEFINE_VALUE_MAP(V_KEY_UCAS),
  /*!
  ENTER satcode
  */
  DEFINE_VALUE_MAP(V_KEY_SAT_CODE),
  /*!
  ctrl + 0
  */
  DEFINE_VALUE_MAP(V_KEY_CTRL0),
  /*!
  ctrl + 1
  */
  DEFINE_VALUE_MAP(V_KEY_CTRL1),
  /*!
  ctrl + 2
  */  
  DEFINE_VALUE_MAP(V_KEY_CTRL2),
  /*!
  ctrl + 3
  */  
  DEFINE_VALUE_MAP(V_KEY_CTRL3),
  /*!
  ctrl + 4
  */  
  DEFINE_VALUE_MAP(V_KEY_CTRL4),
  /*!
  ctrl + 5
  */  
  DEFINE_VALUE_MAP(V_KEY_CTRL5),
  /*!
  ctrl + 6
  */  
  DEFINE_VALUE_MAP(V_KEY_CTRL6),
  /*!
  ctrl + 7
  */  
  DEFINE_VALUE_MAP(V_KEY_CTRL7),
  /*!
  ctrl + 8
  */  
  DEFINE_VALUE_MAP(V_KEY_CTRL8),
  /*!
  ctrl + 9
  */  
  DEFINE_VALUE_MAP(V_KEY_CTRL9),
  /*!
  ctrl + s0
  */
  DEFINE_VALUE_MAP(V_KEY_CTRLS0),
  /*!
  ctrl + s1
  */
  DEFINE_VALUE_MAP(V_KEY_CTRLS1),
  /*!
  ctrl + s2
  */  
  DEFINE_VALUE_MAP(V_KEY_CTRLS2),
  /*!
  ctrl + s3
  */  
  DEFINE_VALUE_MAP(V_KEY_CTRLS3),
  /*!
  ctrl + s4
  */  
  DEFINE_VALUE_MAP(V_KEY_CTRLS4),
  /*!
  ctrl + s5
  */  
  DEFINE_VALUE_MAP(V_KEY_CTRLS5),
  /*!
  ctrl + s6
  */  
  DEFINE_VALUE_MAP(V_KEY_CTRLS6),
  /*!
  ctrl + s7
  */  
  DEFINE_VALUE_MAP(V_KEY_CTRLS7),
  /*!
  ctrl + s8
  */  
  DEFINE_VALUE_MAP(V_KEY_CTRLS8),
  /*!
  ctrl + s9
  */  
  DEFINE_VALUE_MAP(V_KEY_CTRLS9),
  /*!
  pos
  */  
  DEFINE_VALUE_MAP(V_KEY_POS),
  /*!
  rec
  */  
  DEFINE_VALUE_MAP(V_KEY_REC),
  /*!
  pn
  */  
  DEFINE_VALUE_MAP(V_KEY_PN),
  /*!
  ts
  */  
  DEFINE_VALUE_MAP(V_KEY_TS),
  /*!
  back2
  */  
  DEFINE_VALUE_MAP(V_KEY_BACK2),
  /*!
  forw2
  */  
  DEFINE_VALUE_MAP(V_KEY_FORW2),
  /*!
  back
  */  
  DEFINE_VALUE_MAP(V_KEY_BACK),
  /*!
  forw
  */  
  DEFINE_VALUE_MAP(V_KEY_FORW),
  /*!
  revslow
  */  
  DEFINE_VALUE_MAP(V_KEY_REVSLOW),
  /*!
  slow
  */  
  DEFINE_VALUE_MAP(V_KEY_SLOW),
  /*!
  stop
  */  
  DEFINE_VALUE_MAP(V_KEY_STOP),
  /*!
  vdown
  */  
  DEFINE_VALUE_MAP(V_KEY_VDOWN),
  /*!
  vup
  */  
  DEFINE_VALUE_MAP(V_KEY_VUP),
  /*!
  subt
  */  
  DEFINE_VALUE_MAP(V_KEY_SUBT),
  /*!
  zoom
  */  
  DEFINE_VALUE_MAP(V_KEY_ZOOM),
  /*!
  tvsat
  */  
  DEFINE_VALUE_MAP(V_KEY_TVSAT),
  /*!
  find
  */  
  DEFINE_VALUE_MAP(V_KEY_FIND),
  /*!
  d
  */  
  DEFINE_VALUE_MAP(V_KEY_D),
  /*!
  tvav
  */  
  DEFINE_VALUE_MAP(V_KEY_TVAV),
  /*!
  twin port
  */
  DEFINE_VALUE_MAP(V_KEY_TWIN_PORT),
  /*!
  CA Hide Menu
  */
  DEFINE_VALUE_MAP(V_KEY_CA_HIDE_MENU),
  /*!
  colorbar
  */
  DEFINE_VALUE_MAP(V_KEY_COLORBAR),
  /*!
  scan
  */
  DEFINE_VALUE_MAP(V_KEY_SCAN),
  /*!
  tp list
  */
  DEFINE_VALUE_MAP(V_KEY_TPLIST),
  /*!
  scan
  */
  DEFINE_VALUE_MAP(V_KEY_VOLUP),
  /*!
  tp list
  */
  DEFINE_VALUE_MAP(V_KEY_VOLDOWN),
  /*!
  language switch
  */
  DEFINE_VALUE_MAP(V_KEY_LANG),
  /*!
  exit
  */
  DEFINE_VALUE_MAP(V_KEY_EXIT),
  /*!
  sleep
  */
  DEFINE_VALUE_MAP(V_KEY_SLEEP),
  /*!
  video mode
  */
  DEFINE_VALUE_MAP(V_KEY_VIDEO_MODE),
  /*!
  game
  */
  DEFINE_VALUE_MAP(V_KEY_GAME),
  /*!
  tv playback
  */
  DEFINE_VALUE_MAP(V_KEY_TV_PLAYBACK),
  /*!
  p2p
  */
  DEFINE_VALUE_MAP(V_KEY_P2P),
  /*!
  VBI_INSERTER
  */
  DEFINE_VALUE_MAP(V_KEY_VBI_INSERTER),
  /*!
  ASPECT_MODE
  */
  DEFINE_VALUE_MAP(V_KEY_ASPECT_MODE),
  /*!
  DISPLAY_MODE
  */
  DEFINE_VALUE_MAP(V_KEY_DISPLAY_MODE),
  /*!
  LANGUAGE_SWITCH
  */
  DEFINE_VALUE_MAP(V_KEY_LANGUAGE_SWITCH),
  /*!
  PLAY_TYPE
  */
  DEFINE_VALUE_MAP(V_KEY_PLAY_TYPE),
  /*!
  BEEPER
  */
  DEFINE_VALUE_MAP(V_KEY_BEEPER),
  /*!
  CHANNEL_CHANGE
  */
  DEFINE_VALUE_MAP(V_KEY_CHANNEL_CHANGE),
  /*!
  LNB_POWER
  */
  DEFINE_VALUE_MAP(V_KEY_LNB_POWER),
  /*!
  OSD_TRANSPARENCY
  */
  DEFINE_VALUE_MAP(V_KEY_OSD_TRANSPARENCY),
  /*!
  LOOP_THROUGH
  */
  DEFINE_VALUE_MAP(V_KEY_LOOP_THROUGH),
  /*!
  BISS_KEY
  */
  DEFINE_VALUE_MAP(V_KEY_BISS_KEY),
  /*!
  SEARCH key
  */
  DEFINE_VALUE_MAP(V_KEY_SEARCH),
  /*!
  SEARCH key
  */
  DEFINE_VALUE_MAP(V_KEY_MAIL),
  /*!
  FACTORY key
  */
  DEFINE_VALUE_MAP(V_KEY_FACTORY),
  /*!
  FavUp key
  */
  DEFINE_VALUE_MAP(V_KEY_FAVUP),
  /*!
  FavDown key
  */
  DEFINE_VALUE_MAP(V_KEY_FAVDOWN),
  /*!
  NVOD key
  */
  DEFINE_VALUE_MAP(V_KEY_NVOD),
  /*!
  Data broadcast key
  */
  DEFINE_VALUE_MAP(V_KEY_DATA_BROADCAST),
  /*!
  Goto key
  */
  DEFINE_VALUE_MAP(V_KEY_GOTO),
  /*!
  CA Info key
  */
  DEFINE_VALUE_MAP(V_KEY_CA_INFO),
  /*!
  BISS and CryptoWorks SUPER Password_KEY
  */
  DEFINE_VALUE_MAP(V_KEY_SUPERPASSWORD_KEY),
  /*!
  previous
  */
  DEFINE_VALUE_MAP(V_KEY_PREV),
  /*!
  next
  */
  DEFINE_VALUE_MAP(V_KEY_NEXT),
  /*!
  repeat
  */
  DEFINE_VALUE_MAP(V_KEY_REPEAT),
  /*!
  list
  */
  DEFINE_VALUE_MAP(V_KEY_LIST),
  /*!
  gbox
  */
  DEFINE_VALUE_MAP(V_KEY_GBOX),
  /*!
  channel up
  */
  DEFINE_VALUE_MAP(V_KEY_CHUP),
  /*!
  channel down
  */
  DEFINE_VALUE_MAP(V_KEY_CHDOWN),
  /*!
  input
  */
  DEFINE_VALUE_MAP(V_KEY_INPUT),
  /*!
  help
  */
  DEFINE_VALUE_MAP(V_KEY_HELP),
  /*!
  set
  */
  DEFINE_VALUE_MAP(V_KEY_SET),
  /*!
  channel list
  */
  DEFINE_VALUE_MAP(V_KEY_CHANNEL_LIST),
};

static value_map_t g_vdac_types[] =
{
  /*!
  CVBS (RGB) format for video out.
  */
  DEFINE_VALUE_MAP(VDAC_CVBS_RGB),
  /*!
  Signal CVBS video out), need enable dac 3 on concerto platform
  */
  DEFINE_VALUE_MAP(VDAC_SIGN_CVBS),
  /*!
  Signal CVBS low power video out), use dac 3 for cvbs video out), need HW support
  */
  DEFINE_VALUE_MAP(VDAC_SIGN_CVBS_LOW_POWER),
  /*!
  CVBS+CVBS format for video out.
  */
  DEFINE_VALUE_MAP(VDAC_DUAL_CVBS),
  /*!
  SVDIEO format for  video out.
  */
  DEFINE_VALUE_MAP(VDAC_SIGN_SVIDEO),
  /*!
  CVBS + S_VIDEO format for  video out.
  */
  DEFINE_VALUE_MAP(VDAC_CVBS_SVIDEO),
  /*!
  YUV (SD) + CVBS format for  video out.
  */
  DEFINE_VALUE_MAP(VDAC_CVBS_YPBPR_SD),
  /*!
  YUV (HD) + CVBS format for  video out.
  */
  DEFINE_VALUE_MAP(VDAC_CVBS_YPBPR_HD),
};

static value_map_t g_nit_parse_type[] =
{
  /*!
  NIT PARSE FUNC use parse_nit.
  */
  DEFINE_VALUE_MAP(PARSE_NIT_STANDARD),
  /*!
  NIT PARSE FUNC use parse_nit_india.
  */
  DEFINE_VALUE_MAP(PARSE_NIT_INDIA),
};


#define GET_VALUE(_vm, _str, _val)\
  do{\
  int i;\
  for(i=0; i<(int)(sizeof(_vm)/sizeof(_vm[0])); i++){\
  if (! strcmp(_vm[i].p_value_str, _str)){\
  _val = _vm[i].value;\
  break;\
  }\
  }\
  }while(0)

typedef struct
{
  const char *p_cfg_key;
  void (*p_parser)(customer_cfg_t *p_cfg, TiXmlElement *p_element);
}cus_cfg_parser_t;


static void customer_id_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("customer_id");
  if(p_tmp != NULL)
  {
    GET_VALUE(g_customer_ids, p_tmp, p_cfg->customer_id);
  }
}

static void cas_id_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("cas_id");
  if(p_tmp != NULL)
  {
    GET_VALUE(g_cas_ids, p_tmp, p_cfg->cas_id);
  }
}

static void country_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("country");
  if(p_tmp != NULL)
  {
    GET_VALUE(g_countries, p_tmp, p_cfg->country);
  }
}

static void spinor_flash_protect_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("spinor_flash_protect");
  if(p_tmp != NULL)
  {
    GET_VALUE(g_flash_protection, p_tmp, p_cfg->flash_prot);
  }
}

static void stb_id_length_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("stb_id_length");
  if(p_tmp != NULL)
  {
    p_cfg->stb_id_len = atoi(p_tmp);
  }
}

static void is_fast_play_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("fast_dvb_play");
  if(p_tmp != NULL)
  {
    p_cfg->fast_dvb_play = atoi(p_tmp);
  }
}

static void is_enable_best_mux_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("enable_best_mux");
  if(p_tmp != NULL)
  {
    p_cfg->enable_best_mux = atoi(p_tmp);
  }
}

static void is_enable_av_afd_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
	const char *p_tmp = NULL;
  
	p_tmp = p_element->Attribute("enable_av_afd");
	if(p_tmp != NULL)
	{
		p_cfg->enable_av_afd = atoi(p_tmp);
	}
}

static void is_enable_tot_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
	const char *p_tmp = NULL;
  
	p_tmp = p_element->Attribute("enable_tot");
	if(p_tmp != NULL)
	{
		p_cfg->enable_tot = atoi(p_tmp);
	}
}
static void is_enable_service_type_check_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("enable_service_type_check");
  if(p_tmp != NULL)
  {
    p_cfg->enable_service_type_check = atoi(p_tmp);
  }
}

static void is_multi_ecm_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("multi_ecm_cas");
  if(p_tmp != NULL)
  {
    p_cfg->multi_ecm_cas = atoi(p_tmp);
  }
}

static void flash_size_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("flash_size");
  if(p_tmp != NULL)
  {
    p_cfg->flash_size = strtoul(p_tmp, NULL, 16);
  }
}

static void mute_gpio_port_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("mute_gpio_port");
  if(p_tmp != NULL)
  {
    p_cfg->mute_gpio_port = atoi(p_tmp);
  }
}

static void dvbt_lnb_gpio_port_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("dvbt_lnb_gpio_port");
  if(p_tmp != NULL)
  {
    p_cfg->dvbt_lnb_gpio_port = atoi(p_tmp);
  }
}

static void fav_count_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("fav_count");
  if(p_tmp != NULL)
  {
    p_cfg->fav_count = atoi(p_tmp);
  }
}

static void graphic_size_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;
  TiXmlElement *p_sub = NULL;

  p_sub = (TiXmlElement *)p_element->FirstChild("graphic_size");
  if (p_sub != NULL)
  {
    p_tmp = p_sub->Attribute("width");
    if(p_tmp != NULL)
    {
      p_cfg->graphic_w = (u16)atoi(p_tmp);
    }
    p_tmp = p_sub->Attribute("height");
    if(p_tmp != NULL)
    {
      p_cfg->graphic_h = (u16)atoi(p_tmp);
    }
  }
}

static void lcn_cfg_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;
  TiXmlElement *p_sub = NULL;

  p_sub = (TiXmlElement *)p_element->FirstChild("lcn_cfg");
  if (p_sub != NULL)
  {
    p_tmp = p_sub->Attribute("reserved_lcn");
    if(p_tmp != NULL)
    {
      p_cfg->lcn_cfg.reserved_lcn= (u16)atoi(p_tmp);
    }
    p_tmp = p_sub->Attribute("lcn_offset_mode");
    if(p_tmp != NULL)
    {
      p_cfg->lcn_cfg.lcn_offset_mode = (u8)atoi(p_tmp);
    }
    p_tmp = p_sub->Attribute("fixed_lcn_offset");
    if(p_tmp != NULL)
    {
      p_cfg->lcn_cfg.fixed_lcn_offset= (u16)atoi(p_tmp);
    }
  }
}

static void db_cfg_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;
  TiXmlElement *p_sub = NULL;

  p_sub = (TiXmlElement *)p_element->FirstChild("db_cfg");
  if (p_sub != NULL)
  {
    p_tmp = p_sub->Attribute("max_pg_cnt");
    if(p_tmp != NULL)
    {
      p_cfg->db_cfg.max_pg_cnt = (u16)atoi(p_tmp);
    }
    p_tmp = p_sub->Attribute("max_tp_cnt");
    if(p_tmp != NULL)
    {
      p_cfg->db_cfg.max_tp_cnt = (u16)atoi(p_tmp);
    }
    p_tmp = p_sub->Attribute("max_sat_cnt");
    if(p_tmp != NULL)
    {
      p_cfg->db_cfg.max_sat_cnt = (u16)atoi(p_tmp);
    }
  }
}


static void ota_cfg_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;
  TiXmlElement *p_sub = NULL;

  p_sub = (TiXmlElement *)p_element->FirstChild("ota_cfg");
  if (p_sub != NULL)
  {
    p_tmp = p_sub->Attribute("ota_num");
    if(p_tmp != NULL)
    {
      p_cfg->ota_cfg.ota_num = atoi(p_tmp);
    }
    p_tmp = p_sub->Attribute("maincode_ota");
    if(p_tmp != NULL)
    {
      p_cfg->ota_cfg.maincode_ota = atoi(p_tmp);
    }
  }
}

static void nim_type_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("nim_type");
  if(p_tmp != NULL)
  {
    GET_VALUE(g_nim_type, p_tmp, p_cfg->nim_type);
  }
}

static void watermark_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;
  TiXmlElement *p_sub = NULL;

  p_sub = (TiXmlElement *)p_element->FirstChild("watermark");
  if (p_sub != NULL)
  {
    p_tmp = p_sub->Attribute("enable");
    if(p_tmp != NULL)
    {
      p_cfg->b_WaterMark = (atoi(p_tmp) != 0);
    }
    p_tmp = p_sub->Attribute("x_pos");
    if(p_tmp != NULL)
    {
      p_cfg->x_WaterMark = atoi(p_tmp);
    }
    p_tmp = p_sub->Attribute("y_pos");
    if(p_tmp != NULL)
    {
      p_cfg->y_WaterMark = atoi(p_tmp);
    }
  }
}

static void drv_init_cfg_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;
  TiXmlElement *p_sub = NULL;

  p_sub = (TiXmlElement *)p_element->FirstChild("drv_init_cfg");
  if (p_sub != NULL)
  {
    p_tmp = p_sub->Attribute("vdec_policy");
    if(p_tmp != NULL)
    {
      GET_VALUE(g_vdec_policy, p_tmp, p_cfg->drv_init_cfg.vdec_policy);
    }
    p_tmp = p_sub->Attribute("b_wrback_422");
    if(p_tmp != NULL)
    {
      p_cfg->drv_init_cfg.b_wrback_422 = atoi(p_tmp);
    }
    p_tmp = p_sub->Attribute("follow_spec");
    if(p_tmp != NULL)
    {
      p_cfg->drv_init_cfg.vdec_follow_spec = atoi(p_tmp);
    }
    p_tmp = p_sub->Attribute("error_percent_drop");
    if(p_tmp != NULL)
    {
      p_cfg->drv_init_cfg.vdec_error_percent_drop = atoi(p_tmp);
    }
    p_tmp = p_sub->Attribute("uboot_show");
    if(p_tmp != NULL)
    {
      p_cfg->drv_init_cfg.uboot_show = atoi(p_tmp);
    }
  }
}

static void dmh_addr_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;
  TiXmlElement *p_sub = NULL;

  p_sub = (TiXmlElement *)p_element->FirstChild("dmh_addr");
  if (p_sub != NULL)
  {
    p_tmp = p_sub->Attribute("bootloader");
    if(p_tmp != NULL)
    {
      p_cfg->dm_booter_start_addr = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("appall");
    if(p_tmp != NULL)
    {
      p_cfg->dm_hdr_start_addr = strtoul(p_tmp, NULL, 16);
    }
  }
}

static void pvr_cfg_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;
  TiXmlElement *p_sub = NULL;
  TiXmlElement *p_rec = NULL;

  p_sub = (TiXmlElement *)p_element->FirstChild("pvr_cfg");
  if (p_sub != NULL)
  {
    p_tmp = p_sub->Attribute("rec_encrypt");
    if(p_tmp != NULL)
    {
      p_cfg->pvr_cfg.rec_encrypt = atoi(p_tmp);
    }
    p_cfg->pvr_cfg.rec_extern.num = 0;
    p_rec = (TiXmlElement *)p_sub->FirstChild("rec_extern");
    while (p_rec != NULL)
    {
      p_tmp = p_rec->Attribute("pid_type");
      if(p_tmp != NULL)
      {
        GET_VALUE(g_rec_pid_type, p_tmp, p_cfg->pvr_cfg.rec_extern.pid_type[p_cfg->pvr_cfg.rec_extern.num]);
        p_cfg->pvr_cfg.rec_extern.num ++;
      }
      p_rec = (TiXmlElement *)p_rec->NextSibling("rec_extern");
    }
  }
}

static void mem_cfg_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;
  TiXmlElement *p_sub = NULL;

  p_sub = (TiXmlElement *)p_element->FirstChild("mem_cfg");
  if (p_sub != NULL)
  {
    p_tmp = p_sub->Attribute("mem_top_addr");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.mem_top_addr = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("av_stack_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.av_stack_size = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("video_fw_cfg_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.video_fw_cfg_size = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("vid_di_cfg_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.vid_di_cfg_size = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("vbi_buf_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.vbi_buf_size = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("audio_fw_cfg_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.audio_fw_cfg_size = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("ap_av_share_mem_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.ap_av_share_mem_size = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("vid_sd_wr_back_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.vid_sd_wr_back_size = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("vid_sd_wr_back_field_no");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.vid_sd_wr_back_field_no = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("vid_coeff_table_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.vid_coeff_table_size = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("epg_buffer_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.epg_buffer_size = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("rec_buffer_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.rec_buffer_size = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("play_buffer_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.play_buffer_size = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("subosd_buffer_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.subosd_buffer_size = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("subosd_double_buffer_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.subosd_double_buffer_size = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("osd0_buffer_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.osd0_buffer_size = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("osd0_double_buffer_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.osd0_double_buffer_size = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("osd1_buffer_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.osd1_buffer_size = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("osd1_double_buffer_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.osd1_double_buffer_size = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("video_pes_buffer_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.video_pes_buffer_size = strtoul(p_tmp, NULL, 16);
    }
    p_tmp = p_sub->Attribute("audio_pes_buffer_size");
    if(p_tmp != NULL)
    {
      p_cfg->mem_cfg.audio_pes_buffer_size = strtoul(p_tmp, NULL, 16);
    }
  }
}

static void nim_para_parser(nim_para_t *p_tp, TiXmlElement *p_sub)
{
  const char *p_tmp = NULL;

  if (p_sub != NULL)
  {
    p_tp->lock_mode = SYS_DVBC;

    p_tp->lockc.tp_freq = 314000;
    p_tp->lockc.tp_sym = 6875;
    p_tp->lockc.nim_modulate = NIM_MODULA_QAM64;
    p_tp->data_pid = 0x1fff;

    p_tmp = p_sub->Attribute("lock_mode");
    if(p_tmp != NULL)
    {
      int tmp_val;

      tmp_val = p_tp->lock_mode;
      GET_VALUE(g_lock_modes, p_tmp, tmp_val);
      p_tp->lock_mode = (sys_signal_t)tmp_val;
      if (p_tp->lock_mode == SYS_DVBC)
      {
        p_tmp = p_sub->Attribute("freq");
        if(p_tmp != NULL)
        {
          p_tp->lockc.tp_freq = atoi(p_tmp);
        }

        p_tmp = p_sub->Attribute("sym");
        if(p_tmp != NULL)
        {
          p_tp->lockc.tp_sym = atoi(p_tmp);
        }

        p_tmp = p_sub->Attribute("mod");
        if(p_tmp != NULL)
        {
          tmp_val = p_tp->lockc.nim_modulate;
          GET_VALUE(g_modulations, p_tmp, tmp_val);
          p_tp->lockc.nim_modulate = (nim_modulation_t)tmp_val;
        }        
      }
      else if (p_tp->lock_mode == SYS_DVBS)
      {     
        p_tmp = p_sub->Attribute("freq");
        if(p_tmp != NULL)
        {
          p_tp->locks.tp_rcv.freq  = atoi(p_tmp);
        }

        p_tmp = p_sub->Attribute("sym");
        if(p_tmp != NULL)
        {
          p_tp->locks.tp_rcv.sym = atoi(p_tmp);
        }

        p_tmp = p_sub->Attribute("nim_type");
        if(p_tmp != NULL)
        {
          p_tp->locks.tp_rcv.nim_type = atoi(p_tmp);
        }        
        p_tmp = p_sub->Attribute("ota_tri");
        if(p_tmp != NULL)
        {
          p_tp->reserved1 = atoi(p_tmp);
        }        
        p_tmp = p_sub->Attribute("is_fixed");
        if(p_tmp != NULL)
        {
          p_tp->locks.disepc_rcv.is_fixed = atoi(p_tmp);
        }        
        p_tmp = p_sub->Attribute("lnb_high");
        if(p_tmp != NULL)
        {
          p_tp->locks.sat_rcv.lnb_high = atoi(p_tmp);
        }        
        p_tmp = p_sub->Attribute("lnb_low");
        if(p_tmp != NULL)
        {
          p_tp->locks.sat_rcv.lnb_low = atoi(p_tmp);
        }
        p_tmp = p_sub->Attribute("lnb_type");
        if(p_tmp != NULL)
        {
          p_tp->locks.sat_rcv.lnb_type = atoi(p_tmp);
        } 
      }
      else if (p_tp->lock_mode == SYS_DVBT2)
      {
        p_tmp = p_sub->Attribute("freq");
        if(p_tmp != NULL)
        {
          p_tp->lockt.tp_freq = atoi(p_tmp);
        }

        p_tmp = p_sub->Attribute("bandwidth");
        if(p_tmp != NULL)
        {
          p_tp->lockt.band_width = atoi(p_tmp);
        }
      }

      p_tmp = p_sub->Attribute("pid");
      if(p_tmp != NULL)
      {
        p_tp->data_pid = atoi(p_tmp);
      }
    }
  }
}

static void ota_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  TiXmlElement *p_sub = NULL;
  nim_para_t temp_para;
  const char *p_tmp;

  p_sub = (TiXmlElement *)p_element->FirstChild("ota");
  while (p_sub != NULL)
  {
    memset(&temp_para, 0, sizeof(nim_para_t));
    nim_para_parser(&temp_para, p_sub);
    if(temp_para.lock_mode == SYS_DVBC)
    {
      memcpy(&p_cfg->ota_tp_c, &temp_para, sizeof(nim_para_t));
    }
    else if(temp_para.lock_mode == SYS_DVBS)
    {
      memcpy(&p_cfg->ota_tp_s, &temp_para, sizeof(nim_para_t));
    }
    else if(temp_para.lock_mode == SYS_DVBT2)
    {
      memcpy(&p_cfg->ota_tp_t, &temp_para, sizeof(nim_para_t));
    }

    /* find the default ota signal */
    p_tmp = p_sub->Attribute("is_default");
    if(p_tmp != NULL)
    {
      if (atoi(p_tmp) != 0)
      {
        p_cfg->default_ota_signal = temp_para.lock_mode;
      }
    }

    p_sub = (TiXmlElement *)p_sub->NextSibling("ota");
  }
}

static void ads_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  TiXmlElement *p_sub = NULL;

  p_sub = (TiXmlElement *)p_element->FirstChild("ads");
  if (p_sub != NULL)
  {
    nim_para_parser(&p_cfg->ads_tp, p_sub);
  }
}

static void registers_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL, *p_tmp1 = NULL;
  TiXmlElement *p_sub = NULL;
  TiXmlElement *p_reg = NULL;

  p_sub = (TiXmlElement *)p_element->FirstChild("registers");
  if (p_sub != NULL)
  {
    char bit_str[16];
    u8 i;

    p_reg = (TiXmlElement *)p_sub->FirstChild("reg");
    while (p_reg != NULL)
    {
      p_tmp = p_reg->Attribute("addr");
      if(p_tmp != NULL)
      {
        u32 addr = strtoul(p_tmp, NULL, 16);

        if (p_cfg->reg_cfg.num >= MAX_CUSCFG_REG_NUM)
        {
          OS_PRINTF("Too many registers in customer config\n");
          return;
        }

        p_cfg->reg_cfg.data[p_cfg->reg_cfg.num].addr = addr;

        p_tmp1 = p_reg->Attribute("data");
        if (p_tmp1 != NULL)
        {
          //set register
          OS_PRINTF("set register 0x%x to 0x%x\n", p_tmp, p_tmp1);

          p_cfg->reg_cfg.data[p_cfg->reg_cfg.num].mask = 0xFFFFFFFF;
          p_cfg->reg_cfg.data[p_cfg->reg_cfg.num].data = strtoul(p_tmp1, NULL, 16);
          p_cfg->reg_cfg.num++;
        }
        else
        {        
          for (i=0; i<32; i++)
          {
            sprintf(bit_str, "bit%d", i);
            p_tmp1 = p_reg->Attribute(bit_str);
            if (p_tmp1 != NULL)
            {
              p_cfg->reg_cfg.data[p_cfg->reg_cfg.num].mask |= (u32)(1<<i);
              if (atoi(p_tmp1) == 0)
              {
                p_cfg->reg_cfg.data[p_cfg->reg_cfg.num].data &= (u32)(~(1<<i)); 
              }
              else
              {
                p_cfg->reg_cfg.data[p_cfg->reg_cfg.num].data |= (u32)(1<<i);
              }
            }
          }
          p_cfg->reg_cfg.num++;
        }
      }

      p_reg = (TiXmlElement *)p_reg->NextSibling("reg");
    }
  }
}

static void gpios_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL, *p_tmp1 = NULL;
  TiXmlElement *p_sub = NULL;
  TiXmlElement *p_reg = NULL;

  p_sub = (TiXmlElement *)p_element->FirstChild("gpios");
  if (p_sub != NULL)
  {    
    p_reg = (TiXmlElement *)p_sub->FirstChild("gpio");
    while (p_reg != NULL)
    {
      p_tmp = p_reg->Attribute("pin");
      if(p_tmp != NULL)
      {
        u32 pin = atoi(p_tmp);

        p_tmp1 = p_reg->Attribute("value");
        if (p_tmp1 != NULL)
        {
          u8 val = (u8)atoi(p_tmp1);

          //set gpio
          OS_PRINTF("set gpio pin %d to 0x%x\n", p_tmp, p_tmp1);
          if (p_cfg->gpio_cfg.num >= MAX_CUSCFG_GPIO_NUM)
          {
            OS_PRINTF("Too many gpios in customer config\n");
            return;
          }

          p_cfg->gpio_cfg.data[p_cfg->gpio_cfg.num].gpio_pin = pin;
          p_cfg->gpio_cfg.data[p_cfg->gpio_cfg.num].value    = val;
          p_cfg->gpio_cfg.num++;
        }      
      }

      p_reg = (TiXmlElement *)p_reg->NextSibling("gpio");
    }
  }
}

static u16 get_key_value(const char *p_key_str)
{
  u16 ret = 0xFFFF;

  if (p_key_str == NULL || strlen(p_key_str) == 0)
    return 0xFFFF;

  GET_VALUE(g_keys, p_key_str, ret);

  if (ret == 0xFFFF)
  {
    if (p_key_str[0] >= '0' && p_key_str[0] <= '9')
      ret = atoi(p_key_str);
    else
      OS_PRINTF("Invalid key %s\n", p_key_str);
  }

  return ret;
}

static void magic_keys_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL, *p_tmp1 = NULL;
  TiXmlElement *p_sub = NULL;
  TiXmlElement *p_reg = NULL;
  u16 tmp_key;
  const char *p_field;
  cus_magic_key_t *p_mkey;

  p_sub = (TiXmlElement *)p_element->FirstChild("magic_keys");
  if (p_sub != NULL)
  {    
    p_reg = (TiXmlElement *)p_sub->FirstChild("key");
    while (p_reg != NULL && p_cfg->magic_key_cnt < MAX_MAGIC_KEY_CNT)
    {
      p_tmp = p_reg->Attribute("magic_key");
      if(p_tmp != NULL)
      {
        tmp_key = get_key_value(p_tmp);
        if (tmp_key == 0xFFFF)
        {
          p_reg = (TiXmlElement *)p_reg->NextSibling("key");
          continue;
        }

        p_mkey = &p_cfg->magic_keys[p_cfg->magic_key_cnt];
        p_mkey->magic_key = tmp_key;

        p_tmp1 = p_reg->Attribute("combo_keys");
        if (p_tmp1 != NULL)
        {
          p_field = strtok((char*)p_tmp1, ",");
          while (p_field != NULL && p_mkey->key_cnt < MAX_KEYS_PER_MAGIC_KEY)
          {
            tmp_key = get_key_value(p_field);
            if (tmp_key != 0xFFFF)
            {
              p_mkey->key_list[p_mkey->key_cnt] = tmp_key;
              p_mkey->key_cnt++;
            }
            p_field = strtok(NULL, ",");
          }
        }

        if (p_mkey->key_cnt > 0)
          p_cfg->magic_key_cnt++;
      }

      p_reg = (TiXmlElement *)p_reg->NextSibling("key");
    }
  }
}

static void vdac_type_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("vdac_type");
  if(p_tmp != NULL)
  {
    GET_VALUE(g_vdac_types, p_tmp, p_cfg->vdac_type);
  }
}

static void filter_ir_key_time_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("filter_ir_key_time");
  if(p_tmp != NULL)
  {
    p_cfg->filter_ir_key_time = atoi(p_tmp);
  }
}

static void default_order_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;
  p_tmp = p_element->Attribute("default_order_start");
  if(p_tmp != NULL)
  {
    p_cfg->default_order_start = atoi(p_tmp);
  }
}

static void ir_repeat_time_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("ir_repeat_time");
  if(p_tmp != NULL)
  {
    p_cfg->ir_repeat_time = atoi(p_tmp);
  }
}

static void ir_protocol_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("ir_protocol");
  if(p_tmp != NULL)
  {
    p_cfg->ir_protocol = atoi(p_tmp);
  }
}

static void usb_storage_0_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("usb_storage_0");
  if(p_tmp != NULL)
  {
    p_cfg->usb_storage_0 = atoi(p_tmp);
  }
}

static void usb_storage_1_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("usb_storage_1");
  if(p_tmp != NULL)
  {
    p_cfg->usb_storage_1 = atoi(p_tmp);
  }
}

static void standby_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;
  TiXmlElement *p_sub = NULL;

  p_sub = (TiXmlElement *)p_element->FirstChild("standby_cfg");
  if (p_sub != NULL)
  {
    p_tmp = p_sub->Attribute("enable_fast_standby");
    if(p_tmp != NULL)
    {
      p_cfg->standby_cfg.enable_fast_standby = atoi(p_tmp);
    }
    p_tmp = p_sub->Attribute("enable_fp_wakeup");
    if(p_tmp != NULL)
    {
      p_cfg->standby_cfg.enable_fp_wakeup = atoi(p_tmp);
    }
    p_tmp = p_sub->Attribute("enable_time");
    if(p_tmp != NULL)
    {
      p_cfg->standby_cfg.enable_time = atoi(p_tmp);
    }
    p_tmp = p_sub->Attribute("led_chars");
    if(p_tmp != NULL)
    {
      strncpy((char*)p_cfg->standby_cfg.led_chars, p_tmp, sizeof(p_cfg->standby_cfg.led_chars));
    }
    p_tmp = p_sub->Attribute("gpio_port");
    if(p_tmp != NULL)
    {
      strncpy((char*)p_cfg->standby_cfg.gpio_port, p_tmp, sizeof(p_cfg->standby_cfg.gpio_port) - 1);
    }
    p_tmp = p_sub->Attribute("gpio_edge");
    if(p_tmp != NULL)
    {
      p_cfg->standby_cfg.gpio_edge = atoi(p_tmp);
    }
    p_tmp = p_sub->Attribute("colon_pos");
    if(p_tmp != NULL)
    {
      p_cfg->standby_cfg.colon_pos = atoi(p_tmp);
    }
    p_tmp = p_sub->Attribute("standby_pos");
    if(p_tmp != NULL)
    {
      p_cfg->standby_cfg.standby_pos = atoi(p_tmp);
    }
    p_tmp = p_sub->Attribute("led_pos_disorder");
    if(p_tmp != NULL)
    {
      p_cfg->standby_cfg.led_pos_disorder = atoi(p_tmp);
    }
  }
}

static void smc_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;
  TiXmlElement *p_sub = NULL;

  p_sub = (TiXmlElement *)p_element->FirstChild("smc_cfg");
  if (p_sub != NULL)
  {
    p_tmp = p_sub->Attribute("detect_pin_pol");
    if(p_tmp != NULL)
    {
      p_cfg->smc_cfg.detect_pin_pol = atoi(p_tmp);
    }
    p_tmp = p_sub->Attribute("vcc_enable_pol");
    if(p_tmp != NULL)
    {
      p_cfg->smc_cfg.vcc_enable_pol = atoi(p_tmp);
    }
  }
}

static void dvbfinder_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;
  TiXmlElement *p_sub = NULL;

  p_sub = (TiXmlElement *)p_element->FirstChild("dvbfinder_cfg");
  if (p_sub != NULL)
  {
    p_tmp = p_sub->Attribute("enable_biss_qrcode");
    if(p_tmp != NULL)
    {
      p_cfg->dvbfinder_cfg.enable_biss_qrcode = atoi(p_tmp);
    }
    p_tmp = p_sub->Attribute("enable_dvbfinder");
    if(p_tmp != NULL)
    {
      p_cfg->dvbfinder_cfg.enable_dvbfinder = atoi(p_tmp);
    }
  }
}


static void hdcp_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("enable_hdcp");
  if(p_tmp != NULL)
  {
    p_cfg->enable_hdcp = atoi(p_tmp) != 0 ? 1 : 0;
  }
}

static void set_ar_to_hdmi_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("enable_set_ar_to_hdmi");
  if(p_tmp != NULL)
  {
    p_cfg->enable_set_ar_to_hdmi = atoi(p_tmp) != 0 ? 1 : 0;
  }
}

static void nit_parse_type_parser(customer_cfg_t *p_cfg, TiXmlElement *p_element)
{
  const char *p_tmp = NULL;

  p_tmp = p_element->Attribute("nit_parse_type");
  if(p_tmp != NULL)
  {
    GET_VALUE(g_nit_parse_type, p_tmp, p_cfg->nit_parse_type);
  }
}

static cus_cfg_parser_t g_cfg_parsers[] = 
{
  {"customer_id", customer_id_parser},
  {"cas_id",      cas_id_parser},
  {"country",     country_parser},
  {"flash_size", flash_size_parser},
  {"spinor_flash_protect", spinor_flash_protect_parser},
  {"stb_id_length", stb_id_length_parser},
  {"watermark",   watermark_parser},
  {"drv_init_cfg",        drv_init_cfg_parser},
  {"dmh_addr",    dmh_addr_parser},
  {"ota",         ota_parser},
  {"registers",   registers_parser},
  {"gpios",       gpios_parser},
  {"mem_cfg",        mem_cfg_parser},
  {"pvr_cfg",        pvr_cfg_parser},
  {"fast_dvb_play", is_fast_play_parser},
  {"multi_ecm_cas", is_multi_ecm_parser},
  {"nim_type", nim_type_parser},
  {"ota_cfg", ota_cfg_parser},
  {"ads", ads_parser},
  {"mute_gpio_port", mute_gpio_port_parser},
  {"dvbt_lnb_gpio_port", dvbt_lnb_gpio_port_parser},
  {"graphic_size", graphic_size_parser},
  {"fav_count", fav_count_parser},
  {"magic_keys", magic_keys_parser},
  {"vdac_type", vdac_type_parser},
  {"default_order_start", default_order_parser},
  {"lcn_cfg", lcn_cfg_parser},
  {"db_cfg", db_cfg_parser},
  {"ir_repeat_time", ir_repeat_time_parser},
  {"ir_protocol", ir_protocol_parser},
  {"usb_storage_0", usb_storage_0_parser},
  {"usb_storage_1", usb_storage_1_parser},    
  {"standby_cfg", standby_parser},
  {"smc_cfg", smc_parser},
  {"dvbfinder_cfg", dvbfinder_parser}, 
  {"enable_best_mux", is_enable_best_mux_parser},
  {"enable_service_type_check", is_enable_service_type_check_parser},
  {"enable_hdcp", hdcp_parser},
  {"enable_av_afd", is_enable_av_afd_parser}, 
  {"enable_tot", is_enable_tot_parser},
  {"enable_set_ar_to_hdmi", set_ar_to_hdmi_parser},
  {"nit_parse_type", nit_parse_type_parser},
  {"filter_ir_key_time", filter_ir_key_time_parser},
};

extern "C" const u8 *get_customer_config_data(void);

extern "C" RET_CODE parse_customer_config(const char *p_cfg_data, customer_cfg_t *p_cfg)
{
  TiXmlDocument *p_xml = NULL;
  TiXmlElement *p_element = NULL;
  const char *p_xml_data;
  int i;

  memset(p_cfg, 0, sizeof(*p_cfg));

  p_xml_data = p_cfg_data;

  p_xml = new TiXmlDocument();
  if (p_xml == NULL)
    return ERR_NO_MEM;

  p_xml->Parse(p_xml_data, 0, TIXML_ENCODING_UTF8);

  TiXmlHandle hDoc((TiXmlDocument *)p_xml);  

  p_element = hDoc.FirstChild("customer_config").Element();

  if(p_element == NULL)
  {
    OS_PRINTF("xml data error.\n");

    delete p_xml;
    return ERR_FAILURE;
  }  

  p_cfg->customer_id = CUSTOMER_DEFAULT;
  p_cfg->cas_id = CONFIG_CAS_ID_TF;
  p_cfg->country = COUNTRY_INDIA;
  p_cfg->flash_prot = CONFIG_NOT_DEFINED;
  p_cfg->flash_size = 0x800000;
  p_cfg->b_WaterMark = FALSE;
  p_cfg->x_WaterMark = 0;
  p_cfg->y_WaterMark = 0;
  p_cfg->drv_init_cfg.vdec_policy = CONFIG_NOT_DEFINED;
  p_cfg->drv_init_cfg.vdec_follow_spec = CONFIG_NOT_DEFINED;
  p_cfg->drv_init_cfg.vdec_error_percent_drop = CONFIG_NOT_DEFINED;
  p_cfg->drv_init_cfg.b_wrback_422 = 0;
  p_cfg->dm_booter_start_addr = 0;
  p_cfg->dm_hdr_start_addr= 0;
  p_cfg->stb_id_len = 6;
  p_cfg->fast_dvb_play = 1;
  p_cfg->multi_ecm_cas = 0;
  p_cfg->nim_type = 0xFF;
  p_cfg->ota_cfg.maincode_ota = 0;
  p_cfg->ota_cfg.ota_num= 2;
  p_cfg->ota_tp_c.data_pid = 0x1fff;
  p_cfg->ads_tp.data_pid = 0x1fff;
  p_cfg->graphic_w = 1280;
  p_cfg->graphic_h = 720;
  p_cfg->mute_gpio_port = 6;
  p_cfg->dvbt_lnb_gpio_port = 11;
  p_cfg->fav_count = 8;
  p_cfg->vdac_type = (u8)CONFIG_NOT_DEFINED;
  p_cfg->default_order_start = 0;
  p_cfg->enable_best_mux = 0;
  p_cfg->enable_service_type_check = 0;
  p_cfg->lcn_cfg.reserved_lcn = 999;
  p_cfg->lcn_cfg.lcn_offset_mode = 0;
  p_cfg->lcn_cfg.fixed_lcn_offset = 2000;
  p_cfg->db_cfg.max_sat_cnt = 64;
  p_cfg->db_cfg.max_tp_cnt = 3000;
  p_cfg->db_cfg.max_pg_cnt = 5000;
  p_cfg->ir_repeat_time = 300;
  p_cfg->ir_protocol = IRDA_NEC;/* Elsys remote mapping*/
  p_cfg->enable_hdcp = 0;
  p_cfg->standby_cfg.enable_time = 1;
  p_cfg->standby_cfg.colon_pos = 1;
  p_cfg->standby_cfg.standby_pos = 0;
  p_cfg->smc_cfg.detect_pin_pol = 1;
  p_cfg->smc_cfg.vcc_enable_pol = 0;
  p_cfg->dvbfinder_cfg.enable_biss_qrcode = 0;
  p_cfg->dvbfinder_cfg.enable_dvbfinder = 0;
  p_cfg->usb_storage_0 = 1;
  p_cfg->usb_storage_1 = (u8)CONFIG_NOT_DEFINED;
  p_cfg->enable_broadcast_mixed_ad = 0;
  p_cfg->filter_ir_key_time = 10;
  for (i=0; i<(int)(sizeof(g_cfg_parsers)/sizeof(g_cfg_parsers[0])); i++)
  {
    if ((TiXmlElement *)p_element->Attribute(g_cfg_parsers[i].p_cfg_key) != NULL)
    {
      g_cfg_parsers[i].p_parser(p_cfg, p_element);
    }
    else if ((TiXmlElement *)p_element->FirstChild(g_cfg_parsers[i].p_cfg_key))
    {
      g_cfg_parsers[i].p_parser(p_cfg, p_element);
    }
  }

  OS_PRINTF("customer config loaded.\n");
  delete p_xml;

  return SUCCESS;
}


