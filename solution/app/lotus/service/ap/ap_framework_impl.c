/********************************************************************************************/
/* Copyright (c) 2014 Montage Technology Group Limited and its affiliated companies         */
/* Montage Proprietary and Confidential                                                     */
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/********************************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include "sys_types.h"
#include "sys_define.h"
#include "sys_cfg.h"

#include "stdlib.h"
#include "stdio.h"
#include "string.h"

#include "lib_util.h"
#include "lib_rect.h"
#include "hal_gpio.h"
#include "hal_misc.h"

#include "mtos_misc.h"
#include "mtos_task.h"
#include "mtos_sem.h"
#include "mtos_printk.h"
#include "mtos_mem.h"
#include "mtos_msg.h"

#include "common.h"
#include "drv_dev.h"
#include "nim.h"
#include "uio.h"
#include "hal_watchdog.h"
#include "hal_misc.h"
#include "display.h"
#include "vdec.h"
#include "aud_vsb.h"
#include "class_factory.h"
#include "mdl.h"
#include "service.h"
#include "dvb_svc.h"
#include "nim_ctrl_svc.h"
#include "nim_ctrl.h"
#include "dvb_protocol.h"
#include "mosaic.h"
#include "pmt.h"
#include "cat.h"
#include "nit.h"
#include "data_manager.h"
#include "scart.h"
#include "rf.h"
#include "avctrl1.h"
#include "db_dvbs.h"
#include "mt_time.h"
#include "audio.h"
#include "video.h"
#include "charsto.h"
#include "ap_framework.h"
#include "ap_uio.h"
#include "ap_signal_monitor.h"
//#include "ap_flounder_ota.h"

#include "dvbs_util.h"
#include "dvbc_util.h"
#include "dvbt_util.h"
#include "ss_ctrl.h"
#include "sys_status.h"
#include "i2c.h"
#include "hal_base.h"

#include "lib_util.h"
#include "lpower.h"
#ifdef FAST_STANDBY
#include "hdmi.h"
#endif
#include "stb_service.h"
#include "customer_config.h"

static void ap_proc(void)
{
}


static void ap_restore_to_factory(void)
{
 #if ((!defined WIN32) )
#endif

}


BOOL ap_get_standby(void)
{
#if ((!defined WIN32) )
return FALSE;
#else
return FALSE;
#endif
}


void ap_set_standby(u32 flag)
{
  #if ((!defined WIN32) )
 #endif

}

void ap_enter_tkgs_standby()
{
    audio_device_t *p_audio_dev = NULL;
    video_device_t *p_video_dev = NULL;
    void * p_dev = NULL;

#ifdef SCART_ENABLE   
      avc_cfg_scart_select_tv_master(class_get_handle_by_id(AVC_CLASS_ID), SCART_TERM_VCR);
      avc_cfg_scart_vcr_input(class_get_handle_by_id(AVC_CLASS_ID), SCART_TERM_TV);
#endif

    p_video_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_VDEC_VSB);
    MT_ASSERT(NULL != p_video_dev);
    vdec_stop(p_video_dev);

    p_audio_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_AUDIO);
    MT_ASSERT(NULL != p_audio_dev);
    aud_mute_onoff_vsb(p_audio_dev,TRUE);

    p_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_DISPLAY);
    MT_ASSERT(NULL != p_dev);
    disp_cvbs_onoff(p_dev, CVBS_GRP0,  FALSE);

    if(disp_layer_is_show(p_dev, DISP_LAYER_ID_OSD0))
    disp_layer_show(p_dev, DISP_LAYER_ID_OSD0, FALSE);
    if(disp_layer_is_show(p_dev, DISP_LAYER_ID_OSD1))
    disp_layer_show(p_dev, DISP_LAYER_ID_OSD1, FALSE);
        
    p_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_POW);
    MT_ASSERT(NULL != p_dev);
    dev_close(p_dev);
    
    mtos_close_printk();
    mtos_task_delay_ms(100);
      
}

#ifdef CONCERTO_IC
static void ap_config_standby(u32 tm_out)
{
  concerto_standby_config_t info;
  utc_time_t  curn_time,p_time;
  RET_CODE ret = SUCCESS;
  void *p_dev_sty = NULL;
  cus_standby_cfg_t *p_standby_cfg = &g_customer.standby_cfg;

  p_dev_sty = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_POW);
  MT_ASSERT(NULL != p_dev_sty);

  memset(&info, 0, sizeof(info));
  
  time_get(&curn_time, FALSE);

  /* front panel config */
  switch (g_fp_cfg.fp_type)
  {
    case HAL_FD650:
      info.panel_config.panel_type = FD650;
       break;
    case HAL_CT1642:
    default:
      info.panel_config.panel_type = CT1642;
      break;
  }

  if(p_standby_cfg->enable_time && g_fp_cfg.led_num >= 4)
  {
    info.panel_config.time_enable = 1;
    info.panel_config.time_hh = curn_time.hour;
    info.panel_config.time_mm = curn_time.minute;
    OS_PRINTF("Standby time %d:%d\n", curn_time.hour, curn_time.minute);
  }
  else
  {
    u8 i, char_cnt = 0;
    
    info.panel_config.led_chars_enable = p_standby_cfg->led_chars[0] == 0 ? 0 : 1;
    for (i=0; i<sizeof(p_standby_cfg->led_chars); i++)
    {
      if (p_standby_cfg->led_chars[i] == '.')
      {
        if (char_cnt == 1)
          info.panel_config.led_dot_mask |= 0x01;
        else if (char_cnt == 2)
          info.panel_config.led_dot_mask |= 0x02;
        else if (char_cnt == 3)
          info.panel_config.led_dot_mask |= 0x04;
        else if (char_cnt == 4)
          info.panel_config.led_dot_mask |= 0x08;
      }
      else if (char_cnt < 4)
      {
        info.panel_config.led_chars[char_cnt++] = p_standby_cfg->led_chars[i];
      }
    }
  }

  info.panel_config.wakeup_enable = p_standby_cfg->enable_fp_wakeup;
   /* get FP POWER key from DM */ 
  dm_read(class_get_handle_by_id(DM_CLASS_ID), 
          FPKEY_BLOCK_ID, 0, 0, 
          sizeof(u8), 
          (u8*)&info.panel_config.wakeup_key);
  
  info.panel_config.wakeup_key |= 0x100;
  
  /* ir config */
  info.ir_config.wakeup_enable = 1;
  
  /* timer config */
  if(tm_out > 0)
  {
    info.timer_config.wakeup_enable = 1;
    info.timer_config.wakeup_duration = tm_out;
  }

  /* gpio config */
  if (! strcmp((const char*)p_standby_cfg->gpio_port, "ao_gpio0"))
  {
    info.gpio_config.wakeup_source0_enable = 1;
    info.gpio_config.source0_edge = p_standby_cfg->gpio_edge;
  }
  else if (! strcmp((const char*)p_standby_cfg->gpio_port, "ao_gpio1"))
  {
    info.gpio_config.wakeup_source1_enable = 1;
    info.gpio_config.source1_edge = p_standby_cfg->gpio_edge;
  }
  else if (p_standby_cfg->gpio_port[0] != 0)
  {
    info.gpio_config.wakeup_nomal_enable = 1;
    info.gpio_config.normal_gpio_num = (u8)atoi((const char*)p_standby_cfg->gpio_port);
  }

  /* Set config to standby */
  ret = ap_lpower_ioctl(p_dev_sty, SET_STANDBY_CONFIG, (u32)&info);
  MT_ASSERT(ret == SUCCESS);

  /* Save local time */
  sys_status_set_status(BS_UNFIRST_UNPOWER_DOWN,TRUE);
  time_to_gmt(&curn_time, &p_time);
  p_time.second = 0;
  sys_status_set_utc_time(&p_time);

}
#elif defined (SYMPHONY_IC)
static void ap_set_standby_param(void)
{
  config_standby_led_infor_t p_led_infor;
  RET_CODE   ret          = SUCCESS;
  
  memset((void*)&p_led_infor, 0x0, sizeof(config_standby_led_infor_t));

  p_led_infor.config_led_en = TRUE;  //配置灯的位置，不配置则默认是0,1,2,3
  p_led_infor.config_colon_en = TRUE;//配置冒号位置，不配置则默认是1
  p_led_infor.config_lock_en = TRUE;
  p_led_infor.colon_pos = g_customer.standby_cfg.colon_pos&0x03;             //FP_COLON_POS         //冒号位置0-3
  p_led_infor.lock_pos  = g_customer.standby_cfg.standby_pos&0x03;            //FP_STANDBY_POS;          //灯位置0-3

  if(g_customer.standby_cfg.led_pos_disorder)
  {
    p_led_infor.led_pos[0] = 3;
    p_led_infor.led_pos[1] = 2;
    p_led_infor.led_pos[2] = 1;
    p_led_infor.led_pos[3] = 0;
  }
  else
  {
    p_led_infor.led_pos[0] = 0;//四个数码管的位置顺序是1,2,3,0
    p_led_infor.led_pos[1] = 1;
    p_led_infor.led_pos[2] = 2;
    p_led_infor.led_pos[3] = 3;
  }
                
  ret = dev_io_ctrl(dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"standby_aomcu"), 
                    CONFIG_LED_INFOR, 
                    (u32)&p_led_infor);  // set standby led information
                    
  MT_ASSERT(ret == SUCCESS);
}

void ap_set_display_mode(void)
{
  void            *p_dev_sty    = NULL;
  uio_device_t    *p_uio_dev    = NULL;
  standby_time_t  sty_time      = {0};
  utc_time_t      curn_time     = {0};
  utc_time_t      curn_utc_time     = {0};
  
  p_dev_sty = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"standby_aomcu");
  MT_ASSERT(NULL != p_dev_sty);
  p_uio_dev = dev_find_identifier(NULL,DEV_IDT_TYPE, SYS_DEV_TYPE_UIO);
  time_get(&curn_time, FALSE);

  sty_time.cur_hour = curn_time.hour;
  sty_time.cur_min= curn_time.minute;
  sty_time.cur_sec= curn_time.second;

  dev_io_ctrl(p_dev_sty, FP_DISPLAY_LOCAL_TIME | SET_STANDBY_TIME, (u32)&sty_time);

  if(g_customer.standby_cfg.enable_time)
  {
    dev_io_ctrl(p_dev_sty, LED_DIS_TIME, (u32)&sty_time);
  }
  else
  {
    dev_io_ctrl(p_dev_sty, LED_DIS_CHAR, 0); //DRV 会直接显示OFF
  }

  /* Save local time */
  sys_status_set_status(BS_UNFIRST_UNPOWER_DOWN,TRUE);
  time_to_gmt(&curn_time, &curn_utc_time);
  curn_utc_time.second = 0;
  sys_status_set_utc_time(&curn_utc_time);

}

static void ap_config_standby(u32 tm_out)
{
  void      *p_dev_sty    = NULL;
  RET_CODE   ret          = SUCCESS;

  p_dev_sty = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"standby_aomcu");
  MT_ASSERT(NULL != p_dev_sty);
  
  if(tm_out > 0)
  {
    ret = dev_io_ctrl(p_dev_sty, TIME_WAKE_UP, tm_out / (60 * 1000));
    MT_ASSERT(ret == SUCCESS);
  }

  ap_set_standby_param();

  ap_set_display_mode();
}
#endif

static void ap_standby_dev(void)
{
  void *p_dev = NULL;
  drv_dev_t *p_hdmi_dev = dev_find_identifier(NULL,DEV_IDT_TYPE,SYS_DEV_TYPE_HDMI);
  
  //AUDIO/VIDEO
  OS_PRINTF("closing vdec...\n");
  p_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_VDEC_VSB);
  MT_ASSERT(NULL != p_dev);
  vdec_stop(p_dev);

  OS_PRINTF("closing aud...\n");
  p_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_AUDIO);
  MT_ASSERT(NULL != p_dev);
  aud_mute_onoff_vsb(p_dev,TRUE);
  aud_stop_vsb(p_dev);
  
  if (g_customer.standby_cfg.enable_fast_standby)
  {
   // uio_display(p_dev, (u8 *)"    ", 4);
    SSD1311_T dis0;
    dis0.len = 16;
	dis0.disfp = 0;

    u8 str[16];

    memset(str, ' ', 16);
	
	uio_display(p_dev, str,  &dis0);
    dis0.disfp = 1;
    uio_display(p_dev, str,  &dis0);
    
  }
  else
  {
    OS_PRINTF("sleeping tuner...\n");
    //TUNER
    p_dev = dev_find_identifier(NULL, DEV_IDT_TYPE,SYS_DEV_TYPE_NIM);
    dev_io_ctrl(p_dev, (u32)NIM_IOCTRL_SET_LNB_ONOFF, 0); 
    dev_io_ctrl(p_dev, NIM_IOCTRL_SET_TUNER_SLEEP, 0);  
    dev_io_ctrl(p_dev, DEV_IOCTRL_POWER, DEV_POWER_SLEEP);
    
    //disable watchdog before enter standby
    hal_watchdog_disable();
  }

  OS_PRINTF("closing HDMI & DAC...\n");
  //DAC  HDMI
  p_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_DISPLAY);
  MT_ASSERT(NULL != p_dev);

  disp_hdmi_onoff(p_dev, FALSE);
  
  disp_dac_onoff(p_dev, DAC_0,  FALSE);
  disp_dac_onoff(p_dev, DAC_1,  FALSE);
  disp_dac_onoff(p_dev, DAC_2,  FALSE);
  disp_dac_onoff(p_dev, DAC_3,  FALSE);
  
  dev_close(p_hdmi_dev);

  /* avoid pop noise */
  if (g_customer.mute_gpio_port != (u32)CONFIG_NOT_DEFINED)
  {
    OS_PRINTF("setting mute gpio port...\n");
    gpio_io_enable(g_customer.mute_gpio_port, TRUE);
    gpio_set_dir(g_customer.mute_gpio_port, GPIO_DIR_OUTPUT);
    gpio_set_value(g_customer.mute_gpio_port, GPIO_LEVEL_LOW);
  }
}

void ap_enter_standby(u32 tm_out)
{
#ifndef WIN32      
  /* config standby parameters */
  OS_PRINTF("config standby...\n");
  ap_config_standby(tm_out);

  /* Set wavefilter */
  dev_io_ctrl(dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_UIO), 
              UIO_FP_SET_POWER_LBD, FALSE);

  dev_io_ctrl(dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_UIO), 
              (u32)UIO_IR_SET_WAVEFILT, 0);

  /* standby devices */
  ap_standby_dev();

  /* enter standby */
  OS_PRINTF("enter standby...\n");
  
#ifdef SYMPHONY_IC
  dev_io_ctrl(dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"standby_aomcu"), IN_LOW_POWER, 0);
#else
  ap_lpower_enter(dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_POW));
#endif

#endif
}

void ap_test_uart(void)
{
}


static void ap_convert_ui_event(os_msg_t *p_msg)
{
  service_dispatch_msg(p_msg);
}


/*!
   Construct APP. framework policy
  */
ap_frm_policy_t *construct_ap_frm_policy(void)
{
  ap_frm_policy_t *p_policy = mtos_malloc(sizeof(ap_frm_policy_t));
  MT_ASSERT(p_policy != NULL);
  //lint -e668 to avoid pclint check error
  memset(p_policy, 0, sizeof(ap_frm_policy_t));
  //lint +e668
  
  p_policy->enter_standby = ap_enter_standby;
  p_policy->extand_proc = ap_proc;
  p_policy->is_standby = ap_get_standby;
  p_policy->resotre_to_factory = ap_restore_to_factory;
  p_policy->set_standby = ap_set_standby;
  p_policy->test_uart = ap_test_uart;
  p_policy->convert_ui_event = ap_convert_ui_event;


  return p_policy;
}
