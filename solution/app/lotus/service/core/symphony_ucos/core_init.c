/********************************************************************************************/
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/* Montage Proprietary and Confidential                                                     */
/* Copyright (c) 2014 Montage Technology Group Limited and its affiliated companies         */
/********************************************************************************************/
/********************************************************************************************/
/* Copyright (c) 2014 Montage Technology Group Limited and its affiliated companies         */
/* Montage Proprietary and Confidential                                                     */
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/********************************************************************************************/
#include "stdio.h"
#include "ctype.h"
#include "stdlib.h"
#include "string.h"
// system
#include "sys_types.h"
#include "sys_define.h"
#include "sys_regs_symphony.h"
#include "sys_devs.h"
#include "sys_cfg.h"
#include "driver.h"
// os
#include "mtos_task.h"
#include "mtos_sem.h"
#include "mtos_event.h"
#include "mtos_printk.h"
#include "mtos_mem.h"
#include "mtos_msg.h"
#include "mtos_timer.h"
#include "mtos_misc.h"
#include "mtos_int.h"
// util
#include "class_factory.h"
#include "mem_manager.h"
#include "lib_unicode.h"
#include "lib_util.h"
#include "lib_rect.h"
#include "lib_memf.h"
#include "lib_memp.h"
#include "simple_queue.h"
#include "char_map.h"
#include "gb2312.h"
#include "fcrc.h"
// driver
#include "hal_base.h"
#include "hal_gpio.h"
#include "hal_dma.h"
#include "hal_misc.h"
#include "hal_uart.h"
#include "hal_otp.h"
#include "hal.h"
#include "hal_pinmux.h"
#include "ipc.h"
#include "common.h"
#include "drv_dev.h"
#include "drv_misc.h"
#include "drv_svc.h"
#include "glb_info.h"
#include "i2c.h"
#include "uio.h"
#include "charsto.h"
#include "avsync.h"
#include "display.h"
#include "vdec.h"
#include "aud_vsb.h"
#include "gpe_vsb.h"
#include "nim.h"
#include "nim_dm6k.h"
#include "dmx.h"
#include "vbi_inserter.h"
#include "hal_watchdog.h"
#include "scart.h"
#include "rf.h"
#include "sys_types.h"
#include "smc_op.h"
#include "mtos_event.h"
#include "list.h"
#include "block.h"
#include "hal_secure.h"
#ifdef WIN32
//#include "fsioctl.h"
//#include "ufs.h"
#endif
#include "vfs.h"
#include "hdmi.h"
#include "region.h"
#include "display.h"
#include "pdec.h"
// mdl
#include "mdl.h"
#include "data_manager.h"
#include "data_manager_v2.h"
#include "data_base.h"
#include "data_base16v2.h"
#include "service.h"
#include "smc_ctrl.h"
#include "dvb_svc.h"
#include "dvb_protocol.h"
#include "mosaic.h"
#include "nit.h"
#include "pat.h"
#include "pmt.h"
#include "sdt.h"
#include "cat.h"
#include "emm.h"
#include "ecm.h"
#include "bat.h"
#include "video_packet.h"
#include "eit.h"
#include "nim_ctrl_svc.h"
#include "nim_ctrl.h"
#include "dvbs_util.h"
#include "dvbc_util.h"
#include "dvbt_util.h"
#include "dvbc_util.h"
#include "ss_ctrl.h"
#include "mt_time.h"
#include "vfs.h"
#include "avctrl1.h"
#include "vbi_api.h"

#ifndef WIN32
#include "sctrl.h"
#endif
#include "lib_rect.h"
#include "lib_memp.h"
#include "common.h"
#include "gpe_vsb.h"
#include "lib_memf.h"
#include "db_dvbs.h"
#include "mem_cfg.h"
#include "pnp_service.h"
#include "media_data_pvr.h"
#include "media_data.h"
#include "cas_ware.h"
#include "dsmcc.h"
#include "monitor_service.h"
#include "ota_public_def.h"
#include "ota_dm_api.h"
//eva
#include "interface.h"
#include "eva.h"
#include "media_format_define.h"
#include "ipin.h"
#include "ifilter.h"
#include "chain.h"
#include "controller.h"
#include "eva_filter_factory.h"
#include "common_filter.h"
#include "file_source_filter.h"
#include "ts_player_filter.h"
#include "av_render_filter.h"
#include "demux_filter.h"
#include "record_filter.h"
#include "file_sink_filter.h"
#include "Pic_filter.h"
#include "pic_render_filter.h"
#include "mp3_player_filter.h"
#include "lrc_filter.h"
#include "str_filter.h"
#include "flash_sink_filter.h"
#ifdef ENABLE_FILE_PLAY
#include "file_playback_sequence.h"
#include "mplayer_aud_filter.h"
#endif
#include "ota_filter.h"
#include "file_list.h"
#include "dmx_manager.h"
#include "epg_type.h"
//#include "epg_filter.h"
// ap
#include "ap_framework.h"
#include "ap_uio.h"
#include "ap_playback.h"
#include "ap_scan.h"
#include "ap_signal_monitor.h"
#include "ap_satcodx.h"
//#include "ap_ota.h"
#include "ap_upgrade.h"
#include "ap_time.h"
#include "ap_cas.h"
#include "ap_usb_upgrade.h"
#include "pvr_api.h"
//#include "LzmaIf.h"
#include "ui_systatus_api.h"
//customer define
#include "customer_config.h"
#include "lpower.h"
#include "ui_util_api.h"
#include "ui_image_verify.h"
#include "ui_usb_api.h"
#include "sys_status.h"
#include "subt_station_filter.h"
#include "user_parse_table.h"
#include "sys_app.h"
#ifndef WIN32
#include "snf_protab_define.h"
#include "snf_protect_api.h"
#endif

#ifdef NVOD_ENABLE
#include "nvod_data.h"
#include "ap_nvod.h"
#endif

#ifdef BROWSER_APP
#include "browser_adapter.h"
#include "ap_browser.h"
#endif

#if defined DESAI_56_CA || defined DESAI_52_CA
#include "ap_nit.h"
#endif
#include "iconv_ext.h"

#include "spi.h"
#ifndef WIN32
//#include "common_api.h"
#endif

#include "config_cas.h"
#include "ui_ca_api.h"

#include "config_ads.h"
#include "ads_ware.h"

#ifdef TENGRUI_ROLLTITLE
#include "ap_tr_subt.h"
#endif

#ifdef UPDATE_PG_BACKGROUND
#include "ap_table_monitor.h"
#endif
#include "ap_time_consuming.h"
#ifdef CHANNEL_FOLLOWING_TABLE
#include "ap_table_parse.h"
#endif

#include "music_api.h"
#include "commonData.h"
#include "stb_service.h"
#include "client_common.h"
#include "client_system.h"
#include "aud_descrip_api.h"
#include "core_init.h"

#include "network_lotus.h"
#include "nim_config.h"
#include "app_ota.h"

/*lint -e64 -e746 for pc-lint*/
#ifndef WIN32
  #define FF_BSS __attribute__((section(".av_bss")))
  extern FF_BSS ipc_fw_fun_set_t g_ipcfw_f;
  extern RET_CODE usb_mass_stor_attach(char *p_name);
  extern u32 attach_ipcfw_fun_set_symphony(ipc_fw_fun_set_t * p_funset);
#endif

#define DEMOD_RST_PIN 3
#define PTI_PARALLEL

#define AV_SYNC_FLAG_ON
#define GPE_USE_HW (1)

#define DRV_MOD_CNT (sizeof(symphony_ucos_drv_modules)/sizeof(symphony_ucos_drv_modules[0]))
#define APP_MOD_CNT sizeof(symphony_ucos_app_modules)/sizeof(symphony_ucos_app_modules[0])

//OLED
#define MAX_DISPLAY_STR_LEN 16
static void *p_dev_oled = NULL;
void display_on_oled_Channel_info( char *string,u8 line  );
void display_on_oled_string( char *string,u8 line  );

#ifdef WIN32
extern RET_CODE disk_win32_attach(char *p_name);
#endif
extern u8 ufs_dev_init();
extern void ui_init(void);
extern RET_CODE i2c_symphony_attach(char *p_name);
extern RET_CODE spn_gpe_attach(char *p_name);
extern RET_CODE jcodec_symphony_attach(char *p_name);

#ifdef TEMP_SUPPORT_TF_AD
extern RET_CODE ui_adv_tf_show_welcome();
#endif
extern RET_CODE smc_attach_symphony(char *name);

#ifdef SMSX_CA
extern  void channel_init(u32 freq, u32 sym, u16  modulation,u16 pid, u16 table_id);
#endif

extern void  OSStart (void);
extern void ap_init(void);
extern u32 get_os_version();

#ifdef SHOW_MEM_SUPPORT
extern void show_single_memory_mapping(u32 flag,u32 star_addr,u32 size);
#endif

const u8 *get_customer_config_data(void);

extern u32 g_video_fw_cfg_addr;
extern u32 g_sub_buffer_size;
extern u32 g_sub_buffer_addr;
extern u32 g_sub_vscaler_buffer_size;
extern u32 g_sub_vscaler_buffer_addr;
extern u32 g_osd0_buffer_size;
extern u32 g_osd0_buffer_addr;
extern u32 g_osd0_vscaler_buffer_size;
extern u32 g_osd0_vscaler_buffer_addr;
extern u32 g_osd1_buffer_size;
extern u32 g_osd1_buffer_addr;
extern u32 g_osd1_vscaler_buffer_size;
extern u32 g_osd1_vscaler_buffer_addr;
extern u32 g_vid_di_cfg_size;
extern u32 g_vid_di_cfg_addr;
extern u32 g_ap_av_share_mem_size;
extern u32 g_ap_av_share_mem_addr;
extern u32 g_vid_sd_wr_back_size;
extern u32 g_vid_sd_wr_back_addr;
extern u32 g_audio_fw_cfg_size;
extern u32 g_audio_fw_cfg_addr;
extern u32 g_vbi_buf_size;
extern u32 g_vbi_buf_addr;
extern u32 g_vid_coeff_table_size;
extern u32 g_vid_coeff_table_addr;
extern u32 g_video_pes_buffer_size;
extern u32 g_video_pes_buffer_addr;
extern u32 g_audio_pes_buffer_size;
extern u32 g_audio_pes_buffer_addr;
extern util_nim_cfg_t g_nim_cfg[4];
extern u8 g_tuner_num;
extern RET_CODE hook_attach(void);
static const drvsvc_handle_t *g_p_public_drvsvc = NULL;
char buildSTR[]=__DATE__"--"__TIME__;

static inline RET_CODE gpe_vsb_sim_attach_symphony(char *name)
{
    return gpe_soft_attach(name);
}

#define NUM_ENCODINGS (3)
#ifndef WIN32
u32 g_lock_led_pin;
#endif

#ifdef LCN_SWITCH
typedef struct
{
       // The callback of parse nit function
	void (*parse)(handle_t handle, dvb_section_t *p_sec);
	//The callback of request nit function
	void (*request)(dvb_section_t *p_sec, u32 table_id, u32 para);
}register_nit_fuc;

extern void attach_bat_logic_num(register_nit_fuc *p_register_bat);
register_nit_fuc register_lcn = {0,0};
#endif


u32 get_bootload_off_addr(void)
{
  if(g_customer.dm_booter_start_addr != 0)
    return g_customer.dm_booter_start_addr;
  return DM_BOOTER_START_ADDR;
}

u32 get_otaback_off_addr(void)
{
  if(g_customer.dm_hdr_start_addr != 0)
    return g_customer.dm_hdr_start_addr;
  return DM_HDR_START_ADDR_BAK;
}

u32 get_maincode_off_addr(void)
{
  if(g_customer.dm_hdr_start_addr != 0)
    return g_customer.dm_hdr_start_addr;
  return DM_HDR_START_ADDR;
}

#ifdef WIN32
u32 get_mem_addr(void)
{
  static u8 g_sys_mem[SYS_MEMORY_END] = {0};
  return (u32)&g_sys_mem[0];
}

u32 get_flash_addr(void)
{
  extern u8 *p_flash_buf;
  return (u32)p_flash_buf;
}

#else
u32 get_mem_addr(void)
{
  return 0x80000000;
}

u32 get_flash_addr(void)
{
  return 0x9e000000;
}
#endif

void gpio_mux(void)
{
    /* uart1 mux 30:28, 26:24 all 1s */
  *(volatile unsigned int *)(0xbf15600c) &= ~0x77000000;
  *(volatile unsigned int *)(0xbf15600c) |= 0x11000000;
}

#ifdef FAST_STANDBY
static void hdmi_driver_attach(void* p_drvsvc);
static const drvsvc_handle_t* drv_public_svr_init(void);
static void disp_mode_config(void *p_disp);
void open_hdmi_dev(void)
{
 u32 reg= 0, val = 0;
  void * p_i2c_master = NULL;
  drv_dev_t *p_hdmi_dev = NULL;
  s32 ret = SUCCESS;
  hdmi_cfg_t hdmi_cfg = {0};

  OS_PRINTF("open_hdmi_dev\n");
  p_hdmi_dev = dev_find_identifier(NULL,
                              DEV_IDT_TYPE,
                              SYS_DEV_TYPE_HDMI);
  MT_ASSERT(p_hdmi_dev != NULL);

    // PINMUX
  reg = R_PIN3_SEL;
  val = *((volatile u32 *)reg);
  val &= ~(0xFF << 16);
  *((volatile u32 *)reg) = val;

  if (SUCCESS != hal_module_clk_get(HAL_HD_VIDEO, &val))
  {
    OS_PRINTF("HAL_HD_VIDEO mclk: 74.25MHz\n");
    hal_module_clk_set(HAL_HD_VIDEO, 74250000);
  }

  p_i2c_master = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"i2c_HDMI");
  MT_ASSERT(NULL != p_i2c_master);

  hdmi_cfg.p_drvsvc = (void *) drv_public_svr_init();;
  
  hdmi_cfg.i2c_master = p_i2c_master;
  hdmi_cfg.is_initialized = 0;
  hdmi_cfg.is_fastlogo_mode = 0;


  ret = dev_open(p_hdmi_dev, &hdmi_cfg);
  MT_ASSERT(SUCCESS == ret);

  ret = dev_io_ctrl(p_hdmi_dev, HDMI_START_MAIN_SERVICE,0);

  if(ret != SUCCESS)
  {
    OS_PRINTF("Can not start main service ret %d\n",ret);
  }
}
#endif

#ifndef WIN32

typedef struct  bin_board_cfg_st
{
  BOOL bin_flag;
  hal_board_config_t  board_cfg;
}bin_board_cfg;
static bin_board_cfg bin_board_cfg_info;

void set_board_config(void)
{
  u8 cfg_buff[2 * KBYTES] = {0};
  u32 read_len = 0;
  u32 size = 0;

  memset(&bin_board_cfg_info,0,sizeof(bin_board_cfg));
  bin_board_cfg_info.bin_flag = 0;

  size = dm_get_block_size(class_get_handle_by_id(DM_CLASS_ID), BOARD_CONFIG_BLOCK_ID);
  OS_PRINTF("\nset_board_config size = %d\n",size);

  if(size >0)
  {

    if(size > 2 * KBYTES)
    {
      size = 2 * KBYTES;
    }
    read_len = dm_read(class_get_handle_by_id(DM_CLASS_ID),
    BOARD_CONFIG_BLOCK_ID, 0, 0,
    size,cfg_buff);
    if(read_len >0)
    {
      bin_board_cfg_info.bin_flag = 1;
      memcpy(&bin_board_cfg_info.board_cfg,cfg_buff,sizeof(hal_board_config_t));

#if 0
      OS_PRINTK("board confg av_perf.changed_param_cnt :%d\n",bin_board_cfg_info.board_cfg.av_perf.changed_param_cnt);
      OS_PRINTK("board confg av_perf.pal_changed_param_cn :%d\n",bin_board_cfg_info.board_cfg.av_perf.pal_changed_param_cnt);
      OS_PRINTK("board confg av_perf.ntsc_changed_param_cnt :%d\n",bin_board_cfg_info.board_cfg.av_perf.ntsc_changed_param_cnt);

      OS_PRINTK("board confg smc_dete :%d\n",bin_board_cfg_info.board_cfg.misc_config.smc_detect_validity);
      OS_PRINTK("board confg smc_power :%d\n",bin_board_cfg_info.board_cfg.misc_config.smc_power_validity);
      OS_PRINTK("board confg tn_loopout :%d\n",bin_board_cfg_info.board_cfg.misc_config.is_tn_loopthrough);
#endif

      #ifdef USE_BOARD_CONFIG
      hal_board_config(cfg_buff);
      #else
      bin_board_cfg_info.bin_flag = 0;
      #endif
    }
  }
}
#endif

#ifdef WIN32
static void s32_block_int(void)
{
  drv_dev_t *p_dev = NULL;
  block_device_config_t config = {0};
  u8 part_tab[20] = {0};
  u8 value = 0;
  RET_CODE ret = SUCCESS;
  char *disk_win32_dev = "disk_win32";

  assign_drives(1, 5); //fix me andy.chen//what's happened
  disk_win32_attach(disk_win32_dev);

  p_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_WIN32_FAKE_DEV);
  OS_PRINTF("p_dev[0x%x]\n", p_dev);

  /*config  block device*/
  ufs_dev_init();
  config.cmd = 1;  /*support tr transport*/
  config.tr_submit_prio = BLOCK_SERV_PRIORITY;   /*tr task prio*/
  dev_open(p_dev, &config);

  regist_device(p_dev);
  regist_logic_parttion(part_tab, &value);
}
#endif

static inline BOOL get_usb1_onoff_from_otp(void)
{
#ifndef WIN32
  u32 result = 0;
  
  hal_otp_read_random(3752, 32, &result);
  OS_PRINTF("OTP 3752 = 0x%x\n", result);
  if (result == 0)
  {
    //YPbPr+USB0
    return FALSE;
  }
  else
  {
    //USB0+USB1
    return TRUE;
  }
#else
  return FALSE;
#endif
}

#ifndef WIN32
#ifdef ENABLE_USB_CONFIG
static void block_driver_attach(void)
{
  RET_CODE ret = ERR_FAILURE;
  drv_dev_t *p_gUsb_dev = NULL;
  drv_dev_t *p_gUsb_dev_1 = NULL;
  block_device_config_t mass_config;
  block_device_config_t mass_config_1;

  if(g_customer.usb_storage_0 == 1)
  {
    ret = usb_mass_stor_attach(DEV_NAME_BLOCK_USB0);
    MT_ASSERT(ret == SUCCESS);
    p_gUsb_dev = (drv_dev_t *)dev_find_identifier(NULL, DEV_IDT_NAME, (u32)DEV_NAME_BLOCK_USB0);
    MT_ASSERT(NULL != p_gUsb_dev);
    mass_config.cmd = 0;

    mass_config.bus_prio = USB_BUS_TASK_PRIO;
    mass_config.bus_thread_stack_size = DRV_USB_BUS_STKSIZE;

    mass_config.block_mass_stor_prio = USB_MASS_STOR_TASK_PRIO;
    mass_config.block_mass_stor_thread_stack_size = DRV_USB_BUS_STKSIZE;
    mass_config.lock_mode = OS_MUTEX_LOCK;
    mass_config.hub_tt_prio = USB_HUB_TT_TASK_PRIO;
    mass_config.hub_tt_stack_size = USB_HUB_TT_TASK_SIZE;
    ret = dev_open(p_gUsb_dev, (void *)&mass_config);
    MT_ASSERT(SUCCESS == ret);
  }
  
  if(g_customer.usb_storage_1 == 1 ||
    ((g_customer.usb_storage_1 == (u8)CONFIG_NOT_DEFINED) &&
    (get_usb1_onoff_from_otp())))
  {
    ret = usb_mass_stor_attach(DEV_NAME_BLOCK_USB1);
    MT_ASSERT(ret == SUCCESS);
    p_gUsb_dev_1 = (drv_dev_t *)dev_find_identifier(NULL, DEV_IDT_NAME, (u32)DEV_NAME_BLOCK_USB1);
    MT_ASSERT(NULL != p_gUsb_dev_1);
    mass_config_1.cmd = 0;

    mass_config_1.bus_prio = USB_BUS_TASK_PRIO_1;
    mass_config_1.bus_thread_stack_size = DRV_USB_BUS_STKSIZE;

    mass_config_1.block_mass_stor_prio = USB_MASS_STOR_TASK_PRIO_1;
    mass_config_1.block_mass_stor_thread_stack_size = DRV_USB_BUS_STKSIZE;
    mass_config_1.lock_mode = OS_MUTEX_LOCK;
    mass_config_1.hub_tt_prio = USB_HUB_TT_TASK_PRIO_1;
    mass_config_1.hub_tt_stack_size = USB_HUB_TT_TASK_SIZE;
    ret = dev_open(p_gUsb_dev_1, (void *)&mass_config_1);
    MT_ASSERT(SUCCESS == ret);
  }  
}
#endif
#endif

//-----------------------------------------------------------
   /*usb dump code  begin*/
//-----------------------------------------------------------
#ifndef WIN32

#if ENABLE_USB_DUMP_BREAK_LOG
#define USB_DUMP_LOG_TOTAL_NUM 10
#define  USB_DUMP_LOG_CMP_HEADER  "C:\\usb_dump_log_version"   // used for modify txt name
#define USB_DUMP_LOG_PATH_SUFFIX1 "%s"                                     //add or not
#define USB_DUMP_LOG_PATH_SUFFIX2 "(%d)"
#define USB_DUMP_LOG_PATH_SUFFIX3  ".txt"

#define USB_DUMP_LOG_ASSRT_HEADER "AP!# exception!"

extern void os_set_out_buff(void *p_buf);
/*!
   Use the warriors int function
  */
 BOOL usb_dump_log_set_buf()
{
   BOOL ret = FALSE;
   u8 *p_epg_addr = NULL;

   mem_cfg(MEMCFG_T_NORMAL);
   p_epg_addr =  (u8 *)mem_mgr_require_block(BLOCK_REC_BUFFER, 0);
   mem_mgr_release_block(BLOCK_REC_BUFFER);
   os_set_out_buff(p_epg_addr);
    if(!memcmp(p_epg_addr + 4,USB_DUMP_LOG_ASSRT_HEADER,
                 strlen( USB_DUMP_LOG_ASSRT_HEADER)))
    {
    ret = TRUE;
    memcpy(p_epg_addr + 4,"cmp ok!",strlen("cmp ok!"));
    hal_dcache_flush_all();
    }
    return ret;
 }



/*!
   dump log to usb function
  */
  BOOL dump_log_to_usb()
  {
    u8 i = 0,uni_len = 0,sort_num = 0,sort_order = 0,usb_dump_num = 0,dump_txt_count = 0,
    dump_file_path[50] = USB_DUMP_LOG_CMP_HEADER;
    u16 FileCount = 0,block_num = 0,block_size = 0,cmp_len = 0,
    file_name[50] = {0},ffilter_all[32] = {0},
    data[USB_DUMP_LOG_TOTAL_NUM] = {0};
    u32 ret = 0,partition_cnt = 0,flash_base_addr = NORF_BASE_ADDR_NON_CACHE,
    dm_header_addr = 0,dm_block_addr = 0,
    dm_ss_addr = 0,dm_ss_flash_addr = 0;
    char file_name_char[50] = {0};

    hfile_t file = NULL;
    partition_t *p_partition = NULL;
    flist_dir_t flist_dir = NULL;
    u8 *buffer_block = NULL;
    dmh_block_info_t *p_block_info = NULL;
    dmh_block_info_t *dmh_block_info_addr = NULL;

    file_list_t file_list = {0};
    upg_file_ver_t upg_file_version = {{0},{0},{0}, 0};
    utc_time_t sys_time = {0};

     dm_header_addr = get_maincode_off_addr();
    str_asc2uni(dump_file_path,file_name);

    cmp_len = strlen(USB_DUMP_LOG_CMP_HEADER);
    partition_cnt = file_list_get_partition(&p_partition);

    block_num = *(u16 *)(flash_base_addr + dm_header_addr + 24);
    block_size = *(u16 *)(flash_base_addr + dm_header_addr + 26);
    dm_block_addr = flash_base_addr + dm_header_addr + 28;
    dmh_block_info_addr = (dmh_block_info_t *)dm_block_addr;
	if(partition_cnt > 0)
	{
		for(i = 0; i < block_num; i++)
		{
			p_block_info = (dmh_block_info_t *)((u8 *)dmh_block_info_addr + i * block_size);
			if(SS_DATA_BLOCK_ID == p_block_info->id)
			{
			dm_ss_addr = *(u32 *)((u8 *)p_block_info + 4);
			dm_ss_flash_addr = dm_ss_addr + flash_base_addr + dm_header_addr;
		       memcpy(upg_file_version.changeset,(u32 *)dm_ss_flash_addr,19);
		       memcpy(&sys_time,(u32 *)(dm_ss_flash_addr + \
			       (u8) ( (u8 *)&((sys_status_t *)(0))->time_set-(u8 *)0 )),
			        sizeof(utc_time_t));
			break;
			}
		}
		if(upg_file_version.changeset[0] == 0)
		upg_file_version.changeset[0] = (u8)('0');

		str_asc2uni("|txt|nodir", ffilter_all);
		flist_dir = file_list_enter_dir(ffilter_all, 100, p_partition[0].letter);
		file_list_get(flist_dir, FLIST_UNIT_FIRST, &file_list);
	       FileCount = (u16)file_list.file_count;

		if(FileCount > 0)
		{
			for(i=0; i<FileCount; i++)
			{
			  uni_len = uni_strlen(file_list.p_file[i].name);
			  if(!memcmp(file_list.p_file[i].name,file_name,
			          cmp_len * 2))
			  {
			     if((u8)file_list.p_file[i].name[uni_len - 5] != (u8)(')'))
			     {
			        if(!memcmp((file_list.p_file[i].name + cmp_len),
				              (u16 *)upg_file_version.changeset,(uni_len - 4 - cmp_len) * 2))
				 data[i] = 1;
			     }
			     else if((u8)file_list.p_file[i].name[uni_len - 5] == (u8)(')'))
			     {
			        if(!memcmp((file_list.p_file[i].name + cmp_len),
				           (u16 *)upg_file_version.changeset,(uni_len - 7 - cmp_len) *2))
				 data[i] = file_list.p_file[i].name[uni_len - 6] - '0';
			     }
				 dump_txt_count ++;
			  }
			}
			if(data[0] == 0)
			dump_txt_count = 0;
		}

		//sort the num when it has same version
		for(sort_num = 0;sort_num < dump_txt_count;sort_num ++)
		{
                   for(sort_order = 0;sort_order < dump_txt_count - sort_num;sort_order ++)
		     {
		       if(sort_order + 1 == dump_txt_count - sort_num || dump_txt_count - sort_num == 1)
			 break;
		       if(data[sort_order] > data[sort_order + 1])
		       {
		           data[sort_order]  = data[sort_order]  + data[sort_order + 1];
		           data[sort_order + 1] = data[sort_order] - data[sort_order + 1];
		           data[sort_order] = data[sort_order] - data[sort_order + 1];
		       }
		     }
		}

		if(sort_num == 0)
		{
		        strcat(dump_file_path,USB_DUMP_LOG_PATH_SUFFIX1);  //cutomer choose which suffix
		        strcat(dump_file_path,USB_DUMP_LOG_PATH_SUFFIX3);

	               sprintf(file_name_char,dump_file_path,
		                  upg_file_version.changeset);
		}
		else
		{
			if(data[sort_num - 1] == 1)
			usb_dump_num = 2;
			else
			{
			usb_dump_num = (data[sort_num - 1] + 1) % USB_DUMP_LOG_TOTAL_NUM;
			}
			if(usb_dump_num == 0)
			{
		       strcat(dump_file_path,USB_DUMP_LOG_PATH_SUFFIX1);   //cutomer choose which suffix
		       strcat(dump_file_path,USB_DUMP_LOG_PATH_SUFFIX3);
			sprintf(file_name_char,dump_file_path,
			upg_file_version.changeset);
			}
			else
			{
			strcat(dump_file_path,USB_DUMP_LOG_PATH_SUFFIX1);   //cutomer choose which suffix when it has same version
			strcat(dump_file_path,USB_DUMP_LOG_PATH_SUFFIX2);   //if dont need ,you can delete this else
			strcat(dump_file_path,USB_DUMP_LOG_PATH_SUFFIX3);
			sprintf(file_name_char,dump_file_path,
			upg_file_version.changeset,usb_dump_num);
			}
		}
      str_asc2uni((u8 *)file_name_char,(u16 *)file_name);
      time_init(&sys_time);
      file = vfs_open(file_name,  VFS_NEW);
		if(file == NULL)
		{
		   return FALSE;
		}
		vfs_seek( file,0, VFS_SEEK_SET);
		buffer_block =  (u8 *)mem_mgr_require_block(BLOCK_REC_BUFFER, 0);
       memcpy(buffer_block + 4,USB_DUMP_LOG_ASSRT_HEADER,
                 strlen(USB_DUMP_LOG_ASSRT_HEADER));
       block_size = *(u32 *)buffer_block;
       if(block_size == 0)
       {
         ret = vfs_write("ap exection dont write the data size",
                        sizeof("ap exection dont write the data size"), 1, file);
       }
		ret = vfs_write(buffer_block + 4,*(u32 *)buffer_block, 1, file);
		if(ret == 0)
		{
		   return FALSE;
		}
		vfs_close(file);
		memset(buffer_block,0,*(u32 *)buffer_block + 4);
       hal_dcache_flush_all();
		mem_mgr_release_block(BLOCK_REC_BUFFER);
		return TRUE;
	}
       return FALSE;
  }

/*!
   wait for the usb connect  function
  */
 void task_usb_monitor()
{
	while(1)
	{
		vfs_dev_event_t  p_event = {0};
		if(vfs_process(&p_event))
		{
			if(p_event.evt == VFS_PLUG_IN_EVENT)
			{
			   dump_log_to_usb();
			}
		}
	}
}

/*!
   init usb for dump function
  */
void usb_dump_log_init()
{
	ufs_dev_init();   //register callback for block layer
   block_driver_attach();
   vfs_mt_init();
	file_list_init();
	{
		pnp_svc_init_para_t pnp_param = {0};
		pnp_param.nstksize = MDL_PNP_SVC_TASK_STKSIZE;
		pnp_param.service_prio = MDL_PNP_SVC_TASK_PRIORITY;
		pnp_service_init(&pnp_param);
	}
	mul_pvr_init();
	{
		BOOL ret = FALSE;
		u32 *pstack_usb = (u32*)mtos_malloc(USB_DUMP_LOG_STKSIZE);
		MT_ASSERT(pstack_usb != NULL);

		ret = mtos_task_create((u8 *)"usb monitor",
		task_usb_monitor,
		(void *)0,
		USB_TASK_DUMP_PRIO,
		pstack_usb,
		USB_DUMP_LOG_STKSIZE);
		MT_ASSERT(FALSE != ret);
	}
}
#endif
#endif
//-----------------------------------------------------------
   /*usb dump code function end*/
//-----------------------------------------------------------
#ifndef WIN32
#if ENABLE_WATCH_DOG
#ifdef DTMB_USB_DEGUB
extern BOOL get_usb_debug_enable();
extern int get_usb_debug_type();
#endif

static void  _feed_dog(void *p_param)
{
    #ifdef DTMB_USB_DEGUB
    void *p_dev_nim = NULL;
    #endif
    hal_watchdog_feed();
    while(1)
    {
    	hal_watchdog_feed();
      #ifdef DTMB_USB_DEGUB
      if(TRUE == get_usb_debug_enable())
      {
        p_dev_nim = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_NIM);
        if(p_dev_nim != NULL)
        {
          if(get_usb_debug_type() == 0)
          {
            dev_io_ctrl(p_dev_nim, NIM_IOCTRL_GET_TUN_DIAGNOSE_INFO, 0);
            dev_io_ctrl(p_dev_nim, NIM_IOCTRL_GET_NIM_DBG_INFO, 0);
          }
          else if(get_usb_debug_type() == 1)
            dev_io_ctrl(p_dev_nim, NIM_IOCTRL_PRINT_CHANNEL_INFO, 0);
        }
      }
      #endif
      #ifdef DTMB_USB_DEGUB
    	mtos_task_sleep(1000);
      #else
    	mtos_task_sleep(3000);
      #endif
    }
}

void watchdog_init(void)
{
  BOOL ret = FALSE;
  u32 *pstack_usb = (u32*)mtos_malloc(16 * KBYTES);
  MT_ASSERT(pstack_usb != NULL);

  if ((hal_get_chip_rev() >= CHIP_CONCERTO_B0))// only support 7S max in B, 14S max in A
    hal_watchdog_init(7000);
  else
    hal_watchdog_init(14000);

  hal_watchdog_enable();

  ret = mtos_task_create((u8 *)"feed watchdog",
      	_feed_dog,
	(void *)0,
	WATCHDOG_FEED_PRIO,
	pstack_usb,
	16 * KBYTES);
	MT_ASSERT(FALSE != ret);
 }
#endif
#endif

static void uio_init(void)
{
  RET_CODE ret;
#ifndef WIN32
  u32 reg= 0 , val = 0;
  void *p_dev_i2cfp = NULL;
#endif
  void *p_dev = NULL;
  uio_cfg_t uiocfg = {0};
  fp_cfg_t fpcfg = {0};
  irda_cfg_t irdacfg = {0};

  p_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_UIO);
  MT_ASSERT(NULL != p_dev);

  fpcfg.p_info = &(g_fp_cfg.pan_info);
  //fpcfg.p_map = g_fp_cfg.led_bitmap;
  //fpcfg.map_size = g_fp_cfg.map_size;
  fpcfg.fp_type = g_fp_cfg.fp_type;


  app_printf(PRINTF_DEBUG_LEVEL,"===fpcfg.fp_type:%d, map_size %d ,com0:%d, com7:%d,reserve7:%d\n", fpcfg.fp_type, fpcfg.map_size,
    fpcfg.p_info->com[0].pos, fpcfg.p_info->com[7].pos,fpcfg.p_info->com[7].reserve);
#ifndef WIN32
  // PINMUX
  switch (fpcfg.fp_type)
  {
   case HAL_CT1642:
   case HAL_LM8168:
     reg = R_IE3_SEL;
     val = *((volatile u32 *)reg);
     val &= ~(0x7 << 27);
     val |= 0x5 << 27;
     *((volatile u32 *)reg) = val;

     reg = R_PIN8_SEL;
     val = *((volatile u32 *)reg);
     val &= ~(0xFFF << 16);
     val |= 0x444 << 16;
     *((volatile u32 *)reg) = val;
     break;

   case HAL_FD650:
   case HAL_SSD1311:
   case HAL_MKTECH_GC7201CA:	           /* for maike */
     if (hal_get_chip_ic_id() != IC_SYMPHONY)
    {
     reg = R_IE3_SEL;
     val = *((volatile u32 *)reg);
     val |= 0x3 << 27;
     *((volatile u32 *)reg) = val;

     reg = R_PIN8_SEL;
     val = *((volatile u32 *)reg);
     val &= ~(0xFF << 16);
     val |= 0x33 << 16;
     *((volatile u32 *)reg) = val;
    }
    else
    {
      val = *((volatile u32 *)(0xbf15b400));
      val &= ~0xff;
      val |= 0x33;
      *((volatile u32 *)(0xbf15b400)) = val;
    }
     p_dev_i2cfp = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"i2c_FP");
     MT_ASSERT(NULL != p_dev_i2cfp);
     break;
  default:
     break;
  }
  if (NULL != p_dev_i2cfp)
  {
    fpcfg.p_bus_dev = p_dev_i2cfp;
  }
#endif
  g_customer.ir_protocol= IRDA_NEC; // for NEC protocol

  irdacfg.protocol = g_customer.ir_protocol;
  irdacfg.code_mode = OUR_DEF_MODE;

  OS_PRINTF("ir_repeat_time %dms\n", g_customer.ir_repeat_time);
  irdacfg.irda_repeat_time = g_customer.ir_repeat_time;


{
  u8 i;
  u8 power_keys[4];
  u8 tmp;
  u16 user_code;
  u8 block_ids[3] = {IRKEY_BLOCK_ID, IRKEY1_BLOCK_ID, IRKEY2_BLOCK_ID};


  irdacfg.irda_wfilt_channel = 3;

  /* for (i=0; i<3; i++)
  {
    dm_read(class_get_handle_by_id(DM_CLASS_ID),
          block_ids[i], 0, 1,
          1,
          (u8 *)&tmp);
    user_code = tmp;
    dm_read(class_get_handle_by_id(DM_CLASS_ID),
          block_ids[i], 0, 0,
          1,
          (u8 *)&tmp);
    user_code |= ((u16)tmp) << 8;

    dm_read(class_get_handle_by_id(DM_CLASS_ID),
          block_ids[i], 0, 2,
          1,
          (u8 *)&power_keys[i]);


    OS_PRINTF("Raja >user code 0x%x power_keys before [%s]\n", user_code,power_keys );
	//snprintf(power_keys,4,"0x00");



    irdacfg.irda_wfilt_channel_cfg[i].wfilt_code |= ((u32)user_code) << 16;
    irdacfg.irda_wfilt_channel_cfg[i].protocol = g_customer.ir_protocol;
    irdacfg.irda_wfilt_channel_cfg[i].addr_len = 32;
    tmp = ~power_keys[i];
    irdacfg.irda_wfilt_channel_cfg[i].wfilt_code |= tmp;
    irdacfg.irda_wfilt_channel_cfg[i].wfilt_code |= ((u16)power_keys) << 8;

    OS_PRINTF("wavefilter code 0x%x\n", irdacfg.irda_wfilt_channel_cfg[i].wfilt_code);
  }*/


      user_code = 0x7f00;
      irdacfg.irda_wfilt_channel_cfg[0].wfilt_code |= ((u32)user_code) << 16;
	  irdacfg.irda_wfilt_channel_cfg[0].protocol = IRDA_NEC;
	  irdacfg.irda_wfilt_channel_cfg[0].addr_len = 32;
	  tmp = ~0x00;
	  irdacfg.irda_wfilt_channel_cfg[0].wfilt_code |= tmp;
	  irdacfg.irda_wfilt_channel_cfg[0].wfilt_code |= ((u16)0x00) << 8;
      //OS_PRINTF("Raja >user code 0x%x power_keys after [%x]\n", user_code,power_keys );
	  //OS_PRINTF("Raja >addr_len [%d]\n", irdacfg.irda_wfilt_channel_cfg[0].addr_len );
	  //("Raja >[0].wfilt_code [%x] line [%d]\n", irdacfg.irda_wfilt_channel_cfg[0].wfilt_code, __LINE__ );


	irdacfg.irda_wfilt_channel_cfg[1].wfilt_code |= ((u32)user_code) << 16;
    irdacfg.irda_wfilt_channel_cfg[1].protocol = IRDA_NEC;
    irdacfg.irda_wfilt_channel_cfg[1].addr_len = 32;
    tmp = ~0x15;
    irdacfg.irda_wfilt_channel_cfg[1].wfilt_code |= tmp;
    irdacfg.irda_wfilt_channel_cfg[1].wfilt_code |= ((u16)0x15) << 8;
	//OS_PRINTF("Raja >addr_len [%d]\n", irdacfg.irda_wfilt_channel_cfg[1].addr_len );
	//OS_PRINTF("Raja >[1].wfilt_code [%x] line [%d]\n", irdacfg.irda_wfilt_channel_cfg[1].wfilt_code ,__LINE__);

    irdacfg.irda_wfilt_channel_cfg[2].wfilt_code |= ((u32)user_code) << 16;
    irdacfg.irda_wfilt_channel_cfg[2].protocol = IRDA_NEC;
    irdacfg.irda_wfilt_channel_cfg[2].addr_len = 32;
    tmp = ~0x10;
    irdacfg.irda_wfilt_channel_cfg[2].wfilt_code |= tmp;
    irdacfg.irda_wfilt_channel_cfg[2].wfilt_code |= ((u16)0x10) << 8;
	//OS_PRINTF("Raja >addr_len [%d]\n", irdacfg.irda_wfilt_channel_cfg[2].addr_len );
	//OS_PRINTF("Raja >[2].wfilt_code [%x] line [%d]\n", irdacfg.irda_wfilt_channel_cfg[2].wfilt_code,__LINE__ );
	
}	  

  uiocfg.p_ircfg = &irdacfg;
  uiocfg.p_fpcfg = &fpcfg;

  OS_PRINTF( " BKS Testing >> Ir protocol [%d ] \n",uiocfg.p_ircfg->protocol);
  ret = dev_open(p_dev, &uiocfg);
  MT_ASSERT(SUCCESS == ret);

  p_dev_oled = p_dev;
  
  ui_set_com_num((u8)(g_fp_cfg.led_num));
  {
    u8 MSGfontA[2][16]={"  OLED Affect   ","  your life     "};
             u8 MSGfontB[2][16]={"   TECHNISAT    ","    RADIOBOX    "};	
             SSD1311_T dis0, dis1;

	dis0.len = 16;
	dis0.disfp = 0;

	dis1.len = 16;
	dis1.disfp = 1;
	
	uio_display(p_dev, MSGfontA[0],  &dis0);
	uio_display(p_dev, MSGfontA[1],  &dis1);
	//mtos_task_delay_ms(3000);

	uio_display(p_dev, MSGfontB[0],  &dis0);
	uio_display(p_dev, MSGfontB[1],  &dis1);
  }

#if 0
  if(g_fp_cfg.led_num == 4)
  {
    uio_display(p_dev, (u8 *)" ON ", 4);
  }
  else if(g_fp_cfg.led_num == 3)
  {
    uio_display(p_dev, (u8 *)"ON ", 3);
  }
  else if(g_fp_cfg.led_num == 2)
  {
    uio_display(p_dev, (u8 *)"ON ", 2);
  }
  else if(g_fp_cfg.led_num == 1)
  {
    uio_display(p_dev, (u8 *)"O ", 1);
  }
  #endif
  dev_io_ctrl(p_dev, UIO_FP_SET_POWER_LBD, TRUE);
#ifndef WIN32
  switch (fpcfg.fp_type)
  {
    case HAL_CT1642:
    ret = standby_cpu_attach_fw_ct1642_symphony(); //TODO:钚耂RC需要打开
    break;
    case HAL_FD650:
    ret = standby_cpu_attach_fw_fd650_symphony();
    break;
  case HAL_GPIO:
    ret = standby_cpu_attach_fw_no_fp_symphony();
    break;

  case HAL_SSD1311:
    ret = standby_cpu_attach_fw_ssd1311_symphony();
    break;
    
  default :
    ret = standby_cpu_attach_fw_fd650_symphony();
    break;
  }
  standby_cpu_attach_fw_stysram_symphony();
#endif
}

#if 0
static void disp_set_hdmi(hdmi_video_config_t *p_hdmi_vcfg, avc_video_mode_1_t new_std)
{

  disp_sys_t video_mode = VID_SYS_1080I_50HZ;

  switch(new_std)
  {
    case AVC_VIDEO_MODE_NTSC_1:
      if(video_mode != VID_SYS_NTSC_M)
      {
        video_mode = VID_SYS_NTSC_M;
      }
      break;
    case AVC_VIDEO_MODE_PAL_1:
      if(video_mode != VID_SYS_PAL)
      {
        video_mode = VID_SYS_PAL;
      }
      break;
    case AVC_VIDEO_MODE_PAL_M_1:
      if(video_mode != VID_SYS_PAL_M)
      {
        video_mode = VID_SYS_PAL_M;
      }
      break;
    case AVC_VIDEO_MODE_PAL_N_1:
      if(video_mode != VID_SYS_PAL_N)
      {
        video_mode = VID_SYS_PAL_N;
      }
      break;
    case AVC_VIDEO_MODE_480P:
      if(video_mode != VID_SYS_480P)
      {
        video_mode = VID_SYS_480P;
      }
      break;
    case AVC_VIDEO_MODE_576P:
      if(video_mode != VID_SYS_576P_50HZ)
      {
        video_mode = VID_SYS_576P_50HZ;
      }
      break;
    case AVC_VIDEO_MODE_720P_60HZ:
      if(video_mode != VID_SYS_720P)
      {
        video_mode = VID_SYS_720P;
      }
      break;
    case AVC_VIDEO_MODE_720P_50HZ:
      if(video_mode != VID_SYS_720P_50HZ)
      {
        video_mode = VID_SYS_720P_50HZ;
      }
      break;
    case AVC_VIDEO_MODE_1080I_50HZ:
      if(video_mode != VID_SYS_1080I_50HZ)
      {
        video_mode = VID_SYS_1080I_50HZ;
      }
      break;
     case AVC_VIDEO_MODE_1080I_60HZ:
      if(video_mode != VID_SYS_1080I)
      {
        video_mode = VID_SYS_1080I;
      }
      break;
     case AVC_VIDEO_MODE_1080P_60HZ:
      if(video_mode != VID_SYS_1080P)
      {
        video_mode = VID_SYS_1080P;
      }
      break;
     case AVC_VIDEO_MODE_1080P_50HZ:
      if(video_mode != VID_SYS_1080P_50HZ)
      {
        video_mode = VID_SYS_1080P_50HZ;
      }
      break;
     case AVC_VIDEO_MODE_480I:
      if(video_mode != VID_SYS_NTSC_M)
      {
        video_mode = VID_SYS_NTSC_M;
      }
      break;
    case AVC_VIDEO_MODE_576I:
      if(video_mode != VID_SYS_PAL)
      {
        video_mode = VID_SYS_PAL;
      }
      break;
    default:
        video_mode = VID_SYS_1080I_50HZ;
      break;
    }
    switch(video_mode)
    {
      case VID_SYS_PAL:
      case VID_SYS_PAL_N:
      case VID_SYS_PAL_NC:
      case VID_SYS_PAL_M:
        p_hdmi_vcfg->input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
        p_hdmi_vcfg->input_v_cfg.csc = HDMI_COLOR_YUV444;
        p_hdmi_vcfg->input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
        p_hdmi_vcfg->input_v_cfg.pixel_rpt = PIXEL_RPT_2_TIMES;
        p_hdmi_vcfg->output_v_cfg.resolution = HDMI_576I;
        p_hdmi_vcfg->output_v_cfg.shape = HDMI_SHAPE_16X9;
        p_hdmi_vcfg->output_v_cfg.standard = HDMI_PAL;
        p_hdmi_vcfg->output_v_cfg.color_space = HDMI_COLOR_AUTO;
        break;
      case VID_SYS_NTSC_J:
      case VID_SYS_NTSC_M:
      case VID_SYS_NTSC_443:
        p_hdmi_vcfg->input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
        p_hdmi_vcfg->input_v_cfg.csc = HDMI_COLOR_YUV444;
        p_hdmi_vcfg->input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
        p_hdmi_vcfg->input_v_cfg.pixel_rpt = PIXEL_RPT_2_TIMES;
        p_hdmi_vcfg->output_v_cfg.resolution = HDMI_480I;
        p_hdmi_vcfg->output_v_cfg.shape = HDMI_SHAPE_16X9;
        p_hdmi_vcfg->output_v_cfg.standard = HDMI_NTSC;
        p_hdmi_vcfg->output_v_cfg.color_space = HDMI_COLOR_AUTO;
        break;
      case VID_SYS_576P_50HZ:
        p_hdmi_vcfg->input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
        p_hdmi_vcfg->input_v_cfg.csc = HDMI_COLOR_YUV444;
        p_hdmi_vcfg->input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
        p_hdmi_vcfg->input_v_cfg.pixel_rpt = PIXEL_RPT_1_TIMES;
        p_hdmi_vcfg->output_v_cfg.resolution = HDMI_576P;
        p_hdmi_vcfg->output_v_cfg.shape = HDMI_SHAPE_16X9;
        p_hdmi_vcfg->output_v_cfg.standard = HDMI_PAL;
        p_hdmi_vcfg->output_v_cfg.color_space = HDMI_COLOR_AUTO;
        break;
      case VID_SYS_480P:
        p_hdmi_vcfg->input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
        p_hdmi_vcfg->input_v_cfg.csc = HDMI_COLOR_YUV444;
        p_hdmi_vcfg->input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
        p_hdmi_vcfg->input_v_cfg.pixel_rpt = PIXEL_RPT_1_TIMES;
        p_hdmi_vcfg->output_v_cfg.resolution = HDMI_480P;
        p_hdmi_vcfg->output_v_cfg.shape = HDMI_SHAPE_16X9;
        p_hdmi_vcfg->output_v_cfg.standard = HDMI_NTSC;
        p_hdmi_vcfg->output_v_cfg.color_space = HDMI_COLOR_AUTO;
        break;
      case VID_SYS_720P:
      case VID_SYS_720P_30HZ:
        p_hdmi_vcfg->input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
        p_hdmi_vcfg->input_v_cfg.csc = HDMI_COLOR_YUV444;
        p_hdmi_vcfg->input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
        p_hdmi_vcfg->input_v_cfg.pixel_rpt = PIXEL_RPT_1_TIMES;
        p_hdmi_vcfg->output_v_cfg.resolution = HDMI_720P;
        p_hdmi_vcfg->output_v_cfg.shape = HDMI_SHAPE_16X9;
        p_hdmi_vcfg->output_v_cfg.standard = HDMI_NTSC;
        p_hdmi_vcfg->output_v_cfg.color_space = HDMI_COLOR_AUTO;
        break;
      case VID_SYS_720P_50HZ:
      case VID_SYS_720P_24HZ:
      case VID_SYS_720P_25HZ:
        p_hdmi_vcfg->input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
        p_hdmi_vcfg->input_v_cfg.csc = HDMI_COLOR_YUV444;
        p_hdmi_vcfg->input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
        p_hdmi_vcfg->input_v_cfg.pixel_rpt = PIXEL_RPT_1_TIMES;
        p_hdmi_vcfg->output_v_cfg.resolution = HDMI_720P;
        p_hdmi_vcfg->output_v_cfg.shape = HDMI_SHAPE_16X9;
        p_hdmi_vcfg->output_v_cfg.standard = HDMI_PAL;
        p_hdmi_vcfg->output_v_cfg.color_space = HDMI_COLOR_AUTO;
        break;
      case VID_SYS_1080I:
        p_hdmi_vcfg->input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
        p_hdmi_vcfg->input_v_cfg.csc = HDMI_COLOR_YUV444;
        p_hdmi_vcfg->input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
        p_hdmi_vcfg->input_v_cfg.pixel_rpt = PIXEL_RPT_1_TIMES;
        p_hdmi_vcfg->output_v_cfg.resolution = HDMI_1080I;
        p_hdmi_vcfg->output_v_cfg.shape = HDMI_SHAPE_16X9;
        p_hdmi_vcfg->output_v_cfg.standard = HDMI_NTSC;
        p_hdmi_vcfg->output_v_cfg.color_space = HDMI_COLOR_AUTO;
        break;
      case VID_SYS_1080I_50HZ:
        p_hdmi_vcfg->input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
        p_hdmi_vcfg->input_v_cfg.csc = HDMI_COLOR_YUV444;
        p_hdmi_vcfg->input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
        p_hdmi_vcfg->input_v_cfg.pixel_rpt = PIXEL_RPT_1_TIMES;
        p_hdmi_vcfg->output_v_cfg.resolution = HDMI_1080I;
        p_hdmi_vcfg->output_v_cfg.shape = HDMI_SHAPE_16X9;
        p_hdmi_vcfg->output_v_cfg.standard = HDMI_PAL;
        p_hdmi_vcfg->output_v_cfg.color_space = HDMI_COLOR_AUTO;
        break;
      case VID_SYS_1080P:
      case VID_SYS_1080P_30HZ:
        p_hdmi_vcfg->input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
        p_hdmi_vcfg->input_v_cfg.csc = HDMI_COLOR_YUV444;
        p_hdmi_vcfg->input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
        p_hdmi_vcfg->input_v_cfg.pixel_rpt = PIXEL_RPT_1_TIMES;
        p_hdmi_vcfg->output_v_cfg.resolution = HDMI_1080P;
        p_hdmi_vcfg->output_v_cfg.shape = HDMI_SHAPE_16X9;
        p_hdmi_vcfg->output_v_cfg.standard = HDMI_NTSC;
        p_hdmi_vcfg->output_v_cfg.color_space = HDMI_COLOR_AUTO;
        break;
      case VID_SYS_1080P_50HZ:
      case VID_SYS_1080P_25HZ:
      case VID_SYS_1080P_24HZ:
        p_hdmi_vcfg->input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
        p_hdmi_vcfg->input_v_cfg.csc = HDMI_COLOR_YUV444;
        p_hdmi_vcfg->input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
        p_hdmi_vcfg->input_v_cfg.pixel_rpt = PIXEL_RPT_1_TIMES;
        p_hdmi_vcfg->output_v_cfg.resolution = HDMI_1080P;
        p_hdmi_vcfg->output_v_cfg.shape = HDMI_SHAPE_16X9;
        p_hdmi_vcfg->output_v_cfg.standard = HDMI_PAL;
        p_hdmi_vcfg->output_v_cfg.color_space = HDMI_COLOR_AUTO;
        break;
      default: //1080i50
        p_hdmi_vcfg->input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
        p_hdmi_vcfg->input_v_cfg.csc = HDMI_COLOR_YUV444;
        p_hdmi_vcfg->input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
        p_hdmi_vcfg->input_v_cfg.pixel_rpt = PIXEL_RPT_1_TIMES;
        p_hdmi_vcfg->output_v_cfg.resolution = HDMI_1080I;
        p_hdmi_vcfg->output_v_cfg.shape = HDMI_SHAPE_16X9;
        p_hdmi_vcfg->output_v_cfg.standard = HDMI_PAL;
        p_hdmi_vcfg->output_v_cfg.color_space = HDMI_COLOR_AUTO;
    }
}
#endif

static void hdmi_video_first_config()
{
#ifndef WIN32
  hdmi_video_config_t hdmi_vcfg = {{0,},};
  RET_CODE hdmi_ret = 0;
  drv_dev_t *p_hdmi_dev = NULL;
  disp_sys_t fmt = VID_SYS_1080I_50HZ;
  av_set_t av_set;
  avc_video_aspect_1_t  aspect = AVC_VIDEO_ASPECT_169_PANSCAN_1;
  
  disp_get_tv_sys(((void *) dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_DISPLAY)), DISP_CHANNEL_HD, &fmt);
  p_hdmi_dev = dev_find_identifier(NULL,
                              DEV_IDT_TYPE,
                              SYS_DEV_TYPE_HDMI);
  MT_ASSERT(p_hdmi_dev != NULL);
  switch(fmt)
  {
    case VID_SYS_PAL:
    case VID_SYS_PAL_N:
    case VID_SYS_PAL_NC:
               // 576i50

      hdmi_vcfg.input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
      hdmi_vcfg.input_v_cfg.csc = HDMI_COLOR_YUV444;
      hdmi_vcfg.input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
      hdmi_vcfg.input_v_cfg.pixel_rpt = PIXEL_RPT_2_TIMES;
      hdmi_vcfg.output_v_cfg.resolution = HDMI_576I;
      hdmi_vcfg.output_v_cfg.shape = HDMI_SHAPE_16X9;
      hdmi_vcfg.output_v_cfg.standard = HDMI_PAL;
      hdmi_vcfg.output_v_cfg.color_space = HDMI_COLOR_AUTO;


      break;
    case VID_SYS_NTSC_J:
    case VID_SYS_NTSC_M:
    case VID_SYS_NTSC_443:
    case VID_SYS_PAL_M:

      hdmi_vcfg.input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
      hdmi_vcfg.input_v_cfg.csc = HDMI_COLOR_YUV444;
      hdmi_vcfg.input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
      hdmi_vcfg.input_v_cfg.pixel_rpt = PIXEL_RPT_2_TIMES;
      hdmi_vcfg.output_v_cfg.resolution = HDMI_480I;
      hdmi_vcfg.output_v_cfg.shape = HDMI_SHAPE_16X9;
      hdmi_vcfg.output_v_cfg.standard = HDMI_NTSC;
      hdmi_vcfg.output_v_cfg.color_space = HDMI_COLOR_AUTO;
      
      break;
    case VID_SYS_576P_50HZ:

      hdmi_vcfg.input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
      hdmi_vcfg.input_v_cfg.csc = HDMI_COLOR_YUV444;
      hdmi_vcfg.input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
      hdmi_vcfg.input_v_cfg.pixel_rpt = PIXEL_RPT_1_TIMES;
      hdmi_vcfg.output_v_cfg.resolution = HDMI_576P;
      hdmi_vcfg.output_v_cfg.shape = HDMI_SHAPE_16X9;
      hdmi_vcfg.output_v_cfg.standard = HDMI_PAL;
      hdmi_vcfg.output_v_cfg.color_space = HDMI_COLOR_AUTO;
      break;
    case VID_SYS_480P:

      hdmi_vcfg.input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
      hdmi_vcfg.input_v_cfg.csc = HDMI_COLOR_YUV444;
      hdmi_vcfg.input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
      hdmi_vcfg.input_v_cfg.pixel_rpt = PIXEL_RPT_1_TIMES;
      hdmi_vcfg.output_v_cfg.resolution = HDMI_480P;
      hdmi_vcfg.output_v_cfg.shape = HDMI_SHAPE_16X9;
      hdmi_vcfg.output_v_cfg.standard = HDMI_NTSC;
      hdmi_vcfg.output_v_cfg.color_space = HDMI_COLOR_AUTO;
      break;
    case VID_SYS_720P:

      hdmi_vcfg.input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
      hdmi_vcfg.input_v_cfg.csc = HDMI_COLOR_YUV444;
      hdmi_vcfg.input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
      hdmi_vcfg.input_v_cfg.pixel_rpt = PIXEL_RPT_1_TIMES;
      hdmi_vcfg.output_v_cfg.resolution = HDMI_720P;
      hdmi_vcfg.output_v_cfg.shape = HDMI_SHAPE_16X9;
      hdmi_vcfg.output_v_cfg.standard = HDMI_NTSC;
      hdmi_vcfg.output_v_cfg.color_space = HDMI_COLOR_AUTO;
      break;
    case VID_SYS_720P_50HZ:
   
      hdmi_vcfg.input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
      hdmi_vcfg.input_v_cfg.csc = HDMI_COLOR_YUV444;
      hdmi_vcfg.input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
      hdmi_vcfg.input_v_cfg.pixel_rpt = PIXEL_RPT_1_TIMES;
      hdmi_vcfg.output_v_cfg.resolution = HDMI_720P;
      hdmi_vcfg.output_v_cfg.shape = HDMI_SHAPE_16X9;
      hdmi_vcfg.output_v_cfg.standard = HDMI_PAL;
      hdmi_vcfg.output_v_cfg.color_space = HDMI_COLOR_AUTO;
      break;
    case VID_SYS_1080I:

      hdmi_vcfg.input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
      hdmi_vcfg.input_v_cfg.csc = HDMI_COLOR_YUV444;
      hdmi_vcfg.input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
      hdmi_vcfg.input_v_cfg.pixel_rpt = PIXEL_RPT_1_TIMES;
      hdmi_vcfg.output_v_cfg.resolution = HDMI_1080I;
      hdmi_vcfg.output_v_cfg.shape = HDMI_SHAPE_16X9;
      hdmi_vcfg.output_v_cfg.standard = HDMI_NTSC;
      hdmi_vcfg.output_v_cfg.color_space = HDMI_COLOR_AUTO;
      break;
    case VID_SYS_1080I_50HZ:
   
      hdmi_vcfg.input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
      hdmi_vcfg.input_v_cfg.csc = HDMI_COLOR_YUV444;
      hdmi_vcfg.input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
      hdmi_vcfg.input_v_cfg.pixel_rpt = PIXEL_RPT_1_TIMES;
      hdmi_vcfg.output_v_cfg.resolution = HDMI_1080I;
      hdmi_vcfg.output_v_cfg.shape = HDMI_SHAPE_16X9;
      hdmi_vcfg.output_v_cfg.standard = HDMI_PAL;
      hdmi_vcfg.output_v_cfg.color_space = HDMI_COLOR_AUTO;
      break;
    case VID_SYS_1080P:

      hdmi_vcfg.input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
      hdmi_vcfg.input_v_cfg.csc = HDMI_COLOR_YUV444;
      hdmi_vcfg.input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
      hdmi_vcfg.input_v_cfg.pixel_rpt = PIXEL_RPT_1_TIMES;
      hdmi_vcfg.output_v_cfg.resolution = HDMI_1080P;
      hdmi_vcfg.output_v_cfg.shape = HDMI_SHAPE_16X9;
      hdmi_vcfg.output_v_cfg.standard = HDMI_NTSC;
      hdmi_vcfg.output_v_cfg.color_space = HDMI_COLOR_AUTO;
      break;
    case VID_SYS_1080P_50HZ:

      hal_module_clk_set(HAL_HD_VIDEO, 148500000);
      hdmi_vcfg.input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
      hdmi_vcfg.input_v_cfg.csc = HDMI_COLOR_YUV444;
      hdmi_vcfg.input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
      hdmi_vcfg.input_v_cfg.pixel_rpt = PIXEL_RPT_1_TIMES;
      hdmi_vcfg.output_v_cfg.resolution = HDMI_1080P;
      hdmi_vcfg.output_v_cfg.shape = HDMI_SHAPE_16X9;
      hdmi_vcfg.output_v_cfg.standard = HDMI_PAL;
      hdmi_vcfg.output_v_cfg.color_space = HDMI_COLOR_AUTO;
      break;
    default:
      hdmi_vcfg.input_v_cfg.fmt = RGB_YCBCR444_SYNCS;
      hdmi_vcfg.input_v_cfg.csc = HDMI_COLOR_YUV444;
      hdmi_vcfg.input_v_cfg.ddr_edge = DDR_EDGE_FALLING;
      hdmi_vcfg.input_v_cfg.pixel_rpt = PIXEL_RPT_1_TIMES;
      hdmi_vcfg.output_v_cfg.resolution = HDMI_1080I;
      hdmi_vcfg.output_v_cfg.shape = HDMI_SHAPE_16X9;
      hdmi_vcfg.output_v_cfg.standard = HDMI_PAL;
      hdmi_vcfg.output_v_cfg.color_space = HDMI_COLOR_AUTO;
      break;
  }
  hdmi_vcfg.hdcp_on_off = g_customer.enable_hdcp;

  sys_status_get_av_set(&av_set);
  aspect = sys_status_get_video_aspect(av_set.tv_ratio);
  if((aspect == AVC_VIDEO_ASPECT_43_PANSCAN_1) || (aspect == AVC_VIDEO_ASPECT_43_LETTERBOX_1))
    hdmi_vcfg.output_v_cfg.shape = HDMI_SHAPE_4X3;
  else
    hdmi_vcfg.output_v_cfg.shape = HDMI_SHAPE_16X9;
  
  OS_PRINTF("%s %d fmt %d aspect %d \n",__FUNCTION__,__LINE__,fmt,aspect);

  
  hdmi_ret = hdmi_video_config(p_hdmi_dev,&hdmi_vcfg);
  OS_PRINTF("~~~~~~~~~ hdmi_video_config ~~~~~~~~~~~~ hdmi_ret %d \n ",hdmi_ret);
  {
    u8 times = 0;
    while(times < 2)
    {
      if(hdmi_ret == 0)
        break;
      hdmi_ret = hdmi_video_config(p_hdmi_dev,&hdmi_vcfg);
      OS_PRINTF("~~~~~~~~~ hdmi_video_config ~~~~~~~~~~~~ hdmi_ret %d \n ",hdmi_ret);
      times ++;
      
    }
  }
#endif
  return;
}

static void hdcp_enable(void)
{
  u8 *addr = NULL;
  u32 read_len;
  u32 size = ADVANCEDCA_HDMIKEYSIZE;
  u8  have_burn_hdcp_key = 0;
  u32 ch_index = 0;
  ST_ADVANCEDCA_CIPHERENGINEINFO  stCipherEngineInfo;
  u8     * pu8Input = NULL;
  u8     * pu8Output = NULL;
  u8 *p_hdcp_sram = (u8*)0xBF130000;

  OS_PRINTF("HDCP status: %s\n", g_customer.enable_hdcp ? "ON" : "OFF");
  if (! g_customer.enable_hdcp)
  {
    return;
  }
    
  addr = (u8*)mtos_malloc(size);
  MT_ASSERT(addr != NULL);
  read_len = dm_read(class_get_handle_by_id(DM_CLASS_ID),HDCPKEY_BLOCK_ID, 0, 0, size, addr);
  MT_ASSERT(read_len != 0);
  if(addr[0x5] == 'w'&&addr[0x15] == 'h'&&addr[0x25] == 'a'&&addr[0x35] == 't'&&addr[0x45] == 'i'&&addr[0x55] == 's'&&addr[0x65] == 'a'&&addr[0x75] == 'p'&&addr[0x85] == 'p')
  {
    have_burn_hdcp_key = 0;
  }
  else
  {
    have_burn_hdcp_key = 1;
  }
  OS_PRINTF("HDCP burnt %d, size %d, read_len %d\n", have_burn_hdcp_key, size, read_len);
  
  /* import hdcp key into hdmi IP here */
  // variable definitions

  *((volatile u8*)0xBF480009) = 0x7;
  *((volatile u32*)0xBF137000) = 0x87654321;
  *((volatile u32*)0xBF136000) = 0x16885188;
  
  // malloc
  
  //if not 16 align , there maybe not right
  pu8Input = mtos_align_malloc(ADVANCEDCA_HDMIKEYSIZE, 16);
  pu8Output = mtos_align_malloc(ADVANCEDCA_HDMIKEYSIZE, 16);
  memset(pu8Input, 0, ADVANCEDCA_HDMIKEYSIZE);
  memset(pu8Output, 0, ADVANCEDCA_HDMIKEYSIZE);
  
  // copy key
  memcpy(pu8Input, addr, ADVANCEDCA_HDMIKEYSIZE);

  memset(&stCipherEngineInfo, 0, sizeof(stCipherEngineInfo));
  stCipherEngineInfo.channel_index = ch_index; //channel index(0-3)

  stCipherEngineInfo.enCipherEngine = ADVANCEDCA_CIPHERENGINE_DES; 
  stCipherEngineInfo.stKeyConfigInfo.enKeySize = ADVANCEDCA_KEYSIZE_64;
  stCipherEngineInfo.enBlockMode = ADVANCEDCA_BLOCKMODE_ECB;

  stCipherEngineInfo.pu8Input = pu8Input;    //input data
  stCipherEngineInfo.u32Length = 304;    //data length
  stCipherEngineInfo.pu8Output = p_hdcp_sram;

  stCipherEngineInfo.enWaitingMode = ADVANCEDCA_WAITINGMODE_POLLING;
  stCipherEngineInfo.enOperatorMode = ADVANCEDCA_OPERATOR_CPU;
  stCipherEngineInfo.stKeyConfigInfo.enKeyPath = ADVANCEDCA_KEYPATH_BLOCK;
  hal_secure_EngineStart (
                                ADVANCEDCA_APPLICATION_GPEncryption,
                                &stCipherEngineInfo
                                );

  hal_secure_EngineClose(ch_index);

  {
    u16 j;

    for (j=0; j<ADVANCEDCA_HDMIKEYSIZE; j++)
    {
      OS_PRINTF("%02x ", pu8Input[j]);
      if ((j%16) == 0 && j != 0)
        OS_PRINTF("\n");
    }
  }
  
  mtos_align_free(pu8Input);
  mtos_align_free(pu8Output);

  mtos_free(addr);
}

static void hdmi_driver_attach(void* p_drvsvc)
{
  RET_CODE ret;
  u32 reg= 0, val = 0;
  hdmi_cfg_t hdmi_cfg = {0};
  void * p_i2c_master = NULL;
  drv_dev_t *p_hdmi_dev = NULL;

  ret = hdmi_anx8560_attach("hdmi");
  MT_ASSERT(ret == SUCCESS);

  p_hdmi_dev = dev_find_identifier(NULL,
                              DEV_IDT_TYPE,
                              SYS_DEV_TYPE_HDMI);
  MT_ASSERT(p_hdmi_dev != NULL);

  // PINMUX
  reg = R_PIN3_SEL;
  val = *((volatile u32 *)reg);
  val &= ~(0xFF << 16);
  *((volatile u32 *)reg) = val;

  if (SUCCESS != hal_module_clk_get(HAL_HD_VIDEO, &val))
  {
    OS_PRINTF("HAL_HD_VIDEO mclk: 74.25MHz\n");
    hal_module_clk_set(HAL_HD_VIDEO, 74250000);
  }

  p_i2c_master = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"i2c_HDMI");
  MT_ASSERT(NULL != p_i2c_master);

  memset(&hdmi_cfg, 0x0, sizeof(hdmi_cfg_t));
  if(NULL == p_drvsvc)
  {
  hdmi_cfg.task_prio = DRV_HDMI_TASK_PRIORITY;
  hdmi_cfg.stack_size = (8 * KBYTES);
  }
  else
  {
    hdmi_cfg.p_drvsvc = p_drvsvc;
  }
  hdmi_cfg.i2c_master = p_i2c_master;
  if(g_customer.drv_init_cfg.uboot_show == 0)
  {
    hdmi_cfg.is_initialized = 0;
    hdmi_cfg.is_fastlogo_mode = 0;
  }
  else
  {
    hdmi_cfg.is_initialized = 1;
    hdmi_cfg.is_fastlogo_mode = 0;
  }

  hdmi_cfg.is_support_dvi_mode = 1;
  
  ret = dev_open(p_hdmi_dev, &hdmi_cfg);
  MT_ASSERT(SUCCESS == ret);

  hdcp_enable();
}

#ifndef WIN32
static void drv_flash_protect()
{
  /*protect flash*/
  void *p_dev = NULL;
  spi_prot_block_type_t prt_t = PRT_UNPROT_ALL;
  u32 p_cap = CHARSTO_SIZE;

  p_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_CHARSTO);
  MT_ASSERT(NULL != p_dev);

  if(g_customer.flash_prot != CONFIG_NOT_DEFINED)  
  {    
    prt_t = (spi_prot_block_type_t)g_customer.flash_prot;
  }
  else
  {
    switch(p_cap)
    {
      case 0x200000:
        prt_t = PRT_LOWER_1_4;
        break;
      case 0x400000:
        prt_t = PRT_LOWER_3_4;
        break;
      case 0x800000:
        prt_t = PRT_LOWER_3_4;
        break;
      case 0x1000000:
        prt_t = PRT_LOWER_1_4;
        break;
      case 0x2000000:
        prt_t = PRT_LOWER_1_8;
        break;
    }
  }
  if(snf_protect_set(prt_t) != TRUE)
  {
     switch(p_cap)
    {
      case 0x200000:
        prt_t = PRT_LOWER_1_8;
        break;
      case 0x400000:
        prt_t = PRT_LOWER_1_2;
        break;
      case 0x800000:
        prt_t = PRT_LOWER_1_2;
        break;
      case 0x1000000:
        prt_t = PRT_LOWER_1_8;
        break;
      case 0x2000000:
        prt_t = PRT_LOWER_1_16;
        break;
    }
    snf_protect_set(prt_t);
 }
  prt_t = snf_protect_get();
  APPLOGA("current flash size is 0x%x protect block is %d \n",p_cap,prt_t);
}
#endif

void spiflash_cfg( )
{
    RET_CODE ret;
    void *p_dev = NULL;
    void *p_spi = NULL;
    charsto_cfg_t charsto_cfg = {0};
#ifndef WIN32
    spi_cfg_t spiCfg;
    ret = spi_concerto_attach("spi_concerto_0");
    MT_ASSERT(SUCCESS == ret);
    ret = spiflash_jazz_attach("charsto_concerto");
    MT_ASSERT(SUCCESS == ret);
    OS_PRINTF("drv --charsto 1\n");
#endif

#ifndef WIN32
    spiCfg.bus_clk_mhz   = 50;//10;
    spiCfg.bus_clk_delay = 12;//8;
    spiCfg.io_num        = 1;
    spiCfg.lock_mode     = OS_MUTEX_LOCK;
    spiCfg.op_mode       = 0;
    spiCfg.pins_cfg[0].miso1_src  = 0;
    spiCfg.pins_cfg[0].miso0_src  = 1;
    spiCfg.pins_cfg[0].spiio1_src = 0;
    spiCfg.pins_cfg[0].spiio0_src = 0;
    spiCfg.pins_dir[0].spiio1_dir = 3;
    spiCfg.pins_dir[0].spiio0_dir = 3;
    spiCfg.spi_id = 0;

    p_spi = dev_find_identifier(NULL,DEV_IDT_NAME, (u32)"spi_concerto_0");
    spiCfg.spi_id = 0;
    ret = dev_open(p_spi, &spiCfg);
    MT_ASSERT(SUCCESS == ret);
#endif

  ret = ATTACH_DRIVER(CHARSTO, concerto, default, default);
  /*lint +e123 */
  MT_ASSERT(ret == SUCCESS);

  p_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_CHARSTO);
  MT_ASSERT(NULL != p_dev);
  OS_PRINTF("drv --charsto 2\n");

  charsto_cfg.rd_mode = SPI_FR_MODE;
  charsto_cfg.p_bus_handle = p_spi;
  charsto_cfg.size = CHARSTO_SIZE;
  ret = dev_open(p_dev, &charsto_cfg);
  MT_ASSERT(SUCCESS == ret);

#ifndef WIN32
  drv_flash_protect();
#endif
}

static void get_sd_tv_mode_from_hd(disp_sys_t *p_sd, disp_sys_t hd)
{
  switch(hd)
  {
    case VID_SYS_PAL:
    case VID_SYS_PAL_N:
    case VID_SYS_PAL_NC:
    case VID_SYS_PAL_M:
    case VID_SYS_576P_50HZ:
    case VID_SYS_720P_50HZ:
    case VID_SYS_1080I_50HZ:
    case VID_SYS_1080P_50HZ:
      *p_sd = VID_SYS_PAL;
      break;
    case VID_SYS_NTSC_J:
    case VID_SYS_NTSC_M:
    case VID_SYS_NTSC_443:
    case VID_SYS_480P:
    case VID_SYS_720P:
    case VID_SYS_1080I:
    case VID_SYS_1080P:
      *p_sd = VID_SYS_NTSC_M;
      break;
    default:
      *p_sd = VID_SYS_PAL;
      break;
  }
}
static void get_hd_tv_mode_from_sd(disp_sys_t sd, disp_sys_t hd_old, disp_sys_t *p_hd_new)
{
    if((sd == VID_SYS_PAL) ||(sd == VID_SYS_PAL_N)
      ||(sd == VID_SYS_PAL_NC))
    {
      switch(hd_old)
      {
         case VID_SYS_1080P_50HZ:
         case VID_SYS_1080P:
          *p_hd_new = VID_SYS_1080P_50HZ;
           break;
         case VID_SYS_1080I_50HZ:
         case VID_SYS_1080I:
          *p_hd_new = VID_SYS_1080I_50HZ;
           break;
         case VID_SYS_720P_50HZ:
         case VID_SYS_720P:
           *p_hd_new = VID_SYS_720P_50HZ;
           break;
         case VID_SYS_576P_50HZ:
         case VID_SYS_480P:
            *p_hd_new = VID_SYS_576P_50HZ;
            break;
         case VID_SYS_PAL:
         case VID_SYS_PAL_N:
         case VID_SYS_PAL_NC:
         case VID_SYS_PAL_M:
         case VID_SYS_NTSC_J:
         case VID_SYS_NTSC_M:
         case VID_SYS_NTSC_443:
          *p_hd_new = VID_SYS_PAL;
            break;
        default:
          *p_hd_new = VID_SYS_1080I_50HZ;
           break;
      }
    }
    else
    {
      switch(hd_old)
      {
         case VID_SYS_1080P_50HZ:
         case VID_SYS_1080P:
          *p_hd_new = VID_SYS_1080P;
           break;
         case VID_SYS_1080I_50HZ:
         case VID_SYS_1080I:
          *p_hd_new = VID_SYS_1080I;
           break;
         case VID_SYS_720P_50HZ:
         case VID_SYS_720P:
           *p_hd_new = VID_SYS_720P;
           break;
         case VID_SYS_576P_50HZ:
          case VID_SYS_480P:
            *p_hd_new = VID_SYS_480P;
            break;
         case VID_SYS_PAL:
         case VID_SYS_PAL_N:
         case VID_SYS_PAL_NC:
         case VID_SYS_PAL_M:
         case VID_SYS_NTSC_J:
         case VID_SYS_NTSC_M:
         case VID_SYS_NTSC_443:
          *p_hd_new = VID_SYS_NTSC_M;
            break;
        default:
          *p_hd_new = VID_SYS_1080I;
           break;
      }
    }
}

static void disp_switch_video_mode(void *p_disp, avc_video_mode_1_t new_std)
{
  disp_sys_t video_mode;
  aspect_ratio_t video_aspect;
  BOOL b_switch = FALSE;
  disp_channel_t disp_chan = DISP_CHANNEL_SD;
  disp_sys_t sd_mode = VID_SYS_PAL;
  disp_sys_t hd_mode = VID_SYS_720P;
  disp_sys_t hd_old = VID_SYS_720P;
  disp_common_info_t disp_info = {0};

  if(new_std >= AVC_VIDEO_MODE_480P)
  {
    disp_chan = DISP_CHANNEL_HD;
  }
  disp_get_tv_sys(p_disp, disp_chan, &video_mode);
  disp_get_aspect_ratio(p_disp, disp_chan, &video_aspect);

  switch(new_std)
  {
    case AVC_VIDEO_MODE_NTSC_1:
      if(video_mode != VID_SYS_NTSC_M)
      {
        b_switch = TRUE;
        video_mode = VID_SYS_NTSC_M;
      }
      break;
    case AVC_VIDEO_MODE_PAL_1:
      if(video_mode != VID_SYS_PAL)
      {
        b_switch = TRUE;
        video_mode = VID_SYS_PAL;
      }
      break;
    case AVC_VIDEO_MODE_PAL_M_1:
      if(video_mode != VID_SYS_PAL_M)
      {
        b_switch = TRUE;
        video_mode = VID_SYS_PAL_M;
      }
      break;
    case AVC_VIDEO_MODE_PAL_N_1:
      if(video_mode != VID_SYS_PAL_N)
      {
        b_switch = TRUE;
        video_mode = VID_SYS_PAL_N;
      }
      break;
    case AVC_VIDEO_MODE_480P:
      if(video_mode != VID_SYS_480P)
      {
        video_mode = VID_SYS_480P;
        b_switch = TRUE;
      }
      break;
    case AVC_VIDEO_MODE_576P:
      if(video_mode != VID_SYS_576P_50HZ)
      {
        video_mode = VID_SYS_576P_50HZ;
        b_switch = TRUE;
      }
      break;
    case AVC_VIDEO_MODE_720P_60HZ:
      if(video_mode != VID_SYS_720P)
      {
        video_mode = VID_SYS_720P;
        b_switch = TRUE;
      }
      break;
    case AVC_VIDEO_MODE_720P_50HZ:
      if(video_mode != VID_SYS_720P_50HZ)
      {
        video_mode = VID_SYS_720P_50HZ;
        b_switch = TRUE;
      }
      break;
    case AVC_VIDEO_MODE_1080I_50HZ:
      if(video_mode != VID_SYS_1080I_50HZ)
      {
        video_mode = VID_SYS_1080I_50HZ;
        b_switch = TRUE;
      }
      break;
     case AVC_VIDEO_MODE_1080I_60HZ:
      if(video_mode != VID_SYS_1080I)
      {
        video_mode = VID_SYS_1080I;
        b_switch = TRUE;
      }
      break;
     case AVC_VIDEO_MODE_1080P_60HZ:
      if(video_mode != VID_SYS_1080P)
      {
        video_mode = VID_SYS_1080P;
        b_switch = TRUE;
      }
      break;
     case AVC_VIDEO_MODE_1080P_50HZ:
      if(video_mode != VID_SYS_1080P_50HZ)
      {
        video_mode = VID_SYS_1080P_50HZ;
        b_switch = TRUE;
      }
      break;
     case AVC_VIDEO_MODE_480I:
      if(video_mode != VID_SYS_NTSC_M)
      {
        b_switch = TRUE;
        video_mode = VID_SYS_NTSC_M;
      }
      break;
    case AVC_VIDEO_MODE_576I:
      if(video_mode != VID_SYS_PAL)
      {
        b_switch = TRUE;
        video_mode = VID_SYS_PAL;
      }
      break;
    default:
      break;
  }

  if(b_switch == TRUE)
  {

    disp_get_common_info(p_disp, &disp_info);

    if(disp_info.ch_num > 1)//HD IC, warriors/sonata
    {

      if(video_mode >= VID_SYS_1080I)
      {
        get_sd_tv_mode_from_hd(&sd_mode, video_mode);
        hd_mode = video_mode;
      }
      else
      {
        if((new_std == AVC_VIDEO_MODE_480I)
          ||(new_std == AVC_VIDEO_MODE_576I))
        {
          hd_mode = video_mode;
        }
        else
        {
          disp_get_tv_sys(p_disp, DISP_CHANNEL_HD, &hd_old);
          get_hd_tv_mode_from_sd(video_mode, hd_old, &hd_mode);
        }

        sd_mode = video_mode;
      }
      {
       disp_set_tv_sys(p_disp, DISP_CHANNEL_HD, hd_mode);
      }
    }
    else
    {
        sd_mode = video_mode;
    }

    disp_set_tv_sys(p_disp, DISP_CHANNEL_SD, sd_mode);

    OS_PRINTF("set video std sd: %d  hd:  %d======================\n", sd_mode, hd_mode);
  }
}

static void disp_mode_config(void *p_disp)
{
  av_set_t av_set;

  sys_status_get_av_set(&av_set);
   if(av_set.tv_mode != 0)
      disp_switch_video_mode(p_disp, sys_status_get_sd_mode(av_set.tv_mode));
   disp_switch_video_mode(p_disp, sys_status_get_hd_mode_1(av_set.tv_resolution,p_disp));
   hdmi_video_first_config();
}

static void disp_vdac_config(void * p_disp)
{
  vdac_info_t v_dac_info = g_customer.vdac_type;
#ifdef FAST_STANDBY
  BOOL is_standby = FALSE;
  sys_status_get_status(BS_IS_STANDBY, &is_standby);
  if(is_standby == TRUE)
  {
    disp_dac_onoff(p_disp, DAC_0,  FALSE);

    disp_dac_onoff(p_disp, DAC_1,  FALSE);

    disp_dac_onoff(p_disp, DAC_2,  FALSE);

    disp_dac_onoff(p_disp, DAC_3,  FALSE);
    return;
  }
#endif
  OS_PRINTF("[disp_vdac_config] v_dav_info = %d \n",v_dac_info);

  if (v_dac_info == (u8)CONFIG_NOT_DEFINED)
  {
    if (get_usb1_onoff_from_otp())
    {
      OS_PRINTF("Double USB, close YPbPr\n");
      v_dac_info = VDAC_SIGN_CVBS;
    }
  }
  
  switch(v_dac_info)
  {
    case VDAC_CVBS_RGB:
    {
      OS_PRINTF("\nVDAC_CVBS_RGB\n");
      disp_set_dacmode(p_disp,DISP_DAC_CVBS_RGB);
      //disp_cvbs_onoff(p_disp, CVBS_GRP0, FALSE);
      //disp_component_set_type(p_disp, COMPONENT_GRP0,COLOR_CVBS_RGB);
      //disp_component_onoff(p_disp, COMPONENT_GRP0, TRUE);
    }
    break;

    case VDAC_SIGN_CVBS:
    {
      OS_PRINTF("\nVDAC_SIGN_CVBS\n");
      disp_set_dacmode(p_disp,DISP_DAC_SING_CVBS);
      //disp_cvbs_onoff(p_disp, CVBS_GRP0, TRUE);
      //disp_component_set_type(p_disp, COMPONENT_GRP1,COLOR_YUV);
      //disp_component_onoff(p_disp, COMPONENT_GRP1, TRUE);
      //disp_dac_onoff(p_disp, DAC_1,  FALSE);
      //disp_dac_onoff(p_disp, DAC_2,  FALSE);

    }
    break;

    case VDAC_SIGN_CVBS_LOW_POWER:
    {
      OS_PRINTF("\nVDAC_SIGN_CVBS_LOW_POWER\n");
      disp_set_dacmode(p_disp,DISP_DAC_SING_CVBS_LOW_POWER);
      //disp_cvbs_onoff(p_disp, CVBS_GRP3, TRUE);
      //disp_dac_onoff(p_disp, DAC_0,  FALSE);
      //disp_dac_onoff(p_disp, DAC_1,  FALSE);
      //disp_dac_onoff(p_disp, DAC_2,  FALSE);
    }
    break;

    case VDAC_DUAL_CVBS:
    {
      OS_PRINTF("\nVDAC_DUAL_CVBS\n");
      disp_set_dacmode(p_disp,DISP_DAC_DULE_CVBS);
      //disp_cvbs_onoff(p_disp, CVBS_GRP0, TRUE);
      //disp_cvbs_onoff(p_disp, CVBS_GRP1, FALSE);
      //disp_cvbs_onoff(p_disp, CVBS_GRP2, FALSE);
      //disp_cvbs_onoff(p_disp, CVBS_GRP3, TRUE);
    }
    break;

    case VDAC_CVBS_SVIDEO:
    {
      OS_PRINTF("\nVDAC_CVBS_SVIDEO\n");
      disp_set_dacmode(p_disp,DISP_DAC_CVBS_SVIDEO);
      //disp_cvbs_onoff(p_disp, CVBS_GRP0, TRUE);
      //disp_svideo_onoff(p_disp, SVIDEO_GRP0, TRUE);
      //disp_cvbs_onoff(p_disp, CVBS_GRP2, TRUE);
      //disp_cvbs_onoff(p_disp, CVBS_GRP3, TRUE);
    }
    break;

    case VDAC_CVBS_YPBPR_SD:
    {
      OS_PRINTF("\nVDAC_CVBS_YPBPR_SD\n");
      disp_set_dacmode(p_disp,DISP_DAC_CVBS_YPBPR_SD);
      //disp_cvbs_onoff(p_disp, CVBS_GRP0, TRUE);
      //disp_component_set_type(p_disp, COMPONENT_GRP0, COLOR_YUV);
      //disp_component_onoff(p_disp, COMPONENT_GRP0, TRUE);
    }
    break;

    case VDAC_CVBS_YPBPR_HD:
    {
      OS_PRINTF("\nVDAC_CVBS_YPBPR_HD\n");
      disp_set_dacmode(p_disp,DISP_DAC_CVBS_YPBPR_HD);
      //disp_cvbs_onoff(p_disp, CVBS_GRP0, TRUE);
      //disp_component_set_type(p_disp, COMPONENT_GRP1,COLOR_YUV);
      //disp_component_onoff(p_disp, COMPONENT_GRP1, TRUE);
    }
    break;

    default:
    {
      OS_PRINTF("\nVDAC setting default, CVBS + HD COMPONENT \n");
      disp_set_dacmode(p_disp,DISP_DAC_CVBS_YPBPR_HD);
      //disp_cvbs_onoff(p_disp, CVBS_GRP0, TRUE);
      //disp_component_set_type(p_disp, COMPONENT_GRP1,COLOR_YUV);
      //disp_component_onoff(p_disp, COMPONENT_GRP1, TRUE);
    }
    break;
  }
}

static const drvsvc_handle_t* drv_public_svr_init(void)
{
  drvsvc_handle_t *p_public_drvsvc = NULL;
  u32 *p_buf = NULL;

  //this service will be shared by HDMI, Audio and Display driver
  p_buf = (u32 *)mtos_malloc(DRV_HDMI_TASK_STKSIZE);
  MT_ASSERT(NULL != p_buf);
  p_public_drvsvc = drvsvc_create(DRV_HDMI_TASK_PRIORITY,
    p_buf, DRV_HDMI_TASK_STKSIZE, 8);
  MT_ASSERT(NULL != p_public_drvsvc);

  return p_public_drvsvc;
}

static RET_CODE drv_i2c_init(void)
{
#ifndef WIN32
  RET_CODE ret = ERR_FAILURE;
  i2c_cfg_t i2c_cfg = {0};
  void *p_dev_i2c0 = NULL;
  void *p_dev_i2cfp = NULL;
  void *p_dev_i2chdmi = NULL;
  void *p_dev_i2cqam = NULL;

  /* I2C QAM */
  ret = i2c_symphony_attach("i2c_QAM");
  MT_ASSERT(SUCCESS == ret);

  p_dev_i2cqam = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"i2c_QAM");
  MT_ASSERT(NULL != p_dev_i2cqam);
  memset(&i2c_cfg, 0, sizeof(i2c_cfg));
  i2c_cfg.i2c_id = 4;
  i2c_cfg.bus_clk_khz = 100;
  i2c_cfg.lock_mode = OS_MUTEX_LOCK;
  ret = dev_open(p_dev_i2cqam, &i2c_cfg);
  MT_ASSERT(SUCCESS == ret);

  ret = i2c_symphony_attach("i2c0");
  MT_ASSERT(SUCCESS == ret);

  p_dev_i2c0 = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"i2c0");
  MT_ASSERT(NULL != p_dev_i2c0);
  memset(&i2c_cfg, 0, sizeof(i2c_cfg));
  i2c_cfg.i2c_id = 0;
  i2c_cfg.bus_clk_khz = 100;
  i2c_cfg.lock_mode = OS_MUTEX_LOCK;
  ret = dev_open(p_dev_i2c0, &i2c_cfg);
  MT_ASSERT(SUCCESS == ret);

  /* I2C HDMI */
  ret = i2c_symphony_attach("i2c_HDMI");
  MT_ASSERT(SUCCESS == ret);

  p_dev_i2chdmi = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"i2c_HDMI");
  MT_ASSERT(NULL != p_dev_i2chdmi);
  memset(&i2c_cfg, 0, sizeof(i2c_cfg));
  i2c_cfg.i2c_id = 3;
  i2c_cfg.bus_clk_khz = 100;
  i2c_cfg.lock_mode = OS_MUTEX_LOCK;
  ret = dev_open(p_dev_i2chdmi, &i2c_cfg);
  MT_ASSERT(SUCCESS == ret);
  
  /* I2C FP */
  ret = i2c_symphony_attach("i2c_FP");
  MT_ASSERT(SUCCESS == ret);

  p_dev_i2cfp = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"i2c_FP");
  MT_ASSERT(NULL != p_dev_i2cfp);
  memset(&i2c_cfg, 0, sizeof(i2c_cfg));
  i2c_cfg.i2c_id = 2;
  i2c_cfg.bus_clk_khz = 100;
  i2c_cfg.lock_mode = OS_MUTEX_LOCK;
  ret = dev_open(p_dev_i2cfp, &i2c_cfg);
  MT_ASSERT(SUCCESS == ret);
#endif

  return SUCCESS;
}


static RET_CODE drv_vbi_init(void)
{
  RET_CODE ret = ERR_FAILURE;

#ifndef WIN32
  // vbi
  void *p_dev = NULL;
  vbi_cfg_t vbi_cfg = {0};

  /*lint -e123 */
  ATTACH_DRIVER(VBI_INSERTER, symphony, default, default);
  /*lint +e123 */
  p_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_VBI_INSERTER);
  MT_ASSERT(NULL != p_dev);
  app_printf(PRINTF_DEBUG_LEVEL,"find vbi\n");
  vbi_cfg.vbi_pes_buf_addr = g_vbi_buf_addr;
  vbi_cfg.vbi_pes_buf_size = g_vbi_buf_size;
  ret = dev_open(p_dev, &vbi_cfg);
  MT_ASSERT(SUCCESS == ret);
  app_printf(PRINTF_DEBUG_LEVEL,"vbi ATTACH_DRIVER\n");
#endif

  return ret;
}
static RET_CODE drv_gpe_init(void)
{
  RET_CODE ret = ERR_FAILURE;
  void *p_gpe_c = NULL;

  /* GPE */
  app_printf(PRINTF_DEBUG_LEVEL,"GPE_USE_HW\n");
  /*lint -e123 */
#ifndef WIN32
  ret = spn_gpe_attach("spn_gpe");
  MT_ASSERT(SUCCESS == ret);
#else
  ret = gpe_soft_attach("gpe_soft");
  //ret = ATTACH_DRIVER(GPE_VSB, symphony, default, default);
  MT_ASSERT(SUCCESS == ret);
#endif
  /*lint +e123 */

  p_gpe_c = (void *)dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_GPE_VSB);
  ret = dev_open(p_gpe_c, NULL);
#ifndef WIN32
  MT_ASSERT(TRUE == ret);
#else
  MT_ASSERT(SUCCESS == ret);
#endif

  return SUCCESS;
}

static RET_CODE drv_jpeg_init(void)
{
  RET_CODE ret = ERR_FAILURE;
  void *p_dev = NULL;

  /*JPEG*/
  /*lint -e123 */
#ifndef WIN32
  ret = jcodec_symphony_attach("symphony_jpeg");
  /*lint +e123 */
  MT_ASSERT(ret == SUCCESS);
#else
  /*lint -e123 */
  ret = ATTACH_DRIVER(PDEC, symphony, default, default);
  /*lint +e123 */
  MT_ASSERT(ret == SUCCESS);
#endif

#ifndef WIN32
  p_dev = dev_find_identifier(NULL,DEV_IDT_NAME, (u32)"symphony_jpeg");
#else
  p_dev = dev_find_identifier(NULL,DEV_IDT_TYPE, SYS_DEV_TYPE_PDEC);
#endif
  
  MT_ASSERT(NULL != p_dev);

  ret = dev_open(p_dev, NULL);
  MT_ASSERT(SUCCESS == ret);

  return ret;
}

#if 0
#ifndef WIN32
static void config_lock_len_pin(void)
{
  if(g_fp_cfg.pan_info.type_lbd == 0)
  {
    g_lock_led_pin = g_fp_cfg.pan_info.lbd[1].pos;
    gpio_io_enable(g_lock_led_pin, TRUE);
    gpio_set_dir(g_lock_led_pin, GPIO_DIR_OUTPUT);
    gpio_set_value(g_lock_led_pin, 0);
  }
}
#endif
#endif

#ifdef HW_PIN_STANDARD_CFG
//u32 g_lock_led_pin; //AGPIO2
//static u32 g_standby_led_pin=65; //AGPIO1
//static u32 g_standby_key_pin=64; //AGPIO0
static u32 g_fp_type=HAL_FD650;
static u32 g_nim_reset_pin = 50; //GPIO50

static int check_hw_cfg_pin(void)
{
  /*CFG0:GPIO52 CFG1:GPIO51 CFG2:GPIO36 CFG3:GPIO32 */
  /*CFG3|CFG2|CFG1|CFG0:
    1111:No Frontpanel_8key_2LED
    0001:FD650（待机按键650控制）
    0010:FD650（待机按键IO控制）
    0011:CT1642
  */
  u8 val=0;
  u8 cfg=0;
  
  gpio_get_value(32 , &val);
  cfg += val << 3;
  gpio_get_value(36 , &val);
  cfg += val << 2;
  gpio_get_value(51 , &val);
  cfg += val << 1;
  gpio_get_value(52 , &val);
  cfg += val;
  /*
  g_lock_led_pin = 66;
  gpio_io_enable(g_lock_led_pin, TRUE);
  gpio_set_dir(g_lock_led_pin, GPIO_DIR_OUTPUT);
  gpio_set_value(g_lock_led_pin, 0);
  */
  if(cfg == 0xF)
  {
    g_fp_type = HAL_GPIO;
  }
  else if(cfg == 0x1)
  {
    g_fp_type = HAL_FD650;
  }
  else if(cfg == 0x2)
  {
    g_fp_type = HAL_FD650;
  }
  else if(cfg == 0x3)
  {
    g_fp_type = HAL_CT1642;
  }
  else
  {
    g_fp_type = HAL_FD650;
  }
  return cfg;
}
#endif

#ifndef WIN32
static u8 g_dm6k_addr = 0x18;
static u8 g_cs8k_sat_addr = 0xd0;
static u8 g_cs8k_cab_addr = 0x38;
static u8 g_ts6001_tuner_addr = 0x58;
static u8 g_tc3800_tuner_addr = 0xc2;
static u8 g_tc6800_tuner_addr = 0xc6;
static u16 g_dm6k_s_type =  0x1;
static u16 g_dm6k_tc_type = 0x2;
static u16 g_cs8k_s_type =   0x4;
static u16 g_cs8k_tc_type =   0x8;
void nim_pinmux_set(u8 nim_type)
{
  u32  data = 0;
  switch(nim_type)
  {
    case NIM_CS8K_CAB:
    	hal_set_pinmux(SW_PIN4,16,4,1);
    	hal_set_pinmux(SW_PIN4,20,4,1);
    	hal_set_pinmux(SW_PIN2,28,4,1);
    	hal_set_pinmux(SW_PIN3,4,4,2);
    	hal_set_pinmux(I2C_PIN0,0,32,0x211);
      break;
    case NIM_CS8K_S:
      hal_set_pinmux(SW_PIN4,16,4,2);
      hal_set_pinmux(SW_PIN4,20,4,2);
      hal_set_pinmux(SW_PIN2,12,4,2);
      hal_set_pinmux(SW_PIN2,16,4,2);
      hal_set_pinmux(SW_PIN2,20,4,2);
      hal_set_pinmux(SW_PIN2,24,4,2);
      hal_set_pinmux(SW_PIN3,0,4,1);
      hal_set_pinmux(SW_PIN3,4,4,2);
      hal_set_pinmux(SW_PIN3,8,4,1);
      hal_set_pinmux(SW_PIN4,12,4,0);
      hal_set_pinmux(I2C_PIN0,0,32,0x311);
      break;
    case NIM_CS8K_CAB_S:
      break;
    case NIM_DM6K_T2:
      hal_set_pinmux(I2C_PIN0,8,2,0);
      hal_set_pinmux(SW_PIN2,12,4,0);
      hal_set_pinmux(SW_PIN2,16,4,1);
      hal_set_pinmux(SW_PIN2,20,8,0);
      hal_set_pinmux(SW_PIN2,28,4,1);
      hal_set_pinmux(SW_PIN3,0,4,1);
      hal_set_pinmux(SW_PIN3,8,4,1);
      hal_set_pinmux(SW_PIN4,16,8,0);
      hal_set_pinmux(I2C_PIN0,8,2,0);
      break;
    case NIM_DM6K_T2_S2:
      hal_set_pinmux(I2C_PIN0,8,2,0);
      hal_set_pinmux(SW_PIN2,12,4,0);
      hal_set_pinmux(SW_PIN2,16,4,1);
      hal_set_pinmux(SW_PIN2,20,8,0);
      hal_set_pinmux(SW_PIN2,28,4,1);
      hal_set_pinmux(SW_PIN3,0,4,1);
      hal_set_pinmux(SW_PIN3,8,4,1);
      hal_set_pinmux(SW_PIN4,16,8,0);
      hal_set_pinmux(I2C_PIN0,8,2,0);

      break;
    case NIM_DM6K_C_T2_S2:
      //TODO:寄存器的值需要和AE确认
      hal_put_u32((u32 *)0xBF200010, 0);
      hal_put_u32((u32 *)0xBF200010, 0xc7fb01ff);//bit[17:16]=11s  
        
      hal_put_u32((u32 *)0xBF13C000, 0x11111111);
      hal_put_u32((u32 *)0xBF13C004, 0x00011001);
      hal_put_u32((u32 *)0xBF13C008, 0x11111110);
      hal_put_u32((u32 *)0xBF13C00c, 0x10000111);
      hal_put_u32((u32 *)0xBF13C010, 0x00001111);
      hal_put_u32((u32 *)0xBF13C014, 0x00111001);
      data = hal_get_u32((u32 *)0xBF15b400);
      data |= 0x00000100;
      hal_put_u32((u32 *)0xBF15b400, data);
      hal_put_u32((u32 *)0xBF15b404, 0);

      data = hal_get_u32((u32 *)0xBF5D0094);
      data |= 0x1;
      hal_put_u32((u32 *)0xBF5D0094, data);

      hal_put_u32((u32 *)0xBF500024, 0xb0001);
      //
      data = hal_get_u32((u32 *)0xBF138008);
      data &= ~(1<<0);
      hal_put_u32((u32 *)0xBF138008, data);
      break;
     case NIM_DM6K_C_T2:
	hal_put_u32((u32 *)0xBF200010,0XC7FB01ff);
       hal_put_u32((u32 *)0xBF13C000,0X11111111);
       hal_put_u32((u32 *)0xBF13C004,0X00011001);
	hal_put_u32((u32 *)0xBF13C008,0X11111110);
	hal_put_u32((u32 *)0xBF13C00C,0X10000111);
	hal_put_u32((u32 *)0xBF13C010,0X00001111);
	hal_put_u32((u32 *)0xBF13C014,0X00111001);
	hal_put_u32((u32 *)0xBF15B404,0);
	data = hal_get_u32((u32 *)0xBF5D0094);
	data |= 0x1;
	hal_put_u32((u32 *)0xBF5D0094,data);
	hal_put_u32((u32 *)0xBF500024,0Xb0001);
	hal_put_u32((u32 *)0xBF138008,0X10);
     break;
  }
}

static int nim_i2c_dm6k_sys_get_reg(u8 dev_addr, u8 reg_addr, u8 *p_buf)
{
  int rc;
  u8 data = reg_addr;
  i2c_bus_t *g_i2c0 = NULL;

  g_i2c0 = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"i2c_QAM");

  /*
      TODO:
      read n_byte data from demod
   */
  rc = i2c_std_read(g_i2c0, dev_addr, &data, 1, 1, I2C_PARAM_DEV_SOC_INTER);
  if (rc)
  {
    OS_PRINTF("%s %d  rc=%d devaddr=%x reg=%x \n", __FUNCTION__, __LINE__, rc, dev_addr, reg_addr);
    rc = ERR_FAILURE;
    data = 0;
  }
  *p_buf = data;
  
  return rc;
}
static int nim_i2c_dm6k_sys_set_reg(u8 dev_addr, u8 reg_addr, u8 data)
{

  int rc = 0;
  i2c_bus_t *g_i2c0 = NULL;

  g_i2c0 = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"i2c_QAM");

  /*
     TODO:
     write one data to demod

  */
  u8 tmp[2];

  tmp[0] = reg_addr;
  tmp[1] = data;
  
  //nim_fe_i2c_lock();
  rc = i2c_write(g_i2c0, dev_addr, tmp, 2, I2C_PARAM_DEV_SOC_EXTER);
  //nim_fe_i2c_unlock();

  if (rc)
  {
    OS_PRINTF("%s %d  rc=%d demod_address=%x reg=%x \n", __FUNCTION__, __LINE__, rc, dev_addr, reg_addr);
    rc = ERR_FAILURE;
  }
  
  return rc;

}

static int nim_i2c_repeat_enable_dm6k(u8 dev_addr)
{
	int	ret = 0;
	u8 data = 0;

	ret = nim_i2c_dm6k_sys_get_reg(dev_addr, 0x03, &data);
	if (ret != 0)
	{
		return ret;
	}	

	data = (u8)(data | 0x10);
	ret = nim_i2c_dm6k_sys_set_reg(dev_addr, 0x03, data);
	if (ret != 0)
	{
		return ret;
	}
	
	return ret;
}

static int nim_i2c_repeat_disable_dm6k(u8 dev_addr)
{
	int	ret = 0;
	u8 data = 0;

	//ret =  _mt_fe_dmd_get_reg_dm6k(handle, 0x03, &data);
	ret = nim_i2c_dm6k_sys_get_reg(dev_addr, 0x03, &data);
	if (ret != SUCCESS)
	{
		return ret;
	}
	data = (u8)(data & 0xef);
	//ret =  _mt_fe_dmd_set_reg_dm6k(handle, 0x03, data);
	ret = nim_i2c_dm6k_sys_set_reg(dev_addr, 0x03, data);
	if (ret != SUCCESS)
	{
		return ret;
	}

	//handle->bTunerBusOn = FALSE;

	return ret;
}
static int nim_i2c_dm6k_ss2_get_tn_reg(u8 dmd_addr, u8 tuner_addr, u8 reg_addr, u8 *p_buf)
{
  int ret = 0;
  u8 data = reg_addr;
  i2c_bus_t *g_i2c0 = NULL;

  g_i2c0 = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"i2c_QAM");
	
  nim_i2c_repeat_enable_dm6k(dmd_addr);
	
  //ret = mt_fe_tn_read_dm6k(dm6k_handle->m_device_ss2.tuner_cfg.tuner_dev_addr, &reg_addr, 1, &reg_data, 1);
  ret = i2c_std_read(g_i2c0, tuner_addr, &data, 1, 1, I2C_PARAM_DEV_SOC_INTER);
  if (ret != SUCCESS)
  {
    ret = ERR_FAILURE;
	data = 0;
    //OS_PRINTF("dvbs tuner_addr %02x reg_addr %02x reg_data %02x\n", tuner_addr, reg_addr, reg_data);
  }
  *p_buf = data;
	
  nim_i2c_repeat_disable_dm6k(dmd_addr);

  return ret;
}

static int nim_i2c_dm6k_ctt2_get_tn_reg(u8 dmd_addr, u8 tuner_addr, u8 reg_addr, u8 *p_buf)
{
  int ret = 0;
  u8 data = reg_addr;
  i2c_bus_t *g_i2c0 = NULL;

  g_i2c0 = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"i2c_QAM");

  nim_i2c_repeat_enable_dm6k(dmd_addr);
  ret = i2c_std_read(g_i2c0, tuner_addr, &data, 1, 1, I2C_PARAM_DEV_SOC_INTER);
  if (ret != SUCCESS)
  {
    OS_PRINTF("%s %d  rc=%d devaddr=%x reg=%x \n", __FUNCTION__, __LINE__, ret, tuner_addr, reg_addr);
    data = 0;
    ret = ERR_FAILURE;
  }
  *p_buf = data;
  
  nim_i2c_repeat_disable_dm6k(dmd_addr);

  return ret;
}
static int nim_i2c_cs8k_sat_dmd_set_reg_no_lock(u8 dev_addr, u8 reg_addr, u8 data)
{
  int ret;
  u8 buf[2] = {0};
  i2c_bus_t *g_i2c0 = NULL;

  g_i2c0 = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"i2c_QAM");
  
  buf[0] = reg_addr;
  buf[1] = data;

  ret = i2c_write(g_i2c0, dev_addr, buf, 2, I2C_PARAM_DEV_SOC_INTER);
  if (ret != SUCCESS)
  {
  	OS_PRINTF("%s %d  ret=%d devaddr=%02x reg=%02x \n", __FUNCTION__, __LINE__, ret, dev_addr, reg_addr);
  	return ERR_FAILURE;
  }
  
  return ret;
}
int nim_i2c_cs8k_cab_dmd_reg_read_unlock(u8 dev_addr, u8 reg_addr, u8 *p_buf)
{
  int ret = 0;
  u8 data = reg_addr;
  i2c_bus_t *g_i2c0 = NULL;

  g_i2c0 = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"i2c_QAM");

  ret = i2c_std_read(g_i2c0, dev_addr, &data, 1, 1, I2C_PARAM_DEV_SOC_EXTER);
  if (ret != SUCCESS)
  {
    OS_PRINTF("%s %d  rc=%d devaddr=%x reg=%x \n", __FUNCTION__, __LINE__, ret, dev_addr, reg_addr);
  data = 0;
  }

  *p_buf = data;
  return ret;
}
int nim_i2c_cs8k_cab_dmd_reg_write_unlock(u8 dev_addr, u8 reg_addr, u8 data)
{
  s8 ret = 0;
  u8 tmp[2];
  i2c_bus_t *g_i2c0 = NULL;

  g_i2c0 = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"i2c_QAM");

  tmp[0] = reg_addr;
  tmp[1] = data;
  ret = i2c_write(g_i2c0, dev_addr, tmp, 2, I2C_PARAM_DEV_SOC_EXTER);
  if (ret != SUCCESS)
  {
    OS_PRINTF("%s %d  rc=%d devaddr=%x reg=%x \n", __FUNCTION__, __LINE__, ret, dev_addr, reg_addr);
  }

  return ret;
}

static int nim_i2c_cs8k_sat_tn_get_reg(u8 dmd_addr, u8 tuner_addr, u8 reg_addr, u8 *p_buf)
{
  int ret;
  u8 val;
  i2c_bus_t *g_i2c0 = NULL;

  g_i2c0 = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"i2c_QAM");

  val = 0x12;
  ret = nim_i2c_cs8k_sat_dmd_set_reg_no_lock(dmd_addr, 0x03, val);
  if (ret != SUCCESS)
  {
    OS_PRINTF("%s %d  ret=%d devaddr=%02x failed.\n", __FUNCTION__, __LINE__, ret, dmd_addr);
	return ret;
  }
  
  *p_buf = reg_addr;
  ret = i2c_std_read(g_i2c0, tuner_addr, p_buf, 1, 1, I2C_PARAM_DEV_SOC_INTER);
  if (ret != SUCCESS)
  {
  	OS_PRINTF("%s %d  ret=%d devaddr=%02x reg=%02x \n", __FUNCTION__, __LINE__, ret, tuner_addr, reg_addr);
    return ret;
  }

  return SUCCESS;
}

static int nim_i2c_cs8k_cab_tn_get_reg(u8 dem_mode, u8 dmd_addr, u8 tuner_addr, u8 reg_addr, u8 *p_buf)
{
  int ret = 0;
  u8 value = 0;
  u8 data = reg_addr;
  i2c_bus_t *g_i2c0 = NULL;

  g_i2c0 = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"i2c_QAM");

  if (dem_mode != 2)
  {
    nim_i2c_cs8k_cab_dmd_reg_read_unlock(dmd_addr, 0x87, &value);
    value &= 0x0F;
    value |= 0xa0;    // bit7         = 1, Enable I2C repeater
    // bit[6:4]     = 1, Enable I2C repeater for 1 time
    nim_i2c_cs8k_cab_dmd_reg_write_unlock(dmd_addr, 0x87, value);
    ret = i2c_std_read(g_i2c0, tuner_addr, &data, 1, 1, I2C_PARAM_DEV_SOC_EXTER);
	if (ret != SUCCESS)
    {
      OS_PRINTF("%s %d  rc=%d devaddr=%x reg=%x \n", __FUNCTION__, __LINE__, ret, tuner_addr, reg_addr);
    }
  }
  else
  {
    nim_i2c_cs8k_cab_dmd_reg_read_unlock(dmd_addr, 0x86, &value);
    value |= 0x80;
    nim_i2c_cs8k_cab_dmd_reg_write_unlock(dmd_addr, 0x86, value);
    i2c_seq_read(g_i2c0, tuner_addr, &data, 1, 1, I2C_PARAM_DEV_SOC_EXTER);
	if (ret != SUCCESS)
	{
      OS_PRINTF("%s %d  rc=%d devaddr=%x reg=%x \n", __FUNCTION__, __LINE__, ret, tuner_addr, reg_addr);
	}
  }

  return ret;
}


static u16 nim_type_by_addr( )
{
  u8 cs8k_sat_enable = 0, cs8k_cab_enable = 0, dm6k_enable = 0,dm6k_sat_enable = 0,dm6k_tc_enable = 0;
  u8 reg_data = 0;
  u16 nim_type = 0;
  int ret = 0;

  //here should config dm6k pinmux
  nim_pinmux_set(NIM_DM6K_T2_S2);
  ret = nim_i2c_dm6k_sys_get_reg(g_dm6k_addr, 0x0, &reg_data);
  if ((ret == SUCCESS) && ((reg_data & 0xfe) == 0x1c))
  {
    dm6k_enable = 1;
  }

  if (dm6k_enable)
  {
    ret = nim_i2c_dm6k_ss2_get_tn_reg(g_dm6k_addr, g_ts6001_tuner_addr, 0x01, &reg_data);
    if (ret == SUCCESS)
    {
      dm6k_sat_enable = 1;
      nim_type |= g_dm6k_s_type;
    }

    ret = nim_i2c_dm6k_ctt2_get_tn_reg(g_dm6k_addr, g_tc3800_tuner_addr, 0x01, &reg_data);
    if (ret == SUCCESS)
    {
      dm6k_tc_enable = 1;
      nim_type |= g_dm6k_tc_type;
    }
    else
    {
      ret = nim_i2c_dm6k_ctt2_get_tn_reg(g_dm6k_addr, g_tc6800_tuner_addr, 0x01, &reg_data);
      if (ret == SUCCESS)
      {
        dm6k_tc_enable = 1;
        nim_type |= g_dm6k_tc_type;
      }
    }
  }

  if (dm6k_sat_enable == 0)
  {
    //here should config cs8k_sat pinmux
    nim_pinmux_set(NIM_CS8K_S);

    ret = nim_i2c_cs8k_sat_tn_get_reg(g_cs8k_sat_addr, g_ts6001_tuner_addr, 0x01, &reg_data);
    if (ret == SUCCESS)
    {
      cs8k_sat_enable = 1;
      nim_type |= g_cs8k_s_type;
    }
  }

  if (dm6k_tc_enable == 0)
  {
    //here should config cs8k_cab pinmux
    nim_pinmux_set(NIM_CS8K_CAB);

    ret = nim_i2c_cs8k_cab_tn_get_reg(1, g_cs8k_cab_addr, g_tc3800_tuner_addr, 0x01, &reg_data);
    if (ret == SUCCESS)
    {
      cs8k_cab_enable = 1;
      nim_type |= g_cs8k_tc_type;
    }
    else
    {
      ret = nim_i2c_cs8k_cab_tn_get_reg(1, g_cs8k_cab_addr, g_tc6800_tuner_addr, 0x01, &reg_data);
      if (ret == SUCCESS)
      {
        cs8k_cab_enable = 1;
        nim_type |= g_cs8k_tc_type;
      }
    }
  }

  APPLOGA("%s %d nim_type 0x%x\n",__FUNCTION__,__LINE__,nim_type);
  return nim_type;
}

#else
static RET_CODE nim_win32_init(void)
{
  RET_CODE ret = ERR_FAILURE;
  void *p_dev = NULL;
  nim_config_t nim_cfg = {0};
  void *p_dem_bus_0 = NULL;
  void *p_dem_bus_qam = NULL;
  u32 dmd_version = 0;
  u32 tn_version = 0;

  p_dem_bus_0 = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"i2c0");
  p_dem_bus_qam = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"i2c_QAM");

  /*lint -e123 */
  ret = ATTACH_DRIVER(NIM, m88rs2k, default, default);
  /*lint +e123 */
  MT_ASSERT(ret == SUCCESS);

  p_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_NIM);
  MT_ASSERT(NULL != p_dev);
  memset(&nim_cfg, 0, sizeof(nim_config_t));
  nim_cfg.p_dem_bus = p_dem_bus_qam;
  nim_cfg.bs_times = 1;
  nim_cfg.pin_config.lnb_enable = NIM_PIN_LEVEL_LOW;
  nim_cfg.pin_config.vsel_when_13v = NIM_PIN_LEVEL_HIGH;
  nim_cfg.pin_config.vsel_when_lnb_off = NIM_PIN_LEVEL_HIGH;
  nim_cfg.pin_config.diseqc_out_when_lnb_off = NIM_PIN_LEVEL_LOW;
  nim_cfg.task_prio = NIM_NOTIFY_TASK_PRIORITY;
  nim_cfg.stack_size = NIM_NOTIFY_TASK_STK_SIZE;
  nim_cfg.lock_mode = OS_MUTEX_LOCK;

  nim_cfg.dem_addr = 0x38;
  nim_cfg.tun_addr = 0xc2;
  nim_cfg.ts_mode = NIM_TS_INTF_PARALLEL;
  nim_cfg.dem_ver = DEM_VER_3;
  nim_cfg.tuner_loopthrough = 1;

  ret = dev_open(p_dev, &nim_cfg);
  MT_ASSERT(SUCCESS== ret);

  dev_io_ctrl(p_dev, (u32)NIM_IOCTRL_SET_TUNER_WAKEUP, 0);

  dev_io_ctrl(p_dev, (u32)NIM_IOCTRL_GET_DMD_VERSION, (u32)(&dmd_version));
  dev_io_ctrl(p_dev, (u32)NIM_IOCTRL_GET_TN_VERSION, (u32)(&tn_version));
  APPLOGA("Current DMD version is V%d Tuner version is V%d\n",dmd_version,tn_version);

  g_customer.nim_type = NIM_CS8K_CAB_S;

  g_nim_cfg[g_tuner_num].nim_type = (u8)CLT_NIM_CAP_DVBS;
  g_nim_cfg[g_tuner_num].tuner_id = TUNER0;
  g_nim_cfg[g_tuner_num].ts_input = DMX_INPUT_EXTERN1;
  g_tuner_num ++;
  
  g_nim_cfg[g_tuner_num].nim_type = (u8)CLT_NIM_CAP_DVBC;
  g_nim_cfg[g_tuner_num].tuner_id = TUNER1;
  g_nim_cfg[g_tuner_num].ts_input = DMX_INPUT_EXTERN0;
  g_tuner_num ++;

  g_nim_cfg[g_tuner_num].nim_type = (u8)CLT_NIM_CAP_DVBT;
  g_nim_cfg[g_tuner_num].tuner_id = TUNER1;
  g_nim_cfg[g_tuner_num].ts_input = DMX_INPUT_EXTERN0;
  g_tuner_num ++;
  return ret;
}
#endif

static void print_nim_type(u8 nim_type)
{
  const char *p_nim_name;

  switch (nim_type)
  {
    case NIM_CS8K_CAB:
      p_nim_name = "NIM_CS8K_CAB";
      break;
    case NIM_CS8K_S:
      p_nim_name = "NIM_CS8K_S";
      break;
    case NIM_CS8K_CAB_S:
      p_nim_name = "NIM_CS8K_CAB_S";
      break;
    case NIM_DM6K_T2:
      p_nim_name = "NIM_DM6K_T2";
      break;
    case NIM_DM6K_T2_S2:
      p_nim_name = "NIM_DM6K_T2_S2";
      break;
    case NIM_DM6K_C_T2_S2:
      p_nim_name = "NIM_DM6K_C_T2_S2";
      break;
    case NIM_DM6K_C_T2:
      p_nim_name = "NIM_DM6K_C_T2";
      break;

    default :
      p_nim_name = "Unknown";
      break;  
  }

  APPLOGA("Hardware nim type: %s\n", p_nim_name);
}

static RET_CODE drv_nim_init(void)
{
  u16 ret = 0;
  
  memset(g_nim_cfg,0,sizeof(util_nim_cfg_t) * 4);
  g_tuner_num = 0;

#ifdef WIN32
  nim_win32_init();
#else
  if(g_customer.nim_type >= NIM_UNKONW)
  {
    ret = nim_type_by_addr();
    if(ret == g_dm6k_tc_type)
      g_customer.nim_type = NIM_DM6K_T2;
    else if(ret == (g_dm6k_s_type | g_dm6k_tc_type))
      g_customer.nim_type = NIM_DM6K_T2_S2;
    else if(ret == g_cs8k_s_type)
      g_customer.nim_type = NIM_CS8K_S;
    else if(ret == g_cs8k_tc_type)
      g_customer.nim_type = NIM_CS8K_CAB;
    else if(ret == (g_cs8k_s_type | g_cs8k_tc_type))
      g_customer.nim_type = NIM_CS8K_CAB_S;
    else
    {
      APPLOGA("%s %d not find nim config \n",__FUNCTION__,__LINE__);
    }
  }
  
  print_nim_type(g_customer.nim_type);
  
  switch(g_customer.nim_type)
  {
    case NIM_CS8K_CAB:
      //nim_cs8k_cab_init( );
      nim_cs8k_cab_init(g_cs8k_cab_addr,g_tc3800_tuner_addr,g_tc6800_tuner_addr);
      g_nim_cfg[g_tuner_num].nim_type = (u8)CLT_NIM_CAP_DVBC;
      g_nim_cfg[g_tuner_num].tuner_id = TUNER0;
      g_nim_cfg[g_tuner_num].ts_input = DMX_INPUT_EXTERN0;
      g_tuner_num++;
      break;
    case NIM_CS8K_S:
      nim_cs8k_s_init( );
      g_nim_cfg[g_tuner_num].nim_type = (u8)CLT_NIM_CAP_DVBS;
      g_nim_cfg[g_tuner_num].tuner_id = TUNER0;
      g_nim_cfg[g_tuner_num].ts_input = DMX_INPUT_EXTERN1;
      g_tuner_num++;
      break;
    case NIM_CS8K_CAB_S:
      nim_cs8k_s_init( );
      g_nim_cfg[g_tuner_num].nim_type = (u8)CLT_NIM_CAP_DVBS;
      g_nim_cfg[g_tuner_num].tuner_id = TUNER0;
      g_nim_cfg[g_tuner_num].ts_input = DMX_INPUT_EXTERN1;
      //nim_cs8k_cab_init( );
      nim_cs8k_cab_init(g_cs8k_cab_addr,g_tc3800_tuner_addr,g_tc6800_tuner_addr);
      g_tuner_num ++;
      g_nim_cfg[g_tuner_num].nim_type = (u8)CLT_NIM_CAP_DVBC;
      g_nim_cfg[g_tuner_num].tuner_id = TUNER1;
      g_nim_cfg[g_tuner_num].ts_input = DMX_INPUT_EXTERN0;
      g_tuner_num++;
      break;
    case NIM_DM6K_T2:
      //nim_dm6k_tc6800_init( );
      nim_dm6k_tc6800_init(g_tc6800_tuner_addr );
      g_nim_cfg[g_tuner_num].nim_type = (u8)CLT_NIM_CAP_DVBT;
      g_nim_cfg[g_tuner_num].tuner_id = TUNER0;
      g_nim_cfg[g_tuner_num].ts_input = DMX_INPUT_EXTERN0;
      g_tuner_num++;
      break;
    case NIM_DM6K_T2_S2:
      nim_dm6k_tc6800_ts6011_init(g_tc6800_tuner_addr,g_ts6001_tuner_addr);
      g_nim_cfg[g_tuner_num].nim_type = (u8)(CLT_NIM_CAP_DVBT | CLT_NIM_CAP_DVBS);
      g_nim_cfg[g_tuner_num].tuner_id = TUNER0;
      g_nim_cfg[g_tuner_num].ts_input = DMX_INPUT_EXTERN0;
      g_tuner_num++;
      break;
    case NIM_DM6K_C_T2_S2:
      nim_dm6k_tc6800_ts6011_init(g_tc6800_tuner_addr,g_ts6001_tuner_addr);
      g_nim_cfg[g_tuner_num].nim_type = (u8)(CLT_NIM_CAP_DVBC | CLT_NIM_CAP_DVBT | CLT_NIM_CAP_DVBS);
      g_nim_cfg[g_tuner_num].tuner_id = TUNER0;
      g_nim_cfg[g_tuner_num].ts_input = DMX_INPUT_EXTERN0;
      g_tuner_num++;
      break;
    case NIM_DM6K_C_T2:
      nim_dm6k_tc6800_init(g_tc6800_tuner_addr);
      g_nim_cfg[g_tuner_num].nim_type = (u8)(CLT_NIM_CAP_DVBT | CLT_NIM_CAP_DVBC);
      g_nim_cfg[g_tuner_num].tuner_id = TUNER0;
      g_nim_cfg[g_tuner_num].ts_input = DMX_INPUT_EXTERN0;
      g_tuner_num++;
      break;
	  
    default :
      APPLOGA("%s %d don't init nim \n",__FUNCTION__,__LINE__);
      break;
  }
#endif

  return SUCCESS;
}


static RET_CODE drv_dm_init(void)
{
  dm_v2_init_para_t dm_para = {0};
  u32 dm_app_addr = 0;

  spiflash_cfg( );

  //init data manager....
  dm_para.flash_base_addr = get_flash_addr();
  dm_para.max_blk_num = DM_MAX_BLOCK_NUM;
  dm_para.task_prio = MDL_DM_MONITOR_TASK_PRIORITY;
  dm_para.task_stack_size = MDL_DM_MONITOR_TASK_STKSIZE;
  dm_para.open_monitor = TRUE;
  dm_para.para_size = sizeof(dm_v2_init_para_t);
  dm_para.use_mutex = TRUE;
  dm_para.mutex_prio = 1;
  dm_para.test_mode = FALSE;
  app_printf(PRINTF_INFO_LEVEL,"max blk num %d\n",DM_MAX_BLOCK_NUM);
  app_printf(PRINTF_INFO_LEVEL,"dm_para.flash_base_addr  :0x%x \n",dm_para.flash_base_addr );

  dm_init_v2(&dm_para);
  app_printf(PRINTF_INFO_LEVEL,"set header [0x%08x]\n",get_bootload_off_addr());
  dm_set_header(class_get_handle_by_id(DM_CLASS_ID), get_bootload_off_addr());
  app_printf(PRINTF_INFO_LEVEL,"set header [0x%08x]\n", get_maincode_off_addr());
  dm_set_header(class_get_handle_by_id(DM_CLASS_ID), get_maincode_off_addr());
  dm_app_addr = dm_get_block_addr(class_get_handle_by_id(DM_CLASS_ID), DM_APP_BLOCK_ID)
                           - get_flash_addr();
  dm_set_header(class_get_handle_by_id(DM_CLASS_ID),dm_app_addr);

  //load fp config
  customer_config_load_fp_cfg();
  
#ifdef HW_PIN_STANDARD_CFG
  g_fp_cfg.fp_type = g_fp_type;
  APPLOGA("%s fp_type %d \n", __FUNCTION__, g_fp_type);
#endif
  return SUCCESS;
}

static RET_CODE drv_uio_init(void)
{
  RET_CODE ret = ERR_FAILURE;
  /*lint -e123 */
#ifndef WIN32
 // ret = ATTACH_DRIVER(UIO, concerto, default, concerto);//ATTACH_DRIVER(UIO, concerto, default, concerto);
  ret = uio_concerto_attach("FP_OLED");
#else
  ret = ATTACH_DRIVER(UIO, magic, default, gpio);
#endif
  /*lint +e123 */
  MT_ASSERT(ret == SUCCESS);
  uio_init();

  return ret;
}
static RET_CODE drv_smc_init(void)
{
#ifndef WIN32
  #ifndef QUANZHIWUKA_CA
  RET_CODE ret = ERR_FAILURE;
  void *p_dev = NULL;
  scard_open_t smc_open_params ={0};
  u32 reg = 0;
  extern RET_CODE smc_attach_symphony(char *name);

  if(cas_get_cas_id() != CAS_ID_DUMY)
  {
    // PINMUX
    reg = *((volatile u32 *)(0xbf15b400));
    reg &= ~(0xf << 28);
    reg |= (2 << 28);
    *((volatile u32 *)(0xbf15b400)) =  reg;

    reg = *((volatile u32 *)(0xbf15b404));
    reg &= ~(0xf << 0);
    reg |= (2 << 0);
    reg &= ~(0xf << 4);
    reg |= (2 << 4);
    *((volatile u32 *)(0xbf15b404)) =  reg;

    reg = *((volatile u32 *)(0xbf13c004));
    reg &= ~(0xf << 12);
    reg |= (1 << 12);
    reg &= ~(0xf << 16);
    reg |= (1 << 16);
    *((volatile u32 *)(0xbf13c004)) = reg;

    reg = *((volatile u32 *)(0xbf13c008));
    reg &= ~(0xf << 4);
    reg |= (2 << 4);
    reg &= ~(0xf << 8);
    reg |= (2 << 8);
    *((volatile u32 *)(0xbf13c008)) = reg;

    // SMC device 0
    ret = smc_attach_symphony("concerto_smc0");
    MT_ASSERT(ret == SUCCESS);
    p_dev = (scard_device_t *) dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"concerto_smc0");
    MT_ASSERT(NULL != p_dev);
    smc_open_params.p_drvsvc = (void *)g_p_public_drvsvc;
    smc_open_params.smc_id = SMC0_ID;
    smc_open_params.smc_op_pri = DRV_SMC_TASK_PRIORITY;
    smc_open_params.phyclk = 0;
    smc_open_params.convention = 0;
    smc_open_params.convt_value = 0;
    
    smc_open_params.detect_pin_pol = g_customer.smc_cfg.detect_pin_pol;
    smc_open_params.vcc_enable_pol = g_customer.smc_cfg.vcc_enable_pol;
    
    smc_open_params.iopin_mode = 1; // external pull-up 5V
    smc_open_params.rstpin_mode = 1; // external pull-up 5V
    smc_open_params.clkpin_mode = 1; // external pull-up 5V
    
    smc_open_params.read_timeout = 2000;
    
    smc_open_params.smc_op_stksize = 4096;
    #ifdef CUSTOMER_HCI
    extern int atr_timeout;
    atr_timeout = 20;
    #endif
    
    ret = dev_open(p_dev, &smc_open_params);
    MT_ASSERT(SUCCESS == ret);
    
    app_printf(PRINTF_DEBUG_LEVEL,"\nSmart Card 0 Dev Open\n");
  }
  #endif
#endif

  return SUCCESS;
}

static RET_CODE drv_hdmi_init(void)
{
#ifndef WIN32
  hdmi_driver_attach((void *)g_p_public_drvsvc);
#endif

  return SUCCESS;
}

static void drv_mute_contrl()
{
  OS_PRINTF("Mute gpio port %d\n", g_customer.mute_gpio_port);
  //静音电路的gpio?琧vbs??出有声?
  gpio_io_enable(g_customer.mute_gpio_port, TRUE);
  gpio_set_dir(g_customer.mute_gpio_port, GPIO_DIR_OUTPUT);
  gpio_set_value(g_customer.mute_gpio_port, GPIO_LEVEL_HIGH);
}

static RET_CODE drv_audio_init(void)
{
  /* AUDIO */
  RET_CODE ret = ERR_FAILURE;
  void *p_dev = NULL;
  aud_cfg_t aud_cfg = {0};
#ifndef FAST_PLAY
  u32 size = 0;
  u32 align = 0;
#endif
  extern RET_CODE symphony_audio_attach(const char *p_name);

  /*lint -e123 */
#ifndef WIN32
  ret = symphony_audio_attach("audio_symphony");
  /*lint +e123 */
  MT_ASSERT(ret == SUCCESS);
#else
  /*lint -e123 */
  ret = ATTACH_DRIVER(AUDIO_VSB, symphony, default, default);
  /*lint +e123 */
  MT_ASSERT(ret == SUCCESS);
#endif

  p_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_AUDIO);
  MT_ASSERT(NULL != p_dev);

  //aud_cfg.p_drvsvc = p_public_drvsvc;
  aud_cfg.stack_size = 4 * KBYTES;
  aud_cfg.task_prio = DRV_HDMI_TASK_PRIORITY;
  aud_cfg.lock_mode = OS_MUTEX_LOCK;
#ifndef FAST_PLAY
  //aud_cfg.no_need_init = 0;
#else
  aud_cfg.no_need_init = 1;
#endif
  aud_cfg.p_drvsvc = (void *)g_p_public_drvsvc;
  ret = dev_open(p_dev, &aud_cfg);
  MT_ASSERT(SUCCESS == ret);

#ifndef FAST_PLAY
  aud_stop_vsb(p_dev);
  aud_get_buf_requirement_vsb(p_dev, &size, &align);
  MT_ASSERT(g_audio_fw_cfg_size >= size);
  aud_set_buf_vsb(p_dev, g_audio_fw_cfg_addr, size);
#endif

#ifndef WIN32
  /*board config */
  drv_mute_contrl();
#endif

  ui_receiver_mixed_ad_init();
  return SUCCESS;
}
static RET_CODE drv_video_init(void)
{
  RET_CODE ret;
  vdec_cfg_t vdec_cfg = {0};
  void *p_video = NULL;
  u32 size = 0;
  u32 align = 0;
  vdec_buf_policy_t policy = VDEC_OPENDI_64M;
  /*lint -e123 */
#ifndef WIN32
  extern s32 symphony_vdec_attach(char *);

  ret = symphony_vdec_attach("vdec_concerto");
  /*lint +e123 */
  MT_ASSERT(ret == SUCCESS);
#else
  /*lint -e123 */
  ret = ATTACH_DRIVER(VDEC, symphony, default, default);
  /*lint +e123 */
  MT_ASSERT(ret == SUCCESS);
#endif

  p_video = (void *)dev_find_identifier(NULL
   , DEV_IDT_TYPE, SYS_DEV_TYPE_VDEC_VSB);
  MT_ASSERT(NULL != p_video);

  vdec_cfg.is_autotest = FALSE;
  ret = dev_open(p_video, &vdec_cfg);
  MT_ASSERT(SUCCESS == ret);
  app_printf(PRINTF_DEBUG_LEVEL,"Init Vdec success\n");

  if(g_customer.drv_init_cfg.uboot_show == 0)
  {
    vdec_stop(p_video);

    if(g_customer.drv_init_cfg.vdec_policy == CONFIG_NOT_DEFINED)
    {
      policy = VDEC_BUFFER_AD_UNUSEPRESCALE;
    }
    else
    {
      policy = g_customer.drv_init_cfg.vdec_policy;
    }
    vdec_get_buf_requirement(p_video, policy, &size, &align);
    APPLOGI("policy %d vdec buffer size is : 0x%x\n",policy,size);
    //MT_ASSERT(g_video_fw_cfg_size >= size);
    APPLOGI("g_video_fw_cfg_addr :0x%x\n",g_video_fw_cfg_addr);
    vdec_set_buf(p_video, policy, g_video_fw_cfg_addr);

#ifndef WIN32
    vdec_do_avsync_cmd(p_video,AVSYNC_NO_PAUSE_SYNC_CMD,0);
#endif
  }

  if (g_customer.drv_init_cfg.vdec_follow_spec != CONFIG_NOT_DEFINED &&
    g_customer.drv_init_cfg.vdec_error_percent_drop != CONFIG_NOT_DEFINED)
  {
    vdec_set_error_conceal_cfg(p_video, g_customer.drv_init_cfg.vdec_follow_spec, g_customer.drv_init_cfg.vdec_error_percent_drop);
  }
  
  return ret;
}
static RET_CODE drv_dmx_init(void)
{
  RET_CODE ret;
  void *p_pti0 = NULL;
  dmx_config_t dmx_cfg = {0};
  /*************PTI0 attatch************/
#ifndef WIN32
  extern RET_CODE dmx_symphony_attach(char *p_name);

  ret = dmx_symphony_attach("symphony_pti");
#else
  ret = ATTACH_DRIVER(DMX, symphony, default, default);
  MT_ASSERT(ret == SUCCESS);
#endif
  p_pti0 = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_PTI);;
  MT_ASSERT(NULL != p_pti0);
#ifdef AV_SYNC_FLAG_ON
  dmx_cfg.av_sync = TRUE;
#else
  dmx_cfg.av_sync = FALSE;
#endif

  dmx_cfg.input_port_used[0] = TRUE;
  dmx_cfg.ts_input_cfg[0].input_way = FALSE;
  dmx_cfg.ts_input_cfg[0].local_sel_edge = TRUE;
  dmx_cfg.ts_input_cfg[0].error_indicator = FALSE;
  dmx_cfg.ts_input_cfg[0].start_byte_mask = TRUE;

  dmx_cfg.input_port_used[1] = TRUE;
  //dmx_cfg.ts_input_cfg[1].input_way = FALSE;
  dmx_cfg.ts_input_cfg[1].input_way = TRUE;
  dmx_cfg.ts_input_cfg[1].local_sel_edge = TRUE;
  dmx_cfg.ts_input_cfg[1].error_indicator = FALSE;
  //dmx_cfg.ts_input_cfg[1].start_byte_mask = TRUE;
  dmx_cfg.ts_input_cfg[1].start_byte_mask = FALSE;
  dmx_cfg.input_port_used[2] = TRUE;
  //dmx_cfg.ts_input_cfg[2].input_way = FALSE;
  dmx_cfg.ts_input_cfg[2].input_way = TRUE;
  dmx_cfg.ts_input_cfg[2].local_sel_edge = TRUE;
  dmx_cfg.ts_input_cfg[2].error_indicator = FALSE;
  //dmx_cfg.ts_input_cfg[2].start_byte_mask = TRUE;
  dmx_cfg.ts_input_cfg[2].start_byte_mask = FALSE;
  dmx_cfg.input_port_used[3] = TRUE;
  dmx_cfg.ts_input_cfg[3].input_way = FALSE;
  dmx_cfg.ts_input_cfg[3].local_sel_edge = TRUE;
  dmx_cfg.ts_input_cfg[3].error_indicator = FALSE;
  dmx_cfg.ts_input_cfg[3].start_byte_mask = TRUE;

  dmx_cfg.video_pes_parse_config.data_buf_size = g_video_pes_buffer_size;
  dmx_cfg.video_pes_parse_config.data_buf_staddr = g_video_pes_buffer_addr;
  dmx_cfg.audio_pes_parse_config.data_buf_size = g_audio_pes_buffer_size;
  dmx_cfg.audio_pes_parse_config.data_buf_staddr = g_audio_pes_buffer_addr;
  ret = dev_open(p_pti0, &dmx_cfg);
  MT_ASSERT(SUCCESS == ret);
  
  return SUCCESS;
}
static RET_CODE drv_display_init(void)
{
  void *p_disp = NULL;
  disp_cfg_t disp_cfg = {0};
  layer_cfg_t osd0_vs_cfg = {0};
  layer_cfg_t osd1_vs_cfg = {0};
  layer_cfg_t sub_vs_cfg = {0};
  layer_cfg_t osd0_cfg = {0};
  layer_cfg_t osd1_cfg = {0};
  layer_cfg_t sub_cfg = {0};
#ifndef DTMB_PROJECT
  rect_size_t disp_rect;
#endif
  RET_CODE ret;

  //display driver attach
  /*lint -e123 */
  ret = ATTACH_DRIVER(DISP, concerto, default, default);
  /*lint +e123 */
  MT_ASSERT(ret == SUCCESS);
  p_disp = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_DISPLAY);
  MT_ASSERT(NULL != p_disp);
  //adjust these in future

  OS_PRINTF("\ndisp_concerto_attach ok\n");

  if (g_customer.enable_hdcp)
  {
    disp_cfg.b_hdcp_on = TRUE;
  }
  disp_cfg.b_osd_vscle = TRUE;
  disp_cfg.b_osd_hscle = TRUE;
  disp_cfg.b_di_en = TRUE;
  disp_cfg.b_vscale = TRUE;
  if(g_customer.drv_init_cfg.b_wrback_422 == 1)
   disp_cfg.b_wrback_422 = TRUE;
  else
   disp_cfg.b_wrback_422 = FALSE;

  disp_cfg.b_unuse_prescale = FALSE;
  disp_cfg.misc_buf_cfg.sd_wb_addr = g_vid_sd_wr_back_addr;
  disp_cfg.misc_buf_cfg.sd_wb_size = g_vid_sd_wr_back_size;
  disp_cfg.misc_buf_cfg.sd_wb_field_no = g_customer.mem_cfg.vid_sd_wr_back_field_no;
  disp_cfg.av_ap_shared_mem = g_ap_av_share_mem_addr;
  disp_cfg.shared_mem_size = g_ap_av_share_mem_size;
  disp_cfg.coeff_table_mem = g_vid_coeff_table_addr;
  disp_cfg.coeff_table_size = g_vid_coeff_table_size;
  disp_cfg.b_set_ar_to_hdmi = g_customer.enable_set_ar_to_hdmi;

/************ osd layer memory config *********************/
  disp_cfg.p_sub_cfg = &sub_cfg;
  disp_cfg.p_osd0_cfg = &osd0_cfg;
  disp_cfg.p_osd1_cfg = &osd1_cfg;
  disp_cfg.p_sub_vscale_cfg = &sub_vs_cfg;
  disp_cfg.p_osd0_vscale_cfg = &osd0_vs_cfg;
  disp_cfg.p_osd1_vscale_cfg = &osd1_vs_cfg;

  #ifndef WIN32
  sub_cfg.odd_mem_start = g_sub_buffer_addr;
  sub_cfg.odd_mem_end = g_sub_buffer_addr + g_sub_buffer_size;
  
  osd0_cfg.odd_mem_start = g_osd0_buffer_addr;
  osd0_cfg.odd_mem_end = g_osd0_buffer_addr + g_osd0_buffer_size;
  
  osd1_cfg.odd_mem_start = g_osd1_buffer_addr;
  osd1_cfg.odd_mem_end = g_osd1_buffer_addr + g_osd1_buffer_size;
  
  if(TRUE == disp_cfg.b_vscale)
  {
    if(g_sub_vscaler_buffer_size > 0)
    {
      sub_vs_cfg.odd_mem_start = g_sub_vscaler_buffer_addr;
      sub_vs_cfg.odd_mem_end = g_sub_vscaler_buffer_addr + g_sub_vscaler_buffer_size;
      disp_cfg.b_vscale_sub = TRUE;
    }
    if(g_osd0_vscaler_buffer_size > 0)
    {
      osd0_vs_cfg.odd_mem_start = g_osd0_vscaler_buffer_addr;
      osd0_vs_cfg.odd_mem_end = g_osd0_vscaler_buffer_addr + g_osd0_vscaler_buffer_size;
      disp_cfg.b_vscale_osd0 = TRUE;
    }
    if(g_osd1_vscaler_buffer_size > 0)
    {
      osd1_vs_cfg.odd_mem_start = g_osd1_vscaler_buffer_addr;
      osd1_vs_cfg.odd_mem_end = g_osd1_vscaler_buffer_addr + g_osd1_vscaler_buffer_size;
      disp_cfg.b_vscale_osd1 = TRUE;
    }
  }
  #else
  sub_cfg.odd_mem_start = g_sub_buffer_addr & (~0xF0000000);
  sub_cfg.odd_mem_end = sub_cfg.odd_mem_start + g_sub_buffer_size / 2;
  sub_cfg.even_mem_start = sub_cfg.odd_mem_start + g_sub_buffer_size / 2;
  sub_cfg.even_mem_end = sub_cfg.odd_mem_start + g_sub_buffer_size;

  osd0_cfg.odd_mem_start = g_osd0_buffer_addr & (~0xF0000000);
  osd0_cfg.odd_mem_end = osd0_cfg.odd_mem_start + g_osd0_buffer_size / 2;
  osd0_cfg.even_mem_start = osd0_cfg.odd_mem_start + g_osd0_buffer_size / 2;
  osd0_cfg.even_mem_end = osd0_cfg.odd_mem_start + g_osd0_buffer_size;

  osd1_cfg.odd_mem_start = g_osd1_buffer_addr & (~0xF0000000);
  osd1_cfg.odd_mem_end = osd1_cfg.odd_mem_start + g_osd1_buffer_size / 2;
  osd1_cfg.even_mem_start = osd1_cfg.odd_mem_start + g_osd1_buffer_size / 2;
  osd1_cfg.even_mem_end = osd1_cfg.odd_mem_start + g_osd1_buffer_size;
  #endif
  disp_cfg.stack_size = DISP_HDMI_NOTIFY_TASK_STK_SIZE;
  disp_cfg.task_prio = DISP_HDMI_NOTIFY_TASK_PRIORITY;
  disp_cfg.lock_type = OS_MUTEX_LOCK;

  app_printf(PRINTF_DEBUG_LEVEL,"\ndisp_open begin\n");
  if(g_customer.drv_init_cfg.uboot_show == 0)
    disp_cfg.b_uboot_uninit = TRUE;
  else
    disp_cfg.b_uboot_uninit = FALSE;
  ret = dev_open(p_disp, &disp_cfg);
  MT_ASSERT(SUCCESS == ret);

  app_printf(PRINTF_DEBUG_LEVEL,"\ndisp_open ok\n");
  //sys_status_reload_environment_setting(FALSE);
  if(g_customer.drv_init_cfg.uboot_show == 0)
    disp_mode_config(p_disp);
  //minnan add cfg vdac

#ifndef WIN32

  disp_vdac_config(p_disp);
#ifndef DTMB_PROJECT
  disp_rect.w = g_customer.graphic_w;
  disp_rect.h = g_customer.graphic_h;
  disp_set_graphic_size(p_disp,&disp_rect);
  OS_PRINTF("graphic size: w %d h %d\n",disp_rect.w,disp_rect.h);
#endif
#endif

  return ret;
}
static RET_CODE drv_low_power_manager_init(void)
{
  RET_CODE ret = SUCCESS;
#ifndef WIN32
  void *p_dev_lpm = NULL;
  lpower_cfg_t lpm_cfg = {0};
  u8 led_map[8] = {0, 1, 2, 3, 4, 5, 6, 7};
  enum fp_type fptype;
  extern RET_CODE lpower_sysmphony_attach(char *p_name);

  memset(&lpm_cfg, 0, sizeof(lpm_cfg));
  ret = lpower_sysmphony_attach("standby_aomcu");
  p_dev_lpm = dev_find_identifier(NULL, DEV_IDT_NAME, (u32)"standby_aomcu");
  MT_ASSERT(ret == SUCCESS);

  switch (g_fp_cfg.fp_type)
  {
    case HAL_FD650:
      fptype = FD650;
      break;
    case HAL_CT1642:
      fptype = CT1642;
      break;
    case HAL_GPIO:
      fptype = FP_NO;
      break;
    case HAL_SSD1311:
      fptype = FP_SSD1311;
      break;
    default:
      fptype = FD650;
      break;      
  }
  lpm_cfg.standby_cfg.type = fptype;
  /* get FP POWER key from DM */ 
  dm_read(class_get_handle_by_id(DM_CLASS_ID), 
          FPKEY_BLOCK_ID, 0, 0, 
          sizeof(u8), 
          (u8*)&lpm_cfg.standby_cfg.pd_key_fp);
  lpm_cfg.standby_cfg.p_raw_map = led_map;

  ret = dev_open(p_dev_lpm, &lpm_cfg);
  MT_ASSERT(SUCCESS == ret);

  dev_io_ctrl(p_dev_lpm, SET_STANDBY_CONFIG , 0); //gpen拉低,关外围电路

#endif

  return ret;
}

static RET_CODE drv_network_init()
{
  RET_CODE ret = SUCCESS;

  network_set_box_hw_info();
  
  drv_network_eth_init();
  
  drv_network_usb_eth_init();

  drv_network_8188_init();
  
  drv_network_5370_init();

  drv_network_7601_init();

  drv_network_7000_init();

  network_mount_ramfs();

  network_ppp_init();

  network_register_rtmp();
  return ret;
}


static RET_CODE drv_sctrl_init()
{
  RET_CODE ret = SUCCESS;
#ifndef WIN32
  hal_secure_DriverAttach();
#endif
  return ret;
}

static void show_startup_iFram()
{
  RET_CODE ret = SUCCESS;
  u8 *addr = NULL;

  void * p_video_dev = dev_find_identifier(NULL, DEV_IDT_TYPE,SYS_DEV_TYPE_VDEC_VSB);
  void * p_disp_dev = dev_find_identifier(NULL, DEV_IDT_TYPE,SYS_DEV_TYPE_DISPLAY);
  u32 size = get_dm_block_real_file_size(START_LOGO_BLOCK_ID);
  
  #ifdef WIN32
   return ;
  #endif
  
  addr = (u8*)mtos_malloc(size);
  MT_ASSERT(addr != NULL);
  dm_read(class_get_handle_by_id(DM_CLASS_ID),START_LOGO_BLOCK_ID, 0, 0, size, addr);

  dmx_av_reset(dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_PTI));

  disp_layer_show(p_disp_dev, DISP_LAYER_ID_VIDEO_SD, FALSE);
  disp_layer_show(p_disp_dev, DISP_LAYER_ID_VIDEO_HD, FALSE);

  ret = vdec_start(p_video_dev, VIDEO_MPEG_ES, VID_UNBLANK_USER);
  ret = vdec_dec_one_frame(p_video_dev, (u8 *)addr, size);
  MT_ASSERT(SUCCESS == ret);

  disp_layer_show(p_disp_dev, DISP_LAYER_ID_VIDEO_SD, TRUE);
  disp_layer_show(p_disp_dev, DISP_LAYER_ID_VIDEO_HD, TRUE);

  if(addr != NULL)
    mtos_free(addr);
}


static RET_CODE copy_right_asserted(void)
{
#ifdef COPY_RIGHT_ASSERT
  *((volatile u32 *)0xbf450000) = 0x10000001;
  *((volatile u32 *)0xbf45001c) = 0x230;
  *((volatile u32 *)0xbf450004) = 0x10000037;
#endif

  return SUCCESS;
}

#ifdef WIN32
static void* my_malloc(u32 size)
{
  mem_mgr_alloc_param_t para = {0};
  para.id = MEM_SYS_PARTITION;
  para.size = size;
  return MEM_ALLOC(&para);
  //return malloc(size);
}

static void my_free(void *p_addr)
{
  mem_mgr_free_param_t para = {0};
  para.id = MEM_SYS_PARTITION;
  para.p_addr = p_addr;
  FREE_MEM(&para);
  //free(p_addr);
}

static RET_CODE drv_sys_init(void)
{
  BOOL ret = FALSE;
  u32 *pstack_pnt = NULL;
  mem_mgr_partition_param_t partition_param = { 0 };
  extern void Task_SysTimer(void *p_data);
  extern void ap_init(void);
  hal_config_t hal_config = {0};

  //load customer config first
  memcpy(&g_customer, get_customer_config_data(), sizeof(g_customer));

  //win32 disable fast_dvb_play
  g_customer.fast_dvb_play = 0; 
  g_customer.multi_ecm_cas = 0;

  hal_win32_attach(&hal_config);
  
#ifndef DTMB_PROJECT
  mem_map_cfg();
#else
  mem_map_cfg_dtmb_64m();
#endif
  //init memory manager, to the memory end
  mem_mgr_init((u8*)get_mem_addr(), SYS_MEMORY_END);

  /* create SYS partition */
  partition_param.id   = MEM_SYS_PARTITION;
  partition_param.size = 30 * 1024 * 1024;
  partition_param.p_addr = (u8*)get_mem_addr();
  partition_param.atom_size = SYS_PARTITION_ATOM;
  partition_param.user_id = SYS_MODULE_SYSTEM;
  partition_param.method_id = MEM_METHOD_NORMAL;
  ret = MEM_CREATE_PARTITION(&partition_param);
  MT_ASSERT(FALSE != ret);
  OS_PRINTF("create patition ok!\n");

  //register malloc/free function, using system partition
  mtos_mem_init(my_malloc, my_free);
  OS_PRINTF("init mem ok!\n");

  mtos_ticks_init(SYS_CPU_CLOCK);

  //init message queue
  ret = mtos_message_init();
  MT_ASSERT(FALSE != ret);

#ifdef CORE_DUMP_DEBUG
  //start statistic task, MUST BE in the first task!
  mtos_stat_init();
#endif

  //create timer task
  pstack_pnt = (u32*)mtos_malloc(SYS_TIMER_TASK_STKSIZE);
  MT_ASSERT(pstack_pnt != NULL);

  ret = mtos_task_create((u8 *)"Timer",
    Task_SysTimer,
    (void *)0,
    SYS_TIMER_TASK_PRIORITY,
    pstack_pnt,
    SYS_TIMER_TASK_STKSIZE);

  MT_ASSERT(FALSE != ret);
  OS_PRINTF("create timer task ok!\n");

  hook_attach();
  
  return SUCCESS;
}
#else
static RET_CODE drv_sys_init(void)
{
  extern u32 _start;
  extern u32 _end;
  u32 text_start = (u32)(&_start);
  u32 bss_end = (u32)(&_end);
  BOOL ret = FALSE;
  u32 *pstack_pnt = NULL;
  u32 heap_start = 0;
  u32 cpuclk = 0;
  u32 os_version = 0;
  u32 sys_partition_size = 0;
  extern u32 g_gui_resource_buffer_addr;
  extern void hal_symphony_attach(void);

  //load customer config first
  memcpy(&g_customer, get_customer_config_data(), sizeof(g_customer));

  heap_start = ROUNDUP(bss_end,4);
  mtos_irq_init();

  hal_symphony_attach( );

  mtos_register_putchar(uart_write_byte);
  mtos_register_getchar(uart_read_byte);

  hal_module_clk_get(HAL_CPU0, &cpuclk);
  mtos_ticks_init(cpuclk);

  gpio_mux();

  uart_init(0);
  uart_init(1);
  uart_set_param(0, 115200, 8, 1, 0);
  uart_set_param(1, 115200, 8, 1, 0);

  //spiflash_cfg();

  APPLOGA("\n MaincodeTaskStart\n");
  APPLOGA("\n Built at %s \n", buildSTR);
  APPLOGA("\n Chip ID is [0x%08x]\n",hal_get_chip_ic_id());
  APPLOGA("\n Chip version is [0x%08x]\n",hal_get_chip_rev());
  mem_map_cfg();
  APPLOGA("\n <====================== Memory configure ======================> \n");
  APPLOGA("\n Totle Memory : [0x%x]\n",((g_customer.mem_cfg.mem_top_addr + 0x200000) & 0xFFFFFFF));
  APPLOGA("\n Static Memory : \n");
  APPLOGI(" vdec_policy [%d]\n mem_top_addr [0x%x]\n av_stack_size [0x%x]\n"
  	" video_fw_cfg_size [0x%x]\n vid_di_cfg_size [0x%x]\n vbi_buf_size [0x%x]\n"
  	" audio_fw_cfg_size [0x%x]\n ap_av_share_mem_size [0x%x]\n vid_sd_wr_back_size [0x%x]\n"
  	" vid_sd_wr_back_field_no [0x%x]\n epg_buffer_size [0x%x]\n rec_buffer_size [0x%x]\n"
  	" play_buffer_size [0x%x]\n subosd_buffer_size [0x%x]\n subosd_double_buffer_size [0x%x]\n"
  	" osd1_buffer_size [0x%x]\n osd1_double_buffer_size [0x%x]\n vid_coeff_table_size [0x%x]\n"
  	" osd0_buffer_size [0x%x]\n osd0_double_buffer_size [0x%x] \n video_pes_buffer_size  [0x%x] \n audio_pes_buffer_size [0x%x] \n",
  	g_customer.drv_init_cfg.vdec_policy,g_customer.mem_cfg.mem_top_addr,g_customer.mem_cfg.av_stack_size,
  	g_customer.mem_cfg.video_fw_cfg_size,g_customer.mem_cfg.vid_di_cfg_size,g_customer.mem_cfg.vbi_buf_size,
  	g_customer.mem_cfg.audio_fw_cfg_size,g_customer.mem_cfg.ap_av_share_mem_size,g_customer.mem_cfg.vid_sd_wr_back_size,
  	g_customer.mem_cfg.vid_sd_wr_back_field_no,g_customer.mem_cfg.epg_buffer_size,g_customer.mem_cfg.rec_buffer_size,
  	g_customer.mem_cfg.play_buffer_size,g_customer.mem_cfg.subosd_buffer_size,g_customer.mem_cfg.subosd_double_buffer_size,
  	g_customer.mem_cfg.osd1_buffer_size,g_customer.mem_cfg.osd1_double_buffer_size,g_customer.mem_cfg.vid_coeff_table_size,
  	g_customer.mem_cfg.osd0_buffer_size,g_customer.mem_cfg.osd0_double_buffer_size,g_customer.mem_cfg.video_pes_buffer_size,
  	g_customer.mem_cfg.audio_pes_buffer_size);
  
  APPLOGA("\n Heap Memory : \n");
  APPLOGA("\n App start [0x%x] App size [0x%x]\n",text_start,heap_start - text_start);
  sys_partition_size = (((g_gui_resource_buffer_addr & (~0xa0000000)) - (heap_start & (~0xa0000000))) / 0x400) * 0x400;
  APPLOGA("\n Heap_start [0x%x] Heap size [0x%x]\n",heap_start,sys_partition_size);

  APPLOGA("\n <====================== Memory configure ======================> \n");
  
 #ifdef SHOW_MEM_SUPPORT
  show_single_memory_mapping(CODE_SIZE_FLAG,0,heap_start);
  show_single_memory_mapping(SYSTEM_PARTITION_FLAG,heap_start,sys_partition_size);
  #endif

  
  mem_mgr_init((u8 *)heap_start,
  sys_partition_size);

  dlmem_init((void *)heap_start,
  	sys_partition_size);
  mtos_mem_init(dl_malloc, dl_free);
  app_printf(PRINTF_INFO_LEVEL,"Init mem ok!\n");

/*lint -e746*/
  os_version = get_os_version();
/*lint +e746*/

  if (os_version != 0x11112222) {
    app_printf(PRINTF_ERROR_LEVEL,"Wrong os version, please talk with os person!\n");

  }

  app_printf(PRINTF_INFO_LEVEL,"Os version pass!\n");

//#ifdef ENABLE_NETWORK
//  mt_hal_invoke_constructors(); /* just for c++ init,  if no cpp, not needed*/
//#endif
  invoke_constructors();

//enable interrupt
  mtos_irq_enable(TRUE);
  app_printf(PRINTF_DEBUG_LEVEL,"Enable irq!\n");

  ret = mtos_message_init();
  MT_ASSERT(FALSE != ret);


#ifdef CORE_DUMP_DEBUG
  //start statistic task, MUST BE in the first task!
  mtos_stat_init();
#endif

  //create timer task
  pstack_pnt = (u32*)mtos_malloc(SYS_TIMER_TASK_STKSIZE);
  MT_ASSERT(pstack_pnt != NULL);

  ret = mtos_task_create((u8 *)"Timer",
         Task_SysTimer,
         (void *)0,
         SYS_TIMER_TASK_PRIORITY,
         pstack_pnt,
         SYS_TIMER_TASK_STKSIZE);

  MT_ASSERT(FALSE != ret);

  hook_attach();
  
  return SUCCESS;
}
#endif

static RET_CODE drv_firmware_init(void)
{
#ifndef WIN32
  extern u32 attach_ipcfw_fun_set_concerto(ipc_fw_fun_set_t * p_funset);

  hal_dcache_invalidate((void *)&g_ipcfw_f,sizeof(ipc_fw_fun_set_t));
  attach_ipcfw_fun_set_concerto(&g_ipcfw_f);

  ap_ipc_init(32);   // ap ipc fifo create
#endif

  return SUCCESS;
}

static RET_CODE drv_mem_init(void)
{
  mem_cfg(MEMCFG_T_NORMAL);

  return SUCCESS;
}

static RET_CODE drv_otp_init(void)
{
#ifndef WIN32
  extern void ecpu_ipc_init(void);

  ecpu_ipc_init();

  hal_otp_init();
#endif

  return SUCCESS;
}
    
static RET_CODE drv_dma_init(void)
{
  RET_CODE ret;
  ret = hal_dma_init();
  MT_ASSERT(ret == SUCCESS);

  return ret;
}

static RET_CODE drv_svc_init(void)
{
  g_p_public_drvsvc = drv_public_svr_init();

  MT_ASSERT(g_p_public_drvsvc != NULL);

  return SUCCESS;
}

static RET_CODE drv_hw_cfg_init(void)
{
#ifdef HW_PIN_STANDARD_CFG
  check_hw_cfg_pin();
#endif

  return SUCCESS;
}

#if 0
static RET_CODE drv_lock_led_init(void)
{
#ifndef WIN32
  config_lock_len_pin();
#endif

  return SUCCESS;
}
#endif

static RET_CODE drv_show_logo(void)
{
  /*show iFrame*/
  if(g_customer.drv_init_cfg.uboot_show == 0)
  {
#ifdef TEMP_SUPPORT_TF_AD
    if(SUCCESS != ui_adv_tf_show_welcome())
    {
      show_startup_iFram( );  
    }
#else
  {
#ifdef FAST_STANDBY
      BOOL is_standby = FALSE;
      sys_status_get_status(BS_IS_STANDBY, &is_standby);
      if(is_standby == FALSE)
      {
        show_startup_iFram();
      }
#else
    show_startup_iFram( );
#endif
    }
#endif
  }

  app_ota_check_jump_policy();

  return SUCCESS;
}

static RET_CODE drv_board_cfg(void)
{
#ifndef WIN32
  set_board_config();
#endif

  return SUCCESS;
}

static RET_CODE _sys_status_init(void)
{
  db_dvbs_init();
  sys_status_init();
  
  return SUCCESS;
}


static RET_CODE drv_ufs_init(void)
{
#ifdef WIN32
  s32_block_int();
#else
#ifdef ENABLE_USB_CONFIG
  block_driver_attach();
  ufs_dev_init();   //register callback for block layer
#endif
#endif

  return SUCCESS;
}

static RET_CODE customer_reg_gpio_init(void)
{
#ifndef WIN32
  u8 i;
  u32 temp;

  for (i=0; i<g_customer.reg_cfg.num; i++)
  {
    temp = hal_get_u32((volatile unsigned long *)g_customer.reg_cfg.data[i].addr);
    temp &= ~(g_customer.reg_cfg.data[i].mask);
    temp |= g_customer.reg_cfg.data[i].data;
    OS_PRINTF("Set reg %08x = 0x%08x\n", g_customer.reg_cfg.data[i].addr, temp);
    hal_put_u32((volatile unsigned long *)g_customer.reg_cfg.data[i].addr, temp);
  }

  for (i=0; i<g_customer.gpio_cfg.num; i++)
  {
    gpio_io_enable(g_customer.gpio_cfg.data[i].gpio_pin, TRUE);
    gpio_set_dir(g_customer.gpio_cfg.data[i].gpio_pin, GPIO_DIR_OUTPUT);
    OS_PRINTF("Set gpio port %u = 0x%x\n", g_customer.gpio_cfg.data[i].gpio_pin, g_customer.gpio_cfg.data[i].value);
    gpio_set_value(g_customer.gpio_cfg.data[i].gpio_pin, g_customer.gpio_cfg.data[i].value);
  }

  if ((g_customer.nim_type == NIM_DM6K_T2)
    ||(g_customer.nim_type == NIM_DM6K_T2_S2)
    || (g_customer.nim_type == NIM_DM6K_C_T2_S2)
    ||(g_customer.nim_type == NIM_DM6K_C_T2))
  {
    u8 gpio_level = 0;
    
    //init dvbt antenna power pin
    scan_param_t *p_scan_param = sys_status_get_scan_param();

    mtos_printk("[%s, %d] lnb pin is %d\n", __FUNCTION__, __LINE__, g_customer.dvbt_lnb_gpio_port);
    gpio_io_enable(g_customer.dvbt_lnb_gpio_port, TRUE);
    gpio_set_dir(g_customer.dvbt_lnb_gpio_port, GPIO_DIR_OUTPUT);

    if(p_scan_param->antenna_state)
    {
      gpio_level = GPIO_LEVEL_LOW;//lnb on
      mtos_printk("[%s, %d] lnb on\n", __FUNCTION__, __LINE__);
    }
    else
    {
      gpio_level = GPIO_LEVEL_HIGH;//lnb off
      mtos_printk("[%s, %d] lnb off\n", __FUNCTION__, __LINE__);
    }
    gpio_set_value(g_customer.dvbt_lnb_gpio_port, gpio_level);
  }
#endif

  return SUCCESS;
}

static RET_CODE load_customer_cfg(void)
{
  u8 *p_cuscfg_img;
  u32 img_size = 16*KBYTES;
  BOOL overload = FALSE;

  p_cuscfg_img = (u8*)mtos_malloc(img_size);
  if (p_cuscfg_img == NULL)
    return ERR_NO_MEM;
  
  img_size = dm_read(class_get_handle_by_id(DM_CLASS_ID),
                          CUSTOMER_BLOCK_ID, 0, 0,
                          img_size,
                          (u8 *)p_cuscfg_img);
  if (p_cuscfg_img != NULL && img_size > 0)
  {
    u32 data_addr = 0, data_size = 0;
    
    BOOL valid_img = ui_image_verify((u32)p_cuscfg_img, &data_addr, &data_size);

    OS_PRINTF("%s customer config image!\n", valid_img ? "Valid" : "Invalid");
    if (valid_img && data_addr != 0 && data_size > 0)
    {
      /* make data as string */
      char *p_xml = (char*)mtos_malloc(data_size + 1);
      if(p_xml != NULL)
      {
        customer_cfg_t *p_cfg_from_flash = (customer_cfg_t*)mtos_malloc(sizeof(customer_cfg_t));
        if (p_cfg_from_flash != NULL)
        {
          extern RET_CODE parse_customer_config(const char *p_cfg_data, customer_cfg_t *p_cfg);

          memcpy(p_xml, (void*)data_addr, data_size);
          p_xml[data_size] = 0;

          parse_customer_config(p_xml, p_cfg_from_flash);
          /* keep items those cannot be overloaded */
          p_cfg_from_flash->dm_booter_start_addr = g_customer.dm_booter_start_addr;
          p_cfg_from_flash->dm_hdr_start_addr    = g_customer.dm_hdr_start_addr;
          p_cfg_from_flash->mem_cfg              = g_customer.mem_cfg;
#ifndef WIN32
          /* overload the g_customer */
          g_customer = *p_cfg_from_flash;
          overload = TRUE;
#endif          
          mtos_free(p_cfg_from_flash);
        }

        mtos_free(p_xml);
      }
    }
  }

  mtos_free(p_cuscfg_img);

  OS_PRINTF("Customer config using : [%s] \n", overload ? "Flash Block" : "Built-in");

  return SUCCESS;
}

static core_init_module_t symphony_ucos_drv_modules[] = 
{
  {"SYSTEM",    drv_sys_init},
  {"FIRMWARE",  drv_firmware_init},
  {"MEM CFG",   drv_mem_init},
  {"DM",        drv_dm_init},
  {"CUS CFG",   load_customer_cfg},
  {"OTP",       drv_otp_init},
  {"DMA",       drv_dma_init},
  {"DRVSVC",    drv_svc_init},
  {"HW CFG",    drv_hw_cfg_init},
  //{"LOCK LED",  drv_lock_led_init},
  {"SECURE",    drv_sctrl_init},
  {"I2C",       drv_i2c_init},
  {"HDMI",      drv_hdmi_init},
  {"BOARD CFG", drv_board_cfg},
  {"UIO",       drv_uio_init},
  {"DEMUX",     drv_dmx_init},
  {"VIDEO",     drv_video_init},
  {"NIM",       drv_nim_init},
  {"SYS STATUS",_sys_status_init},
  {"DISPLAY",   drv_display_init},
  {"SHOW LOGO", drv_show_logo},
  {"AUDIO",     drv_audio_init},
  {"GPE",       drv_gpe_init},
  {"JPEG",      drv_jpeg_init},
  //{"CHECK SN",  _check_sn},
  {"SMC",       drv_smc_init},
  {"VBI",       drv_vbi_init},
  {"LPM",       drv_low_power_manager_init},
  {"NETWORK",   drv_network_init},
  {"UFS",       drv_ufs_init},
  {"COPYRIGHT", copy_right_asserted},
  {"REG GPIO",   customer_reg_gpio_init},
};

static RET_CODE symphony_ucos_app_service_init(void)
{
  return service_init();
}

static core_init_module_t symphony_ucos_app_modules[] = 
{
  {"service",         symphony_ucos_app_service_init},
};

static RET_CODE symphony_ucos_pre_os_init(void)
{
#ifndef WIN32
  extern void EXCEP_vInstallIntInRam(void);

  EXCEP_vInstallIntInRam();
#endif

  return SUCCESS;
}

static RET_CODE symphony_ucos_os_init(void)
{
  mtos_cfg_t os_cfg = {0};

  /* Initialize uC/OS-II  */
#ifdef WIN32
  os_cfg.enable_bhr = FALSE;
#endif
  mtos_os_init(&os_cfg);

  return SUCCESS;
}

static RET_CODE symphony_ucos_os_start(void)
{
#ifdef WIN32
  mtos_start();
#else
  OSStart(); 
#endif

  return SUCCESS;
}

static RET_CODE symphony_ucos_attach_modules(void)
{
  return attach_common_mdl_modules(&g_this_core);
}

core_init_context_t g_this_core = 
{
  symphony_ucos_pre_os_init,
  symphony_ucos_os_init,
  symphony_ucos_os_start,
  symphony_ucos_attach_modules,
  DRV_MOD_CNT,
  symphony_ucos_drv_modules,
  0,
  NULL,
  APP_MOD_CNT,
  symphony_ucos_app_modules,
};

/*
  temp fix for driver sdk gzip api
  */
int zinit (void *p_addr, unsigned long size)
{
  extern int z_zinit(void *p_addr, unsigned long size);

  return z_zinit(p_addr, size);
}

int zdeinit (void)
{
  extern int z_zdeinit (void);
    
  return z_zdeinit();
}






void display_on_oled_Channel_info( char *string,u8 line  )
{
	char final_str1[16]={0};
	SSD1311_T dis1;
	char i,j;
    memset(final_str1, ' ', 16);

	dis1.len = 16;
	dis1.disfp = line;

	if ( 1 == line )
	{
		for( i=0,j=2;j<=13;i=i+2,j++ )
		{
		    if(string[i])
              final_str1[j] = string[i];
		}

		mtos_printk( "%s> string [%s] line[%d]\n",__FUNCTION__,final_str1,line );
		
		uio_display( p_dev_oled, final_str1,  &dis1);
	}
	else
	{
		final_str1[6] = string[0];
		final_str1[7] = string[1];
		final_str1[8] = string[2];
		final_str1[9] = string[3];

		mtos_printk( "%s> string [%s] line[%d]\n",__FUNCTION__,final_str1,line );
		
		uio_display( p_dev_oled, final_str1,  &dis1);
	}

	return;	

}



void display_on_oled_string( char *string,u8 line  )
{
	SSD1311_T dis1;

	dis1.len = 16;
	dis1.disfp = line;

	uio_display( p_dev_oled, string,  &dis1);

	return;
}




