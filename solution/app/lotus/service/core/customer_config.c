/********************************************************************************************/
/* Copyright (c) 2014 Montage Technology Group Limited and its affiliated companies         */
/* Montage Proprietary and Confidential                                                     */
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/********************************************************************************************/
/****************************************************************************
 ****************************************************************************/
//system
#include "sys_types.h"
#include "sys_define.h"
#include "sys_cfg.h"
#include "mtos_printk.h"
#include "mtos_msg.h"
//util
#include "class_factory.h"

#include "drv_dev.h"
#include "charsto.h"
//mdl
#include "data_manager.h"
#include "mdl.h"

#include "uio.h"
//customer
#include "hal_misc.h"
#include "hal_gpio.h"
#include "customer_config.h"

cus_fp_cfg_t g_fp_cfg;
customer_cfg_t g_customer;

void customer_config_load_fp_cfg(void)
{
  u32 read_len = 0;

  read_len = dm_read(class_get_handle_by_id(DM_CLASS_ID),
                     FPCFG_BLOCK_ID, 0, 0,
                     sizeof(cus_fp_cfg_t),
                     (u8 *)&g_fp_cfg);

  MT_ASSERT (read_len == sizeof(g_fp_cfg));

  g_fp_cfg.fp_type = HAL_SSD1311;
  
}

nim_para_t *customer_config_get_default_ota_tp(void)
{
  nim_para_t *p_ret = NULL;
  
  switch (g_customer.default_ota_signal)
  {
    case SYS_DVBS:
      p_ret = &g_customer.ota_tp_s;
      break;
    case SYS_DVBT2:
      p_ret = &g_customer.ota_tp_t;
      break;
    default:
      p_ret = &g_customer.ota_tp_c;
      break;
  }

  return p_ret;
}

