/********************************************************************************************/
/* Copyright (c) 2014 Montage Technology Group Limited and its affiliated companies         */
/* Montage Proprietary and Confidential                                                     */
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/********************************************************************************************/
/****************************************************************************

 ****************************************************************************/
#include "ui_common.h"
#include "ads_ware.h"
#include "ui_ad_api.h"
#include "iconv_ext.h"
#include "ui_picture_api.h"
#include "sys_app.h"
#include "client_common.h"
#include "client_system.h"
#include "snf_protab_define.h"
#include "snf_protect_api.h"

typedef struct bmpfile_magic {
  unsigned char magic[2];
}BMP_MAGIC;
 
typedef struct bmpfile_header {
  u32 filesz;
  u16 reserve1;
  u16 reserve2;
  u32 bmp_offset;
}BMP_HEADER;

typedef struct dib_header{
  u32 header_sz;
  s32 width;
  s32 height;
  u16 nplanes;
  u16 bitspp;
  u32 compress_type;
  u32 bmp_bytesz;
  s32 hres;
  s32 vres;
  u32 ncolors;
  u32 nimpcolors;
}DIB_HEADER;

#define BMP_HEADER_SIZE (sizeof(BMP_MAGIC)+sizeof(BMP_HEADER))
#define DIB_HEADER_SIZE  (sizeof(DIB_HEADER))

#define AP_VOLUME_MAX 31
static void* p_sub_rgn = NULL;
static void* p_montage_ads_sub_rgn = NULL;
u8 com_num = 0;
static void *p_net_svc = NULL;

u8 g_led_buffer[10] = {0};
u8 g_led_index = 0;
u8 g_is_locked_flag = FALSE;
rect_t g_water_mark_rect = {0};
util_nim_cfg_t g_nim_cfg[4];
u8 g_tuner_num = 0;
static BOOL g_uio_enable = FALSE;
static u8 g_keep_hdcp[512];
static u8 g_keep_sn[255];
static u8 g_keep_macaddr[48];
static iconv_t g_cd_utf8_to_utf16le = NULL;
static iconv_t g_cd_utf16le_to_utf8 = NULL;
static iconv_t g_cd_gb2312_to_utf16le = NULL;
static iconv_t g_cd_iso8859_9_to_utf16le = NULL;

static u8 g_vol_step[AP_VOLUME_MAX + 1] = //fix bug 53750
{
  0,                  // 1
  8, 16, 23, 30, 35,  // 2~6
  40, 43, 46, 49, 52, // 7~11
  55, 58, 61, 64, 67, // 12~16
  70, 73, 76, 79, 82, // 17~21
  84, 86, 88, 90, 92, // 22~26
  94, 95, 96, 97, 98, // 27~31
  99                  // 32
};

void ui_enable_uio(BOOL is_enable)
{
  cmd_t cmd = {0};

  if(g_uio_enable && is_enable)
  {
  	 return;
  }
  else if(g_uio_enable == FALSE && is_enable == FALSE)
  {
  	 return;
  }
  
  if(is_enable == TRUE)
  	{
  	   g_uio_enable = TRUE;
  	}
	else
	{
	   g_uio_enable = FALSE;
	}
  cmd.id = (u8)is_enable ?
           AP_FRM_CMD_ACTIVATE_APP : AP_FRM_CMD_DEACTIVATE_APP;
  cmd.data1 = APP_UIO;
  cmd.data2 = 0;

  ap_frm_do_command(APP_FRAMEWORK, &cmd);
}
void ui_set_com_num(u8 num)
{
  com_num = num;
}
#if 1
static void _set_fp_display(char *content)
{
  void *p_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_UIO);

  return ;

  if(g_customer.customer_id == CUSTOMER_JIULIAN)
  {
    memcpy(g_led_buffer, content, 3);
    g_led_buffer[3] = '.';
    memcpy((u8 *)(g_led_buffer + 4),(u8 *)(content + 3),  1);
    
    if(g_led_buffer[com_num + 1] != 0)
    {
      //memcpy(&(g_led_buffer[g_led_index]), g_led_buffer + g_led_index,com_num - g_led_index);
      g_led_buffer[g_led_index] = '.';
      OS_PRINTF("1~~ led buffer :%s ,num :%d\n",g_led_buffer,com_num + 2);
      uio_display(p_dev, g_led_buffer, com_num + 2);
    }
    else
    {
      OS_PRINTF("0~~ led buffer :%s ,num :%d\n",g_led_buffer,com_num + 1);
      uio_display(p_dev, g_led_buffer, com_num + 1);
    }
  }
   /* for maike */
  else if (g_customer.customer_id == CUSTOMER_MAIKE)
  {
  	if (FALSE == g_is_locked_flag)
  	{
		memcpy(g_led_buffer, content, 3);
		g_led_buffer[3] = '.';
		memcpy((u8 *)(g_led_buffer + 4),(u8 *)(content + 3),  1);

		OS_PRINTF("_set_fp_display not locked  led buffer :%s ,num :%d\n", g_led_buffer, com_num + 1);
		uio_display(p_dev, g_led_buffer, com_num + 1);
  	}
	else if (TRUE == g_is_locked_flag)
	{
		g_led_buffer[0] = content[0];
		g_led_buffer[1] = '.';
		g_led_buffer[2] = content[1];
		g_led_buffer[3] = content[2];
		g_led_buffer[4] = '.';
		g_led_buffer[5] = content[3];

		OS_PRINTF("_set_fp_display  locked!  led buffer :%s ,num :%d\n", g_led_buffer, com_num + 2);
		uio_display(p_dev, g_led_buffer, com_num + 2);
	}
  }
  else
  {
    if(g_led_buffer[com_num] == 0)
    {
      memcpy(g_led_buffer,content,com_num);
      OS_PRINTF("0~~ led buffer :%s ,num :%d\n",g_led_buffer,com_num);
      uio_display(p_dev, g_led_buffer, com_num);
    }
    else
    {
      memcpy(g_led_buffer,content,g_led_index);
      g_led_buffer[g_led_index] = '.';
      memcpy(&(g_led_buffer[g_led_index + 1]),content + g_led_index,com_num - g_led_index);
      OS_PRINTF("1~~ led buffer :%s ,num :%d\n",g_led_buffer,com_num + 1);
      uio_display(p_dev, g_led_buffer, com_num + 1);
    }
  }
}
#endif

void ui_set_front_panel_by_str(const char * str)
{
  #ifndef WIN32
  char content[5];

  if (strlen(str) > 4)
  {
    memcpy(content, str, 4);
    content[4] = '\0';
  }
  else
  {
    if ((com_num == 4)||(com_num == 5))
    sprintf(content, "%4s", str);
   else 
    sprintf(content, "%3s ", str); 
   }

  _set_fp_display(content);
  #endif
}

#if defined(CUSTOMER_JIZHONG_ANXIN)
void ui_set_front_panel_by_signal(u16 num)
{
  #ifndef WIN32
  char content[5];

   if(com_num == 5)
  {
    sprintf(content, "p%.4d", num);
  }
  else if(com_num == 4)
  {
    sprintf(content, "p%.3d", num);
  }
  else if(com_num == 3)
  {
    sprintf(content, "p%.2d", num);
  }
  else
  {
    return;
  }

	
  _set_fp_display(content);
  #endif
}
#endif


void ui_set_front_panel_by_num(u16 num)
{
  #if 1
  char content[6];

   if(com_num == 5)
  {
    sprintf(content, "%.4d", num);
  }
  else if(com_num == 4)
  {
    sprintf(content, "%.4d", num);
  }
  else if(com_num == 3)
  {
    sprintf(content, "%.3d", num);
  }
  else if(com_num == 2)
  {
    sprintf(content, "%.2d", num);
  }
  else if(com_num == 1)
  {
    sprintf(content, "%.1d", num);
  }
  else
  {
    return;
  }

	
  _set_fp_display(content);
  #endif
}

//lint -e438 -e550 -e830
void ui_show_logo(u8 block_id)
{
  RET_CODE ret = SUCCESS;
  void * p_video_dev = dev_find_identifier(NULL, DEV_IDT_TYPE,
    SYS_DEV_TYPE_VDEC_VSB);
  u32 size = get_dm_block_real_file_size(block_id);
  u8 *addr = mtos_malloc(size);
  MT_ASSERT(NULL != addr);
  dm_read(class_get_handle_by_id(DM_CLASS_ID), block_id, 0, 0, size, addr);

  vdec_stop(p_video_dev);
  ret = vdec_start(p_video_dev, VIDEO_MPEG_ES, VID_UNBLANK_STABLE);
  OS_PRINTF("-----ui show logo addr 0x%x , size 0x%x\n",addr,size);
  ret = vdec_dec_one_frame(p_video_dev, (u8 *)addr, size);
  MT_ASSERT(SUCCESS == ret);

  mtos_free(addr);
}

void ui_show_logo_by_data(u8 *data, u32 size)
{
  RET_CODE ret;
  void * p_video_dev = dev_find_identifier(NULL, DEV_IDT_TYPE,
    SYS_DEV_TYPE_VDEC_VSB);
  void * p_disp_dev = dev_find_identifier(NULL, DEV_IDT_TYPE,   
    SYS_DEV_TYPE_DISPLAY);

  MT_ASSERT(data != NULL);
  MT_ASSERT(size > 0);

  dmx_av_reset(dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_PTI));
  vdec_stop(p_video_dev);
  disp_layer_show(p_disp_dev, DISP_LAYER_ID_VIDEO_SD, FALSE);
//  vdec_set_data_input(p_video_dev,1);
  ret = vdec_start(p_video_dev, VIDEO_MPEG_ES, VID_UNBLANK_STABLE);  
  MT_ASSERT(SUCCESS == ret);

#if 0
  u32 size1 = get_dm_block_real_file_size(LOGO_BLOCK_ID_M1);
  u8 *addr = mtos_malloc(size1);
  MT_ASSERT(NULL != addr);
  dm_read(class_get_handle_by_id(DM_CLASS_ID), LOGO_BLOCK_ID_M1, 0, 0, size1, addr);
  ret = vdec_dec_one_frame(p_video_dev, addr, size1);
#endif
  
  ret = vdec_dec_one_frame(p_video_dev, data, size);
  MT_ASSERT(SUCCESS == ret);
  
  ret = disp_layer_show(p_disp_dev, DISP_LAYER_ID_VIDEO_SD, TRUE);
  MT_ASSERT(SUCCESS == ret); 

  
  //mtos_free(addr);
}

//lint +e438 +e550 +e830

//lint -e124 -e578 -e668 -e831
void* logo_region = NULL;
extern RET_CODE pdec_getOutInfo(drv_dev_t *p_dev, pdec_ins_t *p_ins, jpg_out_info_t *p_out);
extern RET_CODE pdec_setDst(drv_dev_t *p_dev, pdec_ins_t *p_ins, jpg_dst_info_t *p_dst);
RET_CODE ui_show_logo_in_osd(u8 *data, u32 size, rect_t *rect, pdec_image_format_t format)
{
  RET_CODE ret = ERR_FAILURE;
  pic_info_t pic_info = {0};
  pic_param_t pic_param = {PIC_FRAME_MODE,};
  drv_dev_t *p_pic_dev = NULL;
  void *p_disp = NULL;
  void *p_gpe = NULL;
  u32 decode_data_size = 0;
  rect_size_t rect_size = {0};
  rect_t _rect = {0};
  rect_t fill_rect = {0};
  point_t pos = {0};
  gpe_param_vsb_t param = {0, 0, 0, 0, 0, 0};
  pdec_ins_t pic_ins = {IMAGE_FORMAT_UNKNOWN,};
  u8 *p_output = NULL;
  void *region_buffer = NULL;
  u32 align = 0;
  u32 region_size = 0;
  void *p_video_dev = NULL;
  u8 sym_jpg_flag = 0;
  jpg_out_info_t jpg_outinfo = {0};
  jpg_dst_info_t jpg_dstcfg = {0};
  u8 *p_output_jpeg_y = NULL;
  u8 *p_output_jpeg_uv = NULL;

  MT_ASSERT(data != NULL);
  MT_ASSERT(size != 0);
  
  switch (format)
  {
    case IMAGE_FORMAT_JPEG:
    case IMAGE_FORMAT_GIF:
    case IMAGE_FORMAT_PNG:
    case IMAGE_FORMAT_BMP:
    break;

    default:
      return ret;
  }

  p_video_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_VDEC_VSB);
  MT_ASSERT(NULL != p_video_dev);
   
  p_disp = (void *)dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_DISPLAY);
  MT_ASSERT(NULL != p_disp);

  p_gpe = (void *)dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_GPE_VSB);
  MT_ASSERT(NULL != p_gpe);

  p_pic_dev = (drv_dev_t *)dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_PDEC);
  MT_ASSERT(p_pic_dev != NULL);
  
  vdec_stop(p_video_dev);// stop video decode.

  memset(&pic_ins, 0, sizeof(pic_ins));
  ret = pdec_getinfo(p_pic_dev, data, size, &pic_info, &pic_ins);
  MT_ASSERT(ret == SUCCESS);
  UI_PRINTF("pic width:%d, height:%d, format:%d, bbp:%d\n", 
              pic_info.src_width, pic_info.src_height, pic_info.image_format, pic_info.bbp);
  if(pic_info.image_format == IMAGE_FORMAT_JPEG)
  {
    decode_data_size = (pic_info.src_width + 16) * (pic_info.src_height + 16) * 4;
    pic_param.output_format = PIX_FMT_AYCBCR8888;
    if(hal_get_chip_ic_id() == IC_SYMPHONY)
      sym_jpg_flag = 1;
  }
  else
  {
    decode_data_size = pic_info.src_width * pic_info.src_height * 4; 
    pic_param.output_format = PIX_FMT_ARGB8888;
  }

  //UI_PRINTF("decode_data_size:%d\n", decode_data_size);
  if(sym_jpg_flag == 0)
  {
    p_output = (u8 *)mtos_align_malloc(decode_data_size, 8);
    MT_ASSERT(p_output != NULL);
    memset(p_output, 0, decode_data_size);
  }
  pic_param.dec_mode = DEC_FRAME_MODE;
  pic_param.scale_w_num = 1;
  pic_param.scale_w_demo = 1;
  pic_param.scale_h_num = 1;
  pic_param.scale_h_demo = 1;
  pic_param.disp_width = pic_info.src_width;
  pic_param.disp_height = pic_info.src_height;      
  pic_param.p_src_buf = data;
  pic_param.p_dst_buf = p_output;
  pic_param.flip = PIC_NO_F_NO_R;
  pic_param.src_size = size;

  //decode pic data
  UI_PRINTF("pdec_setinfo\n");
  ret = pdec_setinfo(p_pic_dev, &pic_param, &pic_ins);
  MT_ASSERT(ret == SUCCESS);
  if(sym_jpg_flag == 1)
  {
    ret = pdec_getOutInfo(p_pic_dev, &pic_ins, &jpg_outinfo);
    MT_ASSERT(ret == SUCCESS);
    jpg_dstcfg.stride_y = jpg_outinfo.stride_y_min;
    jpg_dstcfg.stride_uv = jpg_outinfo.stride_uv_min;
    p_output_jpeg_y = mtos_align_malloc(jpg_dstcfg.stride_y * jpg_outinfo.out_height, 8);
    MT_ASSERT(p_output_jpeg_y != NULL);
    jpg_dstcfg.p_dst_buf_y = (u32 *)p_output_jpeg_y;
    if(jpg_outinfo.out_fmt != PIX_FMT_ARGB8888)
    {
      p_output_jpeg_uv = mtos_align_malloc(jpg_dstcfg.stride_uv * jpg_outinfo.out_height_uv, 8);
      MT_ASSERT(p_output_jpeg_uv != NULL);
      jpg_dstcfg.p_dst_buf_uv = (u32 *)p_output_jpeg_uv;
    }
    else
      jpg_dstcfg.p_dst_buf_uv = NULL;
    ret = pdec_setDst(p_pic_dev, &pic_ins, &jpg_dstcfg);
    MT_ASSERT(ret == SUCCESS);
  }

  UI_PRINTF("pdec_start\n");
  ret = pdec_start(p_pic_dev, &pic_ins);
  MT_ASSERT(ret == SUCCESS);

  UI_PRINTF("pdec_stop\n");
  ret = pdec_stop(p_pic_dev, &pic_ins);
  MT_ASSERT(ret == SUCCESS);

  //region size
  rect_size.w = (unsigned)(int)(RECTWP(rect));//(((rect->right - rect->left) + 8) * 8)/8;
  rect_size.h = (unsigned)(int)RECTHP(rect);//(rect->bottom - rect->top);
  pos.x = rect->left;
  pos.y = rect->top;

  UI_PRINTF("region w:%d, h:%d, x:%d, y:%d\n", rect_size.w, rect_size.h, pos.x, pos.y);

  //show ads rect
  if(sym_jpg_flag == 0)
  {
    _rect.top = (s16)((rect_size.h - pic_info.src_height)/2);
    _rect.bottom = (s16)((_rect.top + (s16)pic_info.src_height));
    _rect.left = (s16)((rect_size.w - pic_info.src_width)/2);
    _rect.right = (s16)(_rect.left + (s16)pic_info.src_width);
  }
  else
  {
    _rect.top = (s16)((rect_size.h - jpg_outinfo.active_h)/2);
    _rect.bottom = (s16)((_rect.top + (s16)jpg_outinfo.active_h));
    _rect.left = (s16)((rect_size.w - jpg_outinfo.active_w)/2);
    _rect.right = (s16)(_rect.left + (s16)jpg_outinfo.active_w);
  }
  if(_rect.top < 0)
  {
    _rect.top = 0;
    _rect.bottom = _rect.top + (s16)rect_size.h;
  }

  if(_rect.left < 0)
  {
    _rect.left = 0;
    _rect.right = _rect.left + (s16)rect_size.w;
  }
  UI_PRINTF("ads _rect top:%d, bottom:%d, left:%d, right:%d\n", _rect.top, _rect.bottom, _rect.left, _rect.right);

  fill_rect.right = RECTWP(&_rect);
  fill_rect.bottom = RECTHP(&_rect);
  if (logo_region != NULL)
  {
    ret = disp_layer_remove_region(p_disp, DISP_LAYER_ID_OSD0, logo_region);
    region_delete(logo_region);
    logo_region = NULL;
  }
  if(logo_region == NULL)
  {
    logo_region = region_create(&rect_size, (pix_fmt_t)pic_param.output_format);
    MT_ASSERT(NULL != logo_region);

    ret = disp_calc_region_size(p_disp, DISP_LAYER_ID_OSD0, logo_region, &align, &region_size);
    region_buffer = (u8 *)mem_mgr_require_block(BLOCK_AV_BUFFER, SYS_MODULE_GDI);
    MT_ASSERT(region_buffer != 0);
    mem_mgr_release_block(BLOCK_AV_BUFFER);
    MT_ASSERT(mem_mgr_get_block_size(BLOCK_AV_BUFFER) >= (region_size));
    memset(region_buffer, 0, region_size);

    ret = disp_layer_add_region(p_disp, DISP_LAYER_ID_OSD0, logo_region, &pos , region_buffer);

    if(sym_jpg_flag == 1)
    {
      if(jpg_outinfo.out_fmt == PIX_FMT_ARGB8888)
      {
        fill_rect.right = jpg_outinfo.active_w;
        fill_rect.bottom = jpg_outinfo.active_h;  
        gpe_draw_image_vsb(p_gpe, logo_region, &_rect, (void *)(jpg_dstcfg.p_dst_buf_y), 
                          NULL, 0, jpg_dstcfg.stride_y, jpg_dstcfg.stride_y * jpg_outinfo.out_height, 
                          PIX_FMT_ARGB8888, &param, &fill_rect);
      }
      else
      {
        spyuv_t spyuv;
        rect_vsb_t src_rect;
        rect_vsb_t dst_rect;

        spyuv.uvswap = 0;
        spyuv.width = jpg_outinfo.out_width;
        spyuv.height = jpg_outinfo.out_height;
        #ifdef SYMPHONY_IC
        if(jpg_outinfo.out_fmt == PIX_FMT_SP_YUV420)
          spyuv.sp_type = 0;
        else if(jpg_outinfo.out_fmt == PIX_FMT_SP_YUV422_1x2)
          spyuv.sp_type = 1;
        else if(jpg_outinfo.out_fmt == PIX_FMT_SP_YUV422_2x1)
          spyuv.sp_type = 3;
        else if(jpg_outinfo.out_fmt == PIX_FMT_SP_CMYK)
          spyuv.sp_type = 4;
        #endif
        src_rect.x = jpg_outinfo.active_x;
        src_rect.y = jpg_outinfo.active_y;
        src_rect.w = jpg_outinfo.active_w;
        src_rect.h = jpg_outinfo.active_h;
        dst_rect.x = _rect.left;
        dst_rect.y = _rect.top;
        dst_rect.w = _rect.right -  _rect.left;
        dst_rect.h = _rect.bottom - _rect.top;
          
        spyuv.p_luma = (u8 *)(jpg_dstcfg.p_dst_buf_y);
        spyuv.p_chroma = (u16 *)(jpg_dstcfg.p_dst_buf_uv);
        spyuv.luma_pitch = jpg_dstcfg.stride_y;
        spyuv.chroma_pitch = jpg_dstcfg.stride_uv;                
        gpe_draw_semiplanner(p_gpe, 
                                                logo_region, 
                                                &dst_rect,
                                                &spyuv, 
                                                &src_rect,
                                                NULL);
      }
    }
    else
    {
      gpe_draw_image_vsb(p_gpe, logo_region, &_rect, (void *)p_output, 
                        NULL, 0, 4 * pic_info.src_width, decode_data_size, 
                        (pix_fmt_t)(pic_param.output_format), &param, &fill_rect);
    }
    disp_layer_update_region(p_disp, logo_region, NULL);
    region_show(logo_region, TRUE);
    ret = disp_layer_show(p_disp, DISP_LAYER_ID_OSD0, TRUE);
    MT_ASSERT(SUCCESS == ret);
  }

  if(sym_jpg_flag == 0)
  {
    if(p_output != NULL)
      mtos_align_free(p_output);
  }
  else
  {
    if(p_output_jpeg_y != NULL)
      mtos_align_free(p_output_jpeg_y);
    if(p_output_jpeg_uv != NULL)
      mtos_align_free(p_output_jpeg_uv);
  }
  return ret;
}
//lint +e124 +e578 +e668 +e831

RET_CODE ui_hide_logo_in_osd(void)
{
  RET_CODE ret = SUCCESS;
  void *p_disp = NULL;
  
  if (logo_region != NULL)
  {
    p_disp = (void *)dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_DISPLAY);
    MT_ASSERT(NULL != p_disp);
    ret = disp_layer_remove_region(p_disp, DISP_LAYER_ID_OSD0, logo_region);
    region_delete(logo_region);
    logo_region = NULL;
  }

  return ret;
}

#if 0
u8* get_pic_buf_addr(u32* p_len)
{
  u32 pic_size = 0;
  u32 block_size = 0;
  u8* p_addr = NULL;
  ads_res_t ads_res = {0};
  
	 p_addr = (u8*)mem_mgr_require_block(BLOCK_REC_BUFFER, 0);
	 mem_mgr_release_block(BLOCK_REC_BUFFER);
	 block_size = mem_mgr_get_block_size(BLOCK_REC_BUFFER);
	 
	 memset(p_addr, 0, block_size);

  ads_res.id = g_montage_ads_pic_id;
  ads_io_ctrl(ADS_ID_ADT_MT, ADS_IOCMD_AD_RES_GET, &ads_res);
  
   if(ads_res.p_head)
   {
      UI_PRINTF("File size:%d\n", ads_res.p_head->dwFileSize);
		  *p_len = ads_res.p_head->dwFileSize;
		  pic_size = ads_res.p_head->dwFileSize;
		  
		  pic_size = ROUNDUP(pic_size, 4);
		  p_addr += (block_size - pic_size);
		  
		  memcpy(p_addr, ads_res.p_data, *p_len);
   }
   else
   	{
		  *p_len =0;
		  pic_size = 0;
		  
		  pic_size = ROUNDUP(pic_size, 4);
		  p_addr += (block_size - pic_size);
		  
		  //memset(p_addr, 0, *p_len);
   	}

  return p_addr;
}

//use it when show pic in no audio and video
void ui_show_logo_bg(ads_type_t ads_type , rect_t *rect)
{
  mul_pic_param_t pic_param;
  
//  u32 addr = dm_get_block_addr(class_get_handle_by_id(DM_CLASS_ID), block_id);
//  u32 size = dm_get_block_size(class_get_handle_by_id(DM_CLASS_ID), block_id);

  if(ads_type == PIC_TYPE_MAIN_MENU)
  {
    g_montage_ads_pic_id = 0xc1;
  }
  else if(ads_type == PIC_TYPE_SUB_MENU)
  {
    g_montage_ads_pic_id = 0xc2;
  }
  
  ui_pic_init(PIC_SOURCE_BUF);

  pic_param.anim = REND_ANIM_NONE;
  pic_param.file_size = 0;
  pic_param.is_file = FALSE;
  pic_param.flip = PIC_NO_F_NO_R;
  pic_param.win_rect.left = rect->left;
  pic_param.win_rect.top = rect->top;
  pic_param.win_rect.right = rect->right;
  pic_param.win_rect.bottom = rect->bottom;
  pic_param.style = REND_STYLE_CENTER;
  pic_param.buf_get = (u32)get_pic_buf_addr; 

  pic_start(&pic_param);
}

void ui_close_logo_bg(void)
{
  OS_PRINTF("enter %s\n", __FUNCTION__);
  pic_stop();
  ui_pic_release();
  OS_PRINTF("exit %s\n", __FUNCTION__);
}
#endif
BOOL is_region_y_coordinate_covered(rect_t *region_rect1, rect_t *region_rect2)
{
 
  if((region_rect1->top <= region_rect2->top && region_rect1->bottom >=region_rect2->top) 
    || (region_rect1->top > region_rect2->top && region_rect1->top <=region_rect2->bottom))
  {
    return TRUE;
  }
  return FALSE;
}

//lint -e732
void ui_show_signal_pic(void)
{
#ifdef ENABLE_SIGNAL_PIC
  pic_info_t pic_info = {0};
  pic_param_t pic_param = {PIC_FRAME_MODE, };
  drv_dev_t *p_pic_dev = NULL;
  void *p_disp = NULL;
  void *p_gpe = NULL;
  u32 decode_data_size = 0;
  rect_size_t rect_size = {0};
  rect_t rect = {0};
  rect_t fill_rect = {0};
  point_t pos = {0};
  pdec_ins_t pic_ins = {IMAGE_FORMAT_UNKNOWN,};
  u8 *p_output = NULL;
  RET_CODE ret = SUCCESS;
  gpe_param_vsb_t param = {0, 0, 0, 0, 0, 0};
  u8 sym_jpg_flag = 0;
  jpg_out_info_t jpg_outinfo = {0};
  jpg_dst_info_t jpg_dstcfg = {0};
  u8 *p_output_jpeg_y = NULL;
  u8 *p_output_jpeg_uv = NULL;

  UI_PRINTF("%s(%d) --start--\n", __FUNCTION__, __LINE__);
  
  p_disp = (void *)dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_DISPLAY);
  MT_ASSERT(NULL != p_disp);

  p_gpe = (void *)dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_GPE_VSB);
  MT_ASSERT(NULL != p_gpe);
  
  p_pic_dev = (drv_dev_t *)dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_PDEC);
  MT_ASSERT(p_pic_dev != NULL);  

 //get signal full screen pic data
  u32 data_size = get_dm_block_real_file_size(LOGO_BLOCK_ID_M0);
  UI_PRINTF("File size:%d\n", data_size);
  MT_ASSERT(data_size > 0);
  u8 *p_data = mtos_malloc(data_size);
  MT_ASSERT(NULL != p_data);
  dm_read(class_get_handle_by_id(DM_CLASS_ID), LOGO_BLOCK_ID_M0, 0, 0, data_size, p_data);

 if(p_data && data_size > 0)
 {
	  memset(&pic_ins, 0, sizeof(pic_ins));
	  ret = pdec_getinfo(p_pic_dev, p_data, data_size, &pic_info, &pic_ins);
	  MT_ASSERT(ret == SUCCESS);
	  UI_PRINTF("pic width:%d, height:%d, format:%d, bbp:%d\n", 
	  	                pic_info.src_width, pic_info.src_height, pic_info.image_format, pic_info.bbp);
	  if(pic_info.image_format == IMAGE_FORMAT_JPEG)
	  {
        decode_data_size = (pic_info.src_width + 16) * (pic_info.src_height + 16) * 4;
        //pic_param.output_format = PIX_FMT_AYCBCR8888;
        pic_param.output_format = PIX_FMT_ARGB8888;
        if(hal_get_chip_ic_id() == IC_SYMPHONY)
          sym_jpg_flag = 1;
	  }
	  else
	  {
        //decode_data_size = pic_info.src_width * pic_info.src_height * 4; 
        decode_data_size = (pic_info.src_width + 16) * (pic_info.src_height + 16) * 4 ;
        pic_param.output_format = PIX_FMT_ARGB8888;
	  }

	  UI_PRINTF("decode_data_size:%d\n", decode_data_size);
      if(sym_jpg_flag == 0)
      {
    	  p_output = (u8 *)mtos_align_malloc(decode_data_size, 8);
    	  MT_ASSERT(p_output != NULL);
    	  memset(p_output, 0, decode_data_size);
      }
	  
	  pic_param.dec_mode = DEC_FRAME_MODE;
	  pic_param.scale_w_num = 1;
	  pic_param.scale_w_demo = 1;
	  pic_param.scale_h_num = 1;
	  pic_param.scale_h_demo = 1;
	  pic_param.disp_width = pic_info.src_width;
	  pic_param.disp_height = pic_info.src_height;      
	  pic_param.p_src_buf = p_data;
	  pic_param.p_dst_buf = p_output;
	  pic_param.flip = PIC_NO_F_NO_R;
	  pic_param.src_size = data_size;

	  //decode pic data
	  UI_PRINTF("pdec_setinfo\n");
	  ret = pdec_setinfo(p_pic_dev, &pic_param, &pic_ins);
	  MT_ASSERT(ret == SUCCESS);
      if(sym_jpg_flag == 1)
      {
        ret = pdec_getOutInfo(p_pic_dev, &pic_ins, &jpg_outinfo);
        MT_ASSERT(ret == SUCCESS);
        jpg_dstcfg.stride_y = jpg_outinfo.stride_y_min;
        jpg_dstcfg.stride_uv = jpg_outinfo.stride_uv_min;
        p_output_jpeg_y = mtos_align_malloc(jpg_dstcfg.stride_y * jpg_outinfo.out_height, 8);
        MT_ASSERT(p_output_jpeg_y != NULL);
        jpg_dstcfg.p_dst_buf_y = (u32 *)p_output_jpeg_y;
        if(jpg_outinfo.out_fmt != PIX_FMT_ARGB8888)
        {
          p_output_jpeg_uv = mtos_align_malloc(jpg_dstcfg.stride_uv * jpg_outinfo.out_height_uv, 8);
          MT_ASSERT(p_output_jpeg_uv != NULL);
          jpg_dstcfg.p_dst_buf_uv = (u32 *)p_output_jpeg_uv;
        }
        else
          jpg_dstcfg.p_dst_buf_uv = NULL;
        ret = pdec_setDst(p_pic_dev, &pic_ins, &jpg_dstcfg);
        MT_ASSERT(ret == SUCCESS);
      }

	  UI_PRINTF("pdec_start\n");
	  ret = pdec_start(p_pic_dev, &pic_ins);
	  MT_ASSERT(ret == SUCCESS);

	  UI_PRINTF("pdec_stop\n");
	  ret = pdec_stop(p_pic_dev, &pic_ins);
	  MT_ASSERT(ret == SUCCESS);

	  //region size
	  rect_size.w = 1280;
	  rect_size.h = 720;
	  pos.x = 0;
	  pos.y = 0;

	  UI_PRINTF("region w:%d, h:%d, x:%d, y:%d\n", rect_size.w, rect_size.h, pos.x, pos.y);
    
	  //show ads rect
    if(sym_jpg_flag == 0)
    {
      rect.top = (s16)((rect_size.h - pic_info.src_height)/2);
      rect.bottom = (s16)((rect.top + (s16)pic_info.src_height));
      rect.left = (s16)((rect_size.w - pic_info.src_width)/2);
      rect.right = (s16)(rect.left + (s16)pic_info.src_width);
    }
    else
    {
      rect.top = (s16)((rect_size.h - jpg_outinfo.active_h)/2);
      rect.bottom = (s16)((rect.top + (s16)jpg_outinfo.active_h));
      rect.left = (s16)((rect_size.w - jpg_outinfo.active_w)/2);
      rect.right = (s16)(rect.left + (s16)jpg_outinfo.active_w);
    }

   if(rect.top < 0)
   	{
   	  rect.top = 0;
	   rect.bottom = rect.top + (s16)rect_size.h;
   	}

   if(rect.left < 0)
   	{
      rect.left = 0;
      rect.right = rect.left + (s16)rect_size.w;
   	}
   UI_PRINTF("ads rect top:%d, bottom:%d, left:%d, right:%d\n", rect.top, rect.bottom, rect.left, rect.right);
    
  fill_rect.right = RECTWP(&rect);
  fill_rect.bottom = RECTHP(&rect);
  if(p_montage_ads_sub_rgn == NULL)
  {
    p_montage_ads_sub_rgn = region_create(&rect_size,PIX_FMT_ARGB8888);
    MT_ASSERT(NULL != p_montage_ads_sub_rgn);
   UI_PRINTF("%s(%d) == ret=[%d] \n", __FUNCTION__, __LINE__, ret);

    ret = disp_layer_add_region(p_disp, DISP_LAYER_ID_SUBTITL,
                                   p_montage_ads_sub_rgn, &pos, NULL);
   UI_PRINTF("%s(%d) == ret=[%d] \n", __FUNCTION__, __LINE__, ret);
  if(sym_jpg_flag == 1)
  {
    if(jpg_outinfo.out_fmt == PIX_FMT_ARGB8888)
    {
      fill_rect.right = jpg_outinfo.active_w;
      fill_rect.bottom = jpg_outinfo.active_h;  
      gpe_draw_image_vsb(p_gpe, p_montage_ads_sub_rgn, &rect, (void *)(jpg_dstcfg.p_dst_buf_y), 
                        NULL, 0, jpg_dstcfg.stride_y, jpg_dstcfg.stride_y * jpg_outinfo.out_height, 
                        PIX_FMT_ARGB8888, &param, &fill_rect);
    }
    else
    {
      spyuv_t spyuv;
      rect_vsb_t src_rect;
      rect_vsb_t dst_rect;

      spyuv.uvswap = 0;
      spyuv.width = jpg_outinfo.out_width;
      spyuv.height = jpg_outinfo.out_height;
      #ifdef SYMPHONY_IC
      if(jpg_outinfo.out_fmt == PIX_FMT_SP_YUV420)
        spyuv.sp_type = 0;
      else if(jpg_outinfo.out_fmt == PIX_FMT_SP_YUV422_1x2)
        spyuv.sp_type = 1;
      else if(jpg_outinfo.out_fmt == PIX_FMT_SP_YUV422_2x1)
        spyuv.sp_type = 3;
      else if(jpg_outinfo.out_fmt == PIX_FMT_SP_CMYK)
        spyuv.sp_type = 4;
      #endif
      src_rect.x = jpg_outinfo.active_x;
      src_rect.y = jpg_outinfo.active_y;
      src_rect.w = jpg_outinfo.active_w;
      src_rect.h = jpg_outinfo.active_h;
      dst_rect.x = rect.left;
      dst_rect.y = rect.top;
      dst_rect.w = rect.right -  rect.left;
      dst_rect.h = rect.bottom - rect.top;

      spyuv.p_luma = (u8 *)(jpg_dstcfg.p_dst_buf_y);
      spyuv.p_chroma = (u16 *)(jpg_dstcfg.p_dst_buf_uv);
      spyuv.luma_pitch = jpg_dstcfg.stride_y;
      spyuv.chroma_pitch = jpg_dstcfg.stride_uv;                
      gpe_draw_semiplanner(p_gpe, 
                                              p_montage_ads_sub_rgn, 
                                              &dst_rect,
                                              &spyuv, 
                                              &src_rect,
                                              NULL);
    }
  }
  else
  {
#ifdef JiZhong_Sm
	     ret = gpe_draw_image_vsb(p_gpe, p_montage_ads_sub_rgn, &rect, (void *)p_output, 
                                        NULL, 0, 4 *pic_param.disp_stride , decode_data_size, 
                                       PIX_FMT_ARGB8888, &param, &fill_rect);
#else										
   ret = gpe_draw_image_vsb(p_gpe, p_montage_ads_sub_rgn, &rect, (void *)p_output, 
                                        NULL, 0, 4 * pic_info.src_width, decode_data_size, 
                                        PIX_FMT_ARGB8888, &param, &fill_rect);
#endif  
  }
   UI_PRINTF("%s(%d) == ret=[%d] \n", __FUNCTION__, __LINE__, ret);
    ret = disp_layer_update_region(p_disp, p_montage_ads_sub_rgn, NULL);
   UI_PRINTF("%s(%d) == ret=[%d] \n", __FUNCTION__, __LINE__, ret);
    ret = region_show(p_montage_ads_sub_rgn, TRUE);
   UI_PRINTF("%s(%d) == ret=[%d] \n", __FUNCTION__, __LINE__, ret);
    ret = disp_layer_show(p_disp, DISP_LAYER_ID_SUBTITL, TRUE);
    MT_ASSERT(SUCCESS == ret);
  }
  else
  {
   UI_PRINTF("p_montage_ads_sub_rgn == NULL \n");
  }
	
    if(sym_jpg_flag == 0)
    {
      if(p_output != NULL)
        mtos_align_free(p_output);
    }
    else
    {
      if(p_output_jpeg_y != NULL)
        mtos_align_free(p_output_jpeg_y);
      if(p_output_jpeg_uv != NULL)
        mtos_align_free(p_output_jpeg_uv);
    }
 }
  UI_PRINTF("%s(%d) --end --\n", __FUNCTION__, __LINE__);
#endif
}
//lint +e732

//lint -e732
#if 0
void ui_show_ads_pic(ads_type_t ads_type , rect_t *region_rect)
{
  pic_info_t pic_info = {0};
  pic_param_t pic_param = {PIC_FRAME_MODE, };
  drv_dev_t *p_pic_dev = NULL;
  void *p_disp = NULL;
  void *p_gpe = NULL;
  u8 *p_data = NULL;
  u32 data_size = 0;
  u32 decode_data_size = 0;
  rect_size_t rect_size = {0};
  rect_t rect = {0};
  rect_t fill_rect = {0};
  point_t pos = {0};
  ads_res_t ads_res = {0};
  RET_CODE ret = SUCCESS;
  gpe_param_vsb_t param = {0, 0, 0, 0, 0, 0};
  
  //param.enable_colorkey = TRUE;
  //param.colorkey = 0x4d00feff; //colorkey alpha 0x4d
  
  
  p_disp = (void *)dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_DISPLAY);
  MT_ASSERT(NULL != p_disp);

  p_gpe = (void *)dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_GPE_VSB);
  MT_ASSERT(NULL != p_gpe);
  
  p_pic_dev = (drv_dev_t *)dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_PDEC);

  MT_ASSERT(p_pic_dev != NULL);  

  if(ads_type == PIC_TYPE_MAIN_MENU)
  {
    ads_res.id = MAIN_MENU_RES_ID;
  }
  else if(ads_type == PIC_TYPE_SUB_MENU)
  {
    ads_res.id = CH_LIST_RES_ID;
  }

 //get montage ads data
 ads_io_ctrl(ADS_ID_ADT_MT, ADS_IOCMD_AD_RES_GET, &ads_res);
 if(ads_res.p_head)
 {
    UI_PRINTF("File size:%d\n", ads_res.p_head->dwFileSize);
    p_data = ads_res.p_data;
    data_size = ads_res.p_head->dwFileSize;
   
	  MT_ASSERT(p_data != NULL);
	  MT_ASSERT(data_size != 0);
    
	  memset(&pic_ins, 0, sizeof(pic_ins));
	  ret = pdec_getinfo(p_pic_dev, p_data, data_size, &pic_info, &pic_ins);
	  MT_ASSERT(ret == SUCCESS);
	  UI_PRINTF("pic width:%d, height:%d, format:%d, bbp:%d\n", 
	  	                pic_info.src_width, pic_info.src_height, pic_info.image_format, pic_info.bbp);
	  if(pic_info.image_format == IMAGE_FORMAT_JPEG)
	  {
        decode_data_size = (pic_info.src_width + 16) * (pic_info.src_height + 16) * 4;
	    pic_param.output_format = PIX_FMT_AYCBCR8888;
	  }
	  else
	  {
      decode_data_size = pic_info.src_width * pic_info.src_height * 4; 
	     pic_param.output_format = PIX_FMT_ARGB8888;
	  }

   UI_PRINTF("decode_data_size:%d\n", decode_data_size);
	  p_output = (u8 *)mtos_align_malloc(decode_data_size, 8);
	  MT_ASSERT(p_output != NULL);
    memset(p_output, 0, decode_data_size);
	  
	  pic_param.dec_mode = DEC_FRAME_MODE;
	  pic_param.scale_w_num = 1;
	  pic_param.scale_w_demo = 1;
	  pic_param.scale_h_num = 1;
	  pic_param.scale_h_demo = 1;
	  pic_param.disp_width = pic_info.src_width;
	  pic_param.disp_height = pic_info.src_height;      
	  pic_param.p_src_buf = p_data;
	  pic_param.p_dst_buf = p_output;
	  pic_param.flip = PIC_NO_F_NO_R;
	  pic_param.src_size = data_size;

	  //decode pic data
	  UI_PRINTF("pdec_setinfo\n");
	  ret = pdec_setinfo(p_pic_dev, &pic_param, &pic_ins);
	  MT_ASSERT(ret == SUCCESS);

	  UI_PRINTF("pdec_start\n");
	  ret = pdec_start(p_pic_dev, &pic_ins);
	  MT_ASSERT(ret == SUCCESS);

	  UI_PRINTF("pdec_stop\n");
	  ret = pdec_stop(p_pic_dev, &pic_ins);
	  MT_ASSERT(ret == SUCCESS);

	  //region size
	  rect_size.w = (((region_rect->right - region_rect->left) + 8) * 8)/8;
	  rect_size.h = (region_rect->bottom - region_rect->top);
    pos.x = region_rect->left;
    pos.y = region_rect->top;

    UI_PRINTF("region w:%d, h:%d, x:%d, y:%d\n", rect_size.w, rect_size.h, pos.x, pos.y);
    
	  //show ads rect
	  rect.top = (s16)((rect_size.h - pic_info.src_height)/2);
	  rect.bottom = rect.top + (s16)pic_info.src_height;
	  rect.left = ((s16)rect_size.w - (s16)pic_info.src_width)/2;
	  rect.right = rect.left + (s16)pic_info.src_width;

   if(rect.top < 0)
   	{
   	  rect.top = 0;
	   rect.bottom = rect.top + (s16)rect_size.h;
   	}

   if(rect.left < 0)
   	{
      rect.left = 0;
      rect.right = rect.left + (s16)rect_size.w;
   	}
   UI_PRINTF("ads rect top:%d, bottom:%d, left:%d, right:%d\n", rect.top, rect.bottom, rect.left, rect.right);
    
  fill_rect.right = RECTWP(&rect);
  fill_rect.bottom = RECTHP(&rect);
  if(p_montage_ads_sub_rgn == NULL)
  {
    p_montage_ads_sub_rgn = region_create(&rect_size,PIX_FMT_ARGB8888);
    MT_ASSERT(NULL != p_montage_ads_sub_rgn);

    ret = disp_layer_add_region(p_disp, DISP_LAYER_ID_SUBTITL,
                                   p_montage_ads_sub_rgn, &pos, NULL);

    gpe_draw_image_vsb(p_gpe, p_montage_ads_sub_rgn, &rect, (void *)p_output, 
                                        NULL, 0, 4 * pic_info.src_width, decode_data_size, 
                                        PIX_FMT_ARGB8888, &param, &fill_rect);
    disp_layer_update_region(p_disp, p_montage_ads_sub_rgn, NULL);
    region_show(p_montage_ads_sub_rgn, TRUE);
    ret = disp_layer_show(p_disp, DISP_LAYER_ID_SUBTITL, TRUE);
    MT_ASSERT(SUCCESS == ret);
  }
  
  mtos_align_free(p_output);
 	}
}
//lint +e732
#endif
//lint -e438 -e550 -e830
void ui_close_ads_pic(void)
{
  RET_CODE ret = ERR_FAILURE;
  void * p_disp_dev = dev_find_identifier(NULL, DEV_IDT_TYPE,
                                          SYS_DEV_TYPE_DISPLAY);

  if(NULL != p_montage_ads_sub_rgn)
  {
  OS_PRINTF("==>ui_close_ads_pic remove region  !!!\n");
    ret = disp_layer_remove_region(p_disp_dev, DISP_LAYER_ID_SUBTITL, p_montage_ads_sub_rgn);
    MT_ASSERT(SUCCESS == ret);

    ret = region_delete(p_montage_ads_sub_rgn);
    MT_ASSERT(SUCCESS == ret);
    p_montage_ads_sub_rgn = NULL;

    disp_layer_show(p_disp_dev, DISP_LAYER_ID_SUBTITL, FALSE);
  }
}
//lint +e438 +e550 +e830

void ui_enable_video_display(BOOL is_enable)
{
  void * p_disp_dev = dev_find_identifier(NULL, DEV_IDT_TYPE,   
    SYS_DEV_TYPE_DISPLAY);
  
  disp_layer_show(p_disp_dev, DISP_LAYER_ID_VIDEO_SD, is_enable);
}


/*
the input size is Kbytes
*/
void ui_conver_file_size_unit(u32 k_size, u8 *p_str)
{
  u32 m_unit = 1024*1024;
  u32 mod_value = 0;
  if(k_size == 0)
  {
    sprintf((char *)p_str,"0M Bytes");
  }
  else if(k_size/1024/1024 >0)
  {
    mod_value = k_size%m_unit;
    sprintf((char *)p_str,"%ld.%ld%ldG Bytes", k_size/m_unit,mod_value*10/m_unit,(mod_value*10%m_unit)*10/m_unit);
  }
  else if(k_size/1024 >0)
  {
    mod_value = k_size%1024;
    sprintf((char *)p_str,"%ld.%ld%ldM Bytes", k_size/1024,mod_value*10/1024,(mod_value*10%1024)*10/1024);
  }
  else
  {
    sprintf((char *)p_str,"%ldK Bytes", k_size);
  }
}

void ui_set_front_panel_by_str_with_upg(const char *str_flag, u8 process)
{
  char str[5] = {0};
  static u8 i,j;
  s32 cmp = 0;
  void *p_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_UIO);

  if (com_num == 0)
    return;

  memset(str,' ',5);
  if(process == 100)
  {
    i = 0;
  }
  if(i % com_num == 0)
  {
    memset(str,(int)'-',1);
    i++;
  }
  else if(i % com_num == 1)
  {
    memset(str+1,(int)'_',1);
    i++;
  }
  else if(i % com_num == 2)
  {
    i++;
    memset(str + 2,(int)'-',1);
  }
  else if(i % com_num == 3)
  {
    memset(str + 3,(int)'_',1);
    i++;
  }
  else if(i % com_num == 4)
  {
    memset(str + 4,(int)'_',1);
    i++;
  }
  else if(i % com_num == 5)
  {
    memset(str + 5,(int)'_',1);
    i++;
  }

  if(memcmp(str_flag,"W",1) == 0)
  {
    j = 1;
  }

 if(process == 100)
 {
     if((memcmp(str_flag,"B",1) == 0))
    {
       memcpy(str,"OFF ",4);
     }
     if(memcmp(str_flag,"R",1) == 0)
     {
        if(j == 1)
        {
          memcpy(str,"END ",3);
          j = 0;
        }
     }
  }
 
cmp = memcmp(str_flag,"ERR",3);

if(cmp == 0)
{
  memcpy(str,"E01 ",4); 
}
  
{
  uio_display(p_dev, (u8 *)str, com_num);
}
}

#if 0
static s32 BmpReader(const u8* bmpaddr, const u32 size, BMP_HEADER* p_bmp_header, DIB_HEADER* p_dib_header)
{
  const u8* p = bmpaddr;

  if(*p != 'B' || *(p+1) != 'M')
    return -1;

  p += 2; //skip magic
  memcpy(p_bmp_header, p, sizeof(BMP_HEADER));

  if(p_bmp_header->filesz != size)
    return -1;

  p += sizeof(BMP_HEADER);
  memcpy(p_dib_header, p, sizeof(DIB_HEADER));
    
  return 0;
}
#endif
char *strdup_utf8(const char *s)
{
    char *new_string;

    new_string = (char *)mtos_malloc(strlen(s) + 1);
    if (new_string != NULL)
    {
        strcpy(new_string, s);
        return new_string;
    }

    return NULL;
}

u16 *strdup_utf8ToUnicode(const char *s)
{
    char *inbuf, *outbuf;
    u16 *new_string;
    size_t src_len, dest_len;

    src_len = strlen(s) + 1;
    dest_len = src_len * sizeof(u16);
    inbuf = (char *)s;
    outbuf = (char *)mtos_malloc(dest_len);
    if (outbuf != NULL)
    {
        new_string = (u16 *)outbuf;
        iconv(g_cd_utf8_to_utf16le, (char **)&inbuf, &src_len, (char **)&outbuf, &dest_len);

        return new_string;
    }

    return NULL;
}
void bmp_up2down(u8 *p_data, u32 image_width, u32 image_height, u16 bpp)
{
    u32 index = bpp/8;
    u32 h, w, coordm, coordn;
    u8 temp;
    for (h = 0; h < image_height/2; h++)
    {
        for (w = 0; w < image_width; w++)
        {
            coordm = index*(h*image_width + w);
            coordn = index*((image_height - h -1)*image_width + w);
            temp = p_data[coordm];
            p_data[coordm] = p_data[coordn];
            p_data[coordn] = temp;
            temp = p_data[coordm+1];
            p_data[coordm + 1] = p_data[coordn + 1]; 
            p_data[coordn + 1] = temp;
            temp = p_data[coordm + 2];
            p_data[coordm + 2] = p_data[coordn + 2];
            p_data[coordn + 2] = temp;
        }
    }
}

BOOL ui_get_watermark_rect(rect_t * rect_region)
{
  BOOL ret = FALSE;

  if(NULL != p_sub_rgn)
  {
    rect_region->top = g_water_mark_rect.top;
    rect_region->bottom = g_water_mark_rect.bottom;
    rect_region->left = g_water_mark_rect.left;
    rect_region->right = g_water_mark_rect.right;
    
    ret = TRUE;
  }
  return ret;
}

//lint -e438 -e550 -e830 -e668 -e831
void ui_show_watermark(void)
{
  pic_info_t pic_info = {PIC_FRAME_MODE,};
  pic_param_t pic_param = {PIC_FRAME_MODE,};
  drv_dev_t *p_pic_dev = NULL;
  void *p_disp = NULL;
  void *p_gpe = NULL;
  u8 *addr = NULL;
  u32 size = 0;
  u32 decode_data_size = 0;
  rect_size_t rect_size = {0};
  rect_t rect = {0};
  rect_t fill_rect = {0};
  point_t pos = {0};
  RET_CODE ret = SUCCESS;
  gpe_param_vsb_t param = {0, 0, 0, 0, 0, 0};
  osd_set_t osd_set;
  pdec_ins_t pic_ins_warter = {IMAGE_FORMAT_UNKNOWN,};
  u8 *p_output_water = NULL;
  //BMP_HEADER bmp_header;
  //DIB_HEADER dib_header;
  //u32 i = 0;
  u32 width=0,height=0;
  u32 tmpwidth = 0;
  rect_t adv_rect = {0};
  u8 sym_jpg_flag = 0;
  jpg_out_info_t jpg_outinfo = {0};
  jpg_dst_info_t jpg_dstcfg = {0};
  u8 *p_output_jpeg_y = NULL;
  u8 *p_output_jpeg_uv = NULL;

  if(!g_customer.b_WaterMark)
  {
    return;
  }

  size = get_dm_block_real_file_size(LOGO_BLOCK_ID_M2);
  addr = mtos_malloc(size);
  //for pc_lint
  if(NULL == addr)
  {
    MT_ASSERT(0);
    return;
  }
  dm_read(class_get_handle_by_id(DM_CLASS_ID), LOGO_BLOCK_ID_M2, 0, 0, size, addr);
  MT_ASSERT(size != 0);
  
  p_disp = (void *)dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_DISPLAY);
  MT_ASSERT(NULL != p_disp);

  p_gpe = (void *)dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_GPE_VSB);
  MT_ASSERT(NULL != p_gpe);
  
  p_pic_dev = (drv_dev_t *)dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_PDEC);
  MT_ASSERT(p_pic_dev != NULL);  
  
  if(p_sub_rgn != NULL)
  {
    #if 0
    if(!disp_layer_is_show(p_disp, DISP_LAYER_ID_SUBTITL))
    {
      ret = disp_layer_show(p_disp, DISP_LAYER_ID_SUBTITL, TRUE);
      MT_ASSERT(SUCCESS == ret);
    }
	#endif
    mtos_free(addr);
    return;
  }
  

  sys_status_get_osd_set(&osd_set);
  if (osd_set.enable_subtitle)
  {
    OS_PRINTF("Water mark cannot display when subtitle on\n");
    mtos_free(addr);
    return;
  }
 
  memset(&pic_ins_warter, 0, sizeof(pic_ins_warter));
  ret = pdec_getinfo(p_pic_dev, addr, size, &pic_info, &pic_ins_warter);
  MT_ASSERT(ret == SUCCESS);

  OS_PRINTF("pic width:%d, height:%d, format:%d, bbp:%d\n", pic_info.src_width, pic_info.src_height, pic_info.image_format, pic_info.bbp);
  if(pic_info.image_format == IMAGE_FORMAT_JPEG)
  {
    decode_data_size = (pic_info.src_width + 16) * (pic_info.src_height + 16) * 4;
    pic_param.output_format = PIX_FMT_AYCBCR8888;
    if(hal_get_chip_ic_id() == IC_SYMPHONY)
      sym_jpg_flag = 1;
  }
  else 
  {
    decode_data_size = (pic_info.src_width/2 + 1) * 2 * pic_info.src_height * 4; 
    pic_param.output_format = PIX_FMT_ARGB8888;
  }
  OS_PRINTF("decode_data_size:%d\n", decode_data_size);
  
  if(sym_jpg_flag == 0)
  {
    p_output_water = (u8 *)mtos_align_malloc(decode_data_size, 8);
    MT_ASSERT(p_output_water != NULL);
    memset(p_output_water, 0, decode_data_size);
  }
  
  pic_param.dec_mode = DEC_FRAME_MODE;
  pic_param.scale_w_num = 1;
  pic_param.scale_w_demo = 1;
  pic_param.scale_h_num = 1;
  pic_param.scale_h_demo = 1;
  pic_param.disp_width = pic_info.src_width;
  pic_param.disp_height = pic_info.src_height;      
  pic_param.p_src_buf = addr;
  pic_param.p_dst_buf = p_output_water;
  pic_param.flip = PIC_NO_F_NO_R;
  pic_param.src_size = size;
  
  UI_PRINTF("pdec_setinfo\n");
  ret = pdec_setinfo(p_pic_dev, &pic_param, &pic_ins_warter);
  MT_ASSERT(ret == SUCCESS);
  if(sym_jpg_flag == 1)
  {
    ret = pdec_getOutInfo(p_pic_dev, &pic_ins_warter, &jpg_outinfo);
    MT_ASSERT(ret == SUCCESS);
    jpg_dstcfg.stride_y = jpg_outinfo.stride_y_min;
    jpg_dstcfg.stride_uv = jpg_outinfo.stride_uv_min;
    p_output_jpeg_y = mtos_align_malloc(jpg_dstcfg.stride_y * jpg_outinfo.out_height, 8);
    MT_ASSERT(p_output_jpeg_y != NULL);
    jpg_dstcfg.p_dst_buf_y = (u32 *)p_output_jpeg_y;
    if(jpg_outinfo.out_fmt != PIX_FMT_ARGB8888)
    {
      p_output_jpeg_uv = mtos_align_malloc(jpg_dstcfg.stride_uv * jpg_outinfo.out_height_uv, 8);
      MT_ASSERT(p_output_jpeg_uv != NULL);
      jpg_dstcfg.p_dst_buf_uv = (u32 *)p_output_jpeg_uv;
    }
    else
      jpg_dstcfg.p_dst_buf_uv = NULL;
    ret = pdec_setDst(p_pic_dev, &pic_ins_warter, &jpg_dstcfg);
    MT_ASSERT(ret == SUCCESS);
  }
  
  UI_PRINTF("pdec_start\n");
  ret = pdec_start(p_pic_dev, &pic_ins_warter);
  MT_ASSERT(ret == SUCCESS);
  
  UI_PRINTF("pdec_stop\n");
  ret = pdec_stop(p_pic_dev, &pic_ins_warter);
  MT_ASSERT(ret == SUCCESS);
  if(sym_jpg_flag == 1)
  {
    width = jpg_outinfo.active_w;
    height = jpg_outinfo.active_h;
  }
  else
  {
    width = pic_info.src_width;
    height = pic_info.src_height;
  }
  tmpwidth = 4 * (( pic_info.src_width + 1 ) / 2 * 2);

  
  rect_size.w = (width + 3 ) / 4 * 4;
  rect_size.h = height;
  pos.x = (s16)g_customer.x_WaterMark;
  pos.y = (s16)g_customer.y_WaterMark;

  if(pos.x + (s16)rect_size.w > g_customer.graphic_w)
  {
    pos.x = g_customer.graphic_w - (s16)rect_size.w;
  }

  if(pos.y + (s16)rect_size.h > g_customer.graphic_h)
  {
    pos.y = g_customer.graphic_h - (s16)rect_size.h;
  }
  
  g_water_mark_rect.left = pos.x;
  g_water_mark_rect.right = pos.x + (s16)rect_size.w;
  g_water_mark_rect.top = pos.y;
  g_water_mark_rect.bottom = pos.y + (s16)rect_size.h;
  
  if(ads_get_ads_id() != ADS_ID_ADT_DUMY)
  {
    if(ui_adv_get_pic_rect(&adv_rect))
    {
      if(is_region_y_coordinate_covered(&g_water_mark_rect, &adv_rect))
      {
        OS_PRINTF("[ui_show_watermark] region covered, won't show watermark! \n");
        
        mtos_align_free(p_output_water);
        mtos_free(addr);
        return;
      }
    }
  }

  //show rect
  rect.top = 0;
  rect.bottom = rect.top + (s16)height;
  rect.left = 0;
  rect.right = rect.left + (s16)width;

  fill_rect.right = RECTWP(&rect);
  fill_rect.bottom = RECTHP(&rect);  

  if(p_sub_rgn == NULL)
  {
    OS_PRINTF("region w:%d, h:%d, x:%d, y:%d\n", rect_size.w, rect_size.h, pos.x, pos.y);

    p_sub_rgn = region_create(&rect_size,PIX_FMT_ARGB8888);
    MT_ASSERT(NULL != p_sub_rgn);
    ret = disp_layer_add_region(p_disp, DISP_LAYER_ID_SUBTITL,
    p_sub_rgn, &pos, NULL);



    //param.enable_colorkey = TRUE;
    //param.colorkey = 0x4d00feff; //colorkey alpha 0x4d

	//Please use PNG picture if want to show alpha
    //param.plane_alpha = 0x7f;
    //param.plane_alpha_en = TRUE;
    //pitch must be 8 bytes align
    if(sym_jpg_flag == 1)
    {
      if(jpg_outinfo.out_fmt == PIX_FMT_ARGB8888)
      {
        fill_rect.right = jpg_outinfo.active_w;
        fill_rect.bottom = jpg_outinfo.active_h;  
        gpe_draw_image_vsb(p_gpe, p_sub_rgn, &rect, (void *)(jpg_dstcfg.p_dst_buf_y), 
                          NULL, 0, jpg_dstcfg.stride_y, jpg_dstcfg.stride_y * jpg_outinfo.out_height, 
                          PIX_FMT_ARGB8888, &param, &fill_rect);
      }
      else
      {
        spyuv_t spyuv;
        rect_vsb_t src_rect;
        rect_vsb_t dst_rect;

        spyuv.uvswap = 0;
        spyuv.width = jpg_outinfo.out_width;
        spyuv.height = jpg_outinfo.out_height;
        #ifdef SYMPHONY_IC
        if(jpg_outinfo.out_fmt == PIX_FMT_SP_YUV420)
          spyuv.sp_type = 0;
        else if(jpg_outinfo.out_fmt == PIX_FMT_SP_YUV422_1x2)
          spyuv.sp_type = 1;
        else if(jpg_outinfo.out_fmt == PIX_FMT_SP_YUV422_2x1)
          spyuv.sp_type = 3;
        else if(jpg_outinfo.out_fmt == PIX_FMT_SP_CMYK)
          spyuv.sp_type = 4;
        #endif
        src_rect.x = jpg_outinfo.active_x;
        src_rect.y = jpg_outinfo.active_y;
        src_rect.w = jpg_outinfo.active_w;
        src_rect.h = jpg_outinfo.active_h;
        dst_rect.x = rect.left;
        dst_rect.y = rect.top;
        dst_rect.w = rect.right -  rect.left;
        dst_rect.h = rect.bottom - rect.top;

        spyuv.p_luma = (u8 *)(jpg_dstcfg.p_dst_buf_y);
        spyuv.p_chroma = (u16 *)(jpg_dstcfg.p_dst_buf_uv);
        spyuv.luma_pitch = jpg_dstcfg.stride_y;
        spyuv.chroma_pitch = jpg_dstcfg.stride_uv;                
        gpe_draw_semiplanner(p_gpe, 
                                                p_sub_rgn, 
                                                &dst_rect,
                                                &spyuv, 
                                                &src_rect,
                                                NULL);
      }
    }
    else
    {
      gpe_draw_image_vsb(p_gpe, p_sub_rgn, &rect, (void *)p_output_water, 
                        NULL, 0, tmpwidth, decode_data_size, 
                        PIX_FMT_ARGB8888, &param, &fill_rect);
    }
    disp_layer_update_region(p_disp, p_sub_rgn, NULL);
    region_show(p_sub_rgn, TRUE);
    ret = disp_layer_show(p_disp, DISP_LAYER_ID_SUBTITL, TRUE);
    MT_ASSERT(SUCCESS == ret);
  }
      
  mtos_align_free(p_output_water);
  mtos_free(addr);
}

void ui_watermark_release(void)
{
  RET_CODE ret = ERR_FAILURE;
  void * p_disp_dev = dev_find_identifier(NULL, DEV_IDT_TYPE,
                                          SYS_DEV_TYPE_DISPLAY);

  if(!g_customer.b_WaterMark || NULL == p_sub_rgn)
  {
    return;
  }

  ret = disp_layer_remove_region(p_disp_dev, DISP_LAYER_ID_SUBTITL, p_sub_rgn);
  MT_ASSERT(SUCCESS == ret);

  ret = region_delete(p_sub_rgn);
  MT_ASSERT(SUCCESS == ret);
  p_sub_rgn = NULL;

  disp_layer_show(p_disp_dev, DISP_LAYER_ID_SUBTITL, FALSE);
  
  memset(&g_water_mark_rect, 0, sizeof(rect_t));
}
void ui_water_mark_refresh(void)
{
#if 0
  void * p_disp_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_DISPLAY);
  if(pic_watermark_region != NULL)
  {
    disp_layer_show(p_disp_dev, DISP_LAYER_ID_SUBTITL, FALSE);
    disp_layer_update_region(p_disp_dev, pic_watermark_region, NULL);
    disp_layer_show(p_disp_dev, DISP_LAYER_ID_SUBTITL, TRUE);
  }
  #endif
  //guoteng: need to be improved
  if(p_sub_rgn != NULL)
  {
  ui_watermark_release();
  ui_show_watermark();
  }
}
//lint +e438 +e550 +e830 +e668 +e831 

void ui_set_net_svc_instance(void *p_instance)
{
  p_net_svc = p_instance;
}

void * ui_get_net_svc_instance(void)
{
  return p_net_svc;
}

void ui_util_api_init_dvb_monitor(void)
{
  static BOOL b_init = FALSE;
//#ifndef WIN32
  m_svc_init_para_t m_init_param = {0};
  
  if(b_init)
    return ;
  m_init_param.nstksize = MDL_MONITOR_TASK_STKSIZE;
  m_init_param.service_prio = MDL_MONITOR_TASK_PRIORITY;
  
  dvb_monitor_service_init(&m_init_param);
  b_init = TRUE;
//#endif
}

RET_CODE ui_util_api_init(void)
{
  g_cd_utf8_to_utf16le      = iconv_open("ucs2le", "utf8");
  g_cd_utf16le_to_utf8      = iconv_open("utf8", "ucs2le");
  g_cd_gb2312_to_utf16le    = iconv_open("ucs2le", "euccn");
  g_cd_iso8859_9_to_utf16le = iconv_open("ucs2le", "iso8859_9");
  
  read_upgrade_need_keep_data();

  return SUCCESS;
}

void ui_string_conv(ui_str_conv_t conv_type, void *p_src, void *p_dest, int dest_buf_len)
{
	int src_len = 0;
	int dest_len = 0;
  iconv_t iconv_handle;

  memset(p_dest, 0, dest_buf_len);
  dest_len = dest_buf_len - 1; // null terminated

  if (p_src == NULL)
    return;
  
  switch(conv_type)
  {
    case GB2312_TO_UNICODE:
      iconv_handle = g_cd_gb2312_to_utf16le;
      src_len = strlen(p_src);
      break;
    case UTF8_TO_UNICODE:
      iconv_handle = g_cd_utf8_to_utf16le;
      src_len = strlen(p_src);
      break;
    case UNICODE_TO_UTF8:
      iconv_handle = g_cd_utf16le_to_utf8;
      src_len = uni_strlen(p_src)*2;
      break;
    default:
      OS_PRINTF("invalid conv_type %d\n", conv_type);
      return;
  }

	iconv(iconv_handle, (char**)&p_src, &src_len, (char**)&p_dest, &dest_len);
}

u8 ui_util_map_volume(u8 volume)
{
  u8 temp = volume;
  
  if(temp > AP_VOLUME_MAX)
    temp = AP_VOLUME_MAX;

  if(hal_get_chip_ic_id() == IC_SYMPHONY)
    return g_vol_step[temp];
  else
    return temp;
}

u8 ui_get_tuner_id_by_pg_id(u16 prog_id)
{
  dvbs_prog_node_t pg_node = {0};
  db_dvbs_ret_t db_ret = DB_DVBS_OK;
  sat_node_t sat_node = {0};

  if(prog_id != INVALIDID)
  {  
    db_ret =  db_dvbs_get_pg_by_id(prog_id, &pg_node);
    if (db_ret == DB_DVBS_OK)
    {
      db_ret = db_dvbs_get_sat_by_id(pg_node.sat_id, &sat_node);
      if (db_ret == DB_DVBS_OK)
        return sat_node.tuner_id;
    }
  }
  
  if ((ui_util_get_nim_caps()&(CLT_NIM_CAP_DVBC|CLT_NIM_CAP_DVBS)) != 0)
  {
    OS_PRINTF("#@ get tuner id for pg %d failed, default to TUNER1 for C+S hardware\n");
    return (u8)TUNER1;
  }
  
  return (u8)TUNER0;
}

u8 ui_util_get_tuner_id_by_sat_type(dvbs_sat_type_t sat_type)
{
  u8 i;

  for (i=0; i<g_tuner_num; i++)
  {
    mtos_printk("nim type %d\n", g_nim_cfg[i].nim_type);
    if (sat_type == SAT_TYPE_DVBC &&
      (g_nim_cfg[i].nim_type&CLT_NIM_CAP_DVBC) != 0)
    {
      return (u8)g_nim_cfg[i].tuner_id;
    }
    else if (sat_type == SAT_TYPE_DVBS &&
      (g_nim_cfg[i].nim_type&CLT_NIM_CAP_DVBS) != 0)
    {
      return (u8)g_nim_cfg[i].tuner_id;
    }
    if (sat_type == SAT_TYPE_DVBT &&
      (g_nim_cfg[i].nim_type&CLT_NIM_CAP_DVBT) != 0)
    {
      return (u8)g_nim_cfg[i].tuner_id;
    }
  }

  OS_PRINTF("#@ Cannot find tuner id for sat_type %d, default to TUNER0\n", sat_type);
  return (u8)TUNER0;
}

u8 ui_util_get_dmx_input_by_tuner_id(u8 tuner_id)
{
  u8 i;

  for (i=0; i<g_tuner_num; i++)
  {
    if (g_nim_cfg[i].tuner_id == tuner_id)
    {
      return (u8)g_nim_cfg[i].ts_input;
    }
  }

  OS_PRINTF("#@ Cannot find demux input for tuner_id %d, default to DMX_INPUT_EXTERN0\n", tuner_id);
  return (u8)DMX_INPUT_EXTERN0;
}

u32 ui_util_get_nim_caps(void)
{
  u8 i;
  static u32 nim_caps = 0;

  if (nim_caps != 0)
    return nim_caps;
  
  for (i=0; i<g_tuner_num; i++)
  {
    nim_caps |= g_nim_cfg[i].nim_type;
    APPLOGA("%s %d nim_caps %d nim_type %d \n",__FUNCTION__,__LINE__,nim_caps, g_nim_cfg[i].nim_type);
  }

  return nim_caps;
}

u32 ui_util_get_play_dmx_input(void)
{
  u8 tuner_id;

  tuner_id = ui_get_tuner_id_by_pg_id(sys_status_get_curn_group_curn_prog_id());
  return ui_util_get_dmx_input_by_tuner_id(tuner_id);    

  return DMX_INPUT_EXTERN0;
}

BOOL is_nim_support_signal(u8 signal)
{
  if (signal == SYS_DVBC)
  {
    return (ui_util_get_nim_caps() & CLT_NIM_CAP_DVBC);
  }
  else if (signal == SYS_DVBS)
  {
    return (ui_util_get_nim_caps() & CLT_NIM_CAP_DVBS);
  }
  else if (signal == SYS_DVBT2)
  {
    return (ui_util_get_nim_caps() & CLT_NIM_CAP_DVBT);
  }

  OS_PRINTF("@# Nim hardware not support signal %d\n", signal);
  return FALSE;
}

void read_upgrade_need_keep_data(void)
{
  u32 read_len = 0;
  memset(g_keep_hdcp, 0, sizeof(g_keep_hdcp));
  memset(g_keep_sn, 0, sizeof(g_keep_sn));
  memset(g_keep_macaddr, 0, sizeof(g_keep_macaddr));
  
  read_len = dm_read(class_get_handle_by_id(DM_CLASS_ID),
          IDENTITY_BLOCK_ID, 0, 0,
          sizeof(g_keep_sn),
          (u8 *)g_keep_sn);
  read_len = dm_read(class_get_handle_by_id(DM_CLASS_ID),
          HDCPKEY_BLOCK_ID, 0, 0,
          sizeof(g_keep_hdcp),
          (u8 *)g_keep_hdcp);
  read_len = dm_read(class_get_handle_by_id(DM_CLASS_ID),
          MACADDR_BLOCK_ID, 0, 0,
          sizeof(g_keep_macaddr),
          (u8 *)g_keep_macaddr);
  return;

}

void restore_upgrade_need_keep_data(void)
{
  charsto_device_t *p_charsto_dev = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_CHARSTO);
  u32 indentity_block_addr;
  u8 *p_buffer = mtos_malloc(CHARSTO_SECTOR_SIZE);
  spi_prot_block_type_t prt_t = PRT_UNPROT_ALL;
  prt_t = snf_protect_get( );
  snf_protect_set(PRT_UNPROT_ALL);
  memset(p_buffer, 0, sizeof(p_buffer));
  if(strlen(g_keep_sn))
  {
    indentity_block_addr = mul_ota_dm_api_get_block_addr(IDENTITY_BLOCK_ID);
    if(charsto_read(p_charsto_dev, (indentity_block_addr / 0x10000 * 0x10000), p_buffer, CHARSTO_SECTOR_SIZE) == SUCCESS)
    {
      memcpy(p_buffer + (indentity_block_addr % 0x10000), g_keep_sn, sizeof(g_keep_sn));
      if(charsto_erase(p_charsto_dev, (indentity_block_addr / 0x10000 * 0x10000), 1) == SUCCESS)
      {
        charsto_writeonly(p_charsto_dev, (indentity_block_addr / 0x10000 * 0x10000), p_buffer, CHARSTO_SECTOR_SIZE);
      }
    }    
    memset(p_buffer, 0, sizeof(p_buffer));
  }
  if(strlen(g_keep_hdcp))
  {
    indentity_block_addr = mul_ota_dm_api_get_block_addr(HDCPKEY_BLOCK_ID);
    if(charsto_read(p_charsto_dev, (indentity_block_addr / 0x10000 * 0x10000), p_buffer, CHARSTO_SECTOR_SIZE) == SUCCESS)
    {
      memcpy(p_buffer + (indentity_block_addr % 0x10000), g_keep_hdcp, sizeof(g_keep_hdcp));
      if(charsto_erase(p_charsto_dev, (indentity_block_addr / 0x10000 * 0x10000), 1) == SUCCESS)
      {
        charsto_writeonly(p_charsto_dev, (indentity_block_addr / 0x10000 * 0x10000), p_buffer, CHARSTO_SECTOR_SIZE);
      }
    }
    memset(p_buffer, 0, sizeof(p_buffer));
  }
  if(strlen(g_keep_macaddr))
  {
    indentity_block_addr = mul_ota_dm_api_get_block_addr(MACADDR_BLOCK_ID);
    if(charsto_read(p_charsto_dev, (indentity_block_addr / 0x10000 * 0x10000), p_buffer, CHARSTO_SECTOR_SIZE) == SUCCESS)
    {
      memcpy(p_buffer + (indentity_block_addr % 0x10000), g_keep_macaddr, sizeof(g_keep_macaddr));
      if(charsto_erase(p_charsto_dev, (indentity_block_addr / 0x10000 * 0x10000), 1) == SUCCESS)
      {
        charsto_writeonly(p_charsto_dev, (indentity_block_addr / 0x10000 * 0x10000), p_buffer, CHARSTO_SECTOR_SIZE);
      }
    }    
    memset(p_buffer, 0, sizeof(p_buffer));
  }
    //restore
  snf_protect_set(prt_t);
  mtos_free(p_buffer);

}

u32 on_preset_prog_for_ae_test(void)
{
  u16 view_id;
  sat_node_t sat_node = {0};
  extern void categories_mem_reset(void);
  extern BOOL _ui_get_sat_by_sat_type(sat_node_t *p_sat_node, dvbs_sat_type_t sat_type);
  extern void db_previosly_program_for_ae(u32 sat_id);
  
  OS_PRINTF(">>> ENTRY on_preset_prog_for_ae_test\n");
  ui_stop_play(STOP_PLAY_BLACK, TRUE);
  //set default
  db_dvbs_restore_to_factory(PRESET_BLOCK_ID,0);
  sys_status_load();

  categories_mem_reset();
  sys_status_check_group();
  sys_status_save();

  // set environment according ss_data
  sys_status_reload_environment_setting(TRUE);
  //sys_static_data_restore_factory();

  _ui_get_sat_by_sat_type(&sat_node, SAT_TYPE_DVBC);
  db_previosly_program_for_ae(sat_node.id);
  OS_PRINTF(">>> LEAVE on_preset_prog_for_ae_test\n");

  //create a new view after load default, and save the new view id.
  view_id = ui_dbase_create_view(DB_DVBS_ALL_TV, 0, NULL);
  ui_dbase_set_pg_view_id((u8)view_id);

  ui_dbase_reset_last_prog(FALSE);
  return SUCCESS;
}
#if 0
nim_tuner_id_t g_cur_nim_tuner_id = TUNER0;

void ui_util_set_full_speed_nim_by_id(nim_tuner_id_t tuner_id)
{
  void *p_tuner0_handle = NULL;
  void *p_tuner1_handle = NULL;
#ifdef WIN32
  return;
#else
  if(g_cur_nim_tuner_id != tuner_id)
  {  
    p_tuner0_handle = dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_NIM);

    MT_ASSERT(p_tuner0_handle != NULL);

    p_tuner1_handle = dev_find_identifier(p_tuner0_handle, DEV_IDT_TYPE, SYS_DEV_TYPE_NIM);

    if(p_tuner1_handle == NULL)
    {
      OS_PRINTF("%s, Single Tuner\n",__FUNCTION__);
      
      return ;
    }

    switch(tuner_id)
    {
      case TUNER0:
        OS_PRINTF("%s, Enable tuner0, Disable tuner1\n",__FUNCTION__);
        dev_io_ctrl(p_tuner0_handle, DEV_IOCTRL_POWER, DEV_POWER_FULLSPEED);
        dev_io_ctrl(p_tuner1_handle, DEV_IOCTRL_POWER, DEV_POWER_TUNER_SLEEP);
        break;

      case TUNER1:
        OS_PRINTF("%s, Enable tuner1, Disable tuner0\n",__FUNCTION__);
        dev_io_ctrl(p_tuner0_handle, DEV_IOCTRL_POWER, DEV_POWER_SLEEP);
        dev_io_ctrl(p_tuner1_handle, DEV_IOCTRL_POWER, DEV_POWER_TUNER_FULLSPEED);
        break;

      default:
        MT_ASSERT(0);
        break;
    }
    
    g_cur_nim_tuner_id = tuner_id;
  }
#endif //WIN32
}
#endif

