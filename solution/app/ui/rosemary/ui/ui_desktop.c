/********************************************************************************************/
/* Copyright (c) 2014 Montage Technology Group Limited and its affiliated companies         */
/* Montage Proprietary and Confidential                                                     */
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/********************************************************************************************/
#include "ui_common.h"
#include "client_common.h"
#include "ui_hw_timer.h"
#include "ui_small_list.h"
#include "ui_infor_bar.h"
#include "ui_volume.h"
#include "ui_notify.h"
#include "ui_menu_manager.h"
#include "ui_auto_search.h"
#include "ui_do_search.h"
#include "ui_mainmenu.h"
#include "ui_num_play.h"
#include "ui_pvr_rec_bar.h"
#include "ui_fav_list.h"
#include "ui_menu_data.h"
#include "ui_timeshift.h"
#include "ui_pop_msg.h"
#include "ui_ca_public.h" 
#include "ui_upgrade_by_usb.h"
#include "ui_subtitle.h"
#include "ui_menu_data.h"

extern RET_CODE open_mute (u32 para1, u32 para2);
extern RET_CODE open_pause (u32 para1, u32 para2);

RET_CODE open_language_setting (u32 para1, u32 para2);
RET_CODE ui_auto_search_boot_up(u32 para1, u32 para2);

u16 ui_desktop_keymap_on_normal(u16 key);
RET_CODE ui_desktop_proc_on_normal(control_t *p_ctrl, u16 msg, u32 para1, u32 para2);
static u16 sys_evtmap(u32 event);
static void on_hw_timer_arrived(void);

extern void fw_set_post_process_msg_cb(void *post_proc_msg_cb);
static BOOL g_usb_status = FALSE; //FALSE: USB OUT; TRUE:USB IN
static BOOL g_desktop_start = FALSE;
static u16  g_roll_timer_cnt = 0;
static BOOL g_is_timer_rolling = FALSE;
static BOOL g_is_timer_rolling_paused = FALSE;
static BOOL show_ask_dlg = FALSE;
static BOOL net_show_ask_dlg = FALSE;
static BOOL usb_show_ask_dlg = FALSE;
static  clt_network_dev_type_t net_type = CLT_NETDEV_LAN;
static BOOL is_already_prompt = FALSE;
#ifdef NIT_SOFTWARE_UPDATE
BOOL is_already_prompt_ota = FALSE;
static BOOL g_nit_ota_check = FALSE;
clt_ota_info_t update_info = {0};
#endif

static RET_CODE on_start_record(control_t *p_ctrl, u16 msg,
                                u32 para1, u32 para2);
u16 ui_desktop_keymap_on_ttx(u16 key);

RET_CODE ui_desktop_proc_on_ttx(control_t *p_ctrl, u16 msg, u32 para1, u32 para2);

static void ui_ext_msg_cb(u32 event, u32 para1, u32 para2)
{
  clt_event_t evt;

  evt.id = event;
  evt.data1 = para1;
  evt.data2 = para2;

  client_spend_ap_frm_msg(g_p_client, &evt);

}

void ui_set_usb_status(BOOL b)
{
  g_usb_status = b;
  mtos_printk("ui_set_usb_status   %d\n", b);
}


BOOL ui_get_usb_status(void)
{
  mtos_printk("ui_get_usb_status   %d\n", g_usb_status);
  #ifdef WIN32
  return TRUE;
  #else
  return g_usb_status;
  #endif
}

static void ui_jump_to_usbupg_menu(void)
{
  client_ads_stop_ads(g_p_client);
  manage_open_menu(ROOT_ID_UPGRADE_BY_USB, 0, 1);
}

static void ui_post_msg_cb(u32 event, u32 para1, u32 para2)
{
  client_post_msg_proc(g_p_client, event, para1, para2);
}

static BOOL menu_open_conflict(control_t *p_curn, u16 new_root)
{
  u16 ctrl_id = ctrl_get_ctrl_id(p_curn);

  if(fw_find_root_by_id(new_root) != NULL)   //this menu has been opened
  {
    return FALSE;
  }

  if(ctrl_id == ROOT_ID_BACKGROUND)
  {
    return TRUE;
  }

  if(ui_menu_data_api_is_fscreen_menu(ctrl_get_ctrl_id(p_curn)))
  {
    if(ctrl_process_msg(p_curn, MSG_EXIT, 0, 0) != SUCCESS)
    {
      fw_destroy_mainwin_by_root(p_curn);
    }
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}

void ui_desktop_init(void)
{
  fw_config_t config =
  {
    {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT},                  /* background */

    (u32)DST_IDLE_TMOUT,                                       /* timeout of idle */
    RECEIVE_MSG_TMOUT,                                    /* timeout of recieving msg */
    POST_MSG_TMOUT,                                       /* timeout of sending msg */

    ROOT_ID_BACKGROUND,                                   /* background root_id */
    MAX_ROOT_CONT_CNT,
    MAX_MESSAGE_CNT,
    MAX_HOST_CNT,
    MAX_TMR_CNT,

    RSI_TRANSPARENCY,
    AP_ID_FRAMEWORK,
    AP_ID_LAST,
  };

  manage_enable_autoswitch(FALSE);
  fw_init(&config,
          ui_desktop_keymap_on_normal,
          ui_desktop_proc_on_normal,
          ui_menu_manage,
          ui_ext_msg_cb);
  manage_enable_autoswitch(TRUE);
  //manage_init();

  fw_set_post_process_msg_cb(ui_post_msg_cb);

  fw_register_ap_evtmap(AP_ID_FRAMEWORK, sys_evtmap);

  ui_create_hw_timer(20, on_hw_timer_arrived);
}

void ui_desktop_release(void)
{
  fw_release();
}

BOOL ui_is_desktop_start(void)
{
  return g_desktop_start;
}

void ui_desktop_start(void)
{
  int cur_pg_index;
  clt_startup_param_t start_para;
  int g_total_pgcount = 0;
  clt_dbase_get_count_para_t para = {{0}};

  OS_PRINTF("[%s] line [%d]\n",__FUNCTION__,__LINE__);  

  /*comm_dlg_data_t dlg_data =
  {
    0,
    DLG_FOR_SHOW | DLG_STR_MODE_STATIC,
    COMM_DLG_X, COMM_DLG_Y, COMM_DLG_W, COMM_DLG_H,
  };*/
  client_get_startup_param(g_p_client, &start_para);
  if (start_para.mode == CLT_STARTUP_PLAY)
  {
    cur_pg_index =  client_dbase_get_pos(g_p_client,"curn_view");
    strncpy(para.view_name, "curn_view", sizeof(para.view_name));
    g_total_pgcount = client_dbase_get_count(g_p_client,&para);
    if(g_total_pgcount > 0)
    {
      open_infor_bar(0, 0);
    }
  }
  else if (start_para.mode  == CLT_STARTUP_AUTO_SCAN)
  {
    if(TEST_UI_MODE(DVBT))
    {
      manage_open_menu(ROOT_ID_DVBT_MAINMENU, 0, 0);
    }
    else if(TEST_UI_MODE(DVBC))
    {
      OS_PRINTF("open_language_setting start line [%d]\n",__LINE__);
	  open_language_setting (0, 0);
      OS_PRINTF("open_language_setting end line [%d]\n",__LINE__);
      //ui_auto_search_boot_up(0,0);
    }
    else if(TEST_UI_MODE(DVBS))
    {
      //todo ui_start_dvbs_auto_search(); 
    }
  } 
  
  ui_ca_open_ca_rolling_menu(0,0);
  g_desktop_start = TRUE;

  if(show_ask_dlg)
   {

       rect_t upgrade_dlg_rc =
       {
       ((SCREEN_WIDTH - 400) / 2),
       ((SCREEN_HEIGHT - 200) / 2),
       ((SCREEN_WIDTH - 400) / 2) + 400,
       ((SCREEN_HEIGHT - 200) / 2) + 200,
       };
       if(ROOT_ID_DO_SEARCH != fw_get_focus_id())
       {
           ui_comm_ask_for_dodlg_open(&upgrade_dlg_rc, IDS_USB_UPG_FILE_EXIST,
                              ui_jump_to_usbupg_menu,
                              NULL, 0);
       }
       show_ask_dlg = FALSE;
   }
    if(usb_show_ask_dlg)
   {
      ui_comm_cfmdlg_open(NULL, IDS_USB_STORAGE_CONNECT, NULL, 2000);
      usb_show_ask_dlg = FALSE;
   }
  if(net_show_ask_dlg)
   {
    if(net_type == CLT_NETDEV_WLAN)
    {
      if(fw_find_root_by_id(ROOT_ID_COMM_DLG) != NULL)
      {
          ui_comm_dlg_close();
      }

      //dlg_data.content = IDS_WIFI_PLUG_IN;
      //dlg_data.dlg_tmout = 2000;
      ui_comm_cfmdlg_open(NULL, IDS_WIFI_PLUG_IN, NULL, 2000);
    }else if(net_type == CLT_NETDEV_LAN){
      if(fw_find_root_by_id(ROOT_ID_COMM_DLG) != NULL)
      {
          ui_comm_dlg_close();
      }

      ui_comm_cfmdlg_open(NULL, IDS_NET_CABLE_PLUG_IN, NULL, 2000);
    }
     net_show_ask_dlg = FALSE;
   }

}

static RET_CODE ui_desktop_open_menu(control_t *p_list,
  u16 msg, u32 para1, u32 para2)
{
  u32 vkey = para1;
  u16 new_root = (msg & 0x7FF);
  menu_attr_t *menu_attr_tmp = NULL;

  OS_PRINTF("BKS [%s] line [%d]\n",__FUNCTION__,__LINE__);
  if (! g_desktop_start)
  {
    OS_PRINTF("desktop not start, cannot open menu\n");
    return SUCCESS;
  }
  
  /*if(ui_timeshift_switch_get())
  {
     return SUCCESS;
  }*/

  if(fw_find_root_by_id(ROOT_ID_TIMESHIFT))  
  {
     return SUCCESS;
  }
  
  if(menu_open_conflict(fw_get_focus(), new_root))
  {
    OS_PRINTF("BKS new_root [0x%x]\n",new_root);
    switch(new_root)
    {
    case ROOT_ID_MAINMENU:

    case ROOT_ID_FAV_LIST:
    case ROOT_ID_EPG:
    
    break;
    case ROOT_ID_MOVIE:
    case ROOT_ID_MUSIC:
      if(!(ui_get_usb_status()))
      {
        ui_comm_cfmdlg_open(NULL, IDS_PLEASE_CONNECT_USB, NULL, 2000);
        return ERR_NOFEATURE;
      }
    break;
    case ROOT_ID_SMALL_LIST:
     break;
    case ROOT_ID_AUDIO_SETTING:
    menu_attr_tmp = _ui_menu_data_api_get_menu_by_id(ROOT_ID_AUDIO_SETTING);
    menu_attr_tmp->play_state = PS_PLAY;
	
    _ui_menu_data_api_set_menu_by_id(ROOT_ID_AUDIO_SETTING,menu_attr_tmp);
    default:
    break;
    }
#ifndef SUPPORT_CHANGHONG_ADS
    client_ads_stop_ads(g_p_client);
#endif
    //if(!ui_recorder_isrecording())
    {
      if(TEST_UI_MODE(DVBT))
      {
        if(new_root == ROOT_ID_MAINMENU)
        {
          new_root = ROOT_ID_DVBT_MAINMENU;
        }
      }
      return manage_open_menu(new_root, vkey, 0);
    }
  }
  return SUCCESS;
}

void ui_desktop_main(void)
{
  OS_PRINTF("ui enter fw mainwin loop!\n");
  fw_default_mainwin_loop(ROOT_ID_BACKGROUND);
}

static RET_CODE on_ethernet_plug_in(control_t *p_ctrl, u16 msg,
                           u32 para1, u32 para2)
{
  /*comm_dlg_data_t dlg_data =
  {
    0,
    DLG_FOR_SHOW | DLG_STR_MODE_STATIC,
    COMM_DLG_X, COMM_DLG_Y, COMM_DLG_W, COMM_DLG_H,
  };*/
  net_type = para1;
  if (g_desktop_start)
  {
    if(net_type == CLT_NETDEV_WLAN)
    {     
      if(fw_find_root_by_id(ROOT_ID_COMM_DLG) != NULL)
      {
          ui_comm_dlg_close();
      }
      //dlg_data.content = IDS_WIFI_PLUG_IN;
      ui_comm_cfmdlg_open(NULL, IDS_WIFI_PLUG_IN, NULL, 2000);
    }else if(net_type == CLT_NETDEV_LAN){
      if(fw_find_root_by_id(ROOT_ID_COMM_DLG) != NULL)
      {
          ui_comm_dlg_close();
      }

      ui_comm_cfmdlg_open(NULL, IDS_NET_CABLE_PLUG_IN, NULL, 2000);
    }
  }
  else
  {
    net_show_ask_dlg = TRUE;
  }
  return SUCCESS;
}

static RET_CODE on_ethernet_plug_out(control_t *p_ctrl, u16 msg,
                           u32 para1, u32 para2)
{
  comm_dlg_data_t dlg_data =
  {
    0,
    DLG_FOR_SHOW | DLG_STR_MODE_STATIC,
    COMM_DLG_X, COMM_DLG_Y, COMM_DLG_W, COMM_DLG_H,
  };

  if(NULL != fw_find_root_by_id(ROOT_ID_COMM_DLG))
  {
    manage_close_menu(ROOT_ID_COMM_DLG, 0, 0);
  }
  if(NULL != fw_find_root_by_id(ROOT_ID_KEYBOARD))
  {
    manage_close_menu(ROOT_ID_KEYBOARD, 0, 0);
  }
  if((fw_get_focus_id() == ROOT_ID_SMALL_LIST) ||
    (fw_get_focus_id() == ROOT_ID_INFOR_BAR) ||
    (fw_get_focus_id() == ROOT_ID_BACKGROUND))
  {      
      dlg_data.content = (para1 == CLT_NETDEV_LAN) ? IDS_NET_CABLE_PLUG_OUT : IDS_WIFI_PLUG_OUT;
      dlg_data.dlg_tmout = 2000;
      ui_comm_dlg_close();
      if(fw_find_root_by_id(ROOT_ID_COMM_DLG) == NULL)
      {
        ui_comm_dlg_open(&dlg_data);
      }
  }
  
  return SUCCESS;
}

static RET_CODE on_find_upg_file(control_t *p_ctrl, u16 msg,
                           u32 para1, u32 para2)
{
  rect_t upgrade_dlg_rc =
  {
    ((SCREEN_WIDTH - 400) / 2),
    ((SCREEN_HEIGHT - 200) / 2),
    ((SCREEN_WIDTH - 400) / 2) + 400,
    ((SCREEN_HEIGHT - 200) / 2) + 200,
  };
  clt_timeshift_partition_info_t partition_info;
  if((ROOT_ID_DO_SEARCH != fw_get_focus_id())&&(ROOT_ID_COMM_DLG!= fw_get_focus_id())&&(ROOT_ID_UPGRADE_BY_USB!= fw_get_focus_id()))
  {
  	if(g_desktop_start)
  	{
  		ui_comm_ask_for_dodlg_open(&upgrade_dlg_rc, IDS_USB_UPG_FILE_EXIST,
  		                         ui_jump_to_usbupg_menu,
  		                         NULL, 0);
  	}
  	else
  	{
  	  show_ask_dlg = TRUE;
  	}

  client_get_partition_info(g_p_client,&partition_info);
  }
  return SUCCESS;
}

static RET_CODE on_find_usb_plug_in(control_t *p_ctrl, u16 msg,
                           u32 para1, u32 para2)
{
  if(g_desktop_start)
  {
    ui_comm_cfmdlg_open(NULL, IDS_USB_STORAGE_CONNECT, NULL, 2000);
  }
  else
  {
    usb_show_ask_dlg = TRUE;
  }
  
  return SUCCESS;
}


static RET_CODE on_usb_plug_in(control_t *p_ctrl, u16 msg,
                           u32 para1, u32 para2)
{
  ui_set_usb_status(TRUE);
  #if 0
  ui_comm_cfmdlg_open(NULL, IDS_USB_STORAGE_CONNECT, NULL, 2000);
  #else
  //client_check_usb_upg_file(g_p_client);
  #endif
  return ERR_FAILURE;
}

static RET_CODE ui_exit_usb_upg(u16 root_id)
{
  control_t *p_cont = NULL;
  u32 tmp_status = 0;

  switch(root_id)
  {
    case ROOT_ID_UPGRADE_BY_USB:
      tmp_status = ui_usb_upgade_sts();
      if((UI_USB_UPG_IDEL != tmp_status)
        && (UI_USB_UPG_LOAD_END != tmp_status))
      {
        return ERR_FAILURE;
      }
      break;
    default:
      return ERR_FAILURE;
  }

  p_cont = fw_find_root_by_id(root_id);
  if(NULL != p_cont)
  {
    ctrl_process_msg(p_cont, MSG_UPG_QUIT, 0, 0);
  }
  return SUCCESS;
}


static RET_CODE on_usb_plug_out(control_t *p_ctrl, u16 msg,
                           u32 para1, u32 para2)
{
  u16 root_id = fw_get_focus_id();
  ui_set_usb_status(FALSE);

  switch(root_id)
  {
    case ROOT_ID_COMM_DLG:
      manage_close_menu(ROOT_ID_COMM_DLG,0,0);
      break;
    case ROOT_ID_UPGRADE_BY_USB:
      ui_exit_usb_upg(root_id);
      break;
    default:
      break;
  }

  ui_comm_cfmdlg_open(NULL, IDS_USB_DISCONNECT, NULL, 2000);

  return ERR_FAILURE;
}

static RET_CODE on_tv_radio_function(control_t *p_cont, u16 msg, u32 para1, u32 para2)
{
  u16 content;
  clt_switch_tvradio_para_t trparam;
  u16 curn_root_id = fw_get_focus_id();
  
  if(curn_root_id == ROOT_ID_TIMESHIFT)  
  {
     return SUCCESS;
  }
  
  if(ui_menu_data_api_is_fscreen_menu(curn_root_id))
  {
    if(client_channel_switch_tv_radio(g_p_client, &trparam) == SUCCESS)
    {
       open_infor_bar(0, 0 );
    }
    else
    {
       content = (u16)((trparam.mode_switched == 1) ? IDS_MSG_NO_TV_PROG : IDS_MSG_NO_RADIO_PROG);
       ui_comm_cfmdlg_open(NULL, content, NULL, 0);
    }
  }

  return SUCCESS;
}

static void get_book_str(u16 *str, u16 *pg_name, u16 *event_name, u16 max_len)
{
  u16 len;
  u16 temp_uni_str[2] = {0x003a, 0};
  len = 0, str[0] = '\0';
  gui_get_string(IDS_BOOKPG_START, str, max_len);

  ui_uni_strcat(str, pg_name, max_len);

  ui_uni_strcat(str, temp_uni_str, max_len);//add colon
  ui_uni_strcat(str, event_name, max_len);
  len = (u16)ui_uni_strlen(str);
  gui_get_string(IDS_BOOKPG_START_1, &str[len], (u16)(max_len - len));

}

static RET_CODE on_time_update(control_t *p_ctrl, u16 msg,
                            u32 para1, u32 para2)
{
  int book_pos;
  int ret;
  comm_dlg_data_t dlg_data = //book prog play,popup msg
  {
    ROOT_ID_INVALID,
    DLG_FOR_ASK | DLG_STR_MODE_EXTSTR,
    COMM_DLG_X, COMM_DLG_Y,
    (COMM_DLG_W + 100), COMM_DLG_WITHTEXT_H,
    0,
    60000,
    0,
  };
  comm_dlg_data_t sleep_notify = //book prog play,popup msg
  {
    ROOT_ID_INVALID,
    DLG_FOR_ASK | DLG_STR_MODE_STATIC,
    COMM_DLG_X, COMM_DLG_Y,
    (COMM_DLG_W + 100), COMM_DLG_WITHTEXT_H,
    0,
    10000,
    0,
  };
  
  dlg_ret_t dlg_ret = DLG_RET_NULL;
  clt_coming_book_t book;
  static BOOL is_dlg_open = FALSE;

  if (is_dlg_open)
    return SUCCESS;
  
  book.event_name[0] = 0;
  book.pg_name[0] = 0;
  book_pos = client_get_coming_book(g_p_client, &book);
  if (book_pos >= 0)
  {
    u16 content[64 + 1];
    
    get_book_str(content, book.pg_name, book.event_name, 64);
    
    is_dlg_open = TRUE;
    dlg_data.content = (u32)content;
    dlg_ret = ui_comm_dlg_open2(&dlg_data);
    
    is_dlg_open = FALSE;
    if(dlg_ret == DLG_RET_YES)
    {
      manage_close_all_menus();
      ui_play_book_prog(book_pos);

      if (book.record_enable)
      {
        on_start_record(NULL,0,(u32)&book.record_duration,book.record_enable);
      }
      else
      {
        if(fw_find_root_by_id(ROOT_ID_INFOR_BAR) == NULL)
        {
          open_infor_bar(0, 0);
        }
      }
    }
    else if(dlg_ret == DLG_RET_NO)
    {
      client_delete_book_by_pos(g_p_client, book_pos);
    }
  }

  ret = client_get_auto_power_off_status(g_p_client);
  if(ret == TRUE)
  {
    //Create a one minute timer for power off.
    is_dlg_open = TRUE;
    dlg_ret = ui_comm_dlg_open2(&sleep_notify);
    is_dlg_open = FALSE;
    if(dlg_ret == DLG_RET_YES)
    {
      fw_tmr_create(ROOT_ID_BACKGROUND, MSG_POWER_OFF, 1000, FALSE);
    }
    else if(dlg_ret == DLG_RET_NO)
    {
      return SUCCESS;
    }
  }

  return SUCCESS;
}

static RET_CODE on_lnb_short(control_t *p_ctrl, u16 msg,
                            u32 para1, u32 para2)
{
  clt_setting_scan_param_t scan_param;
  mtos_printk("[%s, %d] antenna proting, show warning\n", __FUNCTION__, __LINE__);
  client_setting_get_scan_param(g_p_client, &scan_param);
  scan_param.antenna_state = 0;
  client_setting_set_scan_param(g_p_client, &scan_param);
  
  ui_comm_cfmdlg_open(NULL, IDS_OVER_LOAD, NULL, 3000);
  return SUCCESS;
}

static RET_CODE on_ota_reboot_into_ota(control_t *p_ctrl, u16 msg,
                              u32 para1, u32 para2)
{
  if (g_desktop_start)
    return SUCCESS;

  client_stop_ota_check(g_p_client, FALSE);

#ifdef WIN32
  OS_PRINTF("OTA check over!please run ota app\n");
  MT_ASSERT(0);
#else
  client_reboot_system(g_p_client);
#endif

  return SUCCESS;
}

static RET_CODE on_ota_timeout_start_play(control_t *p_ctrl, u16 msg,
                               u32 para1, u32 para2)
{
  if (g_desktop_start)
    return SUCCESS;
  client_cas_set_uio_status(g_p_client,FALSE);

  OS_PRINTF("###debug ota timeout,it will play2\n");
  client_stop_ota_check(g_p_client, FALSE);
  ui_desktop_start();

  return SUCCESS;
}

static RET_CODE on_start_record(control_t *p_ctrl, u16 msg,
                                u32 para1, u32 para2)
{
  u16 focus_id = fw_get_focus_id();
  if(!ui_get_usb_status() && ui_menu_data_api_is_fscreen_menu(focus_id))
  {
    ui_comm_cfmdlg_open(NULL, IDS_USB_DISCONNECT, NULL, 2000);
    return SUCCESS;
  }

  if((focus_id != ROOT_ID_BACKGROUND) && (focus_id != ROOT_ID_INFOR_BAR))
  {
     return SUCCESS;
   }
  /*if(!ui_is_pass_chkpwd(prog_id))
  {
    ui_comm_cfmdlg_open(NULL, IDS_NOT_AVAILABLE, NULL, 2000);
    return ERR_FAILURE;
  }*/

  manage_open_menu(ROOT_ID_PVR_REC_BAR, para1, para2);
  return SUCCESS;
}

static RET_CODE on_check_signal(control_t *p_ctrl, u16 msg,
                                u32 para1, u32 para2)
{
  //ui_signal_check(para1, para2);
  return SUCCESS;
}

static RET_CODE on_close_menu(control_t *p_ctrl, u16 msg,
                              u32 para1, u32 para2)
{
  manage_autoclose();
  return SUCCESS;
}

static RET_CODE on_mute(control_t *p_ctrl, u16 msg,
                        u32 para1, u32 para2)
{
  BOOL is_mute = FALSE;
  control_t *p_win  = NULL;

  p_win = fw_find_root_by_id(ROOT_ID_MUTE);

  client_setting_get_mute(g_p_client, &is_mute);

  client_setting_set_mute(g_p_client, !is_mute);

  if(is_mute)
  {
    if(NULL != p_win)
    {
      fw_destroy_mainwin_by_id(ROOT_ID_MUTE);
    }
  }
  else
  {
    open_mute(0, 0);
  }
  
  return SUCCESS;
}

static RET_CODE on_pause(control_t *p_ctrl, u16 msg,
                        u32 para1, u32 para2)
{
  BOOL is_pause = FALSE;
  BOOL is_usb_plug_in = FALSE;
  control_t *p_win  = NULL;
  u16 focus_id = 0;
  clt_get_hdd_info_t param ={0};
  u16 timeshift_onoff = 0;

  if(ui_pop_msg_get_msg_strid() == IDS_MSG_NO_SIGNAL
   ||ui_pop_msg_get_msg_strid() == IDS_LOCK
   ||ui_pop_msg_get_msg_strid() == IDS_PARENTAL_LOCK
   ||ui_pop_msg_get_msg_strid() == IDS_MSG_NO_PROG)
  {
    return SUCCESS;
  }

  
  focus_id = fw_get_focus_id();
  if(!ui_menu_data_api_is_fscreen_menu(focus_id))
  {
    return SUCCESS;
  }
  
  p_win = fw_find_root_by_id(ROOT_ID_PAUSE);

  client_get_hdd_info(g_p_client, &param);
  timeshift_onoff = param.timeshift_switch_onoff;

  client_setting_get_pause(g_p_client, &is_pause);
  is_usb_plug_in = ui_get_usb_status();

  //usb is not plug in or event if usb plug in, but cur is pause
  if((!is_usb_plug_in) || (is_usb_plug_in & is_pause) || (is_usb_plug_in&& (!timeshift_onoff)))
  {
    client_setting_set_pause(g_p_client, !is_pause);
  
    if(is_pause)
    {
      if(NULL != p_win)
      {
        fw_destroy_mainwin_by_id(ROOT_ID_PAUSE);
      }
    }
    else
    {
      open_pause(0, 0);
    }
  }
  else //do timeshift
  {
    char sel_str[128];
    char fmt_str[64];
    u32 tv_flag;
    snprintf(sel_str, sizeof(sel_str), "select tv_flag, from curn_view where limit 1 pos %d", client_dbase_get_pos(g_p_client,"curn_view"));
    snprintf(fmt_str, sizeof(fmt_str), "%%d");
    client_dbase_select(g_p_client, sel_str, fmt_str, &tv_flag);

    if(tv_flag == 1)
    {
      manage_close_menu(ROOT_ID_INFOR_BAR, 0, 0);
      manage_open_menu(ROOT_ID_TIMESHIFT, 0, 0);
    }
    else
    {
      ui_comm_cfmdlg_open(NULL, IDS_TIMESHIFT_RADIO_PRO, NULL, 2000);
      return ERR_NOFEATURE;
    }
  }
  
  return SUCCESS;
}

static RET_CODE on_power_off(control_t *p_ctrl, u16 msg,
                             u32 para1, u32 para2)
{
  switch(fw_get_focus_id())
  {
    case ROOT_ID_DO_SEARCH:
      /* ask for enter standby on prog scan */
      if(!do_search_is_finish())
      {
        comm_dlg_data_t dlg_data = {0};
        dlg_ret_t ret;

        client_pause_scan(g_p_client);

        // opend dlg
        dlg_data.x = COMM_DLG_X, dlg_data.y = COMM_DLG_Y;
        dlg_data.w = COMM_DLG_W, dlg_data.h = COMM_DLG_H;
        dlg_data.style = DLG_FOR_ASK | DLG_STR_MODE_STATIC;
        dlg_data.content = IDS_MSG_ASK_FOR_STANDBY;

        ret = ui_comm_dlg_open(&dlg_data);

        if(ret == DLG_RET_NO)
        {
          client_resume_scan(g_p_client);
          return SUCCESS;
        }
        else
        {
          client_stop_scan(g_p_client);
        }
        gdi_set_enable(FALSE);
      }
      break;
    case ROOT_ID_COMM_DLG:
      if(!do_search_is_stop())
      {
        client_stop_scan(g_p_client);
      }
      if(fw_find_root_by_id(ROOT_ID_PVR_REC_BAR))
      {
        fw_destroy_all_mainwin(FALSE);
        mtos_task_delay_ms(100);
      }
      break;
    case ROOT_ID_PVR_REC_BAR:
      {
        fw_destroy_all_mainwin(FALSE);
        mtos_task_delay_ms(100);
        break;
      }
    case ROOT_ID_DVBS_OTA_UPGRADE:      
      return SUCCESS;
    default:
      break;
  }
  client_power_off(g_p_client);
  return SUCCESS;
}

static RET_CODE on_desktop_signal_update(control_t *p_cont, u16 msg, u32 para1, u32 para2)
{
  clt_signal_data_t *p_data = (clt_signal_data_t *)(para1);

  if(!ui_check_signal_info(p_data))
  {
    return SUCCESS;
  }
  
  if(p_data->update_flag & CLT_SIGNAL_UPDATE_FL_LOCK)
  {
    OS_PRINTF("@@@@on_desktop_signal_update lock %d \n",p_data->lock);
    if(p_data->lock == FALSE)
    {
      ui_pop_msg_set_msg_strid(IDS_MSG_NO_SIGNAL);
    }
    else
    {
      ui_pop_msg_clear_signal_msg();
    }
    ui_pop_msg_refresh_msg();
  }
  
  return SUCCESS;
}

static RET_CODE on_desktop_signal_turning(control_t *p_cont, u16 msg, u32 para1, u32 para2)
{
  comm_dlg_data_t dlg_data =
  {
    0,
    DLG_FOR_CONFIRM | DLG_STR_MODE_STATIC,
    COMM_DLG_X, COMM_DLG_Y, COMM_DLG_W, COMM_DLG_H,
    IDS_MSG_MOVING_DISH,
    5000,
    0,
  };
  ui_comm_dlg_open(&dlg_data);
  return SUCCESS;
}

static RET_CODE on_desktop_signal_out_range(control_t *p_cont, u16 msg, u32 para1, u32 para2)
{
  comm_dlg_data_t dlg_data =
  {
    0,
    DLG_FOR_CONFIRM | DLG_STR_MODE_STATIC,
    COMM_DLG_X, COMM_DLG_Y, COMM_DLG_W, COMM_DLG_H,
    IDS_MSG_OUT_OF_RANGE,
    5000,
    0,
  };
  ui_comm_dlg_open(&dlg_data);
  return SUCCESS;
}

void ui_enable_timer_rolling(BOOL enable)
{
  g_is_timer_rolling = enable;
}

BOOL ui_is_timer_rolling(void)
{
  return g_is_timer_rolling;
}

void ui_pause_timer_rolling(void)
{
  g_is_timer_rolling_paused = TRUE;
}

void ui_resume_timer_rolling(void)
{
  g_is_timer_rolling_paused = FALSE;
}

static void on_timer_rolling_dec(void)
{
  if(g_roll_timer_cnt != 0)
  {
    g_roll_timer_cnt --;
  }
}

static void on_timer_rolling_inc(void)
{
  static u32 ticks_count = 0;
  static u32 rolling_ticks = 0;
  extern RET_CODE fw_post_tmout(u32 dest, u16 content, u32 para1, u32 para2);
  
  if(g_is_timer_rolling && !g_is_timer_rolling_paused)
  { 
    if(g_roll_timer_cnt == 0) 
    {
      g_roll_timer_cnt ++;
      ticks_count = mtos_ticks_get();

      fw_post_tmout(ROOT_ID_BACKGROUND, MSG_HEART_BEAT, 0, 0);
    }
    rolling_ticks = mtos_ticks_get();
    if((rolling_ticks - ticks_count) > 20)
    {
    	g_roll_timer_cnt = 0;
    }
  }
}

static void on_hw_timer_arrived(void)
{
  on_timer_rolling_inc();
}

static RET_CODE on_heart_beat(control_t *p_ctrl, u16 msg,
                              u32 para1, u32 para2)
{
  gui_rolling();
  on_timer_rolling_dec();
  
  return SUCCESS;
}

static RET_CODE on_switch_video_mode(control_t *p_ctrl, u16 msg,
                                     u32 para1, u32 para2)
{
  clt_setting_av_t tvsys;
  static u8 *video_resolution_hd_str_50hz[] = {(u8 *)"576i",(u8 *) "576p", (u8 *)"720p", (u8 *)"1080i", (u8 *)"1080p"};
  static u8 *video_resolution_hd_str_60hz[] = {(u8 *)"480i", (u8 *)"480p", (u8 *)"720p", (u8 *)"1080i", (u8 *)"1080p"};
  u32 content = 0;
  int video_mode = 0;
  rect_t pn_notify =
  {
    NOTIFY_CONT_X, NOTIFY_CONT_Y,
    NOTIFY_CONT_X + NOTIFY_CONT_W,
    NOTIFY_CONT_Y + NOTIFY_CONT_H,
  };

  if(fw_get_focus_id() == ROOT_ID_BACKGROUND)
  {
    client_setting_get_av(g_p_client, &tvsys);

    if(fw_find_root_by_id(ROOT_ID_NOTIFY) != NULL)
    {
      tvsys.resolution++;
    }

    video_mode = client_setting_get_video_mode(g_p_client);

    switch(video_mode)
    {
      case CLT_SETTING_VIDEO_MODE_NTSC:
        tvsys.resolution %= (sizeof(video_resolution_hd_str_60hz) / sizeof(u8 *));

        content = (u32)video_resolution_hd_str_60hz[tvsys.resolution];
        break;

      case CLT_SETTING_VIDEO_MODE_PAL:
        tvsys.resolution %= (sizeof(video_resolution_hd_str_50hz) / sizeof(u8 *));

        content = (u32)video_resolution_hd_str_50hz[tvsys.resolution];
        break;

      default:
        MT_ASSERT(0);
        break;
    }

    ui_set_notify(&pn_notify, NOTIFY_TYPE_ASC, 0, NOTIFY_AUTOCLOSE_3000MS);
    ui_set_notify(&pn_notify, NOTIFY_TYPE_ASC, content, NOTIFY_AUTOCLOSE_3000MS);
    client_setting_set_av(g_p_client, &tvsys);

  }
  return SUCCESS;
}

static RET_CODE on_switch_aspect_mode(control_t *p_ctrl, u16 msg,
                                     u32 para1, u32 para2)
{  
  clt_setting_av_t tvsys;
  static u16 aspect_mode_str[]  = {IDS_AUTO, IDS_43LETTERBOX, IDS_43PANSCAN, IDS_169};
  u32 content = 0;
  rect_t aspect_notify =
  {
    NOTIFY_CONT_X, NOTIFY_CONT_Y,
    NOTIFY_CONT_X + NOTIFY_CONT_W,
    NOTIFY_CONT_Y + NOTIFY_CONT_H,
  };

  if(fw_get_focus_id() == ROOT_ID_BACKGROUND)
  {
    client_setting_get_av(g_p_client, &tvsys);
    if(fw_find_root_by_id(ROOT_ID_NOTIFY) != NULL)
    {
      tvsys.aspect_ratio++;
    }
    
    tvsys.aspect_ratio %= (sizeof(aspect_mode_str) / sizeof(u16));
    content = (u32)aspect_mode_str[tvsys.aspect_ratio];

    ui_set_notify(&aspect_notify, NOTIFY_TYPE_STRID, content, NOTIFY_AUTOCLOSE_3000MS);
    client_setting_set_av(g_p_client, &tvsys);
  }
  return SUCCESS;
}

static RET_CODE on_playback_descramble(control_t *p_ctrl, u16 msg,
                                     u32 para1, u32 para2)
{
#if 0
  clt_system_status_t system_status = {0};
  u16 str_id = 0;

  client_get_system_status(g_p_client, &system_status);

  //update msg for descramble pg
  if(system_status.is_encrypt)
  {
    str_id = ui_pop_msg_get_msg_strid();
    if(msg == MSG_DESCRAMBLE_FAILED)
    {
      //if is showing ca msg, don't show encrypt prog msg
      if(str_id == RSC_INVALID_ID)
      {
        ui_pop_msg_set_msg_strid(IDS_MSG_ENCRYPT_PROG);
        ui_pop_msg_refresh_msg();
      }
    }
    else if(msg == MSG_DESCRAMBLE_SUCCESS)
    {
      str_id = ui_pop_msg_get_msg_strid();
      if(str_id == IDS_MSG_ENCRYPT_PROG)
      {
        ui_pop_msg_close();
      }
    }
  }
#endif
  return SUCCESS;
}

static RET_CODE on_switch_language(control_t *p_ctrl, u16 msg,
                                     u32 para1, u32 para2)
{
  clt_setting_lang_t lang_set;

  static u16 language_str[]  = {IDS_LANGUAGE_ENGLISH,IDS_LANGUAGE_SIMPLIFIED_CHINESE};

  OS_PRINTF("[%s] line [%d]\n",__FUNCTION__,__LINE__);
  u32 content = 0;
  rect_t lang_notify =
  {
    NOTIFY_CONT_X, NOTIFY_CONT_Y,
    NOTIFY_CONT_X + NOTIFY_CONT_W,
    NOTIFY_CONT_Y + NOTIFY_CONT_H,
  };

  if(fw_get_focus_id() == ROOT_ID_BACKGROUND)
  {
    client_setting_get_language(g_p_client, &lang_set);


    if(fw_find_root_by_id(ROOT_ID_NOTIFY) != NULL)
    {
      lang_set.osd_text++;
    }
    
    lang_set.osd_text %= (sizeof(language_str) / sizeof(u16));
    content = (u32)language_str[lang_set.osd_text];
    
    rsc_set_curn_language(gui_get_rsc_handle(), lang_set.osd_text + 1);

    ui_set_notify(&lang_notify, NOTIFY_TYPE_STRID, content, NOTIFY_AUTOCLOSE_3000MS);
  }
  return SUCCESS;
}

static RET_CODE on_start_ttx(control_t *p_ctrl, u16 msg,
                             u32 para1, u32 para2)
{
  u16 index;
  clt_system_status_t system_status;
  index = fw_get_focus_id();
  client_get_system_status(g_p_client, &system_status);

  if(ui_menu_data_api_is_fscreen_menu(index))
  {
    if(system_status.is_ttx_ready)
    {
      ui_ttx_start();
      fw_set_keymap(ui_desktop_keymap_on_ttx);
      fw_set_proc(ui_desktop_proc_on_ttx);

    }
    else
    {
      ui_comm_cfmdlg_open(NULL, IDS_MSG_NO_TELETEXT, NULL, 2000);
    }
  }

  return SUCCESS;
}

static RET_CODE on_stop_ttx(control_t *p_ctrl, u16 msg,
                            u32 para1, u32 para2)
{
  clt_system_status_t system_status;
  client_get_system_status(g_p_client, &system_status);
  ui_ttx_end();
  mtos_printk("############on_stop_ttx\n");
  fw_set_keymap(ui_desktop_keymap_on_normal);
  fw_set_proc(ui_desktop_proc_on_normal);

  // open prog bar
  if(system_status.is_mute)
  {
    open_mute(0, 0);
  }

  if(system_status.is_pause)
  {
    open_pause(0, 0);
  }

  if(ui_is_notify())
  {
    open_notify(NOTIFY_AUTOCLOSE_3000MS, 0);
  }

  manage_open_menu(ROOT_ID_INFOR_BAR, 0, 0);

  return SUCCESS;
}

static RET_CODE on_stop_ttx_and_rec(control_t *p_ctrl, u16 msg,
                            u32 para1, u32 para2)
{
  clt_system_status_t system_status;
  client_get_system_status(g_p_client, &system_status);
  ui_ttx_end();

  // restore keymap
  fw_set_keymap(ui_desktop_keymap_on_normal);
  fw_set_proc(ui_desktop_proc_on_normal);

  // open prog bar
  if(system_status.is_mute)
  {
    open_mute(0, 0);
  }

  if(system_status.is_pause)
  {
    open_pause(0, 0);
  }

  if(ui_is_notify())
  {
    open_notify(NOTIFY_AUTOCLOSE_3000MS, 0);
  }

  ctrl_process_msg(p_ctrl, MSG_START_RECORD, 0, 0);
  return SUCCESS;
}

static RET_CODE on_ttx_key(control_t *p_ctrl, u16 msg,
                           u32 para1, u32 para2)
{
  mtos_printk("#############on_ttx_key\n");
  client_translate_key(g_p_client, msg & MSG_DATA_MASK);
  return SUCCESS;
}

static RET_CODE on_desktop_subt_ready(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  return SUCCESS;
}

static RET_CODE on_switch_subt(control_t *p_ctrl, u16 msg,
                               u32 para1, u32 para2)
{
  clt_system_status_t system_status;
  client_get_system_status(g_p_client, &system_status);

  if(system_status.is_recording)
    return SUCCESS;
  manage_close_menu(ROOT_ID_INFOR_BAR, 0, 0);
  if(fw_get_focus_id() != ROOT_ID_BACKGROUND)
  {
  return ERR_FAILURE;
  }
  manage_open_menu(ROOT_ID_SUBT_LANGUAGE, 0, 0);

	return SUCCESS;
}

static RET_CODE on_nit_version_update(control_t *p_ctrl, u16 msg,
                                      u32 para1, u32 para2)
{
  u16 focus_root = 0;
  comm_dlg_data_t dlg_ack_data =
  {
    ROOT_ID_INVALID,
    DLG_FOR_ASK | DLG_STR_MODE_STATIC,
    COMM_DLG_X, COMM_DLG_Y,
    COMM_DLG_W, COMM_DLG_H,
    IDS_NIT_UPDATE,
    0,
  };
  clt_scan_input_para_t para;
  clt_setting_main_tp_t tp_info;
  clt_setting_scan_param_t scan_param;
  dlg_ret_t dlg_ret = DLG_RET_NULL;
  u32 new_nit_ver = 0;
  int sat_type = 0;
  char sel_str[128];
  char fmt_str[64];
  int curn_pg_index;

  new_nit_ver = para2;

  focus_root = fw_get_focus_id();
  if(!is_already_prompt)
  {
    is_already_prompt = TRUE;

    if(fw_find_root_by_id(ROOT_ID_INFOR_BAR) != NULL)
    {
      manage_close_menu(ROOT_ID_INFOR_BAR, 0, 0);
    }

    if(fw_find_root_by_id(ROOT_ID_VOLUME) != NULL)
    {
      manage_close_menu(ROOT_ID_VOLUME, 0, 0);
    }

    dlg_ret = ui_comm_dlg_open(&dlg_ack_data);

    if(dlg_ret == DLG_RET_YES)
    {        
      curn_pg_index = client_dbase_get_pos(g_p_client,"curn_view");
      snprintf(sel_str, sizeof(sel_str), "select sat_type from curn_view where limit 1 pos %d", curn_pg_index);
      snprintf(fmt_str, sizeof(fmt_str), "%%d");
      client_dbase_select(g_p_client, sel_str, fmt_str, &sat_type);
      OS_PRINTF("%s(line %d) sat type %d\n", __FUNCTION__, __LINE__, sat_type);
      if(sat_type == CLT_SAT_TYPE_DVBC)
      {
        client_setting_get_main_tp(g_p_client, &tp_info);
        para.tp.freq = tp_info.freq;
        para.tp.sym= tp_info.sym;
        para.tp.mod = tp_info.mod;

        para.scan_type = CLT_SCAN_TYPE_AUTO;
		para.chan_type = CLT_SCAN_CHAN_RADIO; //Raja changed for radio only
        para.nit_type = CLT_SCAN_NIT_SCAN_ONCE;

        manage_open_menu(ROOT_ID_DO_SEARCH, (u32)&para, 0);
        is_already_prompt = FALSE;
      }
      else if(sat_type == CLT_SAT_TYPE_DVBT)
      {
        client_setting_get_scan_param(g_p_client, &scan_param);
        para.scan_type = CLT_SCAN_TYPE_DVBT_AUTO;
        para.free_only = scan_param.free_only;
        para.nit_type  = CLT_SCAN_NIT_SCAN_WITHOUT;
        manage_open_menu(ROOT_ID_DO_SEARCH, (u32)&para, 0);
        is_already_prompt = FALSE;
      }
    }
  }
  return SUCCESS;
}

RET_CODE on_refresh_ads(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  control_t *p_root = NULL;
  p_root = fw_find_root_by_id(ROOT_ID_BACKGROUND);
  if(fw_find_root_by_id(ROOT_ID_SMALL_LIST) || fw_find_root_by_id(ROOT_ID_INFOR_BAR)
  || fw_find_root_by_id(ROOT_ID_MAINMENU)||fw_find_root_by_id(ROOT_ID_VOLUME))
  {
    	return SUCCESS;
  }
  client_ads_refresh_ads(g_p_client);
  return SUCCESS;
}

RET_CODE on_ads_msg_start_roll_osd(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  return SUCCESS;
}

RET_CODE on_ads_rolling_stop_bmap(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  return SUCCESS;
}

RET_CODE on_open_factory_mode(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  client_ads_stop_ads(g_p_client);
  manage_open_menu(ROOT_ID_FACTORY_MODE, 0, 0);
  return SUCCESS;
}

#ifdef NIT_SOFTWARE_UPDATE
void on_desktop_save_ota_info(clt_ota_info_t *ota_info_temp)
{
  memcpy(&update_info, ota_info_temp, sizeof(clt_ota_info_t));
}

extern void ui_ota_api_manual_save_ota_info(void);
extern void  ui_ota_api_save_do_ota_tp(nim_info_t *tp_info);
void do_update_software(void)
{
  nim_info_t set_tp;

  set_tp.data_pid = update_info.data_pid;

  set_tp.lockc.tp_freq = update_info.freq;
  set_tp.lockc.tp_sym = update_info.symbol;
  set_tp.lockc.nim_modulate = update_info.qam_mode;
#ifdef DTMB_PROJECT
  set_tp.lock_mode = SYS_DTMB;
#else
  set_tp.lock_mode = SYS_DVBC;
#endif
#if 1
  OS_PRINTF("==============================\n");
  OS_PRINTF("up_info.freq=%d\n", update_info.freq);
  OS_PRINTF("up_info.symbol=%d\n", update_info.symbol);
  OS_PRINTF("up_info.qam_mode=%d\n", update_info.qam_mode);
  OS_PRINTF("up_info.data_pid=%d\n", update_info.data_pid);
  OS_PRINTF("up_info.ota_type=%d\n", update_info.ota_type);
  OS_PRINTF("up_info.Serial_number_start=%s\n", update_info.Serial_number_start);
  OS_PRINTF("up_info.Serial_number_end=%s\n", update_info.Serial_number_end);
  OS_PRINTF("up_info.swVersion=%d\n", update_info.swVersion);
  OS_PRINTF("up_info.hwVersion=%d\n", update_info.hwVersion);
  OS_PRINTF("==============================\n");
#endif
  g_nit_ota_check = TRUE;
  //if(g_customer.customer_id == CUSTOMER_KONKA_SV)
  {
    ui_ota_api_save_do_ota_tp(&set_tp);
    ui_ota_api_manual_save_ota_info();
  }
#ifndef WIN32
  mtos_task_delay_ms(100);
  hal_pm_reset();
#endif
}
#endif

RET_CODE on_zoom_out_to_small_prev(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  clt_preview_para_t   prev_para = {0};
  menu_attr_t *menu_attr_tmp = NULL;
  prev_para.left = PLIST_PREV_X;
  prev_para.top = PLIST_PREV_Y;
  prev_para.right = PLIST_PREV_X+PLIST_PREV_W;
  prev_para.bottom = PLIST_PREV_Y+PLIST_PREV_H;
  client_enter_prev_mode(g_p_client, &prev_para); 
  menu_attr_tmp = _ui_menu_data_api_get_menu_by_id(ROOT_ID_BACKGROUND);
  menu_attr_tmp->play_state = PS_PREV;
  _ui_menu_data_api_set_menu_by_id(ROOT_ID_BACKGROUND,menu_attr_tmp);
  return SUCCESS;
}

BEGIN_AP_EVTMAP(sys_evtmap)
END_AP_EVTMAP(sys_evtmap)

BEGIN_KEYMAP(ui_desktop_keymap_on_normal, ui_desktop_keymap_cas)
  ON_EVENT(V_KEY_MENU, MSG_OPEN_MENU_IN_TAB | ROOT_ID_MAINMENU)
  ON_EVENT(V_KEY_OK, MSG_OPEN_MENU_IN_TAB | ROOT_ID_SMALL_LIST)
  ON_EVENT(V_KEY_UP, MSG_OPEN_MENU_IN_TAB | ROOT_ID_INFOR_BAR)
  ON_EVENT(V_KEY_DOWN, MSG_OPEN_MENU_IN_TAB | ROOT_ID_INFOR_BAR)
  ON_EVENT(V_KEY_CHUP, MSG_OPEN_MENU_IN_TAB | ROOT_ID_INFOR_BAR)
  ON_EVENT(V_KEY_CHDOWN, MSG_OPEN_MENU_IN_TAB | ROOT_ID_INFOR_BAR)
  ON_EVENT(V_KEY_PAGE_UP, MSG_OPEN_MENU_IN_TAB | ROOT_ID_INFOR_BAR)
  ON_EVENT(V_KEY_PAGE_DOWN, MSG_OPEN_MENU_IN_TAB | ROOT_ID_INFOR_BAR)
  ON_EVENT(V_KEY_LEFT, MSG_OPEN_MENU_IN_TAB | ROOT_ID_VOLUME)
  ON_EVENT(V_KEY_RIGHT, MSG_OPEN_MENU_IN_TAB | ROOT_ID_VOLUME)
  ON_EVENT(V_KEY_1, MSG_OPEN_MENU_IN_TAB | ROOT_ID_NUM_PLAY)
  ON_EVENT(V_KEY_2, MSG_OPEN_MENU_IN_TAB | ROOT_ID_NUM_PLAY)
  ON_EVENT(V_KEY_3, MSG_OPEN_MENU_IN_TAB | ROOT_ID_NUM_PLAY)
  ON_EVENT(V_KEY_4, MSG_OPEN_MENU_IN_TAB | ROOT_ID_NUM_PLAY)
  ON_EVENT(V_KEY_5, MSG_OPEN_MENU_IN_TAB | ROOT_ID_NUM_PLAY)
  ON_EVENT(V_KEY_6, MSG_OPEN_MENU_IN_TAB | ROOT_ID_NUM_PLAY)
  ON_EVENT(V_KEY_7, MSG_OPEN_MENU_IN_TAB | ROOT_ID_NUM_PLAY)
  ON_EVENT(V_KEY_8, MSG_OPEN_MENU_IN_TAB | ROOT_ID_NUM_PLAY)
  ON_EVENT(V_KEY_9, MSG_OPEN_MENU_IN_TAB | ROOT_ID_NUM_PLAY)
  ON_EVENT(V_KEY_RECALL, MSG_OPEN_MENU_IN_TAB | ROOT_ID_INFOR_BAR)
  ON_EVENT(V_KEY_AUDIO, MSG_OPEN_MENU_IN_TAB | ROOT_ID_AUDIO_SETTING)
  ON_EVENT(V_KEY_TVRADIO, MSG_TV_FUNCTION)
  ON_EVENT(V_KEY_REC, MSG_START_RECORD)
  ON_EVENT(V_KEY_FAV, MSG_OPEN_MENU_IN_TAB | ROOT_ID_FAV_LIST)
  ON_EVENT(V_KEY_VDOWN, MSG_OPEN_MENU_IN_TAB | ROOT_ID_VOLUME)
  ON_EVENT(V_KEY_VUP, MSG_OPEN_MENU_IN_TAB | ROOT_ID_VOLUME)
  ON_EVENT(V_KEY_RADIO, MSG_RADIO_FUNCTION)
  ON_EVENT(V_KEY_TV, MSG_TV_FUNCTION)
  ON_EVENT(V_KEY_INFO, MSG_OPEN_MENU_IN_TAB | ROOT_ID_INFOR_BAR)
  ON_EVENT(V_KEY_MUTE, MSG_MUTE)
  ON_EVENT(V_KEY_PAUSE, MSG_PAUSE)  
  ON_EVENT(V_KEY_POWER, MSG_POWER_OFF)
  ON_EVENT(V_KEY_EPG, MSG_OPEN_MENU_IN_TAB | ROOT_ID_EPG)
  ON_EVENT(V_KEY_VIDEO_MODE, MSG_SWITCH_VIDEO_MODE)
  ON_EVENT(V_KEY_ASPECT_MODE, MSG_SWITCH_ASPECT_MODE)
  ON_EVENT(V_KEY_LANG, MSG_SWITCH_LANGUAGE)
  ON_EVENT(V_KEY_SUBT, MSG_SWITCH_SUBT)  
  ON_EVENT(V_KEY_TTX, MSG_START_TTX)  
  ON_EVENT(V_KEY_TEST, MSG_TEST_MENU_UPDATE)  
  ON_EVENT(V_KEY_GBOX, MSG_UI_REFRESH)   
END_KEYMAP(ui_desktop_keymap_on_normal, ui_desktop_keymap_cas)

BEGIN_MSGPROC(ui_desktop_proc_on_normal, ui_desktop_proc_cas)
  ON_COMMAND(MSG_SWITCH_LANGUAGE, on_switch_language) 
  ON_COMMAND(MSG_TEST_MENU_UPDATE, on_open_factory_mode) 
  ON_COMMAND(MSG_UI_REFRESH, on_zoom_out_to_small_prev) 
  ON_COMMAND(MSG_ON_FIND_USB_UPG_FILE, on_find_upg_file)
  ON_COMMAND(MSG_ON_FIND_USB_PLUG_IN, on_find_usb_plug_in)
  
  ON_COMMAND(MSG_HEART_BEAT, on_heart_beat)
  ON_COMMAND(MSG_NETWORK_PLUG_IN, on_ethernet_plug_in)
  ON_COMMAND(MSG_NETWORK_PLUG_OUT, on_ethernet_plug_out)
  ON_COMMAND(MSG_PLUG_IN, on_usb_plug_in)
  ON_COMMAND(MSG_PLUG_OUT, on_usb_plug_out)
  ON_COMMAND(MSG_SIGNAL_CHECK, on_check_signal)
  ON_COMMAND(MSG_CLOSE_MENU, on_close_menu)
  ON_COMMAND(MSG_OPEN_MENU_IN_TAB, ui_desktop_open_menu)
  ON_COMMAND(MSG_RADIO_FUNCTION, on_tv_radio_function)  
  ON_COMMAND(MSG_TV_FUNCTION, on_tv_radio_function)  
  ON_COMMAND(MSG_TIME_UPDATE, on_time_update)  
  ON_COMMAND(MSG_START_RECORD, on_start_record)  
  ON_COMMAND(MSG_OTA_TRIGGER_RESET, on_ota_reboot_into_ota)
  ON_COMMAND(MSG_OTA_TMOUT, on_ota_timeout_start_play)
  ON_COMMAND(MSG_MUTE, on_mute)
  ON_COMMAND(MSG_PAUSE, on_pause)
  ON_COMMAND(MSG_SIGNAL_UPDATE, on_desktop_signal_update)
  ON_COMMAND(MSG_SIGNAL_TURNING, on_desktop_signal_turning)
  ON_COMMAND(MSG_SIGNAL_OUT_RANGE, on_desktop_signal_out_range)
  ON_COMMAND(MSG_POWER_OFF, on_power_off)
  ON_COMMAND(MSG_SWITCH_VIDEO_MODE, on_switch_video_mode)  
  ON_COMMAND(MSG_SWITCH_ASPECT_MODE, on_switch_aspect_mode) 
  ON_COMMAND(MSG_DESCRAMBLE_SUCCESS, on_playback_descramble) 
  ON_COMMAND(MSG_DESCRAMBLE_FAILED, on_playback_descramble) 
  ON_COMMAND(MSG_START_TTX, on_start_ttx)
  ON_COMMAND(MSG_STOP_TTX, on_stop_ttx)
  ON_COMMAND(MSG_STOP_TTX_AND_REC, on_stop_ttx_and_rec)
  ON_COMMAND(MSG_TTX_KEY, on_ttx_key)
  ON_COMMAND(MSG_SUBT_READY, on_desktop_subt_ready)
  ON_COMMAND(MSG_SWITCH_SUBT, on_switch_subt)
  ON_COMMAND(MSG_VERSION_NUMBER_UPDATE, on_nit_version_update)
  ON_COMMAND(MSG_REFRESH_DESKTOP_ADS, on_refresh_ads)
  ON_COMMAND(MSG_ADS_ROLL, on_ads_msg_start_roll_osd)
  ON_COMMAND(MSG_ADS_ROLL_STOP, on_ads_rolling_stop_bmap)
  ON_COMMAND(MSG_LNB_SHORT, on_lnb_short)
END_MSGPROC(ui_desktop_proc_on_normal, ui_desktop_proc_cas)

BEGIN_KEYMAP(ui_desktop_keymap_on_ttx, NULL)
ON_EVENT(V_KEY_TTX, MSG_STOP_TTX)
ON_EVENT(V_KEY_MENU, MSG_STOP_TTX)
ON_EVENT(V_KEY_CANCEL, MSG_STOP_TTX)
ON_EVENT(V_KEY_EXIT, MSG_STOP_TTX)
ON_EVENT(V_KEY_BACK, MSG_STOP_TTX)
ON_EVENT(V_KEY_POWER, MSG_POWER_OFF)
ON_EVENT(V_KEY_REC, MSG_STOP_TTX_AND_REC)

ON_EVENT(V_KEY_0, MSG_TTX_KEY | CLT_TTX_KEY_0)
ON_EVENT(V_KEY_1, MSG_TTX_KEY | CLT_TTX_KEY_1)
ON_EVENT(V_KEY_2, MSG_TTX_KEY | CLT_TTX_KEY_2)
ON_EVENT(V_KEY_3, MSG_TTX_KEY | CLT_TTX_KEY_3)
ON_EVENT(V_KEY_4, MSG_TTX_KEY | CLT_TTX_KEY_4)
ON_EVENT(V_KEY_5, MSG_TTX_KEY | CLT_TTX_KEY_5)
ON_EVENT(V_KEY_6, MSG_TTX_KEY | CLT_TTX_KEY_6)
ON_EVENT(V_KEY_7, MSG_TTX_KEY | CLT_TTX_KEY_7)
ON_EVENT(V_KEY_8, MSG_TTX_KEY | CLT_TTX_KEY_8)
ON_EVENT(V_KEY_9, MSG_TTX_KEY | CLT_TTX_KEY_9)

ON_EVENT(V_KEY_OK, MSG_TTX_KEY | CLT_TTX_KEY_TRANSPARENT)

ON_EVENT(V_KEY_UP, MSG_TTX_KEY | CLT_TTX_KEY_UP)
ON_EVENT(V_KEY_DOWN, MSG_TTX_KEY | CLT_TTX_KEY_DOWN)
ON_EVENT(V_KEY_LEFT, MSG_TTX_KEY | CLT_TTX_KEY_LEFT)
ON_EVENT(V_KEY_RIGHT, MSG_TTX_KEY | CLT_TTX_KEY_RIGHT)
ON_EVENT(V_KEY_PAGE_UP, MSG_TTX_KEY | CLT_TTX_KEY_PAGE_UP)
ON_EVENT(V_KEY_PAGE_DOWN, MSG_TTX_KEY | CLT_TTX_KEY_PAGE_DOWN)

ON_EVENT(V_KEY_RED, MSG_TTX_KEY | CLT_TTX_KEY_RED)
ON_EVENT(V_KEY_GREEN, MSG_TTX_KEY | CLT_TTX_KEY_GREEN)
ON_EVENT(V_KEY_YELLOW, MSG_TTX_KEY | CLT_TTX_KEY_YELLOW)
ON_EVENT(V_KEY_BLUE, MSG_TTX_KEY | CLT_TTX_KEY_CYAN)
END_KEYMAP(ui_desktop_keymap_on_ttx, NULL)

BEGIN_MSGPROC(ui_desktop_proc_on_ttx, cont_class_proc)
ON_COMMAND(MSG_TIME_UPDATE, on_time_update)
ON_COMMAND(MSG_LNB_SHORT, on_lnb_short)
ON_COMMAND(MSG_CLOSE_MENU, on_close_menu)
ON_COMMAND(MSG_STOP_TTX, on_stop_ttx)
ON_COMMAND(MSG_STOP_TTX_AND_REC, on_stop_ttx_and_rec)
ON_COMMAND(MSG_TTX_KEY, on_ttx_key)
ON_COMMAND(MSG_POWER_OFF, on_power_off)
//ON_COMMAND(MSG_SLEEP_TMROUT, on_sleep_tmrout)
END_MSGPROC(ui_desktop_proc_on_ttx, cont_class_proc);

