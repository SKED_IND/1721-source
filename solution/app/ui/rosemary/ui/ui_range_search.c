/********************************************************************************************/
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/* Montage Proprietary and Confidential                                                     */
/* Copyright (c) 2017 Montage Technology Group Limited and its affiliated companies         */
/********************************************************************************************/
#include "ui_common.h"
#include "ui_desc.h"
#include "ui_range_search.h"
#include "ui_do_search.h"

enum range_search_local_msg
{
  MSG_RANGE_SEARCH_START = MSG_LOCAL_BEGIN + 100,
  MSG_RANGE_SEARCH_END,
};

static u16 range_search_cont_keymap(u16 key);
static RET_CODE range_search_cont_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static u16 specify_tp_frm_cont_keymap(u16 key);
static RET_CODE specify_tp_frm_cont_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static RET_CODE specify_start_search_text_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static RET_CODE specify_tp_sym_nbox_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);
static RET_CODE specify_tp_demod_cbox_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);
static void range_search_check_signal(control_t *cont, u16 msg, u32 para1, u32 para2)
{
  clt_nim_info_t para;
  control_t *p_root = fw_find_root_by_id(ROOT_ID_RANGE_SEARCH);
  u16 demod_focus;
  control_t *p_ctrl;

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_RANGE_SEARCH_SPECIFY_TP_FREQ_START);
  para.lockc.tp_freq = nbox_get_num(p_ctrl) * 1000;

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_RANGE_SEARCH_SPECIFY_TP_SYM);
  para.lockc.tp_sym = nbox_get_num(p_ctrl);

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_RANGE_SEARCH_SPECIFY_TP_DEMOD);
  demod_focus = (u8)cbox_static_get_focus(p_ctrl);
  switch(demod_focus)
  {
    case 0:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_AUTO;
      break;

    case 1:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_BPSK;      
      break;

    case 2:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QPSK;      
      break;

    case 3:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_8PSK;      
      break;

    case 4:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QAM16;
      break;
      
    case 5:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QAM32;
      break;

    case 6:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QAM64;      
      break;

    case 7:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QAM128;
      break;

    case 8:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QAM256;
      break;
      
    default:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QAM64;
      break;
  }
 para.lock_mode = CLT_SYS_DVBC;

 client_set_signal_tp(g_p_client, &para);
}

RET_CODE open_range_search (u32 para1, u32 para2)
{
  clt_setting_main_tp_t tp_info;
  control_t *p_cont;
  p_cont = init_desc(ROOT_ID_RANGE_SEARCH, para1, para2);
  if(p_cont == NULL)
  {
    return ERR_FAILURE;
  }
  client_setting_get_main_tp(g_p_client, &tp_info);

  ctrl_set_keymap(p_cont, range_search_cont_keymap);
  ctrl_set_proc(p_cont, range_search_cont_proc);
  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(p_cont, IDC_RANGE_SEARCH_SPECIFY_TP_FRM), 
  	specify_tp_frm_cont_keymap);
  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_RANGE_SEARCH_SPECIFY_TP_FRM),
  	specify_tp_frm_cont_proc);
    
  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_RANGE_SEARCH_SPECIFY_TP_FREQ_START),
   ui_comm_num_keymap);
  
  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_RANGE_SEARCH_SPECIFY_TP_FREQ_END),
   ui_comm_num_keymap);

  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_RANGE_SEARCH_SPECIFY_TP_SYM),
   ui_comm_num_keymap);

  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_RANGE_SEARCH_SPECIFY_TP_DEMOD),
   ui_comm_select_keymap);
 
  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_RANGE_SEARCH_SPECIFY_CHANNEL_FTA),
   ui_comm_select_keymap);
  
  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_RANGE_SEARCH_SPECIFY_START_SEARCH),
   ui_comm_static_keymap);
  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_RANGE_SEARCH_SPECIFY_START_SEARCH),
  	specify_start_search_text_proc);

  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_RANGE_SEARCH_SPECIFY_TP_SYM), 
	  specify_tp_sym_nbox_proc);
  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_RANGE_SEARCH_SPECIFY_TP_FREQ_START), 
	  specify_tp_sym_nbox_proc);
  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_RANGE_SEARCH_SPECIFY_TP_FREQ_END), 
	  specify_tp_sym_nbox_proc);
  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_RANGE_SEARCH_SPECIFY_TP_DEMOD), 
	  specify_tp_demod_cbox_proc);
  
  nbox_set_num_by_dec(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_RANGE_SEARCH_SPECIFY_TP_FREQ_START),314000/1000);
  nbox_set_num_by_dec(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_RANGE_SEARCH_SPECIFY_TP_FREQ_END),818000/1000);
  nbox_set_num_by_dec(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_RANGE_SEARCH_SPECIFY_TP_SYM),tp_info.sym);
  cbox_static_set_focus(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_RANGE_SEARCH_SPECIFY_TP_DEMOD), tp_info.mod);  

  ctrl_paint_ctrl(p_cont, FALSE);
  range_search_check_signal(ctrl_get_ctrl_by_unique_id(p_cont, IDC_RANGE_SEARCH_SPECIFY_TP_FRM), 0, 0, 0);
  return SUCCESS;
}

BOOL range_search_tp_bar_update(control_t *p_bar, control_t *p_txt, u16 val, BOOL is_force,u8 *pox)
{
  u8 str_buf[10];
  BOOL is_redraw = FALSE;

  if(pbar_get_current(p_bar) != val || is_force)
  {
    pbar_set_current(p_bar, val);
    sprintf((char*)str_buf, "%d%s", val,pox);
    text_set_content_by_ascstr(p_txt, (u8*)str_buf);

    is_redraw = TRUE;
  }
  ctrl_paint_ctrl(p_bar, is_redraw);
  ctrl_paint_ctrl(p_txt, is_redraw);
  return is_redraw;
}

static RET_CODE on_range_search_signal_update(control_t *p_cont, u16 msg, u32 para1, u32 para2)
{
  control_t *p_bar, *p_txt;
  clt_signal_data_t *data = (clt_signal_data_t *)(para1);

  if(!ui_check_signal_info(data))
  {
    return SUCCESS;
  }
  
  p_bar = ctrl_get_ctrl_by_unique_id(p_cont, IDC_RANGE_SEARCH_SEARCH_SIG_STRENGTH_PBAR);
  p_txt = ctrl_get_ctrl_by_unique_id(p_cont, IDC_RANGE_SEARCH_SEARCH_SIG_STRENGTH_PERCENT);
  range_search_tp_bar_update(p_bar,p_txt, data->intensity, TRUE, (u8*)"dBuv");
  p_bar = ctrl_get_ctrl_by_unique_id(p_cont, IDC_RANGE_SEARCH_SEARCH_SIG_BER_PBAR);
  p_txt = ctrl_get_ctrl_by_unique_id(p_cont, IDC_RANGE_SEARCH_SEARCH_SIG_BER_PERCENT);
  range_search_tp_bar_update(p_bar,p_txt, data->ber, TRUE, (u8*)"E-6");
  p_bar = ctrl_get_ctrl_by_unique_id(p_cont, IDC_RANGE_SEARCH_SEARCH_SIG_SNR_PBAR);
  p_txt = ctrl_get_ctrl_by_unique_id(p_cont, IDC_RANGE_SEARCH_SEARCH_SIG_SNR_PERCENT);
  range_search_tp_bar_update(p_bar,p_txt, data->quality, TRUE, (u8*)"dB");

  return SUCCESS;
}

static RET_CODE on_start_search(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  clt_scan_input_para_t para;
  control_t *p_root = fw_find_root_by_id(ROOT_ID_RANGE_SEARCH);
  u16 demod_focus;

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_RANGE_SEARCH_SPECIFY_TP_FREQ_START);
  para.tp.freq = nbox_get_num(p_ctrl) * 1000;
  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_RANGE_SEARCH_SPECIFY_TP_FREQ_END);
  para.tp_end.freq = nbox_get_num(p_ctrl) * 1000;

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_RANGE_SEARCH_SPECIFY_TP_SYM);
  para.tp.sym = nbox_get_num(p_ctrl);
  para.tp_end.sym = nbox_get_num(p_ctrl);

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_RANGE_SEARCH_SPECIFY_TP_DEMOD);
  demod_focus = (u8)cbox_static_get_focus(p_ctrl);
  switch(demod_focus)
  {
    case 0:
      para.tp.mod = CLT_SCAN_NIM_MODULA_AUTO;
      para.tp_end.mod = CLT_SCAN_NIM_MODULA_AUTO;
      break;

    case 1:
      para.tp.mod = CLT_SCAN_NIM_MODULA_BPSK;    
      para.tp_end.mod = CLT_SCAN_NIM_MODULA_BPSK;      
      break;

    case 2:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QPSK;     
      para.tp_end.mod = CLT_SCAN_NIM_MODULA_QPSK;       
      break;

    case 3:
      para.tp.mod = CLT_SCAN_NIM_MODULA_8PSK;  
      para.tp_end.mod = CLT_SCAN_NIM_MODULA_8PSK;      
      break;

    case 4:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM16;
      para.tp_end.mod = CLT_SCAN_NIM_MODULA_QAM16;
      break;
      
    case 5:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM32;
      para.tp_end.mod = CLT_SCAN_NIM_MODULA_QAM32;
      break;

    case 6:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM64;   
      para.tp_end.mod = CLT_SCAN_NIM_MODULA_QAM64;       
      break;

    case 7:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM128;
      para.tp_end.mod = CLT_SCAN_NIM_MODULA_QAM128;
      break;

    case 8:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM256;
      para.tp_end.mod = CLT_SCAN_NIM_MODULA_QAM256;
      break;
      
    default:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM64;
      para.tp_end.mod = CLT_SCAN_NIM_MODULA_QAM64;
      break;
  }  
  
  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_RANGE_SEARCH_SPECIFY_CHANNEL_FTA);
  para.free_only = cbox_static_get_focus(p_ctrl) ? TRUE : FALSE;
  para.nit_type = CLT_SCAN_NIT_SCAN_WITHOUT;
  para.scan_type = CLT_SCAN_TYPE_RANGE;
  para.chan_type = CLT_SCAN_CHAN_RADIO; //Raja changed for radio only
  
  open_do_search((u32)&para, 0);
  return SUCCESS;
}

static RET_CODE on_range_search_change_freq_symbol(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  nbox_class_proc(p_ctrl, msg, para1, para2);
  range_search_check_signal(p_ctrl, msg, para1, para2);
  return SUCCESS;
}

static RET_CODE on_range_search_change_qam(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  u16 f_index;
  f_index = cbox_static_get_focus(p_ctrl);
  switch(msg)
  {
	case MSG_INCREASE:
		if(f_index == 8)
		{
			cbox_static_set_focus(p_ctrl,4);
		       ctrl_paint_ctrl(p_ctrl, FALSE);
		}
		else
		{
			 cbox_class_proc(p_ctrl, msg, para1, para2);
		}
		break;
	case MSG_DECREASE:
		if(f_index == 4)
		{
			cbox_static_set_focus(p_ctrl,8);
		       ctrl_paint_ctrl(p_ctrl, FALSE);
		}
		else
		{
			cbox_class_proc(p_ctrl, msg, para1, para2);
		}
		break;
	default:
		break;
  }
 
  range_search_check_signal(p_ctrl, msg, para1, para2);
  return SUCCESS;
}

static RET_CODE on_exit_search(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  manage_close_menu(ROOT_ID_RANGE_SEARCH,0,0);
  return SUCCESS;
}

BEGIN_KEYMAP(range_search_cont_keymap, NULL)
ON_EVENT(V_KEY_EXIT, MSG_EXIT)
ON_EVENT(V_KEY_MENU, MSG_EXIT)
END_KEYMAP(range_search_cont_keymap, NULL)

BEGIN_MSGPROC(range_search_cont_proc, cont_class_proc)
ON_COMMAND(MSG_EXIT, on_exit_search)
ON_COMMAND(MSG_SIGNAL_UPDATE, on_range_search_signal_update)
END_MSGPROC(range_search_cont_proc, cont_class_proc)

BEGIN_KEYMAP(specify_tp_frm_cont_keymap, NULL)
ON_EVENT(V_KEY_UP, MSG_FOCUS_UP)
ON_EVENT(V_KEY_DOWN, MSG_FOCUS_DOWN)
END_KEYMAP(specify_tp_frm_cont_keymap, NULL)

BEGIN_MSGPROC(specify_tp_frm_cont_proc, cont_class_proc)
END_MSGPROC(specify_tp_frm_cont_proc, cont_class_proc)

BEGIN_MSGPROC(specify_start_search_text_proc, text_class_proc)
ON_COMMAND(MSG_SELECT, on_start_search)
END_MSGPROC(specify_start_search_text_proc, text_class_proc)

BEGIN_MSGPROC(specify_tp_sym_nbox_proc, ui_comm_num_proc)
ON_COMMAND(MSG_NUMBER, on_range_search_change_freq_symbol)
END_MSGPROC(specify_tp_sym_nbox_proc, ui_comm_num_proc)

BEGIN_MSGPROC(specify_tp_demod_cbox_proc, cbox_class_proc)
ON_COMMAND(MSG_INCREASE, on_range_search_change_qam)
ON_COMMAND(MSG_DECREASE, on_range_search_change_qam)
END_MSGPROC(specify_tp_demod_cbox_proc, cbox_class_proc)

