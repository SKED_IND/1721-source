/********************************************************************************************/
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/* Montage Proprietary and Confidential                                                     */
/* Copyright (c) 2017 Montage Technology Group Limited and its affiliated companies         */
/********************************************************************************************/
#include "ui_common.h"
#include "ui_desc.h"
#include "ui_mainmenu.h"
#include "ui_comm_dlg.h"
#include "ui_comm_pwdlg.h"
#include "ui_auto_search.h"
#include "ui_manual_search.h"
#include "ui_av_setting.h"
#include "ui_language_setting.h"
#include "ui_time_setting.h"
#include "ui_osd_setting.h"
#include "ui_freq_setting.h"
#include "ui_online_app.h"
#include "ui_network_config.h"
#include "ui_infor_bar.h"
#include "ui_epg.h"
#include "ui_category.h"
#include "ui_fav_edit.h"
#include "ui_fav_list.h"
#include "ui_google_map.h"
#include "ui_channel_edit.h"
#include "ui_channel_infor.h"
#include "ui_range_search.h"
#include "ui_book_manage.h"
#include "ui_stb_info.h"

#include "ui_ota.h"
#include "ui_range_search.h"
#include "ui_book_manage.h"
#include "ui_parental_lock.h"

#include "ui_upgrade_by_usb.h"
#include "ui_music.h"
#include "ui_picture.h"
#include "ui_movie.h"
#include "ui_record_manager.h"
#include "ui_hdd_infor.h"
#include "ui_ts_record.h"

#include "ui_dvr_config.h"
#include "ui_storage_format.h"
#include "ui_yahoo_news.h"
#include "ui_weather.h"
#include "ui_game.h"
#include "ui_ca_menu_data.h"

#define INSTALL_ITEM_MAX_CNT  (16)
#define UPGRADE_ITEM_MAX_CNT  (16)
enum mainmenu_local_msg
{
  MSG_MAINMENU_START = MSG_LOCAL_BEGIN + 40,
  MSG_MAINMENU_END,
};

#define DEFINE_ITEM_SELECT_PROC(m, p) {m, p}
#define MBOX_ONE_ITEM_WIDTH     105

typedef RET_CODE (*mainmenu_item_select_proc_t)(u32 para1,
                                       u32 para2);
typedef struct
{
  u16 content_ids;
  u16 root_id;
  //mainmenu_item_select_proc_t p_proc;
}mainmenu_item_select_map_t;

static u16 g_installation_list_array[INSTALL_ITEM_MAX_CNT] = {0};
static u16 g_upgrade_list_array[UPGRADE_ITEM_MAX_CNT] = {0};
static int g_cur_mbox_focus = 1;
static int g_cur_list_focus = 0;
static int g_cur_index_in_select_map = 0xFF;
static BOOL b_ts_hide = TRUE;
static u8 g_solid_focus_index = 1;
static u16 g_dwIcon_HL[]=
{
  IM_INDEX_CHANNELLIST_FOCUS,
  IM_INDEX_INSTALLATION_FOCUS,
  IM_INDEX_SYSTEM_FOCUS,
  IM_INDEX_TOOLS_FOCUS,
  IM_INDEX_MEDIA_PLAYER_FOCUS,
  IM_INDEX_CA_FOCUS,
  IM_INDEX_NETWORK_FOCUS,
};

static u16 g_dwIcon_SH[]=
{
  IM_INDEX_CHANNELLIST,
  IM_INDEX_INSTALLATION,
  IM_INDEX_SYSTEM,
  IM_INDEX_TOOLS,
  IM_INDEX_MEDIA_PLAYER,
  IM_INDEX_CA,
  IM_INDEX_NETWORK,
};

static u16 g_IDS_name[]=
{
  IDS_CHANNEL,
  IDS_INSTALLATION,
  IDS_SYSTEM,
  IDS_TOOLS,
  IDS_MEDIA,
  IDS_CA2,
  IDS_NETWORK,
};
static u16 g_TitleCount = sizeof(g_IDS_name)/sizeof(u16);

static mainmenu_item_select_map_t item_select_maps[] = 
{
  DEFINE_ITEM_SELECT_PROC(IDS_EPG, ROOT_ID_EPG),
  DEFINE_ITEM_SELECT_PROC(IDS_CHANNEL_EDIT, ROOT_ID_CHANNEL_EDIT),    
  DEFINE_ITEM_SELECT_PROC(IDS_CHANNEL_INFORMATION, ROOT_ID_CHANNEL_INFOR),
  DEFINE_ITEM_SELECT_PROC(IDS_CATEGORY, ROOT_ID_CATEGORY),    
  DEFINE_ITEM_SELECT_PROC(IDS_FAV_LIST, ROOT_ID_FAV_LIST),
  //DEFINE_ITEM_SELECT_PROC(IDS_EDIT_FAV_CHANNEL, ROOT_ID_FAV_EDIT),   
  
  DEFINE_ITEM_SELECT_PROC(IDS_AUTO_SEARCH, ROOT_ID_AUTO_SEARCH),
  DEFINE_ITEM_SELECT_PROC(IDS_MANUAL_SEARCH, ROOT_ID_MANUAL_SEARCH),    
  DEFINE_ITEM_SELECT_PROC(IDS_RANGE_SEARCH, ROOT_ID_RANGE_SEARCH),
  DEFINE_ITEM_SELECT_PROC(IDS_DELETE_ALL, ROOT_ID_AUTO_SEARCH),    
  DEFINE_ITEM_SELECT_PROC(IDS_FACTORY_SET, ROOT_ID_AUTO_SEARCH),
  DEFINE_ITEM_SELECT_PROC(IDS_DVBS_SEARCH, ROOT_ID_DVBS_INSTALLATION_LIST),
  DEFINE_ITEM_SELECT_PROC(IDS_DVBT_SEARCH, ROOT_ID_DVBS_INSTALLATION_LIST),//TODO
  
  DEFINE_ITEM_SELECT_PROC(IDS_LANGUAGE, ROOT_ID_LANGUAGE_SETTING),    
  DEFINE_ITEM_SELECT_PROC(IDS_AV_SETTING, ROOT_ID_AV_SETTING),
  DEFINE_ITEM_SELECT_PROC(IDS_LOCAL_TIME_SET, ROOT_ID_TIME_SETTING),    
  DEFINE_ITEM_SELECT_PROC(IDS_BOOK_MANAGE, ROOT_ID_BOOK_MANAGE),
  DEFINE_ITEM_SELECT_PROC(IDS_PARENTAL_LOCK, ROOT_ID_PARENTAL_LOCK),    
  DEFINE_ITEM_SELECT_PROC(IDS_OSD_SET, ROOT_ID_OSD_SETTING),
  DEFINE_ITEM_SELECT_PROC(IDS_FREQUENCY_SETTING, ROOT_ID_FREQ_SETTING),    
  
  DEFINE_ITEM_SELECT_PROC(IDS_INFORMATION, ROOT_ID_STB_INFO),
  DEFINE_ITEM_SELECT_PROC(IDS_UPGRADE_BY_SAT, ROOT_ID_OTA),    
  DEFINE_ITEM_SELECT_PROC(IDS_UPGRADE_BY_SAT_SAT, ROOT_ID_DVBS_OTA),    
  DEFINE_ITEM_SELECT_PROC(IDS_UPGRADE_BY_USB, ROOT_ID_UPGRADE_BY_USB),
  DEFINE_ITEM_SELECT_PROC(IDS_BACKUP_BY_USB, ROOT_ID_DUMP_DB),
  DEFINE_ITEM_SELECT_PROC(IDS_GAME, ROOT_ID_GAME),
  
  DEFINE_ITEM_SELECT_PROC(IDS_MUSIC, ROOT_ID_MUSIC),    
  DEFINE_ITEM_SELECT_PROC(IDS_PICTURE, ROOT_ID_PICTURE),
  DEFINE_ITEM_SELECT_PROC(IDS_MOVIES, ROOT_ID_MOVIE),    
  DEFINE_ITEM_SELECT_PROC(IDS_RECORD_MANAGER, ROOT_ID_RECORD_MANAGER),
  DEFINE_ITEM_SELECT_PROC(IDS_HDD_INFO, ROOT_ID_HDD_INFOR),    
  DEFINE_ITEM_SELECT_PROC(IDS_STORAGE_FORMAT, ROOT_ID_STORAGE_FORMAT),
  DEFINE_ITEM_SELECT_PROC(IDS_DVR_CONFIG, ROOT_ID_DVR_CONFIG),    
  DEFINE_ITEM_SELECT_PROC(IDS_TS_RECORD, ROOT_ID_TS_RECORD), 
  DEFINE_ITEM_SELECT_PROC(IDS_NETWORK_CONFIG, ROOT_ID_NETWORK_CONFIG),    
  DEFINE_ITEM_SELECT_PROC(IDS_ONLINE_MOVIE_APP, ROOT_ID_ONLINE_APP),
  DEFINE_ITEM_SELECT_PROC(IDS_GOOGLE_MAP, ROOT_ID_GOOGLE_MAP),    
  DEFINE_ITEM_SELECT_PROC(IDS_NETWORK_YAHOO_NEWS, ROOT_ID_YAHOO_NEWS),
  DEFINE_ITEM_SELECT_PROC(IDS_WEATHER_FORECAST, ROOT_ID_WEATHER), 
  
};
static void mainmenu_change_mbox(control_t * p_ctrl, u8 NewLevel, BOOL bforce);

static u16 mainmenu_cont_keymap(u16 key);
static u16 bootup_menu_cont_keymap(u16 key);

static RET_CODE mainmenu_cont_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);
static u16 menu_factory_pwdlg_keymap(u16 key);
static RET_CODE menu_factory_pwdlg_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);
static u16 main_menu_delete_all_pwdlg_keymap(u16 key);
static RET_CODE main_menu_delete_all_pwdlg_proc(control_t *ctrl,u16 msg,u32 para1,u32 para2);
static u16 menu_preopen_pwdlg_keymap(u16 key);
static RET_CODE menu_preopen_pwdlg_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);
RET_CODE open_mainmenu_1 (u32 para1, u32 para2);
static RET_CODE bootup_menu_cont_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);
void display_on_oled_string( char *string,u8 line  );
RET_CODE ui_start_dvbc_auto_search(void);



static void _set_installation_list_array(void)
{
  u8 i = 0;
  u16 *p_tmp = NULL;

  p_tmp = g_installation_list_array;

  if(TEST_UI_MODE(DVBC))
  {
    p_tmp[0] = IDS_AUTO_SEARCH;
    p_tmp[1] = IDS_MANUAL_SEARCH;
    //p_tmp[2] = IDS_RANGE_SEARCH;
    i += 2;
  }

  if(TEST_UI_MODE(DVBS))
  {
    p_tmp[i] = IDS_DVBS_SEARCH;
    i++;
  }

  if(TEST_UI_MODE(DVBT))
  {
    p_tmp[i] = IDS_DVBT_SEARCH;
    i++;
  }

  /*p_tmp[i] = IDS_DELETE_ALL;
  i++;
  
  p_tmp[i] = IDS_FACTORY_SET;
  i++; */
}

static void _set_upgrade_list_array(void)
{
  u8 i = 0;
  u16 *p_tmp = NULL;
  
  p_tmp = g_upgrade_list_array;
   
  p_tmp[i] = IDS_INFORMATION;
  i += 1;
  if(TEST_UI_MODE(DVBC))
  {
    if (TEST_FEATURE(ota))
    {
      p_tmp[i] = IDS_UPGRADE_BY_SAT;
      i += 1;
    }
  }

  if(TEST_UI_MODE(DVBS))
  {
    if (TEST_FEATURE(ota))
    {
      p_tmp[i] = IDS_UPGRADE_BY_SAT_SAT;
      i++;
    }
  }
  p_tmp[i] = IDS_UPGRADE_BY_USB;
  i++;
  
  p_tmp[i] = IDS_BACKUP_BY_USB;
  i++;
  p_tmp[i] = IDS_GAME;
  i++;
  if(!b_ts_hide)
  {
  	p_tmp[i] = IDS_TS_RECORD;
	i++;
  }
}

static u16 _get_installation_list_array_size(void)
{
  u8 i = 0;

  while(g_installation_list_array[i] != 0)
  {
    i++;
  }

  return i;
}

static u16 _get_upgrade_list_array_size(void)
{
  u8 i = 0;

  while(g_upgrade_list_array[i] != 0)
  {
    i++;
  }

  return i;
}

static void channel_list_set_count(control_t *p_list)
{
    u16 channel_list_string[] = 
    {
      IDS_EPG,
      IDS_CHANNEL_EDIT,
      IDS_CHANNEL_INFORMATION,
      IDS_CATEGORY, 
      IDS_FAV_LIST,
      //IDS_EDIT_FAV_CHANNEL, 
    };

    u16 system_list_string[] = 
    {
      IDS_LANGUAGE,
      IDS_AV_SETTING,
      IDS_LOCAL_TIME_SET,
      IDS_BOOK_MANAGE, 
      IDS_PARENTAL_LOCK,
      IDS_OSD_SET, 
      IDS_FREQUENCY_SETTING, 
    };
    u16 mediacenter_list_string[] = 
    {
      IDS_MUSIC,
      IDS_PICTURE,
      IDS_MOVIES,
      IDS_RECORD_MANAGER, 
      IDS_HDD_INFO,
      IDS_STORAGE_FORMAT, 
      IDS_DVR_CONFIG, 
    };
    u16 ca_list_string[] = 
    {
      IDS_CA,
    };
    u16 network_list_string[] = 
    {
      IDS_NETWORK_CONFIG,
      IDS_ONLINE_MOVIE_APP,
      IDS_GOOGLE_MAP,
      IDS_NETWORK_YAHOO_NEWS, 
      IDS_WEATHER_FORECAST,
    }; 
  switch(ctrl_get_ctrl_id(p_list))
  {
     case IDC_MAINMENU_CHANNEL_LIST:
      list_set_count(p_list, sizeof(channel_list_string)/sizeof(u16), list_get_page(p_list));
      break;
     case IDC_MAINMENU_INSTALLATION_LIST:
      list_set_count(p_list, _get_installation_list_array_size(), list_get_page(p_list));
      break;
     case IDC_MAINMENU_SYSTEM_LIST:
      list_set_count(p_list, sizeof(system_list_string)/sizeof(u16), list_get_page(p_list));
      break;
     case IDC_MAINMENU_TOOLS_LIST:
      list_set_count(p_list, _get_upgrade_list_array_size(), list_get_page(p_list));
      break;
     case IDC_MAINMENU_MIDEACENTER_LIST:
      list_set_count(p_list, sizeof(mediacenter_list_string)/sizeof(u16), list_get_page(p_list));
      break;
     case IDC_MAINMENU_CA_LIST:
      list_set_count(p_list, sizeof(ca_list_string)/sizeof(u16), list_get_page(p_list));
      break;
     case IDC_MAINMENU_NETWORK_LIST:
      list_set_count(p_list, sizeof(network_list_string)/sizeof(u16), list_get_page(p_list));
      break;
  }
}

static RET_CODE channel_list_update(control_t *p_list, u16 valid_pos, u16 size, u32 context)
{
  u32 i = 0, this_idx = 0;
  //int cnt = list_get_count(p_list);
  u16 cnt = 0;
  u16 channel_list_string[] = 
  {
    IDS_EPG,
    IDS_CHANNEL_EDIT,
    IDS_CHANNEL_INFORMATION,
    IDS_CATEGORY, 
    IDS_FAV_LIST,
    //IDS_EDIT_FAV_CHANNEL, 
  };

  u16 system_list_string[] = 
  {
    IDS_LANGUAGE,
    IDS_AV_SETTING,
    IDS_LOCAL_TIME_SET,
    IDS_BOOK_MANAGE, 
    IDS_PARENTAL_LOCK,
    IDS_OSD_SET, 
    IDS_FREQUENCY_SETTING, 
  };
  u16 mediacenter_list_string[] = 
  {
    IDS_MUSIC,
    IDS_PICTURE,
    IDS_MOVIES,
    IDS_RECORD_MANAGER, 
    IDS_HDD_INFO,
    IDS_STORAGE_FORMAT, 
    IDS_DVR_CONFIG, 
    IDS_PUSH_MEDIA,
  };
  u16 ca_list_string[] = 
  {
    IDS_CA,
  };
  u16 network_list_string[] = 
  {
    IDS_NETWORK_CONFIG,
    IDS_ONLINE_MOVIE_APP,
    IDS_GOOGLE_MAP,
    IDS_NETWORK_YAHOO_NEWS, 
    IDS_WEATHER_FORECAST,
  };
  switch(ctrl_get_ctrl_id(p_list))
  {
     case IDC_MAINMENU_CHANNEL_LIST:
      for (i = 0; i < size; i++)
      {
        if (i + valid_pos < sizeof(channel_list_string)/sizeof(u16))
        {
          list_set_field_content_by_strid(p_list, valid_pos + i, 0, channel_list_string[i + valid_pos]);
        }
      }
      break;
     case IDC_MAINMENU_INSTALLATION_LIST:
      for(i = 0; i < size; i++)
      {
        if (i + valid_pos < _get_installation_list_array_size())
        {
          list_set_field_content_by_strid(p_list, valid_pos + i, 0, g_installation_list_array[i + valid_pos]);
        }
      }
      break;
     case IDC_MAINMENU_SYSTEM_LIST:
      for(i = 0; i < size; i++)
      {
        if (i + valid_pos < sizeof(system_list_string)/sizeof(u16))
        {
          list_set_field_content_by_strid(p_list, valid_pos + i, 0, system_list_string[i + valid_pos]);
        }
      }
      break;
     case IDC_MAINMENU_TOOLS_LIST:
      for(i = 0; i < size; i++)
      {
        if (i + valid_pos < _get_upgrade_list_array_size())
        {
          list_set_field_content_by_strid(p_list, valid_pos + i, 0, g_upgrade_list_array[i + valid_pos]);
        }
      }
      break;
     case IDC_MAINMENU_MIDEACENTER_LIST:
      cnt = sizeof(mediacenter_list_string)/sizeof(u16);
      if(g_system_capabilities.enable_pushvod == FALSE)
      {
        for(i = 0; i < cnt; i++)
        {
          if(mediacenter_list_string[i] == IDS_PUSH_MEDIA)
          {
            this_idx = i;
            break;
          }
        }
        for(i = this_idx; i < cnt; i++)
        {
          mediacenter_list_string[i] = mediacenter_list_string[i+1];
        }
        cnt -= 1;
      }

      for(i = 0; i < size; i++)
      {
        if (i + valid_pos < cnt)
        {
          list_set_field_content_by_strid(p_list, valid_pos + i, 0, mediacenter_list_string[i + valid_pos]);
        }
      }
      break;
     case IDC_MAINMENU_CA_LIST:
      for(i = 0; i < size; i++)
      {
        if (i + valid_pos < sizeof(ca_list_string)/sizeof(u16))
        {
          list_set_field_content_by_strid(p_list, valid_pos + i, 0, ca_list_string[i + valid_pos]);
        }
      }
      break;
     case IDC_MAINMENU_NETWORK_LIST:
      for(i = 0; i < size; i++)
      {
        if (i + valid_pos < sizeof(network_list_string)/sizeof(u16))
        {
          list_set_field_content_by_strid(p_list, valid_pos + i, 0, network_list_string[i + valid_pos]);
        }
      }
      break;
      
  }
  return SUCCESS;
}

static void mainmenu_refresh_list(int mbox_focus)
{
    control_t *p_root;
    control_t *p_list;
    int i = 0;
    p_root = fw_find_root_by_id(ROOT_ID_MAINMENU);
    for(i = IDC_MAINMENU_CHANNEL_LIST; i <= IDC_MAINMENU_NETWORK_LIST; i++)
    {
      p_list = ctrl_get_ctrl_by_unique_id(p_root, i);
      if((i - IDC_MAINMENU_CHANNEL_LIST) == mbox_focus)
      {      
         list_set_focus_pos(p_list, g_cur_list_focus);
        ctrl_default_proc(p_list, MSG_GETFOCUS, 0, 0);
        ctrl_set_sts(p_list, OBJ_STS_SHOW);
        continue;
      }
      ctrl_default_proc(p_list, MSG_LOSTFOCUS, 0, 0);
      ctrl_set_sts(p_list, OBJ_STS_HIDE);
    } 
    ctrl_paint_ctrl(ctrl_get_parent(p_list), TRUE);
}

static void mainmenu_refresh_mbox_title(int mbox_focus)
{
    control_t *p_root;
    control_t *p_title;
    p_root = fw_find_root_by_id(ROOT_ID_MAINMENU);
    p_title = ctrl_get_ctrl_by_unique_id(p_root, IDC_MAINMENU_MAINMENU_TITLE_TEXT);
    
    text_set_content_by_strid(p_title, g_IDS_name[mbox_focus]);
    ctrl_paint_ctrl(p_title, TRUE);
}

static void _remove_mbox_item(u16 item_str_id)
{
  u16 i, this_idx = 0xffff;

  for (i=0; i<g_TitleCount; i++)
  {
    if (g_IDS_name[i] == item_str_id)
    {
      this_idx = i;
      break;
    }
  }

  if (this_idx == 0xffff)
    return;

  for (i=this_idx; i<(g_TitleCount-1); i++)
  {
    g_IDS_name[i]  = g_IDS_name[i+1];
    g_dwIcon_SH[i] = g_dwIcon_SH[i+1];
    g_dwIcon_HL[i] = g_dwIcon_HL[i+1];
  }

  if (g_TitleCount > 0)
    g_TitleCount--;
}

static void mainmenu_config_mbox_items(void)
{
  if (! TEST_FEATURE(network))
  {
    _remove_mbox_item(IDS_NETWORK);
  }
}

RET_CODE open_mainmenu (u32 para1, u32 para2)
{
  control_t *p_cont;
  control_t *p_mbox,*p_list = NULL;
  rect_t rect_tmp = {0};
  p_cont = init_desc(ROOT_ID_MAINMENU, para1, para2);
  if(p_cont == NULL)
  {
    return ERR_FAILURE;
  }  

  OS_PRINTF("[%s] [%d]\n",__FUNCTION__,__LINE__);
  ctrl_set_keymap(p_cont, mainmenu_cont_keymap);
  ctrl_set_proc(p_cont, mainmenu_cont_proc);       
  mainmenu_config_mbox_items();
  _set_installation_list_array();
  _set_upgrade_list_array();
  
  client_ads_show_ads(g_p_client, CLI_ADS_AD_TYPE_MENU_UP);
  
  p_mbox = ctrl_get_ctrl_by_unique_id(p_cont, IDC_MAINMENU_MAINMENU_MBOX);
  ctrl_get_frame(p_mbox, &rect_tmp);
  rect_tmp.left =(UI_COMMON_WIDTH - g_TitleCount * MBOX_ONE_ITEM_WIDTH)/2; 
  rect_tmp.right = rect_tmp.left + g_TitleCount * MBOX_ONE_ITEM_WIDTH;
  ctrl_resize(p_mbox, &rect_tmp);
  if(g_TitleCount < 6)
  {
    mbox_set_item_interval(p_mbox, 30, 0);
  }
 
  mbox_set_focus(p_mbox, g_solid_focus_index);
  mbox_set_count(p_mbox,g_TitleCount, g_TitleCount,1);
  mainmenu_change_mbox(p_mbox,g_cur_mbox_focus, FALSE);

  p_list = ctrl_get_ctrl_by_unique_id(p_cont, IDC_MAINMENU_CHANNEL_LIST);
  channel_list_set_count(p_list);
  list_set_update(p_list, channel_list_update, 0);
  channel_list_update(p_list, list_get_valid_pos(p_list), list_get_page(p_list),0);
  p_list = ctrl_get_ctrl_by_unique_id(p_cont, IDC_MAINMENU_INSTALLATION_LIST);
  channel_list_set_count(p_list);
  list_set_update(p_list, channel_list_update, 0);
  channel_list_update(p_list, list_get_valid_pos(p_list), list_get_page(p_list),0);
  p_list = ctrl_get_ctrl_by_unique_id(p_cont, IDC_MAINMENU_SYSTEM_LIST);
  channel_list_set_count(p_list);
  list_set_update(p_list, channel_list_update, 0);
  channel_list_update(p_list, list_get_valid_pos(p_list), list_get_page(p_list),0);
  p_list = ctrl_get_ctrl_by_unique_id(p_cont, IDC_MAINMENU_TOOLS_LIST);
  channel_list_set_count(p_list);
  list_set_update(p_list, channel_list_update, 0);
  channel_list_update(p_list, list_get_valid_pos(p_list), list_get_page(p_list),0);
  p_list = ctrl_get_ctrl_by_unique_id(p_cont, IDC_MAINMENU_MIDEACENTER_LIST);
  channel_list_set_count(p_list);
  list_set_update(p_list, channel_list_update, 0);
  channel_list_update(p_list, list_get_valid_pos(p_list), list_get_page(p_list),0);
  p_list = ctrl_get_ctrl_by_unique_id(p_cont, IDC_MAINMENU_CA_LIST);
  channel_list_set_count(p_list);
  list_set_update(p_list, channel_list_update, 0);
  channel_list_update(p_list, list_get_valid_pos(p_list), list_get_page(p_list),0);
  p_list = ctrl_get_ctrl_by_unique_id(p_cont, IDC_MAINMENU_NETWORK_LIST);
  channel_list_set_count(p_list);
  list_set_update(p_list, channel_list_update, 0);
  channel_list_update(p_list, list_get_valid_pos(p_list), list_get_page(p_list),0);
  ctrl_paint_ctrl(p_cont, TRUE);
  mainmenu_refresh_list(g_cur_mbox_focus);
  mainmenu_refresh_mbox_title(g_cur_mbox_focus);
  return SUCCESS;
}

RET_CODE open_mainmenu_1 (u32 para1, u32 para2)
{
  control_t *p_cont;
  control_t *p_mbox,*p_list = NULL;
  rect_t rect_tmp = {0};
  p_cont = init_desc(ROOT_ID_MAINMENU, para1, para2);
  if(p_cont == NULL)
  {
    return ERR_FAILURE;
  }  

  OS_PRINTF("[%s] [%d]\n",__FUNCTION__,__LINE__);
  ctrl_set_keymap(p_cont, bootup_menu_cont_keymap);
  ctrl_set_proc(p_cont, bootup_menu_cont_proc);       
  mainmenu_config_mbox_items();
  _set_installation_list_array();
  
  client_ads_show_ads(g_p_client, CLI_ADS_AD_TYPE_MENU_UP);
  
  p_mbox = ctrl_get_ctrl_by_unique_id(p_cont, IDC_MAINMENU_MAINMENU_MBOX);
  ctrl_get_frame(p_mbox, &rect_tmp);
  rect_tmp.left =(UI_COMMON_WIDTH - g_TitleCount * MBOX_ONE_ITEM_WIDTH)/2; 
  rect_tmp.right = rect_tmp.left + g_TitleCount * MBOX_ONE_ITEM_WIDTH;
  ctrl_resize(p_mbox, &rect_tmp);
  if(g_TitleCount < 6)
  {
    mbox_set_item_interval(p_mbox, 30, 0);
  }
 
  mbox_set_focus(p_mbox, g_solid_focus_index);
  mbox_set_count(p_mbox,g_TitleCount, g_TitleCount,1);
  mainmenu_change_mbox(p_mbox,g_cur_mbox_focus, FALSE);

  p_list = ctrl_get_ctrl_by_unique_id(p_cont, IDC_MAINMENU_INSTALLATION_LIST);
  channel_list_set_count(p_list);
  list_set_update(p_list, channel_list_update, 0);
  channel_list_update(p_list, list_get_valid_pos(p_list), list_get_page(p_list),0);

  ctrl_paint_ctrl(p_cont, TRUE);
  mainmenu_refresh_list(g_cur_mbox_focus);
  mainmenu_refresh_mbox_title(g_cur_mbox_focus);
  u8 MSGfontA[2][16]={"      Auto     >","<    Manual     "};
  
  display_on_oled_string(MSGfontA[g_cur_list_focus] ,1  ); 
  display_on_oled_string( " Channel Search ",0  );
  
  return SUCCESS;
}


static void mainmenu_change_mbox(control_t * p_ctrl, u8 NewLevel, BOOL bforce)
{
  u8 i,index;
  control_t *p_root = fw_find_root_by_id(ROOT_ID_MAINMENU);
  control_t *p_mbox = ctrl_get_ctrl_by_unique_id(p_root, IDC_MAINMENU_MAINMENU_MBOX);

  for(i = 0; i<g_TitleCount; i++)
  {
    index = (u8)((2*g_TitleCount-g_solid_focus_index+NewLevel+i)%g_TitleCount);

    mbox_set_content_by_icon(p_mbox, i, g_dwIcon_HL[index], g_dwIcon_SH[index]);
  }
  if(bforce)
  {
    ctrl_paint_ctrl(p_mbox, bforce);
  }
}

static RET_CODE on_mbox_item_change_focus(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  control_t *p_mbox,*p_root;
  u16 TitleCount;
  p_root = fw_find_root_by_id(ROOT_ID_MAINMENU);
  p_mbox = ctrl_get_ctrl_by_unique_id(p_root, IDC_MAINMENU_MAINMENU_MBOX);
  TitleCount = g_TitleCount;
  OS_PRINTF("[%s] [%d] msg[%d]g_cur_list_focus =  %d \n",__FUNCTION__,__LINE__, msg, g_cur_list_focus);
  switch(msg)
  {
    case MSG_FOCUS_LEFT:
      if((g_cur_mbox_focus-1)<0)
      {
        g_cur_mbox_focus = TitleCount-1;
      }
      else
      {
        g_cur_mbox_focus--;
      }
      break;
      case MSG_FOCUS_RIGHT:
      if((g_cur_mbox_focus+1)>=TitleCount)
      {
        g_cur_mbox_focus = 0;
      }
      else
      {
        g_cur_mbox_focus++;
      }
      break;
      default:
        break;
  }  
  g_cur_list_focus = 0;
  mbox_set_focus(p_mbox, g_solid_focus_index);
  mainmenu_refresh_list(g_cur_mbox_focus);
  mainmenu_refresh_mbox_title(g_cur_mbox_focus);
  mainmenu_change_mbox(p_mbox,g_cur_mbox_focus, TRUE);

  return SUCCESS;
}

static RET_CODE on_item_update(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  control_t *p_root,*p_list = NULL;
  u16 i;
  u16 count = 0 ;
  p_root = fw_find_root_by_id(ROOT_ID_MAINMENU);

  u8 MSGfontA[2][16]={"      Auto     >","<    Manual     "};
  for(i = IDC_MAINMENU_CHANNEL_LIST; i <= IDC_MAINMENU_NETWORK_LIST; i++)
  {    
    if((i - IDC_MAINMENU_CHANNEL_LIST) == g_cur_mbox_focus)
    {
      p_list = ctrl_get_ctrl_by_unique_id(p_root, i);
      count = list_get_count(p_list);
      break;
    }
  } 
  OS_PRINTF("[%s] [%d] msg [%d]count[%d] p_ctrl->id [%d] \n",__FUNCTION__,__LINE__,msg,count,p_ctrl->id);
  if(p_list == NULL) 
  {
    return SUCCESS;
  }
  switch(msg)
  {
    case MSG_FOCUS_UP:
      if((g_cur_list_focus-1)<0)
      {
        g_cur_list_focus = count-1;
      }
      else
      {
        g_cur_list_focus--;
      }
      break;
      case MSG_FOCUS_DOWN:
      if((g_cur_list_focus+1)>=count)
      {
        g_cur_list_focus = 0;
      }
      else
      {
        g_cur_list_focus++;
      }
      break;
      default:
        break;
  }  
  list_set_focus_pos(p_list,g_cur_list_focus);
  ctrl_paint_ctrl(ctrl_get_parent(p_list),TRUE);
  
  display_on_oled_string(MSGfontA[g_cur_list_focus] ,1  ); 
  display_on_oled_string( " Channel Search ",0  );

  return SUCCESS;
}

static RET_CODE on_item_select(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  control_t *p_list = NULL;
  u16 i;
  clt_network_dev_data_t net_data;
  u16 item_ids = 0 ;
  OS_PRINTF("[%s] [%d] p_ctrl->id [%d]\n",__FUNCTION__,__LINE__,p_ctrl->id);
  comm_pwdlg_data_t factory_pwdlg_data =
  {
    ROOT_ID_MAINMENU,
    MAINMENU_PWD_DLG_FOR_CHK_X,
    MAINMENU_PWD_DLG_FOR_CHK_Y,
    IDS_MSG_INPUT_PASSWORD,
    menu_factory_pwdlg_keymap,
    menu_factory_pwdlg_proc,
    PWDLG_T_COMMON
  };

  comm_pwdlg_data_t delete_all_pwdlg_data =
  {
      ROOT_ID_MAINMENU,
      PWD_DLG_FOR_CHK_X,
      PWD_DLG_FOR_CHK_Y,
      IDS_MSG_INPUT_PASSWORD,
      main_menu_delete_all_pwdlg_keymap,
      main_menu_delete_all_pwdlg_proc,
      PWDLG_T_COMMON
  };
  
  comm_dlg_data_t p_data =
  {
    ROOT_ID_INVALID,
    DLG_FOR_SHOW | DLG_STR_MODE_STATIC,
    USB_NOTIFY_L, USB_NOTIFY_T,
    USB_NOTIFY_W, USB_NOTIFY_H,
    IDS_PLEASE_CONNECT_USB,
    800,
  };
  comm_pwdlg_data_t preopen_pwdlg_data =
  {
    ROOT_ID_MAINMENU,
    MAINMENU_PWD_DLG_FOR_CHK_X,
    MAINMENU_PWD_DLG_FOR_CHK_Y,
    IDS_MSG_INPUT_PASSWORD,
    menu_preopen_pwdlg_keymap,
    menu_preopen_pwdlg_proc,
    PWDLG_T_COMMON
  };  
  comm_dlg_data_t network_data =
  {
    ROOT_ID_INVALID,
    DLG_FOR_SHOW | DLG_STR_MODE_STATIC,
    USB_NOTIFY_L, USB_NOTIFY_T,
    USB_NOTIFY_W, USB_NOTIFY_H,
    IDS_DLNA_WARNING_INFO1,
    800,
  };
  clt_setting_lock_pwd_t para = {CLT_SETTING_MENU_LOCK_STATUS,0};

  for(i = IDC_MAINMENU_CHANNEL_LIST; i <= IDC_MAINMENU_NETWORK_LIST; i++)
  {    
    if((i - IDC_MAINMENU_CHANNEL_LIST) == g_cur_mbox_focus)
    {
      p_list = ctrl_get_ctrl_by_unique_id(p_ctrl, i);
      break;
    }
  } 
  if(p_list == NULL)
  {
    return SUCCESS;
  }
  item_ids = (u16)list_get_field_content(p_list, g_cur_list_focus, 0);
  
  for (i=0; i<sizeof(item_select_maps)/sizeof(item_select_maps[0]); i++)
  {
    if (item_select_maps[i].content_ids == item_ids)
    {
       if(item_ids == IDS_FACTORY_SET)
      {
        ui_comm_pwdlg_open(&factory_pwdlg_data);
        break;
      }
      else if(item_ids == IDS_DELETE_ALL)
      {
        ui_comm_pwdlg_open(&delete_all_pwdlg_data);
        break;
      } 
       else if(((item_ids == IDS_PARENTAL_LOCK)||(item_ids == IDS_CHANNEL_EDIT)||(item_ids == IDS_EDIT_FAV_CHANNEL))
        &&(client_setting_get_lock(g_p_client,&para) == TRUE))
      {
        ui_comm_pwdlg_open(&preopen_pwdlg_data);
        g_cur_index_in_select_map = i;
        break;
      }       
      else if((item_ids == IDS_AUTO_SEARCH) ||(item_ids == IDS_MANUAL_SEARCH) ||(item_ids == IDS_RANGE_SEARCH)||
        (item_ids == IDS_INSTALLATION)||
        (item_ids == IDS_LANGUAGE) ||(item_ids == IDS_AV_SETTING)||(item_ids == IDS_LOCAL_TIME_SET)||
        (item_ids == IDS_OSD_SET) ||(item_ids == IDS_FREQUENCY_SETTING)||(item_ids == IDS_ONLINE_MOVIE_APP)||
        (item_ids == IDS_NETWORK_CONFIG)||(item_ids == IDS_MAC_ADDRESS)||(item_ids == IDS_UPGRADE_BY_USB)||(item_ids == IDS_BACKUP_BY_USB)
        ||(item_ids == IDS_UPGRADE_BY_SAT)||(item_ids == IDS_PICTURE)||(item_ids == IDS_MUSIC)||(item_ids == IDS_MOVIES) || (item_ids == IDS_PUSH_MEDIA)
        ||(item_ids == IDS_RECORD_MANAGER)||(item_ids == IDS_HDD_INFO)||(item_ids == IDS_STORAGE_FORMAT)||
        (item_ids == IDS_DVR_CONFIG)||(item_ids == IDS_TS_RECORD))
      {
        OS_PRINTF("[%s] [%d]\n",__FUNCTION__,__LINE__);
        if(item_ids == IDS_ONLINE_MOVIE_APP)
        {
          client_get_active_netdev(g_p_client, &net_data);
          if(net_data.status != CLT_NETDEV_ST_CONNECTED)
          {
            if(fw_find_root_by_id(ROOT_ID_COMM_DLG) != NULL)
            {
                ui_comm_dlg_close();
            }
            ui_comm_dlg_open(&network_data);
            break;
          }          
        }
        else if((item_ids == IDS_UPGRADE_BY_USB)||(item_ids == IDS_BACKUP_BY_USB)||(item_ids == IDS_PICTURE)||(item_ids == IDS_MUSIC)||(item_ids == IDS_MOVIES)
        ||(item_ids == IDS_RECORD_MANAGER)||(item_ids == IDS_HDD_INFO)||(item_ids == IDS_STORAGE_FORMAT)||
        (item_ids == IDS_DVR_CONFIG)||(item_ids == IDS_TS_RECORD) || (item_ids == IDS_PUSH_MEDIA))
        {
          if(!(ui_get_usb_status()))
          {
            ui_comm_dlg_open(&p_data);
            break;
          }
        }
		else if(item_ids == IDS_AUTO_SEARCH)
		{
			ui_start_dvbc_auto_search();
			break;
		}
        client_ads_stop_ads(g_p_client);
        manage_open_menu(item_select_maps[i].root_id, 0, 0);
        break;
      }
      else
      {
        client_ads_stop_ads(g_p_client);
        manage_open_menu(item_select_maps[i].root_id, 0, 0);
      }
    }
  }

  for (i=0; i<g_ca_menu_item_count; i++)
  {
    if (ca_item_select_maps[i].content_ids == item_ids)
    {
      client_ads_stop_ads(g_p_client);
      manage_open_menu(ca_item_select_maps[i].root_id, 0, 0);
    }
  }

  return SUCCESS;
}

static void restore_to_factory(void)
{
  clt_setting_osd_t osd;
  client_setting_restore_factory(g_p_client);
  client_setting_get_osd(g_p_client, &osd);
  OS_PRINTF("set global alpha = %d\n", (100 - osd.transparent) * 255 / 100);
  if(TEST_UI_MODE(DVBC))
  {
    OS_PRINTF("BKS DVBC [%s] line [%d]\n",__FUNCTION__,__LINE__);
    ui_start_dvbc_auto_search(); 
  }
  else if(TEST_UI_MODE(DVBS))
  {
    //todo ui_start_dvbs_auto_search(); 
  }
  else if(TEST_UI_MODE(DVBT))
  {
    //todo ui_start_dvbt_auto_search(); 
  }
  
  return;
}

static void delete_all_prog(void)
{
  client_setting_delete_all_pg(g_p_client);
}

static RET_CODE on_factory_pwdlg_cancel(control_t *p_ctrl,
                                u16 msg,
                                u32 para1,
                                u32 para2)
{
  u16 key;
  switch(msg)
  {
    case MSG_FOCUS_UP:
      key = V_KEY_UP;
      break;
    case MSG_FOCUS_DOWN:
      key = V_KEY_DOWN;
      break;
    default:
      key = V_KEY_INVALID;
  }
  fw_notify_parent(ROOT_ID_COMM_PWDLG, NOTIFY_T_KEY, FALSE, key, 0, 0);
  g_cur_index_in_select_map = 0xFF;
  ui_comm_pwdlg_close();
  return SUCCESS;
}

static RET_CODE on_factory_pwdlg_exit(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  ui_comm_pwdlg_close();
  g_cur_index_in_select_map= 0xFF;
  return SUCCESS;
}

static RET_CODE on_factory_pwdlg_correct(control_t *p_ctrl,
                                 u16 msg,
                                 u32 para1,
                                 u32 para2)
{
  rect_t g_restore_dlg_rc =
  {
    MAINMENU_PWD_DLG_FOR_CHK_X,
    MAINMENU_PWD_DLG_FOR_CHK_Y,
    MAINMENU_PWD_DLG_FOR_CHK_X + COMM_DLG_W,
    MAINMENU_PWD_DLG_FOR_CHK_Y + COMM_DLG_H,
  };

  ui_comm_pwdlg_close();

  ui_comm_ask_for_savdlg_open(&g_restore_dlg_rc,
                                IDS_MSG_ASK_FOR_RESTORE_TO_FACTORY,
                                restore_to_factory, NULL, 0);


  //update_signal();

  return SUCCESS;
}

static RET_CODE on_preopen_pwdlg_correct(control_t *p_ctrl,
                                 u16 msg,
                                 u32 para1,
                                 u32 para2)
{
  ui_comm_pwdlg_close();
  
  client_ads_stop_ads(g_p_client);

  manage_open_menu(item_select_maps[g_cur_index_in_select_map].root_id, 0, 0);
  g_cur_index_in_select_map = 0xFF;

  return SUCCESS;
}

static RET_CODE on_delete_all_pwdlg_correct(control_t *p_ctrl,
							                                 u16 msg,
							                                 u32 para1,
							                                 u32 para2)
{
  rect_t g_del_all_dlg_rc =
  {
    MAINMENU_PWD_DLG_FOR_CHK_X,
    MAINMENU_PWD_DLG_FOR_CHK_Y,
    MAINMENU_PWD_DLG_FOR_CHK_X + COMM_DLG_W,
    MAINMENU_PWD_DLG_FOR_CHK_Y + COMM_DLG_H,
  };
	ui_comm_pwdlg_close();

	ui_comm_ask_for_savdlg_open(&g_del_all_dlg_rc,
								  IDS_MSG_ASK_FOR_DEL,
								  delete_all_prog,NULL,0);
 	 return SUCCESS;
}

static RET_CODE on_item_exit(control_t *p_ctrl,
                                 u16 msg,
                                 u32 para1,
                                 u32 para2)
{
  client_ads_stop_ads(g_p_client);
  manage_close_menu(ROOT_ID_MAINMENU,0,0);

  //open_infor_bar(0,0);
  return SUCCESS;
}

static RET_CODE on_item_update_tools(control_t *p_ctrl,
                                 u16 msg,
                                 u32 para1,
                                 u32 para2)
{
  control_t *p_list = NULL;
  memset(g_upgrade_list_array,0,sizeof(g_upgrade_list_array));
  b_ts_hide = (b_ts_hide == TRUE) ? FALSE: TRUE;
  _set_upgrade_list_array();
  p_list = ctrl_get_ctrl_by_unique_id(fw_find_root_by_id(ROOT_ID_MAINMENU), IDC_MAINMENU_TOOLS_LIST);
  list_set_count(p_list, _get_upgrade_list_array_size(), list_get_page(p_list));
  mtos_printk("list_count = %d\n",_get_upgrade_list_array_size());
  if(ctrl_get_sts(p_list) == OBJ_STS_SHOW)
    channel_list_update(p_list, list_get_valid_pos(p_list), list_get_page(p_list),0);
  ctrl_paint_ctrl(ctrl_get_parent(p_list), TRUE);
  return SUCCESS;
}

BEGIN_KEYMAP(mainmenu_cont_keymap, NULL)
ON_EVENT(V_KEY_UP, MSG_FOCUS_UP)
ON_EVENT(V_KEY_DOWN, MSG_FOCUS_DOWN)
ON_EVENT(V_KEY_LEFT, MSG_FOCUS_LEFT)
ON_EVENT(V_KEY_RIGHT, MSG_FOCUS_RIGHT)
ON_EVENT(V_KEY_OK, MSG_SELECT)
ON_EVENT(V_KEY_MENU, MSG_EXIT)
ON_EVENT(V_KEY_BACK, MSG_EXIT)
ON_EVENT(V_KEY_EXIT, MSG_EXIT)
ON_EVENT(V_KEY_BLUE, MSG_EMPTY)
END_KEYMAP(mainmenu_cont_keymap, NULL)

BEGIN_MSGPROC(mainmenu_cont_proc, cont_class_proc)
ON_COMMAND(MSG_EMPTY, on_item_update_tools)
ON_COMMAND(MSG_FOCUS_UP, on_item_update)
ON_COMMAND(MSG_FOCUS_DOWN, on_item_update)
ON_COMMAND(MSG_FOCUS_LEFT, on_mbox_item_change_focus)
ON_COMMAND(MSG_FOCUS_RIGHT, on_mbox_item_change_focus)
ON_COMMAND(MSG_SELECT, on_item_select)
ON_COMMAND(MSG_EXIT, on_item_exit)
END_MSGPROC(mainmenu_cont_proc, cont_class_proc)

BEGIN_KEYMAP(menu_factory_pwdlg_keymap, NULL)
  ON_EVENT(V_KEY_UP, MSG_FOCUS_UP)
  ON_EVENT(V_KEY_DOWN, MSG_FOCUS_DOWN)
  ON_EVENT(V_KEY_LEFT, MSG_FOCUS_LEFT)
  ON_EVENT(V_KEY_RIGHT, MSG_FOCUS_RIGHT)
  ON_EVENT(V_KEY_CANCEL, MSG_EXIT)
  ON_EVENT(V_KEY_EXIT, MSG_EXIT)
  ON_EVENT(V_KEY_BACK, MSG_EXIT)
  ON_EVENT(V_KEY_MENU, MSG_EXIT)
END_KEYMAP(menu_factory_pwdlg_keymap, NULL)

BEGIN_MSGPROC(menu_factory_pwdlg_proc, cont_class_proc)
  ON_COMMAND(MSG_FOCUS_UP, on_factory_pwdlg_cancel)
  ON_COMMAND(MSG_FOCUS_DOWN, on_factory_pwdlg_cancel)
  ON_COMMAND(MSG_CORRECT_PWD, on_factory_pwdlg_correct)
  ON_COMMAND(MSG_EXIT, on_factory_pwdlg_exit)
END_MSGPROC(menu_factory_pwdlg_proc, cont_class_proc)

BEGIN_KEYMAP(menu_preopen_pwdlg_keymap, NULL)
  ON_EVENT(V_KEY_UP, MSG_FOCUS_UP)
  ON_EVENT(V_KEY_DOWN, MSG_FOCUS_DOWN)
  ON_EVENT(V_KEY_LEFT, MSG_FOCUS_LEFT)
  ON_EVENT(V_KEY_RIGHT, MSG_FOCUS_RIGHT)
  ON_EVENT(V_KEY_CANCEL, MSG_EXIT)
  ON_EVENT(V_KEY_EXIT, MSG_EXIT)
  ON_EVENT(V_KEY_BACK, MSG_EXIT)
  ON_EVENT(V_KEY_MENU, MSG_EXIT)
END_KEYMAP(menu_preopen_pwdlg_keymap, NULL)

BEGIN_MSGPROC(menu_preopen_pwdlg_proc, cont_class_proc)
  ON_COMMAND(MSG_FOCUS_UP, on_factory_pwdlg_cancel)//same as factory pwdlg
  ON_COMMAND(MSG_FOCUS_DOWN, on_factory_pwdlg_cancel)
  ON_COMMAND(MSG_CORRECT_PWD, on_preopen_pwdlg_correct)
  ON_COMMAND(MSG_EXIT, on_factory_pwdlg_exit)
END_MSGPROC(menu_preopen_pwdlg_proc, cont_class_proc)

BEGIN_KEYMAP(main_menu_delete_all_pwdlg_keymap, NULL)
  ON_EVENT(V_KEY_UP, MSG_FOCUS_UP)
  ON_EVENT(V_KEY_DOWN, MSG_FOCUS_DOWN)
  ON_EVENT(V_KEY_LEFT, MSG_FOCUS_LEFT)
  ON_EVENT(V_KEY_RIGHT, MSG_FOCUS_RIGHT)
  ON_EVENT(V_KEY_CANCEL, MSG_EXIT)
  ON_EVENT(V_KEY_MENU, MSG_EXIT)
END_KEYMAP(main_menu_delete_all_pwdlg_keymap, NULL)

BEGIN_MSGPROC(main_menu_delete_all_pwdlg_proc, cont_class_proc)
  ON_COMMAND(MSG_FOCUS_UP, on_factory_pwdlg_cancel)
  ON_COMMAND(MSG_FOCUS_DOWN, on_factory_pwdlg_cancel)
  ON_COMMAND(MSG_CORRECT_PWD, on_delete_all_pwdlg_correct)
  ON_COMMAND(MSG_EXIT, on_factory_pwdlg_cancel)
END_MSGPROC(main_menu_delete_all_pwdlg_proc, cont_class_proc)


BEGIN_KEYMAP(bootup_menu_cont_keymap, NULL)
ON_EVENT(V_KEY_LEFT, MSG_FOCUS_UP)
ON_EVENT(V_KEY_RIGHT, MSG_FOCUS_DOWN)
ON_EVENT(V_KEY_OK, MSG_SELECT)
ON_EVENT(V_KEY_EXIT, MSG_EXIT)
END_KEYMAP(bootup_menu_cont_keymap, NULL)

BEGIN_MSGPROC(bootup_menu_cont_proc, cont_class_proc)
ON_COMMAND(MSG_EMPTY, on_item_update_tools)
ON_COMMAND(MSG_FOCUS_UP, on_item_update)
ON_COMMAND(MSG_FOCUS_DOWN, on_item_update)
ON_COMMAND(MSG_SELECT, on_item_select)
ON_COMMAND(MSG_EXIT, on_item_exit)
END_MSGPROC(bootup_menu_cont_proc, cont_class_proc)



