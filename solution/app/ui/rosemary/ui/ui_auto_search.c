/********************************************************************************************/
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/* Montage Proprietary and Confidential                                                     */
/* Copyright (c) 2017 Montage Technology Group Limited and its affiliated companies         */
/********************************************************************************************/
#include "ui_common.h"
#include "ui_desc.h"
#include "ui_auto_search.h"
#include "ui_do_search.h"

enum auto_search_local_msg
{
  MSG_AUTO_SEARCH_START = MSG_LOCAL_BEGIN + 60,
  MSG_AUTO_SEARCH_END,
};

void display_on_oled_string( char *string,u8 line  );
static u16 auto_search_cont_keymap(u16 key);
//static u16 auto_search_boot_up_keymap(u16 key);

static RET_CODE auto_search_cont_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);
//static RET_CODE auto_search_boot_up_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);
RET_CODE search_select_proc(control_t *p_ctrl, u16 msg, u32 para1, u32 para2);


static u16 specify_tp_frm_cont_keymap(u16 key);
static RET_CODE specify_tp_frm_cont_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static RET_CODE specify_start_search_text_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static RET_CODE specify_tp_sym_nbox_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static RET_CODE specify_tp_demod_cbox_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static void auto_search_check_signal(control_t *cont, u16 msg, u32 para1, u32 para2)
{
  clt_nim_info_t para;
  control_t *p_root = fw_find_root_by_id(ROOT_ID_AUTO_SEARCH);
  u16 demod_focus;
  control_t *p_ctrl;

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_AUTO_SEARCH_SPECIFY_TP_FREQ);
  para.lockc.tp_freq = nbox_get_num(p_ctrl) * 1000;

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_AUTO_SEARCH_SPECIFY_TP_SYM);
  para.lockc.tp_sym = nbox_get_num(p_ctrl);

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_AUTO_SEARCH_SPECIFY_TP_DEMOD);
  demod_focus = (u8)cbox_static_get_focus(p_ctrl);
  switch(demod_focus)
  {
    case 0:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_AUTO;
      break;

    case 1:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_BPSK;      
      break;

    case 2:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QPSK;      
      break;

    case 3:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_8PSK;      
      break;

    case 4:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QAM16;
      break;
      
    case 5:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QAM32;
      break;

    case 6:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QAM64;      
      break;

    case 7:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QAM128;
      break;

    case 8:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QAM256;
      break;
      
    default:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QAM64;
      break;
  }
 para.lock_mode = CLT_SYS_DVBC;

 client_set_signal_tp(g_p_client, &para);

}

RET_CODE open_auto_search (u32 para1, u32 para2)
{
  control_t *p_cont;
  clt_setting_main_tp_t tp_info;
  OS_PRINTF("[%s] para1[%d] para2[%d]\n",__FUNCTION__,para1,para2);
  p_cont = init_desc(ROOT_ID_AUTO_SEARCH, para1, para2);


  if(p_cont == NULL)
  {
    return ERR_FAILURE;
  }
  client_setting_get_main_tp(g_p_client, &tp_info);
  ctrl_set_keymap(p_cont, auto_search_cont_keymap);
  ctrl_set_proc(p_cont, auto_search_cont_proc);
  
  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(p_cont, IDC_AUTO_SEARCH_SPECIFY_TP_FRM), 
  	specify_tp_frm_cont_keymap);
  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_AUTO_SEARCH_SPECIFY_TP_FRM),
  	specify_tp_frm_cont_proc);

  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_AUTO_SEARCH_SPECIFY_TP_FREQ),
   ui_comm_num_keymap);

  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_AUTO_SEARCH_SPECIFY_TP_SYM),
   ui_comm_num_keymap);

  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_AUTO_SEARCH_SPECIFY_TP_DEMOD),
   ui_comm_select_keymap);
 
  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_AUTO_SEARCH_SPECIFY_CHANNEL_FTA),
   ui_comm_select_keymap);

  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_AUTO_SEARCH_SPECIFY_START_SEARCH),
   ui_comm_static_keymap);
  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_AUTO_SEARCH_SPECIFY_START_SEARCH),
  	specify_start_search_text_proc);
  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_AUTO_SEARCH_SPECIFY_TP_FREQ), 
	  specify_tp_sym_nbox_proc);

  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_AUTO_SEARCH_SPECIFY_TP_SYM), 
	  specify_tp_sym_nbox_proc);
  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_AUTO_SEARCH_SPECIFY_TP_DEMOD), 
	  specify_tp_demod_cbox_proc);

  nbox_set_num_by_dec(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_AUTO_SEARCH_SPECIFY_TP_FREQ),tp_info.freq/1000);
  nbox_set_num_by_dec(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_AUTO_SEARCH_SPECIFY_TP_SYM),tp_info.sym);
  cbox_static_set_focus(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_AUTO_SEARCH_SPECIFY_START_SEARCH), tp_info.mod);
  
  ctrl_paint_ctrl(p_cont, FALSE);
  auto_search_check_signal(p_cont, 0, 0, 0);
  return SUCCESS;
}

static RET_CODE on_start_search(control_t *p_cont, u16 msg, u32 para1, u32 para2)
{
  clt_scan_input_para_t para;
  control_t *p_root = fw_find_root_by_id(ROOT_ID_AUTO_SEARCH);
  u16 demod_focus;
  control_t *p_ctrl;
  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_AUTO_SEARCH_SPECIFY_TP_FREQ);
  para.tp.freq = nbox_get_num(p_ctrl) * 1000;

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_AUTO_SEARCH_SPECIFY_TP_SYM);
  para.tp.sym = nbox_get_num(p_ctrl);

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_AUTO_SEARCH_SPECIFY_TP_DEMOD);
  demod_focus = (u8)cbox_static_get_focus(p_ctrl);

  OS_PRINTF ( "%s %d> caller [%x] \n",__FUNCTION__,__LINE__ ,__builtin_return_address((unsigned int )1) );

  switch(demod_focus)
  {
    case 0:
      para.tp.mod = CLT_SCAN_NIM_MODULA_AUTO;
      break;

    case 1:
      para.tp.mod = CLT_SCAN_NIM_MODULA_BPSK;      
      break;

    case 2:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QPSK;      
      break;

    case 3:
      para.tp.mod = CLT_SCAN_NIM_MODULA_8PSK;      
      break;

    case 4:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM16;
      break;
      
    case 5:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM32;
      break;

    case 6:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM64;      
      break;

    case 7:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM128;
      break;

    case 8:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM256;
      break;
      
    default:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM64;
      break;
  }
  
  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_AUTO_SEARCH_SPECIFY_CHANNEL_FTA);
  para.free_only = cbox_static_get_focus(p_ctrl) ? TRUE : FALSE;
  
  para.scan_type = CLT_SCAN_TYPE_AUTO;
  para.chan_type = CLT_SCAN_CHAN_RADIO; //Raja changed for radio only
  para.nit_type = CLT_SCAN_NIT_SCAN_ONCE;

  OS_PRINTF("[%s] para1[%d] para2[%d]\n",__FUNCTION__,para1,para2);
  open_do_search((u32)&para, 0);
  return SUCCESS;
}

static RET_CODE on_exit_search(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  manage_close_menu(ROOT_ID_AUTO_SEARCH,0,0);
  return SUCCESS;
}

BOOL auto_search_tp_bar_update(control_t *p_bar, control_t *p_txt, u16 val, BOOL is_force,u8 *pox)
{
  u8 str_buf[10];
  BOOL is_redraw = FALSE;

  if(pbar_get_current(p_bar) != val || is_force)
  {
    pbar_set_current(p_bar, val);
    sprintf((char*)str_buf, "%d%s", val,pox);
    text_set_content_by_ascstr(p_txt, (u8*)str_buf);

    is_redraw = TRUE;
  }
  ctrl_paint_ctrl(p_bar, is_redraw);
  ctrl_paint_ctrl(p_txt, is_redraw);
  return is_redraw;
}

static RET_CODE on_auto_search_signal_update(control_t *p_cont, u16 msg, u32 para1, u32 para2)
{
  control_t *p_bar, *p_txt;
  clt_signal_data_t *data = (clt_signal_data_t *)(para1);

  if(!ui_check_signal_info(data))
  {
    return SUCCESS;
  }
  p_bar = ctrl_get_ctrl_by_unique_id(p_cont, IDC_AUTO_SEARCH_SEARCH_SIG_STRENGTH_BAR);
  p_txt = ctrl_get_ctrl_by_unique_id(p_cont, IDC_AUTO_SEARCH_SEARCH_SIG_STRENGTH_BAR_PER);
  auto_search_tp_bar_update(p_bar,p_txt, data->intensity, TRUE, (u8*)"dBuv");
  p_bar = ctrl_get_ctrl_by_unique_id(p_cont, IDC_AUTO_SEARCH_SEARCH_SIG_BER_BAR);
  p_txt = ctrl_get_ctrl_by_unique_id(p_cont, IDC_AUTO_SEARCH_SEARCH_SIG_BER_BAR_PER);
  auto_search_tp_bar_update(p_bar,p_txt, data->ber, TRUE, (u8*)"E-6");
  p_bar = ctrl_get_ctrl_by_unique_id(p_cont, IDC_AUTO_SEARCH_SEARCH_SIG_SNR_BAR);
  p_txt = ctrl_get_ctrl_by_unique_id(p_cont, IDC_AUTO_SEARCH_SEARCH_SIG_SNR_BAR_PER);
  auto_search_tp_bar_update(p_bar,p_txt, data->quality, TRUE, (u8*)"dB");

  return SUCCESS;
}

static RET_CODE on_auto_search_change_freq_symbol(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  nbox_class_proc(p_ctrl, msg, para1, para2);
  auto_search_check_signal(p_ctrl, msg, para1, para2);
  return SUCCESS;
}

static RET_CODE on_auto_search_change_qam(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  u16 f_index;
  f_index = cbox_static_get_focus(p_ctrl);
  switch(msg)
  {
	case MSG_INCREASE:
		if(f_index == 8)
		{
			cbox_static_set_focus(p_ctrl,4);
		       ctrl_paint_ctrl(p_ctrl, FALSE);
		}
		else
		{
			 cbox_class_proc(p_ctrl, msg, para1, para2);
		}
		break;
	case MSG_DECREASE:
		if(f_index == 4)
		{
			cbox_static_set_focus(p_ctrl,8);
		       ctrl_paint_ctrl(p_ctrl, FALSE);
		}
		else
		{
			cbox_class_proc(p_ctrl, msg, para1, para2);
		}
		break;
	default:
		break;
  }
 
  auto_search_check_signal(p_ctrl, msg, para1, para2);
  return SUCCESS;
}

RET_CODE ui_start_dvbc_auto_search(void)
{
  clt_setting_main_tp_t tp_info;
  clt_scan_input_para_t para;

  client_setting_get_main_tp(g_p_client, &tp_info);
  para.tp.freq = tp_info.freq;
  para.tp.sym = tp_info.sym;
  switch(tp_info.mod)
  {
    case 0:
      para.tp.mod = CLT_SCAN_NIM_MODULA_AUTO;
      break;

    case 1:
      para.tp.mod = CLT_SCAN_NIM_MODULA_BPSK;      
      break;

    case 2:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QPSK;      
      break;

    case 3:
      para.tp.mod = CLT_SCAN_NIM_MODULA_8PSK;      
      break;

    case 4:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM16;
      break;
      
    case 5:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM32;
      break;

    case 6:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM64;      
      break;

    case 7:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM128;
      break;

    case 8:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM256;
      break;
      
    default:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM64;
      break;
  }
  para.free_only = FALSE;

  para.scan_type = CLT_SCAN_TYPE_AUTO;
  para.chan_type = CLT_SCAN_CHAN_RADIO; //Raja changed for radio only
  para.nit_type = CLT_SCAN_NIT_SCAN_ONCE;

  display_on_oled_string("   Auto Scan    ",0  ); 
  display_on_oled_string("  In Progress   ",1  );
  
  return manage_open_menu(ROOT_ID_DO_SEARCH, (u32)&para, 0); 
}

#if 0
RET_CODE ui_auto_search_boot_up(u32 para1, u32 para2)
{
  int search_index;
  control_t *p_search;
  control_t *p_cont;
  clt_setting_main_tp_t tp_info;
  OS_PRINTF("[%s] line [%d]\n",__FUNCTION__,__LINE__);
  p_cont = init_desc(ROOT_ID_AUTO_SEARCH, para1, para2);
  if(p_cont == NULL)
  {
    return ERR_FAILURE;
  }
  client_setting_get_main_tp(g_p_client, &tp_info);
  ctrl_set_keymap(p_cont, auto_search_boot_up_keymap);
  ctrl_set_proc(p_cont, auto_search_boot_up_proc);

    p_search = ctrl_get_ctrl_by_unique_id(p_cont, 
    IDC_AUTO_SEARCH_SPECIFY_START_SEARCH);
	
  //ctrl_set_proc(p_search, search_select_proc);

      u8 MSGfontA[2][16]={"    Auto    >","<   Manual    "};
    search_index = para1;

	search_index %= (2);
    display_on_oled_string(MSGfontA[search_index] ,1  ); 
    display_on_oled_string( " Channel Search ",0  );
	
  OS_PRINTF("[%s] END line [%d]\n",__FUNCTION__,__LINE__);
  return SUCCESS;

}

static RET_CODE on_auto_select(control_t *p_ctrl,
                                       u16 msg,
                                       u32 para1,
                                       u32 para2)
{

    OS_PRINTF("[%s] END line [%d]\n",__FUNCTION__,__LINE__);

	ui_start_dvbc_auto_search();
	return SUCCESS;
}

/*
static RET_CODE on_select_search_focus(control_t *p_ctrl,
                                       u16 msg,
                                       u32 para1,
                                       u32 para2)
{  
  control_t *p_root;

  OS_PRINTF("[%s] [%d]\n",__FUNCTION__,__LINE__);

  int cur_id = ctrl_get_ctrl_id(p_ctrl);
  int search_index;
  u8 MSGfontA[2][16]={"    Auto    >","<   Manual    "};
  
  p_root = fw_find_root_by_id(ROOT_ID_AUTO_SEARCH);
  

  search_index = para1;
  OS_PRINTF("cur_id[%d] msg[%d] para1[%d] para2[%d]\n",cur_id,msg,para1,para2);
  switch(cur_id)
  {
    case IDC_AUTO_SEARCH_SPECIFY_START_SEARCH:
        if(p_root != NULL)
        {
          ctrl_paint_ctrl(p_root, TRUE);
        }
    break;
    default:
      MT_ASSERT(0);
      return ERR_FAILURE;
  }
  search_index %= (2);
  display_on_oled_string(MSGfontA[search_index] ,1  ); 
  display_on_oled_string( " Channel Search ",0  );

  return SUCCESS;
}
*/
#endif

BEGIN_KEYMAP(auto_search_cont_keymap, NULL)
ON_EVENT(V_KEY_EXIT, MSG_EXIT)
ON_EVENT(V_KEY_MENU, MSG_EXIT)
END_KEYMAP(auto_search_cont_keymap, NULL)

BEGIN_MSGPROC(auto_search_cont_proc, cont_class_proc)
ON_COMMAND(MSG_EXIT, on_exit_search)
ON_COMMAND(MSG_SIGNAL_UPDATE, on_auto_search_signal_update)
END_MSGPROC(auto_search_cont_proc, cont_class_proc)

BEGIN_KEYMAP(specify_tp_frm_cont_keymap, NULL)
ON_EVENT(V_KEY_UP, MSG_FOCUS_UP)
ON_EVENT(V_KEY_DOWN, MSG_FOCUS_DOWN)
END_KEYMAP(specify_tp_frm_cont_keymap, NULL)

BEGIN_MSGPROC(specify_tp_frm_cont_proc, cont_class_proc)
END_MSGPROC(specify_tp_frm_cont_proc, cont_class_proc)

BEGIN_MSGPROC(specify_start_search_text_proc, text_class_proc)
ON_COMMAND(MSG_SELECT, on_start_search)
END_MSGPROC(specify_start_search_text_proc, text_class_proc)

BEGIN_MSGPROC(specify_tp_sym_nbox_proc, ui_comm_num_proc)
ON_COMMAND(MSG_NUMBER, on_auto_search_change_freq_symbol)
END_MSGPROC(specify_tp_sym_nbox_proc, ui_comm_num_proc)

BEGIN_MSGPROC(specify_tp_demod_cbox_proc, cbox_class_proc)
ON_COMMAND(MSG_INCREASE, on_auto_search_change_qam)
ON_COMMAND(MSG_DECREASE, on_auto_search_change_qam)
END_MSGPROC(specify_tp_demod_cbox_proc, cbox_class_proc)

/*

BEGIN_KEYMAP(auto_search_boot_up_keymap, NULL)
ON_EVENT(V_KEY_OK, MSG_YES)
ON_EVENT(V_KEY_LEFT, MSG_DECREASE)
ON_EVENT(V_KEY_RIGHT, MSG_INCREASE)
END_KEYMAP(auto_search_boot_up_keymap, NULL)



BEGIN_MSGPROC(auto_search_boot_up_proc, cont_class_proc)
ON_COMMAND(MSG_YES, on_auto_select)
END_MSGPROC(auto_search_boot_up_proc, cont_class_proc)


BEGIN_MSGPROC(search_select_proc, cbox_class_proc)
ON_COMMAND(MSG_CHANGED, on_select_search_focus)
END_MSGPROC(search_select_proc, cbox_class_proc)*/

