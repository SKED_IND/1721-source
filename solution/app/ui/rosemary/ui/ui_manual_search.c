/********************************************************************************************/
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/* Montage Proprietary and Confidential                                                     */
/* Copyright (c) 2017 Montage Technology Group Limited and its affiliated companies         */
/********************************************************************************************/
#include "ui_common.h"
#include "ui_desc.h"
#include "ui_manual_search.h"
#include "ui_do_search.h"
#include "ui_mainmenu.h"

enum manual_search_local_msg
{
  MSG_MANUAL_SEARCH_START = MSG_LOCAL_BEGIN + 80,
  MSG_MANUAL_SEARCH_END,
};

static u16 g_cur_list_focus = 0;

void display_on_oled_string( char *string,u8 line  );
RET_CODE open_infor_bar (u32 para1, u32 para2);


static u16 manual_search_cont_keymap(u16 key);
static RET_CODE manual_search_cont_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static u16 specify_tp_frm_cont_keymap(u16 key);
static RET_CODE specify_tp_frm_cont_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static RET_CODE specify_start_search_text_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static RET_CODE specify_tp_sym_nbox_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static RET_CODE specify_tp_demod_cbox_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static RET_CODE specify_tp_nit_cbox_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static RET_CODE specify_tp_fta_cbox_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static void manual_search_check_signal(control_t *cont, u16 msg, u32 para1, u32 para2)
{
  clt_nim_info_t para;
  control_t *p_root = fw_find_root_by_id(ROOT_ID_MANUAL_SEARCH);
  u16 demod_focus;
  control_t *p_ctrl;
  u8 MSGDSP[16];
  static u32 freq_cnt, symb_cnt;
  static u32 tp_freq,tp_sym;

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_MANUAL_SEARCH_SPECIFY_TP_FREQ);
  para.lockc.tp_freq = nbox_get_num(p_ctrl) * 1000;
  OS_PRINTF("[%s] tp_freq[%d] p_ctrl->id [%d] cont->id [%d]\n",__FUNCTION__,para.lockc.tp_freq,p_ctrl->id,cont->id);
  if(cont->id == IDC_MANUAL_SEARCH_SPECIFY_TP_FREQ)
  {
      freq_cnt++;
	  tp_freq = nbox_get_num(p_ctrl);
      snprintf(MSGDSP,17,"      %03d       ",(int)tp_freq);
	  display_on_oled_string("   Frequency    ",0  ); 
      display_on_oled_string(MSGDSP,1  ); 
	  if(freq_cnt > 3)
	  	freq_cnt = 0;
  }

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_MANUAL_SEARCH_SPECIFY_TP_SYM);
  para.lockc.tp_sym = nbox_get_num(p_ctrl);
  OS_PRINTF("[%s] tp_sym[%d] p_ctrl->id [%d] cont->id [%d]\n",__FUNCTION__,para.lockc.tp_sym,p_ctrl->id,cont->id);
  if(cont->id == IDC_MANUAL_SEARCH_SPECIFY_TP_SYM)
  {
      symb_cnt++;
  	  tp_sym = nbox_get_num(p_ctrl);
      snprintf(MSGDSP,17,"      %03d      ",(int)tp_sym);
	  display_on_oled_string("  Symbol Rate   ",0  ); 
      display_on_oled_string(MSGDSP,1  ); 
	  if(symb_cnt > 3)
	  	symb_cnt = 0;
  }

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_MANUAL_SEARCH_SPECIFY_TP_DEMOD);
  demod_focus = (u8)cbox_static_get_focus(p_ctrl);
  switch(demod_focus)
  {
    case 0:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_AUTO;
      break;

    case 1:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_BPSK;      
      break;

    case 2:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QPSK;      
      break;

    case 3:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_8PSK;      
      break;

    case 4:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QAM16;
      break;
      
    case 5:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QAM32;
      break;

    case 6:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QAM64;      
      break;

    case 7:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QAM128;
      break;

    case 8:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QAM256;
      break;
      
    default:
      para.lockc.nim_modulate = CLT_SCAN_NIM_MODULA_QAM64;
      break;
  }
 para.lock_mode = CLT_SYS_DVBC;

 client_set_signal_tp(g_p_client, &para);

}

RET_CODE open_manual_search (u32 para1, u32 para2)
{
  control_t *p_cont;
  clt_setting_main_tp_t tp_info;
  p_cont = init_desc(ROOT_ID_MANUAL_SEARCH, para1, para2);
  if(p_cont == NULL)
  {
    return ERR_FAILURE;
  }
  g_cur_list_focus = 0;
  client_setting_get_main_tp(g_p_client, &tp_info);
  ctrl_set_keymap(p_cont, manual_search_cont_keymap);
  ctrl_set_proc(p_cont, manual_search_cont_proc);
  
  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(p_cont, IDC_MANUAL_SEARCH_SPECIFY_TP_FRM), 
  	specify_tp_frm_cont_keymap);
  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_MANUAL_SEARCH_SPECIFY_TP_FRM),
  	specify_tp_frm_cont_proc);
  
  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_MANUAL_SEARCH_SPECIFY_TP_FREQ),
   ui_comm_num_keymap);

  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_MANUAL_SEARCH_SPECIFY_TP_SYM),
   ui_comm_num_keymap);

  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_MANUAL_SEARCH_SPECIFY_TP_DEMOD),
   ui_comm_select_keymap);

  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_MANUAL_SEARCH_SPECIFY_TP_NIT),
   ui_comm_select_keymap);
 
  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_MANUAL_SEARCH_FTA),
   ui_comm_select_keymap);
  
  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_MANUAL_SEARCH_SPECIFY_START_SEARCH),
   ui_comm_static_keymap);
  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_MANUAL_SEARCH_SPECIFY_START_SEARCH),
  	specify_start_search_text_proc);

  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_MANUAL_SEARCH_SPECIFY_TP_SYM), 
	  specify_tp_sym_nbox_proc);
  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_MANUAL_SEARCH_SPECIFY_TP_FREQ), 
	  specify_tp_sym_nbox_proc);

  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_MANUAL_SEARCH_SPECIFY_TP_DEMOD), 
	  specify_tp_demod_cbox_proc);

  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_MANUAL_SEARCH_SPECIFY_TP_NIT), 
	  specify_tp_nit_cbox_proc);

  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_MANUAL_SEARCH_FTA), 
	  specify_tp_fta_cbox_proc);
  
  nbox_set_num_by_dec(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_MANUAL_SEARCH_SPECIFY_TP_FREQ),tp_info.freq/1000);
  nbox_set_num_by_dec(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_MANUAL_SEARCH_SPECIFY_TP_SYM),tp_info.sym);
  cbox_static_set_focus(ctrl_get_ctrl_by_unique_id(
   p_cont, IDC_MANUAL_SEARCH_SPECIFY_TP_DEMOD), tp_info.mod);
  
  ctrl_paint_ctrl(p_cont, FALSE);
  manual_search_check_signal(p_cont, 0, 0, 0);
  
  display_on_oled_string( "   Frequency    ",0  );
  display_on_oled_string( "      314       ",1	);

  return SUCCESS;
}

static RET_CODE on_start_search(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  clt_scan_input_para_t para;
  control_t *p_root = fw_find_root_by_id(ROOT_ID_MANUAL_SEARCH);
  u16 demod_focus;

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_MANUAL_SEARCH_SPECIFY_TP_FREQ);
  para.tp.freq = nbox_get_num(p_ctrl) * 1000;

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_MANUAL_SEARCH_SPECIFY_TP_SYM);
  para.tp.sym = nbox_get_num(p_ctrl);

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_MANUAL_SEARCH_SPECIFY_TP_DEMOD);
  demod_focus = (u8)cbox_static_get_focus(p_ctrl);
  switch(demod_focus)
  {
    case 0:
      para.tp.mod = CLT_SCAN_NIM_MODULA_AUTO;
      break;

    case 1:
      para.tp.mod = CLT_SCAN_NIM_MODULA_BPSK;      
      break;

    case 2:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QPSK;      
      break;

    case 3:
      para.tp.mod = CLT_SCAN_NIM_MODULA_8PSK;      
      break;

    case 4:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM16;
      break;
      
    case 5:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM32;
      break;

    case 6:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM64;      
      break;

    case 7:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM128;
      break;

    case 8:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM256;
      break;
      
    default:
      para.tp.mod = CLT_SCAN_NIM_MODULA_QAM64;
      break;
  }
  
  
  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_MANUAL_SEARCH_SPECIFY_TP_NIT);
  para.nit_type = cbox_static_get_focus(p_ctrl) ? TRUE : FALSE;
  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_MANUAL_SEARCH_FTA);
  para.free_only = cbox_static_get_focus(p_ctrl) ? TRUE : FALSE;
  OS_PRINTF("FTA %d\n", para.free_only);
  para.scan_type = CLT_SCAN_TYPE_MANUAL;
  para.chan_type = CLT_SCAN_CHAN_RADIO; //Raja changed for radio only

  display_on_oled_string("  Manual Scan   ",0  ); 
  display_on_oled_string("  In Progress   ",1  );
  
  open_do_search((u32)&para, 0);
  return SUCCESS;
}

static RET_CODE on_exit_search(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  control_t *p_root, *p_list = NULL;

  u8 MSGfontA[2][16]={"      Auto     >","<    Manual     "};
  p_root = fw_find_root_by_id(ROOT_ID_MAINMENU);
  p_list = ctrl_get_ctrl_by_unique_id(p_root, IDC_MAINMENU_INSTALLATION_LIST);

  OS_PRINTF("[%s] [%d]\n",__FUNCTION__,list_get_focus_pos(p_list));
  
  display_on_oled_string(MSGfontA[list_get_focus_pos(p_list)] ,1  ); 
  display_on_oled_string( " Channel Search ",0  );
  
  manage_close_menu(ROOT_ID_MANUAL_SEARCH,0,0);
  return SUCCESS;
}

BOOL manual_search_tp_bar_update(control_t *p_bar, control_t *p_txt, u16 val, BOOL is_force,u8 *pox)
{
  u8 str_buf[10];
  BOOL is_redraw = FALSE;

  if(pbar_get_current(p_bar) != val || is_force)
  {
    pbar_set_current(p_bar, val);
    sprintf((char*)str_buf, "%d%s", val,pox);
    text_set_content_by_ascstr(p_txt, (u8*)str_buf);

    is_redraw = TRUE;
  }
  ctrl_paint_ctrl(p_bar, is_redraw);
  ctrl_paint_ctrl(p_txt, is_redraw);
  return is_redraw;
}

static RET_CODE on_manual_search_signal_update(control_t *p_cont, u16 msg, u32 para1, u32 para2)
{
  control_t *p_bar, *p_txt;
  clt_signal_data_t *data = (clt_signal_data_t *)(para1);

  OS_PRINTF("func[%s] line[%d]\n", __FUNCTION__, __LINE__);
  if(!ui_check_signal_info(data))
  {
    return SUCCESS;
  }
  
  p_bar = ctrl_get_ctrl_by_unique_id(p_cont, IDC_MANUAL_SEARCH_SEARCH_SIG_STRENGTH_BAR);
  p_txt = ctrl_get_ctrl_by_unique_id(p_cont, IDC_MANUAL_SEARCH_SEARCH_SIG_STRENGTH_BAR_PER);
  manual_search_tp_bar_update(p_bar,p_txt, data->intensity, TRUE, (u8*)"dBuv");
  p_bar = ctrl_get_ctrl_by_unique_id(p_cont, IDC_MANUAL_SEARCH_SEARCH_SIG_BER_BAR);
  p_txt = ctrl_get_ctrl_by_unique_id(p_cont, IDC_MANUAL_SEARCH_SEARCH_SIG_BER_BAR_PER);
  manual_search_tp_bar_update(p_bar,p_txt, data->ber, TRUE, (u8*)"E-6");
  p_bar = ctrl_get_ctrl_by_unique_id(p_cont, IDC_MANUAL_SEARCH_SEARCH_SIG_SNR_BAR);
  p_txt = ctrl_get_ctrl_by_unique_id(p_cont, IDC_MANUAL_SEARCH_SEARCH_SIG_SNR_BAR_PER);
  manual_search_tp_bar_update(p_bar,p_txt, data->quality, TRUE, (u8*)"dB");

  //open_infor_bar(0,0);
  return SUCCESS;
}

static RET_CODE on_manual_search_change_freq_symbol(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  nbox_class_proc(p_ctrl, msg, para1, para2);
  manual_search_check_signal(p_ctrl, msg, para1, para2);
  return SUCCESS;
}

static RET_CODE on_manual_search_change_qam(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  u16 f_index;
  f_index = cbox_static_get_focus(p_ctrl);
  OS_PRINTF("[%s] f_index [%d] [0x%x]\n",__FUNCTION__,f_index,msg);
  
  u8 MSGDSP[5][16]={"<    QAM256    >","<    QAM16     >","<    QAM32     >","<    QAM64     >","<    QAM128    >"};
  switch(msg)
  {
	case MSG_INCREASE:
		if(f_index == 8)
		{
			cbox_static_set_focus(p_ctrl,4);
		       ctrl_paint_ctrl(p_ctrl, FALSE);
		}
		else
		{
			 cbox_class_proc(p_ctrl, msg, para1, para2);
		}
        display_on_oled_string("   Modulation   ",0  ); 
        display_on_oled_string(MSGDSP[(f_index + 3)%5],1  ); 
		break;
	case MSG_DECREASE:
		if(f_index == 4)
		{
			cbox_static_set_focus(p_ctrl,8);
		       ctrl_paint_ctrl(p_ctrl, FALSE);
		}
		else
		{
			cbox_class_proc(p_ctrl, msg, para1, para2);
		}
        display_on_oled_string("   Modulation   ",0  ); 
        display_on_oled_string(MSGDSP[f_index - 4],1  ); 
		break;
	default:
		break;
  }

  manual_search_check_signal(p_ctrl, msg, para1, para2);
  return SUCCESS;
}

static RET_CODE on_manual_search_change_nit(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  u16 f_index;
  clt_scan_nit_scan_type_t nit_type;
  control_t *p_root = fw_find_root_by_id(ROOT_ID_MANUAL_SEARCH);
  
  f_index = cbox_static_get_focus(p_ctrl);
  OS_PRINTF("[%s] f_index [%d]\n",__FUNCTION__,f_index);
  
  u8 MSGDSP[2][16]={"<     NO       >","<      YES     >"};
  
  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_MANUAL_SEARCH_SPECIFY_TP_NIT);
  nit_type = cbox_static_get_focus(p_ctrl);

  cbox_static_set_focus(p_ctrl,!nit_type);
  ctrl_paint_ctrl(p_ctrl, FALSE);
  
  display_on_oled_string("      NIT       ",0  ); 
  display_on_oled_string(MSGDSP[!nit_type], 1	); 
  return SUCCESS;
}

static RET_CODE on_manual_search_change_fta(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  u16 f_index;
  clt_scan_nit_scan_type_t nit_type;
  control_t *p_root = fw_find_root_by_id(ROOT_ID_MANUAL_SEARCH);
  
  f_index = cbox_static_get_focus(p_ctrl);
  OS_PRINTF("[%s] f_index [%d]\n",__FUNCTION__,f_index);
  
  u8 MSGDSP[2][16]={"<     NO       >","<      YES     >"};
  
  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_MANUAL_SEARCH_FTA);
  nit_type = cbox_static_get_focus(p_ctrl);

  cbox_static_set_focus(p_ctrl,!nit_type);
  ctrl_paint_ctrl(p_ctrl, FALSE);
  
  display_on_oled_string("      FTA       ",0  ); 
  display_on_oled_string(MSGDSP[!nit_type], 1	); 
  return SUCCESS;
}



static RET_CODE on_update_list(control_t *cont, u16 msg, u32 para1, u32 para2)
{
  BOOL is_out;
  u32 border;
  control_t *p_ctrl;
  static u32 tp_data;
  u8 buff[17];
  u8 MSGDSP[6][16]={"   Frequency    ","  Symbol Rate   ","   Modulation   ","      NIT       ","    FTA ONLY    ","  START SEARCH  "};
  u8 MSGDSPA[2][16]={"<     NO       >","<      YES     >"};
  u8 MSGDSPB[5][16]={"<    QAM256    >","<    QAM16     >","<    QAM32     >","<    QAM64     >","<    QAM128    >"};
  control_t *p_root = fw_find_root_by_id(ROOT_ID_MANUAL_SEARCH);
    
  //g_cur_list_focus = list_get_focus_pos(p_ctrl);
  OS_PRINTF("[%s] [%d] msg [%d]g_cur_list_focus[%d] \n",__FUNCTION__,__LINE__,msg,g_cur_list_focus);

  switch(msg)
  {
	case MSG_FOCUS_UP:
	  if((g_cur_list_focus-1)<0)
	  {
		g_cur_list_focus = 5;
	  }
	  else
	  {
		g_cur_list_focus--;
	  }
      OS_PRINTF("line [%d] MSG_FOCUS_UP[%d] \n",__LINE__,g_cur_list_focus);
	  break;
	  case MSG_FOCUS_DOWN:
	  if((g_cur_list_focus+1)>= 6)
	  {
		g_cur_list_focus = 0;
	  }
	  else
	  {
		g_cur_list_focus++;
	  }
      OS_PRINTF("line [%d] MSG_FOCUS_DOWN[%d] \n",__LINE__,g_cur_list_focus);
	  break;
	  default:
		break;
  }  

  OS_PRINTF("line [%d] g_cur_list_focus[%d] \n",__LINE__,g_cur_list_focus);

  switch(g_cur_list_focus)
  {
  	case 0:
		p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_MANUAL_SEARCH_SPECIFY_TP_FREQ);
		snprintf(buff,17,"      %03d       ",(int)nbox_get_num(p_ctrl));
		display_on_oled_string(MSGDSP[g_cur_list_focus] ,0	); 
		display_on_oled_string(buff,1);
		break;
	case 1:
		p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_MANUAL_SEARCH_SPECIFY_TP_SYM);
		snprintf(buff,17,"      %04d       ",(int)nbox_get_num(p_ctrl));
		display_on_oled_string(MSGDSP[g_cur_list_focus] ,0	); 
		display_on_oled_string(buff,1);
		break;
	case 2:
		p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_MANUAL_SEARCH_SPECIFY_TP_DEMOD);
		tp_data = cbox_static_get_focus(p_ctrl);
		display_on_oled_string(MSGDSP[g_cur_list_focus] ,0	);
		display_on_oled_string(MSGDSPB[tp_data - 4],1  ); 
		break;
	case 3:
		p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_MANUAL_SEARCH_SPECIFY_TP_NIT);
		tp_data = cbox_static_get_focus(p_ctrl);
		display_on_oled_string(MSGDSP[g_cur_list_focus] ,0	); 
		display_on_oled_string(MSGDSPA[tp_data], 1	);
		break;
	case 4:
		p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_MANUAL_SEARCH_FTA);
		tp_data = cbox_static_get_focus(p_ctrl);
		display_on_oled_string(MSGDSP[g_cur_list_focus] ,0	); 
		display_on_oled_string(MSGDSPA[tp_data], 1	);
		break;
	case 5:
		display_on_oled_string(MSGDSP[g_cur_list_focus] ,0	); 
		display_on_oled_string("    PRESS OK    ",1);
		break;
	default:
		break;
  }

  MT_ASSERT(cont != NULL);
  if((cont->priv_attr & NBOX_HL_STATUS_MASK))
  {
    is_out = nbox_is_outrange(cont, &border);
    
    nbox_exit_edit(cont);
    
    if(is_out)
    {
      return SUCCESS;
    }
  }
  OS_PRINTF("[%s] END line [%d] \n",__FUNCTION__,__LINE__);

  return ERR_NOFEATURE;
  
}

BEGIN_KEYMAP(manual_search_cont_keymap, NULL)
ON_EVENT(V_KEY_EXIT, MSG_EXIT)
ON_EVENT(V_KEY_MENU, MSG_EXIT)
ON_EVENT(V_KEY_UP, MSG_FOCUS_UP)
ON_EVENT(V_KEY_DOWN, MSG_FOCUS_DOWN)
END_KEYMAP(manual_search_cont_keymap, NULL)

BEGIN_MSGPROC(manual_search_cont_proc, cont_class_proc)
ON_COMMAND(MSG_EXIT, on_exit_search)
ON_COMMAND(MSG_SIGNAL_UPDATE, on_manual_search_signal_update)
END_MSGPROC(manual_search_cont_proc, cont_class_proc)

BEGIN_KEYMAP(specify_tp_frm_cont_keymap, NULL)
ON_EVENT(V_KEY_UP, MSG_FOCUS_UP)
ON_EVENT(V_KEY_DOWN, MSG_FOCUS_DOWN)
END_KEYMAP(specify_tp_frm_cont_keymap, NULL)

BEGIN_MSGPROC(specify_tp_frm_cont_proc, cont_class_proc)
ON_COMMAND(MSG_FOCUS_UP, on_update_list)
ON_COMMAND(MSG_FOCUS_DOWN, on_update_list)
END_MSGPROC(specify_tp_frm_cont_proc, cont_class_proc)

BEGIN_MSGPROC(specify_start_search_text_proc, text_class_proc)
ON_COMMAND(MSG_SELECT, on_start_search)
END_MSGPROC(specify_start_search_text_proc, text_class_proc)

BEGIN_MSGPROC(specify_tp_sym_nbox_proc, ui_comm_num_proc)
ON_COMMAND(MSG_NUMBER, on_manual_search_change_freq_symbol)
END_MSGPROC(specify_tp_sym_nbox_proc, ui_comm_num_proc)

BEGIN_MSGPROC(specify_tp_demod_cbox_proc, cbox_class_proc)
ON_COMMAND(MSG_INCREASE, on_manual_search_change_qam)
ON_COMMAND(MSG_DECREASE, on_manual_search_change_qam)
END_MSGPROC(specify_tp_demod_cbox_proc, cbox_class_proc)

BEGIN_MSGPROC(specify_tp_nit_cbox_proc, cbox_class_proc)
ON_COMMAND(MSG_INCREASE, on_manual_search_change_nit)
ON_COMMAND(MSG_DECREASE, on_manual_search_change_nit)
END_MSGPROC(specify_tp_nit_cbox_proc, cbox_class_proc)

BEGIN_MSGPROC(specify_tp_fta_cbox_proc, cbox_class_proc)
ON_COMMAND(MSG_INCREASE, on_manual_search_change_fta)
ON_COMMAND(MSG_DECREASE, on_manual_search_change_fta)
END_MSGPROC(specify_tp_fta_cbox_proc, cbox_class_proc)

