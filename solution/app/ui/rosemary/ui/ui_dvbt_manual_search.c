/********************************************************************************************/
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/* Montage Proprietary and Confidential                                                     */
/* Copyright (c) 2017 Montage Technology Group Limited and its affiliated companies         */
/********************************************************************************************/
#include "ui_common.h"
#include "ui_desc.h"
#include "ui_dvbt_manual_search.h"
#include "ui_do_search.h"

enum dvbt_manual_search_local_msg
{
  MSG_DVBT_MANUAL_SEARCH_START = MSG_LOCAL_BEGIN + 1740,
  MSG_DVBT_MANUAL_SEARCH_END,
};
BOOL dvbt_manual_search_tp_bar_update(control_t *p_bar, control_t *p_txt, u16 val, BOOL is_force,u8 *pox);

static u16 dvbt_manual_search_cont_keymap(u16 key);
static RET_CODE dvbt_manual_search_cont_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static u16 cont_tp_frm_cont_keymap(u16 key);
static RET_CODE cont_tp_frm_cont_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static u16 channel_nbox_keymap(u16 key);
static RET_CODE channel_nbox_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static RET_CODE freq_nbox_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static RET_CODE cb_bandwidth_cbox_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);
static RET_CODE cb_nit_search_cbox_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

//static u16 start_search_text_keymap(u16 key);
static RET_CODE start_search_text_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);

static void dvbt_manual_search_check_signal(void)
{
  clt_nim_info_t para;
  control_t *p_root = fw_find_root_by_id(ROOT_ID_DVBT_MANUAL_SEARCH);
  control_t *p_freq_ctrl;
  control_t *p_bandwidth_ctrl;
  control_t *p_bar, *p_txt;
  
  //clear signal info firstly
  p_bar = ctrl_get_ctrl_by_unique_id(p_root, IDC_DVBT_MANUAL_SEARCH_SIG_STRENGTH_BAR);
  p_txt = ctrl_get_ctrl_by_unique_id(p_root, IDC_DVBT_MANUAL_SEARCH_SIG_STRENGTH_PERF);
  dvbt_manual_search_tp_bar_update(p_bar, p_txt, 0, TRUE, (u8*)"%");

  p_bar = ctrl_get_ctrl_by_unique_id(p_root, IDC_DVBT_MANUAL_SEARCH_SIG_QUALITY_BAR);
  p_txt = ctrl_get_ctrl_by_unique_id(p_root, IDC_DVBT_MANUAL_SEARCH_SIG_QUALITY_PERF);
  dvbt_manual_search_tp_bar_update(p_bar, p_txt, 0, TRUE, (u8*)"%");
  
  //freq
  p_freq_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_DVBT_MANUAL_SEARCH_FREQ);
  para.lockt.tp_freq = nbox_get_num(p_freq_ctrl);

  //bandwidth
  p_bandwidth_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_DVBT_MANUAL_SEARCH_CB_BANDWIDTH);
  switch(cbox_dync_get_focus(p_bandwidth_ctrl))
  {
  case 0:
    para.lockt.tp_bandwidth = 6;
    break;

  case 1:
    para.lockt.tp_bandwidth = 7;   
    break;

  case 2:
  default:
    para.lockt.tp_bandwidth = 8;   
    break;
  }
  para.lock_mode = CLT_SYS_DVBT2;

  client_set_signal_tp(g_p_client, &para);
}

RET_CODE open_dvbt_manual_search (u32 para1, u32 para2)
{
  control_t *p_cont;
  control_t *p_nbox_chan;
  control_t *p_nbox_freq;
  control_t *p_cb_bandwidth;
  clt_dvbt_chan_info_t chan_info = {0};
  clt_setting_scan_param_t scan_param = {0};
  
  p_cont = init_desc(ROOT_ID_DVBT_MANUAL_SEARCH, para1, para2);
  if(p_cont == NULL)
  {
    return ERR_FAILURE;
  }

  ctrl_set_keymap(p_cont, dvbt_manual_search_cont_keymap);
  ctrl_set_proc(p_cont, dvbt_manual_search_cont_proc);

  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(p_cont, IDC_DVBT_MANUAL_SEARCH_CONT_TP_FRM), 
  	cont_tp_frm_cont_keymap);
  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_DVBT_MANUAL_SEARCH_CONT_TP_FRM),
  	cont_tp_frm_cont_proc);

  p_nbox_chan = ctrl_get_ctrl_by_unique_id(p_cont, IDC_DVBT_MANUAL_SEARCH_CHANNEL);
  p_nbox_freq = ctrl_get_ctrl_by_unique_id(p_cont, IDC_DVBT_MANUAL_SEARCH_FREQ);
  p_cb_bandwidth = ctrl_get_ctrl_by_unique_id(p_cont, IDC_DVBT_MANUAL_SEARCH_CB_BANDWIDTH);
    
  //channel option
  ctrl_set_keymap(p_nbox_chan, channel_nbox_keymap);
  ctrl_set_proc(p_nbox_chan, channel_nbox_proc);
  
  //freq option
  ctrl_set_keymap(p_nbox_freq, ui_comm_num_keymap);
  ctrl_set_proc(p_nbox_freq, freq_nbox_proc);
  
  //bandwidth option
  ctrl_set_keymap(p_cb_bandwidth, ui_comm_select_keymap);
  ctrl_set_proc(p_cb_bandwidth, cb_bandwidth_cbox_proc);

  //nit search option
  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
    p_cont, IDC_DVBT_MANUAL_SEARCH_CB_NIT_SEARCH),
    ui_comm_select_keymap);
  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_DVBT_MANUAL_SEARCH_CB_NIT_SEARCH),
  	cb_nit_search_cbox_proc);

  //start search
  ctrl_set_keymap(ctrl_get_ctrl_by_unique_id(
    p_cont, IDC_DVBT_MANUAL_SEARCH_START_SEARCH),
    ui_comm_static_keymap);
  ctrl_set_proc(ctrl_get_ctrl_by_unique_id(p_cont, IDC_DVBT_MANUAL_SEARCH_START_SEARCH),
  	start_search_text_proc);

  client_setting_get_scan_param(g_p_client, &scan_param);
  chan_info.country = scan_param.country;
  client_dvbt_get_country_channel_info(g_p_client, &chan_info);
  nbox_set_range(p_nbox_chan, chan_info.min_chan, chan_info.max_chan, 2);
  nbox_set_num_by_dec(p_nbox_chan, chan_info.min_chan);

  chan_info.cur_chan = chan_info.min_chan;
  client_dvbt_get_channel_detail_info(g_p_client, &chan_info);
  nbox_set_num_by_dec(p_nbox_freq, chan_info.freq);
  if(chan_info.bandwidth == 6000)
  {
    cbox_static_set_focus(p_cb_bandwidth, 0);
  }
  else if(chan_info.bandwidth == 7000)
  {
    cbox_static_set_focus(p_cb_bandwidth, 1);
  }
  else
  {
    cbox_static_set_focus(p_cb_bandwidth, 2);
  }

  //paint
  ctrl_paint_ctrl(p_cont, FALSE);

  dvbt_manual_search_check_signal();
  return SUCCESS;
}

static RET_CODE on_dvbt_manual_search_exit(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  manage_close_menu(ROOT_ID_DVBT_MANUAL_SEARCH,0,0);
  return SUCCESS;
}

BOOL dvbt_manual_search_tp_bar_update(control_t *p_bar, control_t *p_txt, u16 val, BOOL is_force,u8 *pox)
{
  u8 str_buf[10];
  BOOL is_redraw = FALSE;

  if(pbar_get_current(p_bar) != val || is_force)
  {
    pbar_set_current(p_bar, val);
    sprintf((char*)str_buf, "%d%s", val,pox);
    text_set_content_by_ascstr(p_txt, (u8*)str_buf);

    is_redraw = TRUE;
  }
  ctrl_paint_ctrl(p_bar, is_redraw);
  ctrl_paint_ctrl(p_txt, is_redraw);
  return is_redraw;
}

static RET_CODE on_dvbt_manual_search_signal_update(control_t *p_cont, u16 msg, u32 para1, u32 para2)
{
  control_t *p_bar, *p_txt;
  clt_signal_data_t *data = (clt_signal_data_t *)(para1);

  if(!ui_check_signal_info(data))
  {
    return SUCCESS;
  }
  
  p_bar = ctrl_get_ctrl_by_unique_id(p_cont, IDC_DVBT_MANUAL_SEARCH_SIG_STRENGTH_BAR);
  p_txt = ctrl_get_ctrl_by_unique_id(p_cont, IDC_DVBT_MANUAL_SEARCH_SIG_STRENGTH_PERF);
  dvbt_manual_search_tp_bar_update(p_bar,p_txt, data->intensity, TRUE, (u8*)"%");

  p_bar = ctrl_get_ctrl_by_unique_id(p_cont, IDC_DVBT_MANUAL_SEARCH_SIG_QUALITY_BAR);
  p_txt = ctrl_get_ctrl_by_unique_id(p_cont, IDC_DVBT_MANUAL_SEARCH_SIG_QUALITY_PERF);
  dvbt_manual_search_tp_bar_update(p_bar,p_txt, data->quality, TRUE, (u8*)"%");
  return SUCCESS;
}

static RET_CODE on_dvbt_manual_search_channel_changed(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  clt_dvbt_chan_info_t chan_info = {0};
  clt_setting_scan_param_t scan_param = {0};
  control_t *p_root = fw_find_root_by_id(ROOT_ID_DVBT_MANUAL_SEARCH);
  control_t *p_nbox_freq = ctrl_get_ctrl_by_unique_id(p_root, IDC_DVBT_MANUAL_SEARCH_FREQ);
  control_t *p_cb_bandwidth = ctrl_get_ctrl_by_unique_id(p_root, IDC_DVBT_MANUAL_SEARCH_CB_BANDWIDTH);

  if(msg == MSG_SELECT)
  {
    ui_comm_num_proc(p_ctrl, msg, para1, para2);
    if(nbox_is_on_edit(p_ctrl))
    {
      return SUCCESS;
    }
  }
  else
  {
    if(nbox_is_on_edit(p_ctrl))
    {
      nbox_class_proc(p_ctrl, msg, para1, para2);
      return SUCCESS;
    }
  }
  
  client_setting_get_scan_param(g_p_client, &scan_param);
  chan_info.country = scan_param.country;
  chan_info.cur_chan = (u8)nbox_get_num(p_ctrl);
  client_dvbt_get_channel_detail_info(g_p_client, &chan_info);
  
  if(msg == MSG_FOCUS_RIGHT)
  {
    chan_info.cur_chan = chan_info.next_chan;
  }
  else if(msg == MSG_FOCUS_LEFT)
  {
    chan_info.cur_chan = chan_info.prev_chan;
  }

  nbox_set_num_by_dec(p_ctrl, chan_info.cur_chan);

  client_dvbt_get_channel_detail_info(g_p_client, &chan_info);

  nbox_set_num_by_dec(p_nbox_freq, chan_info.freq);

  if(chan_info.bandwidth == 6000)
  {
    if(cbox_static_get_focus(p_cb_bandwidth) != 0)
    {
      cbox_static_set_focus(p_cb_bandwidth, 0);
    }
  }
  else if(chan_info.bandwidth  == 7000)
  {
    if(cbox_static_get_focus(p_cb_bandwidth) != 1)
    {
      cbox_static_set_focus(p_cb_bandwidth, 1);
    }
  }
  else if(chan_info.bandwidth  == 8000)
  {
    if(cbox_static_get_focus(p_cb_bandwidth) != 2)
    {
      cbox_static_set_focus(p_cb_bandwidth, 2);
    }
  }
  ctrl_paint_ctrl(p_ctrl, TRUE);
  ctrl_paint_ctrl(p_nbox_freq, TRUE);
  ctrl_paint_ctrl(p_cb_bandwidth, TRUE);

  dvbt_manual_search_check_signal();
  return SUCCESS;
}

static RET_CODE on_dvbt_manual_search_freq_changed(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  if(msg == MSG_SELECT)
  {
    ui_comm_num_proc(p_ctrl, msg, para1, para2);
  }

  if(nbox_is_on_edit(p_ctrl))
  {
    return SUCCESS;
  }

  dvbt_manual_search_check_signal();
  return SUCCESS;
}

static RET_CODE on_start_search(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  clt_scan_input_para_t para;
  control_t *p_root = fw_find_root_by_id(ROOT_ID_DVBT_MANUAL_SEARCH);

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_DVBT_MANUAL_SEARCH_FREQ);
  para.tp.freq = nbox_get_num(p_ctrl);

  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_DVBT_MANUAL_SEARCH_CB_BANDWIDTH);
  switch(cbox_static_get_focus(p_ctrl))
  {
    case 0:
      para.tp.sym = 6;
      break;

    case 1:
      para.tp.sym = 7;
      break;

    case 2:
      para.tp.sym = 8;
      break;

    default:
      para.tp.sym = 8;
      break;
  }
  
  
  p_ctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_DVBT_MANUAL_SEARCH_CB_NIT_SEARCH);
  para.nit_type = cbox_static_get_focus(p_ctrl) ? TRUE : FALSE;
  para.free_only = FALSE;
  para.scan_type = CLT_SCAN_TYPE_DVBT_MANUAL;
  para.chan_type = CLT_SCAN_CHAN_RADIO; //Raja changed for radio only
  
  open_do_search((u32)&para, 0);
  return SUCCESS;
}

static RET_CODE on_dvbt_manual_search_change_bandwidth(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  cbox_class_proc(p_ctrl, msg, para1, para2);
 
  dvbt_manual_search_check_signal();
  return SUCCESS;
}


static RET_CODE on_dvbt_manual_search_nit(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  RET_CODE ret = SUCCESS;
  u8 focus = (u8)para2;
  clt_setting_scan_param_t scan_param = {0};  

  ret = cbox_class_proc(p_ctrl, msg, para1, para2);

  client_setting_get_scan_param(g_p_client, &scan_param);
  scan_param.nit_type = focus;
  client_setting_set_scan_param(g_p_client, &scan_param);
  return SUCCESS;
}

BEGIN_KEYMAP(dvbt_manual_search_cont_keymap, NULL)
  ON_EVENT(V_KEY_EXIT, MSG_EXIT)
  ON_EVENT(V_KEY_MENU, MSG_EXIT)
END_KEYMAP(dvbt_manual_search_cont_keymap, NULL)

BEGIN_MSGPROC(dvbt_manual_search_cont_proc, cont_class_proc)
  ON_COMMAND(MSG_EXIT, on_dvbt_manual_search_exit)
  ON_COMMAND(MSG_SIGNAL_UPDATE, on_dvbt_manual_search_signal_update)
END_MSGPROC(dvbt_manual_search_cont_proc, cont_class_proc)

BEGIN_KEYMAP(cont_tp_frm_cont_keymap, NULL)
  ON_EVENT(V_KEY_UP, MSG_FOCUS_UP)
  ON_EVENT(V_KEY_DOWN, MSG_FOCUS_DOWN)
END_KEYMAP(cont_tp_frm_cont_keymap, NULL)

BEGIN_MSGPROC(cont_tp_frm_cont_proc, cont_class_proc)
END_MSGPROC(cont_tp_frm_cont_proc, cont_class_proc)

BEGIN_KEYMAP(channel_nbox_keymap, ui_comm_num_keymap)
  ON_EVENT(V_KEY_LEFT, MSG_FOCUS_LEFT)
  ON_EVENT(V_KEY_RIGHT, MSG_FOCUS_RIGHT)
END_KEYMAP(channel_nbox_keymap, ui_comm_num_keymap)

BEGIN_MSGPROC(channel_nbox_proc, nbox_class_proc)
  ON_COMMAND(MSG_FOCUS_LEFT, on_dvbt_manual_search_channel_changed)
  ON_COMMAND(MSG_FOCUS_RIGHT, on_dvbt_manual_search_channel_changed)
  ON_COMMAND(MSG_SELECT, on_dvbt_manual_search_channel_changed)
END_MSGPROC(channel_nbox_proc, nbox_class_proc)

BEGIN_MSGPROC(freq_nbox_proc, nbox_class_proc)
  ON_COMMAND(MSG_SELECT, on_dvbt_manual_search_freq_changed)
  //ON_COMMAND(MSG_NUMBER, on_dvbt_manual_search_change_freq)
END_MSGPROC(freq_nbox_proc, nbox_class_proc)

BEGIN_MSGPROC(cb_bandwidth_cbox_proc, cbox_class_proc)
  ON_COMMAND(MSG_INCREASE, on_dvbt_manual_search_change_bandwidth)
  ON_COMMAND(MSG_DECREASE, on_dvbt_manual_search_change_bandwidth)
END_MSGPROC(cb_bandwidth_cbox_proc, cbox_class_proc)

BEGIN_MSGPROC(cb_nit_search_cbox_proc, cbox_class_proc)
	ON_COMMAND(MSG_CHANGED, on_dvbt_manual_search_nit)
END_MSGPROC(cb_nit_search_cbox_proc, cbox_class_proc)

BEGIN_MSGPROC(start_search_text_proc, text_class_proc)
  ON_COMMAND(MSG_SELECT, on_start_search)
END_MSGPROC(start_search_text_proc, text_class_proc)

