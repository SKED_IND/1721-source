/********************************************************************************************/
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/* Montage Proprietary and Confidential                                                     */
/* Copyright (c) 2017 Montage Technology Group Limited and its affiliated companies         */
/********************************************************************************************/
#include "ui_common.h"
#include "ui_desc.h"
#include "ui_infor_bar.h"
#include "ui_pvr_rec_bar.h"

static int g_pg_count = 0;
static int g_cur_pg_index = 0;
clt_setting_osd_t osd_set = {0};

enum infor_bar_local_msg
{
  MSG_INFOR_BAR_START = MSG_LOCAL_BEGIN + 220,
  MSG_INFOR_BAR_HIDE,
  MSG_INFOR_BAR_END,
};
u16 prog_bar_cont_keymap(u16 key);
RET_CODE prog_bar_cont_proc(control_t *cont, u16 msg,
                       u32 para1, u32 para2);
static void ui_shift_prog(s16 offset, BOOL is_play);

void destroy_infor_bar_timer(void)
{
  fw_tmr_destroy(ROOT_ID_INFOR_BAR, MSG_INFOR_BAR_HIDE);
}

static void fill_time_info(BOOL is_redraw)
{
  clt_setting_time_t time_para;
  u8 time_str[32];
  control_t *p_root,*p_date,*p_time;
  p_root = fw_find_root_by_id(ROOT_ID_INFOR_BAR);
  p_date = ctrl_get_ctrl_by_unique_id(p_root, IDC_INFOR_BAR_DATE);
  p_time = ctrl_get_ctrl_by_unique_id(p_root, IDC_INFOR_BAR_TIME); 
  client_setting_get_time(g_p_client, &time_para);
  sprintf((char *)time_str, "%.4d/%.2d/%.2d", time_para.sys_time.year, time_para.sys_time.mon, time_para.sys_time.day);
  text_set_content_by_ascstr(p_date, time_str);
  if (is_redraw)
  {
    ctrl_paint_ctrl(p_date, TRUE);
  }
  sprintf((char *)time_str, "%.2d: %.2d", time_para.sys_time.hour, time_para.sys_time.min);
  text_set_content_by_ascstr(p_time, time_str);
  if (is_redraw)
  {
    ctrl_paint_ctrl(p_time, TRUE);
  }
}


static void fill_epg_info(void)
{	
  control_t *p_event_time_p,*p_event_p,*p_root;
  control_t *p_event_time_f,*p_event_f,*p_subctrl;
  u16 p_str[32]={0};
  u8 ascstr1[32];
  u8 ascstr2[32];
  u8 ascstr[63];
  char sel_str[128];
  char fmt_str[64];
  u16 eventname[CLT_MAX_EVENT_NAME_LENGTH];
  u16 starttime[32];
  u16 endtime[32];
  int actcnt = 0;
  int mark;
  p_root = fw_find_root_by_id(ROOT_ID_INFOR_BAR);
  p_event_time_p = ctrl_get_ctrl_by_unique_id(p_root, IDC_INFOR_BAR_EPG_P_DURATION);
  p_event_p = ctrl_get_ctrl_by_unique_id(p_root, IDC_INFOR_BAR_EPG_P);
  p_event_time_f = ctrl_get_ctrl_by_unique_id(p_root, IDC_INFOR_BAR_EPG_F_DURATION);  
  p_event_f = ctrl_get_ctrl_by_unique_id(p_root, IDC_INFOR_BAR_EPG_F);
  p_subctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_INFOR_BAR_INFO_EPG);

  snprintf(sel_str, sizeof(sel_str), "select evt_name,start_time,end_time from epg "
  "where epg_type=0,pg_pos=%d,day_offset=0,day_count=1 limit 1 pos 0", g_cur_pg_index);
  snprintf(fmt_str, sizeof(fmt_str), "%%s:%d,%%s:%d,%%s:%d",CLT_MAX_EVENT_NAME_LENGTH,32,32);
  actcnt = client_dbase_select(g_p_client, sel_str, fmt_str, eventname, starttime, endtime);

  if(actcnt == 0)
  {
    gui_get_string(IDS_MSG_NO_CONTENT, p_str, 32);
    text_set_content_by_ascstr(p_event_time_p, (u8 *)"  -  -  -  -");
    text_set_content_by_unistr(p_event_p, p_str);
    mark = FALSE;
  }
  else
  {
    ui_str_uni2asc((u8 *)ascstr1,(u16 *)starttime+11);
    ui_str_uni2asc((u8 *)ascstr2,(u16 *)endtime+11);
    sprintf(ascstr, "%s-%s",ascstr1, ascstr2);
    text_set_content_by_ascstr(p_event_time_p, (u8 *)ascstr);
    text_set_content_by_unistr(p_event_p,(u16 *) eventname);
    mark = TRUE;
  }
  memset(sel_str, 0 ,sizeof(sel_str));
  memset(fmt_str, 0 ,sizeof(fmt_str));
  
  snprintf(sel_str, sizeof(sel_str), "select evt_name,start_time,end_time from epg "
  "where epg_type=1,pg_pos=%d,day_offset=0,day_count=1 limit 1 pos 0", g_cur_pg_index);
  snprintf(fmt_str, sizeof(fmt_str), "%%s:%d,%%s:%d,%%s:%d",CLT_MAX_EVENT_NAME_LENGTH,32,32);
  actcnt = client_dbase_select(g_p_client, sel_str, fmt_str, eventname, starttime, endtime);

  if(actcnt == 0)
  {
    gui_get_string(IDS_MSG_NO_CONTENT, p_str, 32);
    text_set_content_by_ascstr(p_event_time_f, (u8 *)"  -  -  -  -");
    text_set_content_by_unistr(p_event_f, p_str);
    mark = FALSE;
  }
  else
  {
    ui_str_uni2asc((u8 *)ascstr1,(u16 *)starttime+11);
    ui_str_uni2asc((u8 *)ascstr2,(u16 *)endtime+11);
    sprintf(ascstr, "%s-%s",ascstr1, ascstr2);
    text_set_content_by_ascstr(p_event_time_f, (u8 *)ascstr);
    text_set_content_by_unistr(p_event_f,(u16 *) eventname);
    mark = TRUE;
  }
 bmap_set_content_by_id(p_subctrl,
  (u16)(mark ? IM_INFORMATION_ICON_EPG: 0));
}

static void fill_mark_info(control_t *p_root)
{
  control_t *p_subctrl;
  char sel_str[128];
  int scrambled = 0;
  int fav_flag = 0;
  int lck_flag = 0;
  clt_system_status_t system_status;
  snprintf(sel_str, sizeof(sel_str), "select is_scrambled,fav_grp_flag,lck_flag from curn_view where limit 1 pos %d", g_cur_pg_index);
  client_dbase_select(g_p_client, sel_str, "%d,%d,%d", &scrambled,&fav_flag,&lck_flag);
  client_get_system_status(g_p_client, &system_status);

  p_subctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_INFOR_BAR_ICON_MONEY);
  bmap_set_content_by_id(p_subctrl,
    (u16)(scrambled ? IM_INFORMATION_ICON_MONEY :0));
  p_subctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_INFOR_BAR_ICON_FAV);
  bmap_set_content_by_id(p_subctrl,
    (u16)(fav_flag ? IM_INFORMATION_ICON_7 : 0));
  p_subctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_INFOR_BAR_ICON_LOCK);
  bmap_set_content_by_id(p_subctrl, (u16)(lck_flag ? IM_INFORMATION_ICON_6:0));

  p_subctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_INFOR_BAR_INFO_SUBTT);
  bmap_set_content_by_id(p_subctrl,
   (u16)(system_status.is_subt_ready? IM_INFORMATION_ICON_SUBTITLE: 0));

  p_subctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_INFOR_BAR_INFO_TELTEXT);
 bmap_set_content_by_id(p_subctrl,
   (u16)(system_status.is_ttx_ready? IM_INFORMATION_ICON_TTX: 0));

}

static void convert_nim_info(u32 nim_mode,u16 *nim_modulate)
{
  switch(nim_mode)
  {
    case CLT_NIM_MODULA_QAM16:
       *nim_modulate = 16;
      break;

    case CLT_NIM_MODULA_QAM32:
       *nim_modulate = 32;
      break;

    case CLT_NIM_MODULA_QAM64:
       *nim_modulate = 64;
      break;

    case CLT_NIM_MODULA_QAM128:
       *nim_modulate = 128;
      break;

    case CLT_NIM_MODULA_QAM256:
      *nim_modulate = 256;
      break;

    default:
       *nim_modulate = 64;
      break;
  }
}

static void clear_ttx_flag(void)
{
  control_t *p_root = fw_find_root_by_id(ROOT_ID_INFOR_BAR);
  control_t *p_subctrl = NULL;
  p_subctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_INFOR_BAR_INFO_SUBTT);
  bmap_set_content_by_id(p_subctrl, 0);

  p_subctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_INFOR_BAR_INFO_TELTEXT);
 bmap_set_content_by_id(p_subctrl, 0);

}

static void fill_prog_info(void)
{
  control_t *p_root,*p_number,*p_name,*p_subctrl;
  u8 asc_str[32];
  char sel_str[128];
  char fmt_str[64];
  u16 pgname[CLT_MAX_PROG_NAME_LENGTH];
  u32 lcn_num = 0;
  u32 tp_id = 0;
  u32 tp_freq = 0;
  u32 tp_sym = 0;
  u32 tp_nim_modulate = 0;
  u16 nim_modulate = 64;
  int tp_polarity = 0;
  int sat_type = 0;
  p_root = fw_find_root_by_id(ROOT_ID_INFOR_BAR);
  p_number = ctrl_get_ctrl_by_unique_id(p_root, IDC_INFOR_BAR_NUMBER);
  p_name = ctrl_get_ctrl_by_unique_id(p_root, IDC_INFOR_BAR_NAME);
  snprintf(sel_str, sizeof(sel_str), "select logical_num,name,sat_type from curn_view where limit 1 pos %d", g_cur_pg_index);
  snprintf(fmt_str, sizeof(fmt_str), "%%d,%%s:%d,%%d", CLT_MAX_PROG_NAME_LENGTH);
  client_dbase_select(g_p_client, sel_str, fmt_str, &lcn_num, pgname,&sat_type);

  sprintf((char *)asc_str, "%.4d", (u16)lcn_num);
  text_set_content_by_ascstr(p_number, (u8 *)asc_str);
  text_set_content_by_unistr(p_name, pgname);


  void display_on_oled_Channel_info( char *string,u8 line  );
  
  display_on_oled_Channel_info((char *) pgname,1 );
  display_on_oled_Channel_info((char *) asc_str,0 );

  
  p_subctrl = ctrl_get_ctrl_by_unique_id(p_root, IDC_INFOR_BAR_CA_INFO);
  snprintf(sel_str, sizeof(sel_str), "select tp_id from curn_view where limit 1 pos %d", g_cur_pg_index);
  snprintf(fmt_str, sizeof(fmt_str), "%%d");
  client_dbase_select(g_p_client, sel_str, fmt_str, &tp_id);
  if(sat_type == CLT_SAT_TYPE_DVBS)
  {
    snprintf(sel_str, sizeof(sel_str), "select freq,sym,polarity from "VIEW_TP_BY_PG" where limit 1 pos %d", (int)tp_id);
    snprintf(fmt_str, sizeof(fmt_str), "%%d,%%d,%%d");
    client_dbase_select(g_p_client, sel_str, fmt_str, &tp_freq,&tp_sym,&tp_polarity);//////////   ����
    //tp info
    sprintf(asc_str, "%d/%c/%d", (int)tp_freq, tp_polarity ? 'V' : 'H', (int)tp_sym);
  }
  else if(sat_type == CLT_SAT_TYPE_DVBT)
  {
    snprintf(sel_str, sizeof(sel_str), "select freq from "VIEW_TP_BY_PG" where limit 1 pos %d", (int)tp_id);
    snprintf(fmt_str, sizeof(fmt_str), "%%d");
    client_dbase_select(g_p_client, sel_str, fmt_str, &tp_freq);

    convert_nim_info(tp_nim_modulate,&nim_modulate);
    sprintf((char *)asc_str, "%d", (int)tp_freq);
  }
  else
  {
    snprintf(sel_str, sizeof(sel_str), "select freq,sym,nim_modulate from "VIEW_TP_BY_PG" where limit 1 pos %d", (int)tp_id);
    snprintf(fmt_str, sizeof(fmt_str), "%%d,%%d,%%d");
    client_dbase_select(g_p_client, sel_str, fmt_str, &tp_freq,&tp_sym,&tp_nim_modulate);

    convert_nim_info(tp_nim_modulate,&nim_modulate);
    sprintf((char *)asc_str, "%d/QAM%d/%d", (int)tp_freq, nim_modulate, (int)tp_sym);
  }
  text_set_content_by_ascstr(p_subctrl, asc_str);   

  fill_epg_info();
  fill_mark_info(p_root);
}

RET_CODE open_infor_bar (u32 para1, u32 para2)
{
  control_t *p_cont;
  clt_dbase_get_count_para_t para = {{0}};
  
  client_epg_start_policy(g_p_client, CLT_EPG_START_POLICY_ALLPF);

  strncpy(para.view_name, "curn_view", sizeof(para.view_name));
  g_pg_count =  client_dbase_get_count(g_p_client,&para);
  if(g_pg_count<=0)
  {
  	if(fw_get_focus_id() != ROOT_ID_BACKGROUND)
        ui_comm_cfmdlg_open(NULL, IDS_MSG_NO_PROG, NULL, 0);
    return ERR_FAILURE;
  }
  g_cur_pg_index = client_dbase_get_pos(g_p_client,"curn_view");
  // shift prog
  switch (para1)
  {
    case V_KEY_UP:
    case V_KEY_CHUP:
      ui_shift_prog(1, FALSE);
      break;	  
    case V_KEY_DOWN:
    case V_KEY_CHDOWN:
      ui_shift_prog(-1, FALSE);
      break;	  
    case V_KEY_PAGE_UP:
      ui_shift_prog(10, FALSE);
      break;	  
    case V_KEY_PAGE_DOWN:
      ui_shift_prog(-10, FALSE);
      break;	  
    case V_KEY_RECALL:
      //ui_recall(FALSE);
      ui_play_recall();
      g_cur_pg_index = client_dbase_get_pos(g_p_client,"curn_view");
      break;      
    default:
      /* do nothing */;
  }

  if(fw_find_root_by_id(ROOT_ID_INFOR_BAR) != NULL)
  {
    manage_close_menu(ROOT_ID_INFOR_BAR, 0, 0);
  }
  p_cont = init_desc(ROOT_ID_INFOR_BAR, para1, para2);
  if(p_cont == NULL)
  {
    return ERR_FAILURE;
  }
  ctrl_set_keymap(p_cont, prog_bar_cont_keymap);
  ctrl_set_proc(p_cont, prog_bar_cont_proc);
  if(fw_find_root_by_id(ROOT_ID_COMM_DLG)!=NULL){
    ui_comm_dlg_close();
  }
  if(V_KEY_RECALL != para1)
  {
  ui_play_prog_by_pos(g_cur_pg_index);
  }
  fill_time_info(FALSE);
  fill_prog_info();
  fill_epg_info();
  client_setting_get_osd(g_p_client, &osd_set);
  mtos_printk("osd_set.timeout = %d\n",osd_set.timeout);
  fw_tmr_create(ROOT_ID_INFOR_BAR, MSG_INFOR_BAR_HIDE, osd_set.timeout * 1000, FALSE);
  ctrl_paint_ctrl(ctrl_get_root(p_cont), TRUE);

  client_ads_show_ads(g_p_client, CLI_ADS_AD_TYPE_CHBAR);

  return SUCCESS;
}

static void ui_shift_prog(s16 offset, BOOL is_play)
{
  s32 dividend = 0;
  clt_dbase_edit_mark_t clt_dbase_edit_mark={0};
  
  if(g_pg_count == 0)
  {
    return;
  }
   do
  {
  dividend = g_cur_pg_index + offset; 
  if(dividend >= g_pg_count)
  {
    if(g_cur_pg_index == (g_pg_count - 1))
    {
      g_cur_pg_index = 0;
    }
    else
    {
      g_cur_pg_index = g_pg_count - 1;
    }
  }
  else
  {
    if(dividend <= 0)
    {
      if(g_cur_pg_index == 0)
      {
        g_cur_pg_index = g_pg_count - 1;
      }
      else
      {
        g_cur_pg_index = 0;
      }
    }
    else
    {
      g_cur_pg_index = (u16)(dividend) % g_pg_count;
    }
  }

  clt_dbase_edit_mark.view_pos = g_cur_pg_index;
  clt_dbase_edit_mark.flag = CLT_DB_MARK_SKP;
  clt_dbase_edit_mark.param = 0;
  }
  while(client_dbase_get_mark_status(g_p_client,&clt_dbase_edit_mark) == TRUE);
  
  if(is_play)
  {
    ui_play_prog_by_pos(g_cur_pg_index);
  }
}

static RET_CODE shift_prog_in_bar(control_t *p_ctrl, s16 offset)
{
  if(FALSE)//ui_recorder_isrecording())
  {
    /*cur_pg_id = sys_status_get_curn_group_curn_prog_id();
  	db_dvbs_get_pg_by_id(cur_pg_id, &cur_pg);
    next_pg_id = shift_prog_id(offset);
    db_dvbs_get_pg_by_id(next_pg_id, &next_pg);
    if(next_pg.tp_id == cur_pg.tp_id)
    {
      ui_shift_prog(offset, FALSE, &curn_prog_id);
      fill_prog_info(p_ctrl, next_pg_id);
      return SUCCESS;
    }*/
    ui_comm_ask_for_dodlg_open_ex(NULL, IDS_EXIT_RECORD,\
                                  on_pvr_recbar_save_exittoplay, 0, 0, 0);
    return SUCCESS;
  }  
  // change prog
  ui_shift_prog(offset, TRUE);
  fill_prog_info();
  clear_ttx_flag();//modify bug 95793

  return SUCCESS;
}

static RET_CODE on_focus_up(control_t *p_ctrl, u16 msg,
                            u32 para1, u32 para2)
{
  shift_prog_in_bar(p_ctrl, 1);
  ctrl_paint_ctrl(p_ctrl,TRUE);
  fw_tmr_reset(ROOT_ID_INFOR_BAR, MSG_INFOR_BAR_HIDE, osd_set.timeout * 1000);
 
  return SUCCESS;
}

static RET_CODE on_focus_down(control_t *p_ctrl, u16 msg,
                            u32 para1, u32 para2)
{
  shift_prog_in_bar(p_ctrl, -1);

  ctrl_paint_ctrl(p_ctrl,TRUE);
 
  fw_tmr_reset(ROOT_ID_INFOR_BAR, MSG_INFOR_BAR_HIDE, osd_set.timeout * 1000);
 
  return SUCCESS;
}

static RET_CODE on_page_up(control_t *p_ctrl, u16 msg,
                            u32 para1, u32 para2)
{
  shift_prog_in_bar(p_ctrl, 10);

  ctrl_paint_ctrl(p_ctrl,TRUE);
 
  fw_tmr_reset(ROOT_ID_INFOR_BAR, MSG_INFOR_BAR_HIDE, osd_set.timeout * 1000);
 
  return SUCCESS;
}

static RET_CODE on_page_down(control_t *p_ctrl, u16 msg,
                            u32 para1, u32 para2)
{
  shift_prog_in_bar(p_ctrl, -10);

  ctrl_paint_ctrl(p_ctrl,TRUE);
 
  fw_tmr_reset(ROOT_ID_INFOR_BAR, MSG_INFOR_BAR_HIDE, osd_set.timeout * 1000);
 
  return SUCCESS;
}

static RET_CODE on_infor_bar_exit(control_t *p_ctrl, u16 msg,
                            u32 para1, u32 para2)
{

  mtos_printk("on_infor_bar_exit\n");
  fw_tmr_destroy(ROOT_ID_INFOR_BAR, MSG_INFOR_BAR_HIDE);
  manage_close_menu(ROOT_ID_INFOR_BAR,0,0);
  return SUCCESS;
}

static RET_CODE on_time_update(control_t *p_ctrl, u16 msg,
                            u32 para1, u32 para2)
{
  if(fw_get_focus_id() != ROOT_ID_INFOR_BAR)
  {
    return ERR_FAILURE;
  }
  fill_prog_info();
  fill_time_info(TRUE);
  return SUCCESS;
}

static RET_CODE on_info_bar_detail(control_t *p_ctrl, u16 msg,
                                    u32 para1, u32 para2)
{
  RET_CODE ret = SUCCESS;
  return ret;
}

static RET_CODE on_info_bar_tvradio(control_t *p_ctrl, u16 msg,
                            u32 para1, u32 para2)
{
  u16 content;
  clt_switch_tvradio_para_t trparam;

  if(client_channel_switch_tv_radio(g_p_client, &trparam) == SUCCESS)
  {
     g_cur_pg_index = trparam.pg_pos;
     g_pg_count     = trparam.pg_count;
     fill_prog_info();
     clear_ttx_flag();//modify bug 95793

     ctrl_paint_ctrl(p_ctrl, TRUE);
     fw_tmr_reset(ROOT_ID_INFOR_BAR, MSG_INFOR_BAR_HIDE, osd_set.timeout * 1000);
  }
  else
  {
     content = (u16)((trparam.mode_switched == 1) ? IDS_MSG_NO_TV_PROG : IDS_MSG_NO_RADIO_PROG);
     ui_comm_cfmdlg_open(NULL, content, NULL, 0);
  }
  return SUCCESS;
}

static RET_CODE on_pbar_destory(control_t *p_ctrl, u16 msg,
                            u32 para1, u32 para2)
{
  client_ads_stop_ads(g_p_client);
  return ERR_NOFEATURE;
}

static RET_CODE on_recall(control_t *p_ctrl, u16 msg,
                            u32 para1, u32 para2)
{
#if 0
  if(ui_recorder_isrecording())
    return SUCCESS;
  if (ui_recall(TRUE, &prog_id))
  {
    fill_prog_info();
  }
  else
  {
    UI_PRINTF("PROGBAR: recall is failed\n");
  }
#else  
  ui_play_recall();
  g_cur_pg_index = client_dbase_get_pos(g_p_client,"curn_view");
  fill_prog_info();
  clear_ttx_flag();//modify bug 95793

  ctrl_paint_ctrl(p_ctrl, TRUE);
#endif
  return SUCCESS;
}

static RET_CODE on_ttx_ready(control_t *p_ctrl, u16 msg, 
                            u32 para1, u32 para2)
{
  control_t *p_frm, *p_subctrl;
  clt_system_status_t system_status;
  client_get_system_status(g_p_client, &system_status);
  p_frm = fw_find_root_by_id(ROOT_ID_INFOR_BAR);
  p_subctrl = ctrl_get_ctrl_by_unique_id(p_frm, IDC_INFOR_BAR_INFO_TELTEXT);
  bmap_set_content_by_id(p_subctrl,
 (u16)(system_status.is_ttx_ready? IM_INFORMATION_ICON_TTX: 0));

  ctrl_paint_ctrl(p_subctrl, TRUE);

  return SUCCESS;
}

static RET_CODE on_subt_ready(control_t *p_ctrl, u16 msg, 
                            u32 para1, u32 para2)
{
  control_t *p_frm, *p_subctrl;
  clt_system_status_t system_status;
  client_get_system_status(g_p_client, &system_status);
  p_frm = fw_find_root_by_id(ROOT_ID_INFOR_BAR);
  p_subctrl = ctrl_get_ctrl_by_unique_id(p_frm, IDC_INFOR_BAR_INFO_SUBTT);
  bmap_set_content_by_id(p_subctrl,
 (u16)(system_status.is_subt_ready? IM_INFORMATION_ICON_SUBTITLE: 0));

  ctrl_paint_ctrl(p_subctrl, TRUE);

  return SUCCESS;
}

BEGIN_KEYMAP(prog_bar_cont_keymap, NULL)
  ON_EVENT(V_KEY_UP, MSG_FOCUS_UP)
  ON_EVENT(V_KEY_DOWN, MSG_FOCUS_DOWN)
  ON_EVENT(V_KEY_CHUP, MSG_FOCUS_UP)
  ON_EVENT(V_KEY_CHDOWN, MSG_FOCUS_DOWN)
  ON_EVENT(V_KEY_PAGE_UP, MSG_PAGE_UP)
  ON_EVENT(V_KEY_PAGE_DOWN, MSG_PAGE_DOWN)
  ON_EVENT(V_KEY_CANCEL, MSG_EXIT)
  ON_EVENT(V_KEY_EXIT, MSG_EXIT)
  ON_EVENT(V_KEY_BACK, MSG_EXIT)
  ON_EVENT(V_KEY_INFO, MSG_INFO)
  ON_EVENT(V_KEY_RECALL, MSG_RECALL)
  ON_EVENT(V_KEY_TVRADIO, MSG_TVRADIO)
END_KEYMAP(prog_bar_cont_keymap, NULL)

BEGIN_MSGPROC(prog_bar_cont_proc, cont_class_proc)
  ON_COMMAND(MSG_INFOR_BAR_HIDE, on_infor_bar_exit)
  ON_COMMAND(MSG_EXIT, on_infor_bar_exit)
  ON_COMMAND(MSG_TIME_UPDATE, on_time_update)
  ON_COMMAND(MSG_TVRADIO, on_info_bar_tvradio)
  ON_COMMAND(MSG_INFO, on_info_bar_detail)
  ON_COMMAND(MSG_FOCUS_UP, on_focus_up)
  ON_COMMAND(MSG_FOCUS_DOWN, on_focus_down)
  ON_COMMAND(MSG_PAGE_UP, on_page_up)
  ON_COMMAND(MSG_PAGE_DOWN, on_page_down)
  ON_COMMAND(MSG_RECALL, on_recall)
  ON_COMMAND(MSG_DESTROY, on_pbar_destory)
  ON_COMMAND(MSG_TTX_READY, on_ttx_ready)
  ON_COMMAND(MSG_SUBT_READY, on_subt_ready)
END_MSGPROC(prog_bar_cont_proc, cont_class_proc)


