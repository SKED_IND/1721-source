/********************************************************************************************/
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/* Montage Proprietary and Confidential                                                     */
/* Copyright (c) 2017 Montage Technology Group Limited and its affiliated companies         */
/********************************************************************************************/
#include "ui_common.h"
#include "ui_desc.h"
#include "ui_language_setting.h"
#include "ui_mainmenu.h"


enum language_setting_local_msg
{
  MSG_LANGUAGE_SETTING_START = MSG_LOCAL_BEGIN + 280,
  MSG_LANGUAGE_SETTING_END,
};

RET_CODE ui_auto_search_boot_up(u32 para1, u32 para2);
RET_CODE open_mainmenu_1 (u32 para1, u32 para2);
RET_CODE open_mainmenu(u32 para1, u32 para2);


u16 language_cont_keymap(u16 key);
RET_CODE manage_open_menu(u16 root_id, u32 para1, u32 para2);
RET_CODE language_cont_proc(control_t *p_ctrl, u16 msg, u32 para1, u32 para2);
RET_CODE language_select_proc(control_t *p_ctrl, u16 msg, u32 para1, u32 para2);
RET_CODE first_language_select_proc(control_t *p_ctrl, u16 msg, u32 para1, u32 para2);
RET_CODE second_language_select_proc(control_t *p_ctrl, u16 msg, u32 para1, u32 para2);
void display_on_oled_Channel_info( char *string,u8 line  );
void display_on_oled_string( char *string,u8 line  );


RET_CODE open_language_setting (u32 para1, u32 para2)
{
  control_t *p_cont;
  control_t *p_lang;
  clt_setting_lang_t lang_set;
  p_cont = init_desc(ROOT_ID_LANGUAGE_SETTING, para1, para2);
  if(p_cont == NULL)
  {
    return ERR_FAILURE;
  }
  ctrl_set_keymap(p_cont, language_cont_keymap);
  ctrl_set_proc(p_cont, language_cont_proc);

  p_lang = ctrl_get_ctrl_by_unique_id(p_cont, 
    IDC_LANGUAGE_SETTING_LANGUAGE);

  ctrl_set_keymap(p_lang,language_cont_keymap);
  ctrl_set_proc(p_lang, language_select_proc);
  client_setting_get_language(g_p_client, &lang_set);
  cbox_static_set_focus(p_lang, lang_set.osd_text);
  OS_PRINTF("lang_set.osd_text [%d]\n",lang_set.osd_text);
  
  ctrl_paint_ctrl(p_cont, TRUE);
  return SUCCESS;
}

static RET_CODE on_select_change_focus(control_t *p_ctrl,
                                       u16 msg,
                                       u32 para1,
                                       u32 para2)
{  
  control_t *p_root;

  clt_setting_lang_t lang_set;
  int cur_id = ctrl_get_ctrl_id(p_ctrl);
  int lang_index;
  u8 fav_name[32] = {0};
  u16 fav_name_unistr[32] = {0};
  u16 content[32];
  u16 i;
  u8 MSGfontA[2][16]={"    English    >","<   Chineese    "};
  
  p_root = fw_find_root_by_id(ROOT_ID_LANGUAGE_SETTING);
  client_setting_get_language(g_p_client, &lang_set);

  OS_PRINTF("cur_id[%d] msg[%d] para1[%d] para2[%d]\n",cur_id,msg,para1,para2);
  switch(cur_id)
  {
    case IDC_LANGUAGE_SETTING_LANGUAGE:
      lang_set.osd_text = cbox_static_get_focus(p_ctrl);
      lang_index = lang_set.osd_text;
      if(ctrl_is_onfocus(p_ctrl))
      {
        // reset all language setting
        lang_set.first_audio = lang_index;
        lang_set.second_audio = lang_index;
        // set language & redraw
        rsc_set_curn_language(gui_get_rsc_handle(), lang_index + 1);

        for(i=0; i<5; i++)
        {
          memset(content, 0, sizeof(content));
          gui_get_string(IDS_FAV_LIST, content, 64);
          sprintf((char *)fav_name, "%d", i+1);
          ui_str_asc2uni(fav_name, fav_name_unistr);
          ui_uni_strcat(content, fav_name_unistr, 256);
          //sys_status_set_fav_name((u8)(sys_status_get_categories_count() - 5 + i), content);
        }

        if(p_root != NULL)
        {
          ctrl_paint_ctrl(p_root, TRUE);
        }
      }
      break;
    default:
      MT_ASSERT(0);
      return ERR_FAILURE;
  }
  OS_PRINTF("client_setting_set_language [%d]\n",__LINE__);
  client_setting_set_language(g_p_client, &lang_set);

  lang_set.osd_text %= (2);
  display_on_oled_string(MSGfontA[0] ,1  ); 
  display_on_oled_string( "    Language    ",0  );

  return SUCCESS;
}
static RET_CODE on_language_exit(control_t *p_ctrl,
                                       u16 msg,
                                       u32 para1,
                                       u32 para2)
{
  manage_close_menu(ROOT_ID_LANGUAGE_SETTING, 0, 0);
  //ui_auto_search_boot_up(0,0);
  open_mainmenu_1(0,0);
  return SUCCESS;
}

BEGIN_MSGPROC(language_select_proc, cbox_class_proc)
ON_COMMAND(MSG_CHANGED, on_select_change_focus)
END_MSGPROC(language_select_proc, cbox_class_proc)

BEGIN_KEYMAP(language_cont_keymap, NULL)
ON_EVENT(V_KEY_RIGHT, MSG_INCREASE)
ON_EVENT(V_KEY_LEFT, MSG_DECREASE)
ON_EVENT(V_KEY_OK, MSG_EXIT)
ON_EVENT(V_KEY_EXIT, MSG_EXIT)
ON_EVENT(V_KEY_MENU,MSG_EXIT)

END_KEYMAP(language_cont_keymap, NULL)

BEGIN_MSGPROC(language_cont_proc, cont_class_proc)
ON_COMMAND(MSG_EXIT, on_language_exit)
END_MSGPROC(language_cont_proc, cont_class_proc)

