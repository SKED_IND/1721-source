/********************************************************************************************/
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/* Montage Proprietary and Confidential                                                     */
/* Copyright (c) 2017 Montage Technology Group Limited and its affiliated companies         */
/********************************************************************************************/
#include "ui_common.h"
#include "ui_factory_test.h"

#include "ui_pop_msg.h"
typedef void (*SET_ALIGN_TYPE)(control_t *, u32);
typedef void (*SET_FONT_STYLE)(control_t *, u32, u32, u32);
static usb_status usb_act_status = USB_INVALID_STATUS;

list_xstyle_t list_item_style = {

  RSI_IGNORE,
  RSI_IGNORE,
  RSI_YELLOW,
  RSI_YELLOW,
  RSI_YELLOW,
};
static list_xstyle_t factory_list_field_rstyle =
{
  RSI_IGNORE,
  RSI_IGNORE,
  RSI_IGNORE,
  RSI_IGNORE,
  RSI_IGNORE,
};

static list_xstyle_t factory_list_field_fstyle =
{
  FSI_WHITE_22,
  FSI_WHITE_22,
  FSI_BLACK_22,
  FSI_BLACK_22,
  FSI_BLACK_22,
};

static list_field_attr_t factory_list_field_attr[FACTORY_LIST_FILED] =
{
  { LISTFIELD_TYPE_STRID | STL_VCENTER | STL_VCENTER,
    150, 0, 0, &factory_list_field_rstyle, &factory_list_field_fstyle },
};

static item_attr_t list_child_item[]=
{
    {
      IDS_BASIC_INFO,8,
     {
      {IDS_HARDWARE_VER, COMM_STATIC,IDC_BASIC_HW_VER, 1,0,{0,}},
      {IDS_SOFTWARE_VER, COMM_STATIC,IDC_BASIC_SW_VER, 0,0,{0,}},
      {IDS_CA_SMART_CARD_NUMBER, COMM_STATIC,IDC_BASIC_CARD_NO, 0, 0, {0,}},
      {IDS_CA_CARD_PROVIDERS, COMM_STATIC,IDC_BASIC_CA_PROVIDER, 0, 0, {0,}},
      {IDS_CAS_CA_VERSION, COMM_STATIC,IDC_BASIC_CA_VER, 0,0, {0,}},
      {IDS_CUR_STBID, COMM_STATIC,IDC_BASIC_STB_ID, 1,0,{0,}},
      {IDS_USB_STORAGE_CONNECT, COMM_STATIC,IDC_BASIC_USB_INFO, 0, 0, {0,}},
      {IDS_FLASH_LOCK, COMM_STATIC,IDC_BASIC_FLSH_INFO, 0, 0,{0,}},
    }
  },
  {IDS_PLAY,7,
    {
    {0, COMM_TEXT,IDC_DISPLAY_TV_WIN, 0,0,{0,}},
    {IDS_PROGRAM,COMM_STATIC, IDC_DISPLAY_TV_SELECT, 1, 2,{IDS_TV, IDS_RADIO}},
    {IDS_VOLUME, COMM_TEXT, IDC_DISPLAY_VOLUME_TEXT, 0, 0,{0,}},
    {0, COMM_BAR, IDC_DISPLAY_VOLUME, 1, 0, {0, }},
    {0, COMM_TEXT,IDC_DISPLAY_VOLUME_TEXT1, 0, 0,{0,}},
    {0, COMM_SELECT_BAR, IDC_DISPLAY_SING_BAR,0, 0, {0,}},
    {0, COMM_SELECT_BAR, IDC_DISPLAY_SING_BAR1,0, 0, {0,}},
    }
  },
  {IDS_SCAN,7,
  {
    {IDS_FREQUENCY, COMM_NUMEDIT,IDC_SCAN_FREQ_INFO, 1, 0, {0,}},
    {IDS_SYMBOL, COMM_NUMEDIT,IDC_SCAN_SYMB_INFO, 1, 0, {0,}},
    {IDS_QAM, COMM_SELECT, IDC_SCAN_QAM_INFO, 1, 5,{IDS_QAM16,IDS_QAM32,IDS_QAM64,IDS_QAM128,IDS_QAM256}},
    {IDS_START_SEARCH, COMM_TEXT,IDC_SCAN_START_SEARCH, 1,0,{0,}},
    {0,COMM_SELECT_BAR,IDC_SCAN_SEARCH_PROGRESS,0,0,{0,}},
    {0,COMM_SELECT_BAR, IDC_SCAN_SEARCH_SING_BAR, 0, 0, {0,}},
    {0,COMM_SELECT_BAR, IDC_SCAN_SEARCH_SING_BAR1, 0, 0, {0,} }
  },

  },
  {IDS_PORT,5,
  {
    {0, COMM_TEXT, IDC_PORT_KEY_POWER, 1, 0, {0,}},
    {0, COMM_TEXT, IDC_PORT_KEY_LEFT, 0,0, {0,}},
	{0, COMM_TEXT, IDC_PORT_KEY_RIGHT, 0, 0, {0,}},
	{0, COMM_TEXT, IDC_PORT_KEY_UP, 0, 0, {0,}},
	{0, COMM_TEXT, IDC_PORT_KEY_DOWN, 0, 0, {0,}},
  }
  },
};

static ca_card_info_t card_info;
static u16 prog_curn[2];
static BOOL factory_play_pg = FALSE;

static u8 global_volue = 31;
static RET_CODE factory_setting_cont_proc(control_t *p_block, u16 msg, u32 para1, u32 para2);
static u16 factory_setting_list_keymap(u16 key);
static RET_CODE factory_setting_list_proc(control_t *p_block, u16 msg, u32 para1, u32 para2);
RET_CODE factory_comm_basic_info_select_proc(control_t *p_block, u16 msg, u32 para1, u32 para2);
static u16 factory_setting_cont_keymap(u16 key);
static u16  factory_down_bg_keymap(u16 key);
static RET_CODE factory_down_bg_proc(control_t *p_block, u16 msg, u32 para1, u32 para2);
static RET_CODE factory_search_num_edit_proc(control_t *p_block, u16 msg, u32 para1, u32 para2);

static RET_CODE factory_search_qam_proc(control_t *p_block, u16 msg, u32 para1, u32 para2);
static u16 factory_manual_search_select_keymap(u16 key);
static RET_CODE factory_manual_search_select_proc(control_t *p_block, u16 msg, u32 para1, u32 para2);

u16 ui_desktop_keymap_on_factory(u16 key);
RET_CODE ui_desktop_proc_on_factory(control_t *p_ctrl, u16 msg, u32 para1, u32 para2);

static RET_CODE on_factory_up_or_down_switch_focus(control_t *p_ctrl, u16 msg, u32 para1, u32 para2);

static control_t *on_factory_test_child_ctrl_create(u8 type,
                                    control_t *parent,
                                    u8 ctrl_id,
                                    u16 x,
                                    u16 y,
                                    u16 lw,
                                    u16 rw)
{
  control_t *cont, *txt, *p_ctrl = NULL;
  u8 rsi_hl;
  ctrl_type_t class_name = CTRL_TCNT;
  SET_ALIGN_TYPE set_align_type = NULL;
  SET_FONT_STYLE set_font_style = NULL;

  keymap_t keymap;

  cont = ctrl_create_ctrl(CTRL_CONT, ctrl_id,
                     x, y, (u16)(lw + rw), COMM_CTRL_H,
                     parent, 0);
  ctrl_set_style (cont, STL_EX_WHOLE_HL);
  ctrl_set_rstyle(cont,
                  RSI_IGNORE,
                  RSI_DROPLIST_ITEM_FOCUS,
                  RSI_IGNORE);
  ctrl_set_style(cont, STL_EX_WHOLE_HL);

  if(lw > 0)
  {
    txt =
      ctrl_create_ctrl(CTRL_TEXT, IDC_COMM_TXT,
                       0, 0, lw, COMM_CTRL_H,
                       cont, 0);
    ctrl_set_rstyle(txt,
                    RSI_IGNORE,
                    RSI_IGNORE,
                    RSI_IGNORE);
    text_set_content_type(txt, TEXT_STRTYPE_STRID);
    text_set_font_style(txt,
                        FSI_WHITE_22,
                        FSI_WHITE_22,
                        FSI_WHITE_22);
    text_set_align_type(txt, STL_LEFT | STL_VCENTER);
    text_set_offset(txt, COMM_CTRL_OX, 0);
  }

  switch(type)
  {
    case COMM_STATIC:
      class_name = CTRL_TEXT;
      rsi_hl = RSI_IGNORE;
      keymap = ui_comm_static_keymap;
      set_align_type = (SET_ALIGN_TYPE)text_set_align_type;
      set_font_style = (SET_FONT_STYLE)text_set_font_style;
      break;
    case COMM_NUMEDIT:
      class_name = CTRL_NBOX;
      rsi_hl = RSI_IGNORE;
      keymap = ui_comm_num_keymap;
      set_align_type = (SET_ALIGN_TYPE)nbox_set_align_type;
      set_font_style = (SET_FONT_STYLE)nbox_set_font_style;
      break;
    case COMM_SELECT:
      class_name = CTRL_CBOX;
      rsi_hl = RSI_LEFT_RIGHT_HL;
      keymap = ui_comm_select_keymap;
      set_align_type = (SET_ALIGN_TYPE)cbox_set_align_style;
      set_font_style = (SET_FONT_STYLE)cbox_set_font_style;
      break;
    default:
      rsi_hl = 0;
      class_name = CTRL_TCNT;
      keymap = NULL;
      set_align_type = NULL;
      set_font_style = NULL;
      MT_ASSERT(0);
  }

  p_ctrl = ctrl_create_ctrl(class_name, IDC_COMM_CTRL,
                            lw , 0, rw, COMM_CTRL_H,
                            cont, 0);
  
  ctrl_set_rstyle(p_ctrl, RSI_IGNORE, rsi_hl, RSI_IGNORE);
  ctrl_set_keymap(p_ctrl, keymap);

  if((NULL != set_align_type) || (NULL !=  set_font_style))
  {
    if(set_align_type != NULL)
    {
      set_align_type(p_ctrl, STL_CENTER | STL_VCENTER);
    }
    if(set_font_style != NULL)
    {
      set_font_style(p_ctrl,
                     FSI_WHITE_22,
                     FSI_WHITE_22,
                     FSI_WHITE_22);
    }
  }
  /* after creating */
  switch(type)
  {
    case COMM_NUMEDIT:
      ctrl_set_proc(p_ctrl, ui_comm_num_proc);
      break;
   
    default:
      break;
  }

  ctrl_set_active_ctrl(cont, p_ctrl);
  return cont;
}

control_t *on_factory_test_static_create(control_t *parent,
                                 u8 ctrl_id,
                                 u16 x,
                                 u16 y,
                                 u16 lw,
                                 u16 rw)
{
  return on_factory_test_child_ctrl_create(COMM_STATIC, parent, ctrl_id, x, y, lw, rw);
}

void on_factory_test_static_set_rstyle(control_t *cont,
                               u8 rsi_cont,
                               u8 rsi_stxt,
                               u8 rsi_ctrl)
{
  control_t *p_ctrl;

  ctrl_set_rstyle(cont, rsi_cont, rsi_cont, rsi_cont);

  p_ctrl = ctrl_get_child_by_id(cont, IDC_COMM_TXT);
  ctrl_set_rstyle(p_ctrl, rsi_stxt, rsi_stxt, rsi_stxt);

  p_ctrl = ctrl_get_child_by_id(cont, IDC_COMM_CTRL);
  ctrl_set_rstyle(p_ctrl, rsi_ctrl, rsi_ctrl, rsi_ctrl);
}


void on_factory_test_static_set_align_type(control_t *cont,
                                   u16 stxt_ox,
                                   u32 stxt_align,
                                   u16 ctrl_ox,
                                   u32 ctrl_align)
{
  control_t *p_ctrl;

  p_ctrl = ctrl_get_child_by_id(cont, IDC_COMM_TXT);
  text_set_align_type(p_ctrl, stxt_align);
  text_set_offset(p_ctrl, stxt_ox, 0);

  p_ctrl = ctrl_get_child_by_id(cont, IDC_COMM_CTRL);
  text_set_align_type(p_ctrl, ctrl_align);
  text_set_offset(p_ctrl, ctrl_ox, 0);
}

void on_factory_test_static_set_param(control_t *p_ctrl, u32 content_type)
{
  control_t *txt;

  txt = ctrl_get_child_by_id(p_ctrl, IDC_COMM_CTRL);
  text_set_content_type(txt, content_type);
}


void on_factory_test_static_set_static_txt(control_t *p_ctrl, u16 strid)
{
  control_t *txt;

  txt = ctrl_get_child_by_id(p_ctrl, IDC_COMM_TXT);
  if(txt != NULL)
  {
    text_set_content_by_strid(txt, strid);
  }
}

control_t *on_factory_test_bar_create(control_t *parent,
                              u8 ctrl_id,
                              u16 x,
                              u16 y,
                              u16 w,
                              u16 h,
                              u16 txt_x,
                              u16 txt_y,
                              u16 txt_w,
                              u16 txt_h,
                              u16 percent_x,
                              u16 percent_y,
                              u16 percent_w,
                              u16 percent_h)
{
  rect_t rc_cont, rc_tmp;
  control_t *cont, *bar, *txt, *percent;

  // calc the rect of container
  set_rect(&rc_cont, (s16)x, (s16)y,
           (s16)(x + w), (s16)(y + h));
  set_rect(&rc_tmp, (s16)txt_x, (s16)txt_y,
           (s16)(txt_x + txt_w), (s16)(txt_y + txt_h));
  generate_boundrect(&rc_cont, &rc_cont, &rc_tmp);

  set_rect(&rc_tmp, (s16)percent_x, (s16)percent_y,
           (s16)(percent_x + percent_w), (s16)(percent_y + percent_h));
  generate_boundrect(&rc_cont, &rc_cont, &rc_tmp);

  // create
  cont = ctrl_create_ctrl(CTRL_CONT, ctrl_id,
                          (u16)(rc_cont.left), (u16)(rc_cont.top),
                          (u16)RECTW(rc_cont), (u16)RECTH(rc_cont),
                          parent, 0);
  ctrl_set_rstyle(cont, RSI_IGNORE, RSI_IGNORE, RSI_IGNORE);

  txt = ctrl_create_ctrl(CTRL_TEXT, IDC_COMM_BAR_TXT,
                         (u16)(txt_x - rc_cont.left), (u16)(txt_y - rc_cont.top),
                         txt_w, txt_h,
                         cont, 0);
  text_set_align_type(txt, STL_LEFT | STL_VCENTER);

  bar = ctrl_create_ctrl(CTRL_PBAR, IDC_COMM_BAR,
                         (u16)(x - rc_cont.left), (u16)(y - rc_cont.top), w, h,
                         cont, 0);
  ctrl_set_mrect(bar,
                    0, 0, (s16)(w), (s16)(h));

  percent = ctrl_create_ctrl(CTRL_TEXT, IDC_COMM_BAR_PERCENT,
                             (u16)(percent_x - rc_cont.left),
                             (u16)(percent_y - rc_cont.top),
                             percent_w, percent_h,
                             cont, 0);
  text_set_align_type(percent, STL_RIGHT | STL_VCENTER);
  text_set_content_type(percent, TEXT_STRTYPE_UNICODE);
  text_set_content_by_ascstr(percent, (u8 *)"0%");

  return cont;
}


void on_factory_test_bar_set_style(control_t *cont,
                           u8 rsi_bar,
                           u8 rsi_mid,
                           u8 rsi_txt,
                           u8 fsi_txt,
                           u8 rsi_percent,
                           u8 fsi_percent)
{
  control_t *p_ctrl;

  p_ctrl = ctrl_get_child_by_id(cont, IDC_COMM_BAR_TXT);
  ctrl_set_rstyle(p_ctrl, rsi_txt, rsi_txt, rsi_txt);
  text_set_font_style(p_ctrl, fsi_txt, fsi_txt, fsi_txt);

  p_ctrl = ctrl_get_child_by_id(cont, IDC_COMM_BAR);
  ctrl_set_rstyle(p_ctrl, rsi_bar, rsi_bar, rsi_bar);
  pbar_set_rstyle(p_ctrl, rsi_mid, RSI_IGNORE, INVALID_RSTYLE_IDX);

  p_ctrl = ctrl_get_child_by_id(cont, IDC_COMM_BAR_PERCENT);

  ctrl_set_rstyle(p_ctrl, rsi_percent, rsi_percent, rsi_percent);
 
  text_set_font_style(p_ctrl, fsi_percent, fsi_percent, fsi_percent);
}


void on_factory_test_bar_set_param(control_t *cont,
                           u16 rsc_id,
                           u16 min,
                           u16 max,
                           u16 step)
{
  control_t *p_ctrl;

  p_ctrl = ctrl_get_child_by_id(cont, IDC_COMM_BAR_TXT);
  text_set_content_type(p_ctrl, TEXT_STRTYPE_STRID);
  text_set_content_by_strid(p_ctrl, rsc_id);

  p_ctrl = ctrl_get_child_by_id(cont, IDC_COMM_BAR);
  pbar_set_count(p_ctrl, min, max, step);
}


BOOL on_factory_test_bar_update(control_t *cont, u16 val, BOOL is_force)
{
  control_t *p_ctrl;
  u8 str_buf[10];
  BOOL is_redraw = FALSE;

  p_ctrl = ctrl_get_child_by_id(cont, IDC_COMM_BAR);
  if(pbar_get_current(p_ctrl) != val || is_force)
  {
    pbar_set_current(p_ctrl, val);
    p_ctrl = ctrl_get_child_by_id(cont, IDC_COMM_BAR_PERCENT);
    sprintf((char *)str_buf, "%d%%", val);
    text_set_content_by_ascstr(p_ctrl, str_buf);

    is_redraw = TRUE;
  }

  return is_redraw;
}

BOOL on_factory_test_tp_bar_update(control_t *cont, u16 val, BOOL is_force,u8 *pox)
{
  control_t *p_ctrl;
  u8 str_buf[10];
  BOOL is_redraw = FALSE;

  p_ctrl = ctrl_get_child_by_id(cont, IDC_COMM_BAR);
  if(pbar_get_current(p_ctrl) != val || is_force)
  {
    pbar_set_current(p_ctrl, val);
    p_ctrl = ctrl_get_child_by_id(cont, IDC_COMM_BAR_PERCENT);
    sprintf((char*)str_buf, "%d%s", val,pox);
    text_set_align_type(p_ctrl, STL_CENTER|STL_VCENTER);
    text_set_content_by_ascstr(p_ctrl, (u8*)str_buf);

    is_redraw = TRUE;
  }
  
  return is_redraw;
}

void on_factory_test_bar_paint(control_t *cont, BOOL is_force)
{
  control_t *p_ctrl;

  p_ctrl = ctrl_get_child_by_id(cont, IDC_COMM_BAR);
  ctrl_paint_ctrl(p_ctrl, is_force);

  p_ctrl = ctrl_get_child_by_id(cont, IDC_COMM_BAR_PERCENT);
  ctrl_paint_ctrl(p_ctrl, is_force);
}

control_t *on_factory_test_numedit_create(control_t *parent,
                                  u8 ctrl_id,
                                  u16 x,
                                  u16 y,
                                  u16 lw,
                                  u16 rw)
{
  return on_factory_test_child_ctrl_create(COMM_NUMEDIT, parent, ctrl_id, x, y, lw, rw);
}

void on_factory_test_numedit_set_postfix(control_t *p_ctrl, u16 strid)
{
  control_t *nbox;

  nbox = ctrl_get_child_by_id(p_ctrl, IDC_COMM_CTRL);
  nbox_set_postfix_type(nbox, NBOX_ITEM_POSTFIX_TYPE_STRID);
  nbox_set_postfix_by_strid(nbox, strid);
}

void on_factory_test_numedit_set_num(control_t *p_ctrl, u32 num)
{
  control_t *nbox;

  nbox = ctrl_get_child_by_id(p_ctrl, IDC_COMM_CTRL);
  nbox_set_num_by_dec(nbox, num);
}


u32 on_factory_test_numedit_get_num(control_t *p_ctrl)
{
  control_t *nbox;

  nbox = ctrl_get_child_by_id(p_ctrl, IDC_COMM_CTRL);
  return nbox_get_num(nbox);
}

void on_factory_test_numedit_set_decimal_places(control_t *p_ctrl, u8 places)
{
  control_t *nbox;

  nbox = ctrl_get_child_by_id(p_ctrl, IDC_COMM_CTRL);
  nbox_set_separator(nbox, '.');
  nbox_set_separator_pos(nbox, places);
}

void on_factory_test_numedit_set_param(control_t *p_ctrl,
                               u32 type,
                               u32 min,
                               u32 max,
                               u8 bit_length,
                               u8 focus)
{
  control_t *nbox;

  nbox = ctrl_get_child_by_id(p_ctrl, IDC_COMM_CTRL);
  nbox_set_num_type(nbox, type);
  nbox_set_postfix_type(nbox, type);
  nbox_set_range(nbox, (s32)min, (s32)max, bit_length);
  nbox_set_focus(nbox, focus);
}

void on_factory_test_set_content_by_ascstr(control_t *p_ctrl, u8 *ascstr)
{
  control_t *txt;

  txt = ctrl_get_child_by_id(p_ctrl, IDC_COMM_CTRL);
  text_set_content_by_ascstr(txt, ascstr);
}

static RET_CODE factory_setting_list_update(control_t *ctrl, u16 start, u16 size,u32 context)
{
    u16 i = 0;
    u16 cnt = list_get_count(ctrl);
    
  for (i = 0; i < size; i++)
  {
    if (i + start < cnt)
    {
      // NAME 
      list_set_field_content_by_strid(ctrl, (u16)(start + i),0, list_child_item[i + start].str_id); //pg name
    }
  }
  
  return SUCCESS;
} 

control_t *on_factory_test_select_create(control_t *parent,
                                 u8 ctrl_id,
                                 u16 x,
                                 u16 y,
                                 u16 lw,
                                 u16 rw)
{
  return on_factory_test_child_ctrl_create(COMM_SELECT, parent, ctrl_id, x, y, lw, rw);
}

u16 on_factory_test_select_get_focus(control_t *p_ctrl)
{
  u16 focus = 0;
  control_t *cbox;

  cbox = ctrl_get_child_by_id(p_ctrl, IDC_COMM_CTRL);
  switch(cbox_get_work_mode(cbox))
  {
    case CBOX_WORKMODE_STATIC:
      focus = cbox_static_get_focus(cbox);
      break;
    case CBOX_WORKMODE_DYNAMIC:
      focus = cbox_dync_get_focus(cbox);
      break;
   case CBOX_WORKMODE_NUMBER:
     focus = (u16)cbox_num_get_curn(cbox);
    break;

    default:
      MT_ASSERT(0);
  }
  return focus;
}

void on_factory_test_ctrl_set_proc(control_t *p_ctrl, msgproc_t proc)
{
  control_t *sub_ctrl;

  sub_ctrl = ctrl_get_child_by_id(p_ctrl, IDC_COMM_CTRL);
  ctrl_set_proc(sub_ctrl, proc);
}

void on_factory_test_set_content_by_unistr(control_t *p_ctrl, u16 *unistr)
{
  control_t *txt;

  txt = ctrl_get_child_by_id(p_ctrl, IDC_COMM_CTRL);
  text_set_content_by_unistr(txt, unistr);
}

static void on_factory_test_flash_prot_fill_content(control_t *p_ctrl, clt_spi_prot_block_type flash_protect)
{
  u8 asc_str[64] = {0};
  switch(flash_protect)
  {
     case CLT_PRT_UNPROT_ALL:      
		sprintf((char *)asc_str,"%s", "NO PROECT");
       break;
     case CLT_PRT_PROT_ALL:
		sprintf((char *)asc_str,"%s", "ALL PROECT");
       break;
     case CLT_PRT_UPPER_1_64:
		sprintf((char *)asc_str,"%s", "UPPER 1/64");
       break;
     case CLT_PRT_UPPER_1_32:
		sprintf((char *)asc_str,"%s", "UPPER 1/32");
       break;
     case CLT_PRT_UPPER_1_16:
		sprintf((char *)asc_str,"%s", "UPPER 1/16");
       break;
     case CLT_PRT_UPPER_1_8:
		sprintf((char *)asc_str,"%s", "UPPER 1/8");
       break;
     case CLT_PRT_UPPER_1_4:
		sprintf((char *)asc_str,"%s", "UPPER 1/4");
       break;
     case CLT_PRT_UPPER_1_2:
		sprintf((char *)asc_str,"%s", "UPPER 1/2");
       break;
     case CLT_PRT_LOWER_1_64:
		sprintf((char *)asc_str,"%s", "LOWER 1/64");
       break;
     case CLT_PRT_LOWER_1_32:
		sprintf((char *)asc_str,"%s", "LOWER 1/32");
       break;
     case CLT_PRT_LOWER_1_16:
		sprintf((char *)asc_str,"%s", "LOWER 1/16");
       break;
     case CLT_PRT_LOWER_1_8:
		sprintf((char *)asc_str,"%s", "LOWER 1/8");
       break;
     case CLT_PRT_LOWER_1_4:
		sprintf((char *)asc_str,"%s", "LOWER 1/4");
       break;
     case CLT_PRT_LOWER_1_2:
		sprintf((char *)asc_str,"%s", "LOWER 1/2");
       break;
     case CLT_PRT_LOWER_3_4:
		sprintf((char *)asc_str,"%s", "LOWER 3/4");
       break;
     case CLT_PRT_LOWER_7_8:
		sprintf((char *)asc_str,"%s", "LOWER 7/8");
       break;
     case CLT_PRT_LOWER_15_16:
		sprintf((char *)asc_str,"%s", "LOWER 15/16");
       break;
     case CLT_PRT_LOWER_31_32:
		sprintf((char *)asc_str,"%s", "LOWER 31/32");
       break;
     case CLT_PRT_LOWER_63_64:
		sprintf((char *)asc_str,"%s", "LOWER 63/64");
       break;
     case CLT_PRT_UPPER_3_4:
		sprintf((char *)asc_str,"%s", "UPPER 3/4");
       break;
     case CLT_PRT_UPPER_7_8:
		sprintf((char *)asc_str,"%s", "UPPER 7/8");
       break;
     case CLT_PRT_UPPER_15_16:
		sprintf((char *)asc_str,"%s", "UPPER 15/16");
       break;
     case CLT_PRT_UPPER_31_32:
		sprintf((char *)asc_str,"%s", "UPPER 31/32");
       break;
     case CLT_PRT_UPPER_63_64:
		sprintf((char *)asc_str,"%s", "UPPER 63/64");
       break;
     case CLT_PRT_BLOCK_0:
		sprintf((char *)asc_str,"%s", "PROTECT BLOCK0");
       break;
     default:
       break;
  }
  on_factory_test_static_set_rstyle(p_ctrl, RSI_GRAY, RSI_GRAY, RSI_BLUE);
  on_factory_test_set_content_by_ascstr(p_ctrl,asc_str);

  return ;
}

static void get_basic_info(control_t *p_parent,u16 index)
{
  u16 i = 0;
  control_t *p_ctrl;
  u8 asc_str[64] = {0};
  clt_setting_info_t p_info;
  clt_factory_test_para_t para;
  client_setting_get_info(g_p_client, &p_info);
  for(i = 0; i < list_child_item[index].child_total; i++)
  {
        switch (list_child_item[index].child_style[i].id)
        {
          case IDC_BASIC_HW_VER:
             p_ctrl = ctrl_get_child_by_id(p_parent, list_child_item[index].child_style[i].id);
             MT_ASSERT(p_ctrl!= NULL);
             sprintf((char *)asc_str, "V%s", p_info.hw_ver);
             on_factory_test_set_content_by_ascstr(p_ctrl,p_info.hw_ver);
          break;
          case IDC_BASIC_SW_VER:
              p_ctrl = ctrl_get_child_by_id(p_parent, list_child_item[index].child_style[i].id);

              MT_ASSERT(p_ctrl!= NULL);
              sprintf((char *)asc_str, "V%s", p_info.sw_ver);
              on_factory_test_set_content_by_ascstr(p_ctrl,asc_str);

            break;
          case IDC_BASIC_CARD_NO:
              p_ctrl = ctrl_get_child_by_id(p_parent, list_child_item[index].child_style[i].id);
              MT_ASSERT(p_ctrl!= NULL);
              sprintf((char*)asc_str, "%s", card_info.sn);
              on_factory_test_set_content_by_ascstr(p_ctrl,asc_str);

          break;
        case IDC_BASIC_CA_PROVIDER:
          p_ctrl = ctrl_get_child_by_id(p_parent, list_child_item[index].child_style[i].id);
          MT_ASSERT(p_ctrl!= NULL);
#ifdef WIN32
          sprintf((char *)asc_str, "%s", "CA_PROVIDER");
          on_factory_test_set_content_by_ascstr(p_ctrl,asc_str);
#else
        if(strlen(card_info.cas_manu_name) == 0)
        {
          sprintf((char *)asc_str, "%s", "YXSB");
        }
        else
        {
          sprintf((char *)asc_str, "%s",card_info.cas_manu_name);
        }
        on_factory_test_set_content_by_ascstr(p_ctrl,asc_str);
#endif
        break;
      case IDC_BASIC_CA_VER:
        p_ctrl = ctrl_get_child_by_id(p_parent, list_child_item[index].child_style[i].id);
        sprintf((char *)asc_str, "0X%X%X%X%X",card_info.card_ver[3],
        card_info.card_ver[2], card_info.card_ver[1], card_info.card_ver[0]);
        on_factory_test_set_content_by_ascstr(p_ctrl,asc_str);
        MT_ASSERT(p_ctrl!= NULL);
        break;
      case IDC_BASIC_STB_ID:
       p_ctrl = ctrl_get_child_by_id(p_parent, list_child_item[index].child_style[i].id);
       MT_ASSERT(p_ctrl!= NULL);
       on_factory_test_set_content_by_ascstr(p_ctrl,p_info.stb_id);
       break;	 

      case IDC_BASIC_USB_INFO:
        p_ctrl = ctrl_get_child_by_id(p_parent, list_child_item[index].child_style[i].id);
        MT_ASSERT(p_ctrl!= NULL);
        if(ui_get_usb_status() == TRUE)
        {
          on_factory_test_static_set_rstyle(p_ctrl, RSI_GRAY, RSI_GRAY, RSI_GRAY);
          sprintf((char*)asc_str, "%s","Usb Connect And Write Success !");            
        }
        else
        {
          on_factory_test_static_set_rstyle(p_ctrl, RSI_GRAY, RSI_GRAY, RSI_BLUE);
          sprintf((char*)asc_str, "%s","Usb Not Connect !");
        }
        
        on_factory_test_set_content_by_ascstr(p_ctrl,asc_str);       
        
          break;
      case IDC_BASIC_FLSH_INFO:
        p_ctrl = ctrl_get_child_by_id(p_parent, list_child_item[index].child_style[i].id);
        para.op = CLT_FT_FLASH_PROTECT;
        client_perform_factory_test(g_p_client,&para);
        on_factory_test_flash_prot_fill_content(p_ctrl, para.flash_protect);
        break;  
      default:
        OS_PRINTF("BASIC INFO NO THE ID \n");
      break;
    }
          
  }
      return;
}

static void get_default_load_param( control_t *p_parent ,u16 index)
{
  if(index > MAX_INVAL_COLUMN)
  {
    return;
  }
  switch(index)
  {
    case BASC_INFO_COLUMN:
      get_basic_info(p_parent,BASC_INFO_COLUMN);
      break;
    case  DIS_PLAY_INFO_COLUMN:
      break;
    case C_SCAN_INFO_COLUMN:
      break;
    default: 
      break;
  }
}

static void set_ctrl_child_position(u32 i, u16 k, u16 *x, u16 *y, u16 *w, u16 *h)
{
     u16 m = 0;
	  m= k/2;
      switch (i)
      {
        case BASC_INFO_COLUMN:
			
          {
            if((k+1)%2 == 0)
            {
              *x = CHILD_BASIC_HW_VER_X + CHILD_BASIC_HW_VER_W + CHILD_BASIC_HW_VER_GAP;
            }
            else
            {
              
              *y = CHILD_BASIC_HW_VER_Y + m*(CHILD_BASIC_HW_VER_H+CHILD_BASIC_HW_VER_VERTICAL_GAP);
              *x = CHILD_BASIC_HW_VER_X;
            }     
            
          break;
          }
        case DIS_PLAY_INFO_COLUMN:
          {
            if ((list_child_item[i].child_style[k].id== IDC_DISPLAY_TV_WIN))
            {
                *x = FACTORY_PREV_X;
                *y = FACTORY_PREV_Y;
                *w = FACTORY_PREV_W;
                *h = FACTORY_PREV_H;
            }
            else if ((list_child_item[i].child_style[k].id== IDC_DISPLAY_TV_SELECT))
            {
              *x = FACTORY_PREV_SELECT_X;
              *y = FACTORY_PREV_SELECT_Y;
            }
            else if((list_child_item[i].child_style[k].id== IDC_DISPLAY_VOLUME_TEXT))
            {
               *x = FACTORY_PREV_VOLUME_TEXT_X;
               *y = FACTORY_PREV_VOLUME_TEXT_Y;
               *w = FACTORY_PREV_VOLUME_TEXT_W;
               *h = FACTORY_PREV_VOLUME_TEXT_H;
            }
           else if((list_child_item[i].child_style[k].id== IDC_DISPLAY_VOLUME))
            {
              *x = FACTORY_PREV_VOLUME_X;
              *y = FACTORY_PREV_VOLUME_Y;
              *w = FACTORY_PREV_VOLUME_W;
              *h = FACTORY_PREV_VOLUME_H;
            }
           else if((list_child_item[i].child_style[k].id== IDC_DISPLAY_VOLUME_TEXT1))
           {
               *x = FACTORY_PREV_VOLUME_TEXT1_X;
               *y = FACTORY_PREV_VOLUME_TEXT1_Y;
               *w = FACTORY_PREV_VOLUME_TEXT1_W;
               *h = FACTORY_PREV_VOLUME_TEXT1_H;               
           }
            break;
          }
          
        case C_SCAN_INFO_COLUMN:
          {
           if(list_child_item[i].child_style[k].id== IDC_SCAN_FREQ_INFO
            || list_child_item[i].child_style[k].id==IDC_SCAN_SYMB_INFO
            || list_child_item[i].child_style[k].id==IDC_SCAN_QAM_INFO)
           {
              *x = FACTORY_SCAN_FREQ_X;
              *y = FACTORY_SCAN_FREQ_Y + k*(FACTORY_ITEM_GAP + COMM_CTRL_H);
           }
           else if(list_child_item[i].child_style[k].id== IDC_SCAN_START_SEARCH)
           {
              *x = FACTORY_SCAN_FREQ_X;
              *y = FACTORY_SCAN_FREQ_Y + k*(FACTORY_ITEM_GAP + COMM_CTRL_H);
              *w = FACTORY_SCAN_INFO_TOTAL_W;
              *h = FACTORY_PREV_VOLUME_TEXT_H;
           }
          break;
        }
        case PORT_INFO_COLUMN:
			{			
		  	if((k+1)%2 == 0)
		  	{
		   	  *x = (FACTROY_PORT_KEY_BEGIN_X + FACTORY_PORT_KEY_W + CHILD_BASIC_HW_VER_GAP);

			}
			else
			{
				*x = FACTROY_PORT_KEY_BEGIN_X;
				*y = FACTROY_PORT_KEY_BEGIN_Y + m*(FACTORY_PORT_KEY_H + FACTORY_PORT_KEY_H_GAP);
				*w = FACTORY_PORT_KEY_W;
				*h = FACTORY_PORT_KEY_H;
			}
           break;
          }
			break;
        default:
			
          break;

      }
      return;
}

static RET_CODE factory_mode_create_child(control_t *p_parent, u32 show_idx)
{
    control_t *p_ctrl[CHILD_MAX_NUM] = {NULL,};
    u16 i, k, x, y, w, h, j = 0;
	clt_setting_main_tp_t tp_info = {0};
  	client_setting_get_main_tp(g_p_client, &tp_info);
    for(i = 0;i < MAX_INVAL_COLUMN; i ++)
    { 
      for(k = 0; k < list_child_item[i].child_total; ++k)
      {
        set_ctrl_child_position(i,k, &x, &y, &w, &h);
        switch(list_child_item[i].child_style[k].ctrl_type)
        {
          case COMM_STATIC:
            {
				if(i == BASC_INFO_COLUMN)
				{
                p_ctrl[k] = on_factory_test_static_create(p_parent, 
                              list_child_item[i].child_style[k].id, 
                                      x,  y, 
                                      CHILD_BASIC_HW_VER_LW, 
                                      CHILD_BASIC_HW_VER_RW);
                
               MT_ASSERT( p_ctrl[k] != NULL);
               on_factory_test_static_set_rstyle(p_ctrl[k],RSI_MAP_STYLE, RSI_MAP_STYLE, RSI_MAP_STYLE);
               on_factory_test_static_set_align_type(p_ctrl[k], 0, STL_LEFT, 0,STL_CENTER);
               on_factory_test_static_set_param(p_ctrl[k], TEXT_STRTYPE_UNICODE);
               on_factory_test_static_set_static_txt(p_ctrl[k],list_child_item[i].child_style[k].child_strid);
				}
				else if(i == DIS_PLAY_INFO_COLUMN && list_child_item[i].child_style[k].id == IDC_DISPLAY_TV_SELECT)
				{
					p_ctrl[k] = on_factory_test_static_create(p_parent, 
	                              list_child_item[i].child_style[k].id, 
	                                      x,  y, 
	                                      CHILD_BASIC_HW_VER_LW, 
	                                     CHILD_BASIC_HW_VER_RW);
					MT_ASSERT( p_ctrl[k] != NULL);
					on_factory_test_static_set_rstyle(p_ctrl[k],RSI_IGNORE, RSI_IGNORE, RSI_IGNORE);
					on_factory_test_static_set_align_type(p_ctrl[k], 0, STL_LEFT, 0,STL_LEFT);
					on_factory_test_static_set_static_txt(p_ctrl[k],list_child_item[i].child_style[k].child_strid);
					on_factory_test_static_set_param(p_ctrl[k], TEXT_STRTYPE_UNICODE);
				}
               break;
            } 
          case COMM_SELECT:
            {
              if(i== C_SCAN_INFO_COLUMN&& list_child_item[i].child_style[k].id == IDC_SCAN_QAM_INFO)
              {
                  p_ctrl[k] = on_factory_test_select_create(p_parent, 
                  list_child_item[i].child_style[k].id,
                           x, y, FACTORY_SCAN_FREQ_LW,
                                   FACTORY_SCAN_FREQ_RW);   
              }
          else
          {
              p_ctrl[k] = on_factory_test_select_create(p_parent, 
                list_child_item[i].child_style[k].id,
                         x, y, 2*CHILD_BASIC_HW_VER_LW,
                                  2*CHILD_BASIC_HW_VER_RW);
           }
            MT_ASSERT(p_ctrl[k] != NULL);
            on_factory_test_static_set_static_txt(p_ctrl[k], list_child_item[i].child_style[k].child_strid);
            if(i== C_SCAN_INFO_COLUMN&& list_child_item[i].child_style[k].id == IDC_SCAN_QAM_INFO)
            {
              on_factory_test_ctrl_set_proc(p_ctrl[k], factory_search_qam_proc); 
              ui_comm_select_set_param(p_ctrl[k], IDC_COMM_CTRL,TRUE,
                CBOX_WORKMODE_STATIC,list_child_item[i].child_style[k].cbox_cnt, 
                    CBOX_ITEM_STRTYPE_STRID,NULL);
              for(j = 0; j < list_child_item[i].child_style[k].cbox_cnt; ++j)
              {
                ui_comm_select_set_content(p_ctrl[k], IDC_COMM_CTRL, j, list_child_item[i].child_style[k].cbox_strid[j]);
              }
              ui_comm_select_set_focus(p_ctrl[k], IDC_COMM_CTRL, tp_info.mod);
              ctrl_set_related_id(p_ctrl[k], 0, IDC_SCAN_SYMB_INFO, 0, IDC_SCAN_START_SEARCH);               
            }
            break;
            }
            case COMM_TEXT:
            {              
               p_ctrl[k] = ctrl_create_ctrl(CTRL_TEXT,list_child_item[i].child_style[k].id,
                            x, y,
                            w, h,
                            p_parent, 0);
               MT_ASSERT(p_ctrl[k]!= NULL);
               if(i== DIS_PLAY_INFO_COLUMN &&list_child_item[i].child_style[k].id == IDC_DISPLAY_TV_WIN)
               {
                  ctrl_set_rstyle(p_ctrl[k], RSI_IGNORE, RSI_IGNORE, RSI_IGNORE);

               }
               else if(i== DIS_PLAY_INFO_COLUMN )
               {
                  
                  text_set_font_style(p_ctrl[k], FSI_WHITE_22, FSI_WHITE_22,FSI_WHITE_22);
                  text_set_align_type(p_ctrl[k], STL_CENTER| STL_VCENTER);
                  if(list_child_item[i].child_style[k].id ==IDC_DISPLAY_VOLUME_TEXT)
                  {
                    ctrl_set_rstyle(p_ctrl[k], RSI_IGNORE, RSI_IGNORE, RSI_IGNORE);
                    text_set_content_type(p_ctrl[k], TEXT_STRTYPE_STRID);
                    text_set_content_by_strid(p_ctrl[k], list_child_item[i].child_style[k].child_strid);
                    ctrl_set_related_id(p_ctrl[k], 0,IDC_DISPLAY_TV_SELECT, 0, IDC_DISPLAY_TV_SELECT);
                  }
                  else if(list_child_item[i].child_style[k].id ==IDC_DISPLAY_VOLUME_TEXT1)
                  {
                    ctrl_set_rstyle(p_ctrl[k], RSI_IGNORE, RSI_IGNORE, RSI_IGNORE);
                    text_set_content_type(p_ctrl[k],TEXT_STRTYPE_DEC);
                    text_set_content_by_dec(p_ctrl[k],global_volue);
                  }
               }else if(i== C_SCAN_INFO_COLUMN)
               {
                  if(list_child_item[i].child_style[k].id ==IDC_SCAN_START_SEARCH)
                  {
                   ctrl_set_keymap(p_ctrl[k], factory_manual_search_select_keymap);
                   ctrl_set_proc(p_ctrl[k], factory_manual_search_select_proc);
                   ctrl_set_rstyle(p_ctrl[k],
                                      RSI_IGNORE,
                                      RSI_DROPLIST_ITEM_FOCUS,
                                      RSI_IGNORE);
                   text_set_content_type(p_ctrl[k], TEXT_STRTYPE_STRID);
                   text_set_font_style(p_ctrl[k],
                                      FSI_WHITE_22,
                                      FSI_WHITE_22,
                                      FSI_WHITE_22);
                   text_set_align_type(p_ctrl[k], STL_LEFT| STL_VCENTER);
                   text_set_content_by_strid(p_ctrl[k],list_child_item[i].child_style[k].child_strid);
                   text_set_offset(p_ctrl[k], COMM_CTRL_OX, 0);
                   ctrl_set_related_id(p_ctrl[k], 0, IDC_SCAN_QAM_INFO,0, 0);
                  }
               }
			   else if(i ==PORT_INFO_COLUMN)
			   {
					ctrl_set_rstyle(p_ctrl[k], RSI_TV_SMALL, RSI_TV_SMALL, RSI_TV_SMALL);
                    text_set_content_type(p_ctrl[k], TEXT_STRTYPE_UNICODE);
				  	text_set_align_type(p_ctrl[k], STL_VCENTER| STL_VCENTER);
                text_set_font_style(p_ctrl[k],
                       FSI_WHITE_22,
                       FSI_WHITE_22,
                       FSI_WHITE_22);

					if(list_child_item[i].child_style[k].id == IDC_PORT_KEY_POWER)
					{
						text_set_content_by_ascstr(p_ctrl[k],"KEY_POWER");
					}
					else if(list_child_item[i].child_style[k].id == IDC_PORT_KEY_LEFT)
					{
						text_set_content_by_ascstr(p_ctrl[k],"KEY_LEFT");
					}
					else if(list_child_item[i].child_style[k].id == IDC_PORT_KEY_RIGHT)
					{
						text_set_content_by_ascstr(p_ctrl[k],"KEY_RIGHT");
					}
					else if(list_child_item[i].child_style[k].id == IDC_PORT_KEY_UP)
					{
						text_set_content_by_ascstr(p_ctrl[k],"KEY_UP");
					}else if(list_child_item[i].child_style[k].id ==IDC_PORT_KEY_DOWN)
					{
						text_set_content_by_ascstr(p_ctrl[k],"KEY_DOWN");
					}
										
			   }
              break;
             
            }   
            case COMM_BAR:
             {         
                 p_ctrl[k] = ctrl_create_ctrl(CTRL_PBAR, list_child_item[i].child_style[k].id,
                                         x, y,
                                         w, h,
                                         p_parent, 0);
                ctrl_set_rstyle(p_ctrl[k], RSI_PROGRESS_BAR_BG, RSI_PROGRESS_BAR_BG, RSI_PROGRESS_BAR_BG);
                ctrl_set_mrect(p_ctrl[k], 0, 0, w, h);
                pbar_set_rstyle(p_ctrl[k], RSI_PROGRESS_BAR_MID_ORANGE, RSI_PROGRESS_BAR_BG, RSI_PROGRESS_BAR_CUR);
                pbar_set_count(p_ctrl[k], 0, MAX_FACTORY_VOLUME, MAX_FACTORY_VOLUME);
                pbar_set_direction(p_ctrl[k], 1);
                pbar_set_workmode(p_ctrl[k], 1, 0);
                pbar_set_current(p_ctrl[k], global_volue);
              break;
              }
           case COMM_SELECT_BAR:
              if(list_child_item[i].child_style[k].id == IDC_DISPLAY_SING_BAR)
              {                
                p_ctrl[k] = on_factory_test_bar_create(p_parent,list_child_item[i].child_style[k].id, 
                                      FACTORY_PREV_SING_PERCENT_X,
                                      FACTORY_PREV_SING_PERCENT_Y,
                                      FACTORY_PREV_SING_PERCENT_W,
                                      FACTORY_PREV_SING_PERCENT_H,
                                      FACTORY_PREV_SING_NAME_X,
                                      FACTORY_PREV_SING_NAME_Y,
                                      FACTORY_PREV_SING_NAME_W,
                                      FACTORY_PREV_SING_NAME_H,
                                      FACTORY_PREV_SING_ARG_X,
                                      FACTORY_PREV_SING_ARG_Y,
                                      FACTORY_PREV_SING_ARG_W,
                                      FACTORY_PREV_SING_ARG_H
                                      );
                MT_ASSERT(p_ctrl[k]!= NULL);
                on_factory_test_bar_set_param(p_ctrl[k], IDS_RFLEVEL, 0, 100, 100);
                on_factory_test_bar_set_style(p_ctrl[k],
                        RSI_PROGRESS_BAR_BG, RSI_PROGRESS_BAR_MID_ORANGE,
                        RSI_IGNORE_DRAW, FSI_WHITE_22,
                        RSI_IGNORE, FSI_WHITE_22);
                on_factory_test_tp_bar_update(p_ctrl[k], 0, TRUE, (u8*)"dBuv");
              }
              else if(list_child_item[i].child_style[k].id == IDC_DISPLAY_SING_BAR1)
              {
                  p_ctrl[k] = on_factory_test_bar_create(p_parent,list_child_item[i].child_style[k].id, 
                                      FACTORY_PREV_SING_PERCENT1_X,
                                      FACTORY_PREV_SING_PERCENT1_Y,
                                      FACTORY_PREV_SING_PERCENT1_W,
                                      FACTORY_PREV_SING_PERCENT1_H,
                                      FACTORY_PREV_SING_NAME1_X,
                                      FACTORY_PREV_SING_NAME1_Y,
                                      FACTORY_PREV_SING_NAME1_W,
                                      FACTORY_PREV_SING_NAME1_H,
                                      FACTORY_PREV_SING_ARG1_X,
                                      FACTORY_PREV_SING_ARG1_Y,
                                      FACTORY_PREV_SING_ARG1_W,
                                      FACTORY_PREV_SING_ARG1_H
                                      );
                   MT_ASSERT(p_ctrl[k]!= NULL);
                  on_factory_test_bar_set_param(p_ctrl[k], IDS_BER, 0, 100, 100);
                  on_factory_test_bar_set_style(p_ctrl[k],
                                        RSI_PROGRESS_BAR_BG, RSI_PROGRESS_BAR_MID_ORANGE,
                                        RSI_IGNORE_DRAW, FSI_WHITE_22,
                                        RSI_IGNORE, FSI_WHITE_22);
                  on_factory_test_tp_bar_update(p_ctrl[k], 0, TRUE, (u8*)"E-6");

              }else if(list_child_item[i].child_style[k].id == IDC_SCAN_SEARCH_PROGRESS)
              {
                  p_ctrl[k] = on_factory_test_bar_create(p_parent, list_child_item[i].child_style[k].id,
                                 FACTPRY_SCAN_SEARCH_PROGRESS_X,
                                 FACTPRY_SCAN_SEARCH_PROGRESS_Y,
                                 FACTPRY_SCAN_SEARCH_PROGRESS_W,
                                 FACTPRY_SCAN_SEARCH_PROGRESS_H,
                                 FACTPRY_SCAN_SEARCH_PROGRESS_X, 
                                 FACTPRY_SCAN_SEARCH_PROGRESS_Y,
                                 0,
                                 FACTPRY_SCAN_SEARCH_PROGRESS_H,
                                 FACTPRY_SCAN_SEARCH_PROGRESS_TEXT_X,
                                 FACTPRY_SCAN_SEARCH_PROGRESS_TEXT_Y,
                                 FACTPRY_SCAN_SEARCH_PROGRESS_TEXT_W,
                                 FACTPRY_SCAN_SEARCH_PROGRESS_TEXT_H);
                  on_factory_test_bar_set_param(p_ctrl[k], RSC_INVALID_ID, 0, 100, 100);

                  on_factory_test_bar_set_style(p_ctrl[k],
                                          RSI_PROGRESS_BAR_BG, RSI_PROGRESS_BAR_MID_ORANGE,
                                          RSI_IGNORE_DRAW, FSI_WHITE_22,
                                          RSI_IGNORE, FSI_WHITE_22);
                  on_factory_test_bar_update(p_ctrl[k], 0, TRUE);
              }
              else if(list_child_item[i].child_style[k].id == IDC_SCAN_SEARCH_SING_BAR)
              {
                   p_ctrl[k] = on_factory_test_bar_create(p_parent,list_child_item[i].child_style[k].id, 
                                      FACTORY_SCAN_SING_PERCENT_X,
                                      FACTORY_SCAN_SING_PERCENT_Y,
                                      FACTORY_SCAN_SING_PERCENT_W,
                                      FACTORY_SCAN_SING_PERCENT_H,
                                      FACTPRY_SCAN_SEARCH_SIGN_NAME_X,
                                      FACTPRY_SCAN_SEARCH_SIGN_NAME_Y,
                                      FACTPRY_SCAN_SEARCH_SIGN_NAME_W,
                                      FACTPRY_SCAN_SEARCH_SIGN_NAME_H,
                                      FACTPRY_SCAN_SEARCH_SIGN_ARG_X,
                                      FACTPRY_SCAN_SEARCH_SIGN_ARG_Y,
                                      FACTPRY_SCAN_SEARCH_SIGN_ARG_W,
                                      FACTPRY_SCAN_SEARCH_SIGN_ARG_H
                                      );
                MT_ASSERT(p_ctrl[k]!= NULL);
                on_factory_test_bar_set_param(p_ctrl[k], IDS_RFLEVEL, 0, 100, 100);
                on_factory_test_bar_set_style(p_ctrl[k],
                        RSI_PROGRESS_BAR_BG, RSI_PROGRESS_BAR_MID_ORANGE,
                        RSI_IGNORE_DRAW, FSI_WHITE_22,
                        RSI_IGNORE, FSI_WHITE_22);
                on_factory_test_tp_bar_update(p_ctrl[k], 0, TRUE, (u8*)"dBuv");
              }
              else if(list_child_item[i].child_style[k].id == IDC_SCAN_SEARCH_SING_BAR1)
              {
                 p_ctrl[k] = on_factory_test_bar_create(p_parent,list_child_item[i].child_style[k].id, 
                                    FACTORY_SCAN_SING_PERCENT1_X,
                                    FACTORY_SCAN_SING_PERCENT1_Y,
                                    FACTORY_SCAN_SING_PERCENT1_W,
                                    FACTORY_SCAN_SING_PERCENT1_H,
                                    FACTPRY_SCAN_SEARCH_SIGN_NAME1_X,
                                    FACTPRY_SCAN_SEARCH_SIGN_NAME1_Y,
                                    FACTPRY_SCAN_SEARCH_SIGN_NAME1_W,
                                    FACTPRY_SCAN_SEARCH_SIGN_NAME1_H,
                                    FACTPRY_SCAN_SEARCH_SIGN_ARG1_X,
                                    FACTPRY_SCAN_SEARCH_SIGN_ARG1_Y,
                                    FACTPRY_SCAN_SEARCH_SIGN_ARG1_W,
                                    FACTPRY_SCAN_SEARCH_SIGN_ARG1_H
                                    );
              MT_ASSERT(p_ctrl[k]!= NULL);
              on_factory_test_bar_set_param(p_ctrl[k], IDS_BER, 0, 100, 100);
              on_factory_test_bar_set_style(p_ctrl[k],
                      RSI_PROGRESS_BAR_BG, RSI_PROGRESS_BAR_MID_ORANGE,
                      RSI_IGNORE_DRAW, FSI_WHITE_22,
                      RSI_IGNORE, FSI_WHITE_22);
              on_factory_test_tp_bar_update(p_ctrl[k], 0, TRUE, (u8*)"E-6");
            }
              
            break;
            {
            case COMM_NUMEDIT:
              p_ctrl[k] = on_factory_test_numedit_create(p_parent, list_child_item[i].child_style[k].id,
                                             x, y,
                                             FACTORY_SCAN_FREQ_LW,
                                             FACTORY_SCAN_FREQ_RW);
              on_factory_test_ctrl_set_proc(p_ctrl[k], factory_search_num_edit_proc);
              on_factory_test_static_set_static_txt(p_ctrl[k], list_child_item[i].child_style[k].child_strid);
              if(list_child_item[i].child_style[k].id == IDC_SCAN_FREQ_INFO)
              {
                  on_factory_test_numedit_set_postfix(p_ctrl[k], IDS_UNIT_FREQ);
                  on_factory_test_numedit_set_param(p_ctrl[k], NBOX_NUMTYPE_DEC|NBOX_ITEM_POSTFIX_TYPE_STRID, 45000, 862000, 6, 0);
                  on_factory_test_numedit_set_decimal_places(p_ctrl[k], 3);
                  on_factory_test_numedit_set_num(p_ctrl[k], tp_info.freq);  // for test
                  ctrl_set_related_id(p_ctrl[k], 0, 0, 0, IDC_SCAN_SYMB_INFO);
              }
              else if(list_child_item[i].child_style[k].id == IDC_SCAN_SYMB_INFO)
              {
                  on_factory_test_numedit_set_postfix(p_ctrl[k], IDS_UNIT_SYMB);
                  on_factory_test_numedit_set_param(p_ctrl[k], NBOX_NUMTYPE_DEC|NBOX_ITEM_POSTFIX_TYPE_STRID, 0, 9999, 4, 0);
                  on_factory_test_numedit_set_decimal_places(p_ctrl[k], 3);
                  on_factory_test_numedit_set_num(p_ctrl[k], tp_info.sym);
                  
                  ctrl_set_related_id(p_ctrl[k], 0, IDC_SCAN_FREQ_INFO,0, IDC_SCAN_QAM_INFO);
              }
              
              break;
            }
          default:
              break;
        }
      if(show_idx == i)
      {
       ctrl_set_sts(p_ctrl[k], OBJ_STS_SHOW);
      }
      else
      {                            
        ctrl_set_sts(p_ctrl[k], OBJ_STS_HIDE);
      }
     }
      
      get_default_load_param(p_parent, i);
    }
    return SUCCESS;
}
RET_CODE open_factory_mode_ui(u32 para1, u32 para2)
{
  u16 i = 0;
  control_t *p_cont ,*p_ctrl, *p_container, *p_list;
  p_cont = fw_create_mainwin(ROOT_ID_FACTORY_MODE,    
                                        FACTORY_BACKGROUND_X,
                                        FACTORY_BACKGROUND_Y,
                                        FACTORY_BACKGROUND_W,
                                        FACTORY_BACKGROUND_H,
                                      0, 0, OBJ_ATTR_ACTIVE, 0);
  MT_ASSERT(p_cont != NULL);
  ctrl_set_rstyle(p_cont, RSI_MAINMENU_BG, RSI_MAINMENU_BG, RSI_MAINMENU_BG);
  ctrl_set_proc(p_cont, factory_setting_cont_proc);
  ctrl_set_keymap(p_cont, factory_setting_cont_keymap);
  p_container = ctrl_create_ctrl(CTRL_CONT, IDC_FACTORY_UP_CONTAINER,
                                FACTORY_CONTAINER_UP_X,
                                FACTORY_CONTAINER_UP_Y, 
                                FACTORY_CONTAINER_UP_W, 
                                FACTORY_CONTAINER_UP_H,
                                    p_cont, 0);
  MT_ASSERT( p_container != NULL);
  ctrl_set_rstyle(p_container, RSI_TITLE_BG, RSI_TITLE_BG, RSI_TITLE_BG);
  p_ctrl = ctrl_create_ctrl(CTRL_TEXT, IDC_FACTORY_UP_CONTAINER_TEXT,
                            FACTORY_CONTAINER_UP_TEXT_X, 
                            FACTORY_CONTAINER_UP_TEXT_Y,
                            FACTORY_CONTAINER_UP_TEXT_W, 
                            FACTORY_CONTAINER_UP_TEXT_H, 
                            p_container, 0);
  MT_ASSERT(p_ctrl != NULL );
  ctrl_set_rstyle(p_ctrl, RSI_IGNORE, RSI_IGNORE, RSI_IGNORE);
  text_set_font_style(p_ctrl, FSI_WHITE_22, FSI_WHITE_22, FSI_WHITE_22);
  text_set_align_type(p_ctrl, STL_CENTER|STL_CENTER);
  text_set_content_by_strid(p_ctrl, IDS_FACTORY_SET);

    p_list = ctrl_create_ctrl(CTRL_LIST,
                            IDC_FACTORY_UP_CONTAINER_LIST, 
                            FACTORY_CONTAINER_UP_LIST_X, 
                            FACTORY_CONTAINER_UP_LIST_Y,
                            FACTORY_CONTAINER_UP_LIST_W,
                            FACTORY_CONTAINER_UP_LIST_H,
                            p_container, 0);
    MT_ASSERT(p_list!= NULL);
    ctrl_set_rstyle(p_list, RSI_IGNORE,RSI_IGNORE, RSI_IGNORE);
    ctrl_set_keymap(p_list,  factory_setting_list_keymap);
    ctrl_set_proc(p_list, factory_setting_list_proc);
    ctrl_set_mrect(p_list,FACTORY_CONTAINER_UP_LIST_L,FACTORY_CONTAINER_UP_LIST_T,
                    FACTORY_CONTAINER_UP_LIST_R, FACTORY_CONTAINER_UP_LIST_B);
    list_enable_select_mode(p_list, TRUE);
    list_set_select_mode(p_list, LIST_SINGLE_SELECT);
    list_set_item_rstyle(p_list, &list_item_style);
    list_set_count(p_list, 4, FACTORY_SETTING_LIST_PAGE);
    list_set_columns(p_list, FACTORY_SETTING_COLUMNS, TRUE);
    list_set_field_count(p_list, FACTORY_LIST_FILED, FACTORY_SETTING_LIST_PAGE);
    for(i = 0; i < FACTORY_LIST_FILED; ++i)
    {
      list_set_field_attr(p_list, 
        i, (u32)factory_list_field_attr[i].attr,(u16)factory_list_field_attr[i].width,
       (u16)factory_list_field_attr[i].left, (u16)factory_list_field_attr[i].top);
      list_set_field_rect_style(p_list, i, factory_list_field_attr[i].rstyle);
      list_set_field_font_style(p_list, i, factory_list_field_attr[i].fstyle);
    }
    list_set_update(p_list, factory_setting_list_update, 0);
    list_set_focus_pos(p_list, 0);
    factory_setting_list_update(p_list, list_get_valid_pos(p_list), FACTORY_SETTING_LIST_PAGE, 0);
    p_ctrl = ctrl_create_ctrl(CTRL_CONT,
                        IDC_FACTORY_DOWN_MENU_BG, 
                          FACTORY_DOWN_MENU_BG_X,
                          FACTORY_DOWN_MENU_BG_Y, 
                          FACTORY_DOWN_MENU_BG_W, 
                          FACTORY_DOWN_MENU_BG_H, 
                          p_cont, 0);
    ctrl_set_keymap(p_ctrl, factory_down_bg_keymap);
    ctrl_set_proc(p_ctrl, factory_down_bg_proc);
    ctrl_set_rstyle(p_ctrl, RSI_IGNORE, RSI_IGNORE, RSI_IGNORE);
    factory_mode_create_child(p_ctrl, 0);
    ctrl_default_proc(p_list, MSG_GETFOCUS, 0, 0);
    ctrl_paint_ctrl(p_cont, FALSE);
	on_factory_up_or_down_switch_focus(p_list, 0, 0, 0);
    fw_tmr_create(ROOT_ID_FACTORY_MODE, MSG_SLEEP_TMROUT, 1000, TRUE);
  client_cas_select(g_p_client,"select "CLT_CAS_SN_DATA","CLT_CAS_CARD_VER_DATA","
    CLT_CAS_MANU_NAME" from "CLT_CAS_CARD_INFO_TAB,"%c:32,%c:32,%c:32");

  return SUCCESS;
}

static RET_CODE on_factory_ca_info_update(control_t *p_ctrol, u16 msg, u32 para1, u32 para2)
{
  u8 asc_str[64] = {0};
  control_t *p_container, *p_card_ver, *p_card_num, *p_card_prov;
  p_container = ctrl_get_child_by_id(p_ctrol,IDC_FACTORY_DOWN_MENU_BG);
  MT_ASSERT(p_container != NULL);
  p_card_ver = ctrl_get_child_by_id(p_container , IDC_BASIC_CA_VER);
  p_card_num = ctrl_get_child_by_id(p_container,IDC_BASIC_CARD_NO);
  p_card_prov = ctrl_get_child_by_id(p_container, IDC_BASIC_CA_PROVIDER);
  MT_ASSERT(p_card_ver !=NULL && p_card_num != NULL && p_card_prov != NULL);
  if((void *)para1 == NULL)
  {
    fw_tmr_reset(ROOT_ID_FACTORY_MODE, MSG_UPDATE_MODE, 500);
  }
  
  if((void *)para1 != NULL)
  {
    fw_tmr_destroy(ROOT_ID_FACTORY_MODE, MSG_UPDATE_MODE);

    memset(&card_info,0,sizeof(ca_card_info_t));
    CLT_CAS_GET_DATA_FUNC((void *)para1, &card_info, sizeof(ca_card_info_t));
    mtos_printk("sn=>%s manu_name =%s ca_ver = %s\n",
      card_info.sn,card_info.cas_manu_name,card_info.card_ver);
  }
  if(strlen(card_info.cas_manu_name) == 0)
  {
      sprintf((char *)asc_str, "%s","YXSB");
  }else
  {
      sprintf((char *)asc_str, "%s",card_info.cas_manu_name);
  }
   on_factory_test_set_content_by_ascstr(p_card_prov,asc_str);

  sprintf((char *)asc_str, "%s",card_info.sn);
  on_factory_test_set_content_by_ascstr(p_card_num,asc_str);

  mtos_printk("VER = %d,%d, %d, %d \n",card_info.card_ver[3],
            card_info.card_ver[2], card_info.card_ver[1], card_info.card_ver[0]);
  sprintf((char *)asc_str, "0X%x%x%x%x",card_info.card_ver[3],
            card_info.card_ver[2], card_info.card_ver[1], card_info.card_ver[0]);
  on_factory_test_set_content_by_ascstr(p_card_ver,asc_str);
  ctrl_paint_ctrl(p_ctrol, TRUE);
  return SUCCESS;
}

static RET_CODE on_factory_usb_notify(control_t *p_ctrol, u16 msg, u32 para1, u32 para2)
{
  control_t *p_container, *p_usb_info;
  u8 asc_str[64] = {0};
  clt_factory_test_para_t para;
  BOOL ret =FALSE;
  p_container = ctrl_get_child_by_id(p_ctrol,IDC_FACTORY_DOWN_MENU_BG);
  p_usb_info = ctrl_get_child_by_id(p_container, IDC_BASIC_USB_INFO);
  switch (msg)
  {
      case MSG_PLUG_IN:
      {
	    #ifndef WIN32
        para.op = CLT_FT_USB_RW;
        ret = client_perform_factory_test(g_p_client,&para);
        if(ret)
        {
           usb_act_status = USB_CONNECT_AND_WRITE_SUCCESS;
           on_factory_test_static_set_rstyle(p_usb_info, RSI_GRAY, RSI_GRAY, RSI_GRAY);
           sprintf((char*)asc_str, "%s","Usb Test Ok");  
         }
         else
		#endif
        {
          usb_act_status = USB_CONNECT_BUT_WRITE_FAILED;
          on_factory_test_static_set_rstyle(p_usb_info, RSI_GRAY, RSI_GRAY, RSI_BLUE);
          sprintf((char*)asc_str, "%s","Usb Connect But Write Err !");
        }
          break;
        }
      case MSG_PLUG_OUT:
      {
           OS_PRINTF("usb not connect \n");
           usb_act_status = USB_NOT_CONNECT;
           on_factory_test_static_set_rstyle(p_usb_info, RSI_GRAY, RSI_GRAY, RSI_BLUE);
           sprintf((char*)asc_str, "%s","Usb Not Connect !");
          break;
      }
      default:
          break;
  }
  on_factory_test_set_content_by_ascstr(p_usb_info,asc_str);
  ctrl_paint_ctrl(p_usb_info,TRUE);
  return SUCCESS;
}

static RET_CODE on_factory_signal_update(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  control_t	*down_back, *p_play_sig_bar,*p_play_sig_bar1,*p_scan_sig_bar, *p_scan_sig_bar1;
  clt_signal_data_t *data = (clt_signal_data_t *)(para1);
  down_back = ctrl_get_child_by_id(p_ctrl, IDC_FACTORY_DOWN_MENU_BG);
  MT_ASSERT(down_back != NULL);
  p_play_sig_bar = ctrl_get_child_by_id(down_back,IDC_DISPLAY_SING_BAR);
  p_play_sig_bar1 = ctrl_get_child_by_id(down_back,IDC_DISPLAY_SING_BAR1);
  p_scan_sig_bar = ctrl_get_child_by_id(down_back,IDC_SCAN_SEARCH_SING_BAR);
  p_scan_sig_bar1 = ctrl_get_child_by_id(down_back,IDC_SCAN_SEARCH_SING_BAR1);
  on_factory_test_tp_bar_update(p_play_sig_bar, data->intensity, TRUE, (u8*)"dBuv");
  on_factory_test_tp_bar_update(p_play_sig_bar1, data->quality, TRUE, (u8*)"dB");
  on_factory_test_tp_bar_update(p_scan_sig_bar, data->intensity, TRUE, (u8*)"dBuv");
  on_factory_test_tp_bar_update(p_scan_sig_bar1, data->quality, TRUE, (u8*)"dB");
  ctrl_paint_ctrl(p_play_sig_bar, TRUE);
  ctrl_paint_ctrl(p_play_sig_bar1, TRUE);
  ctrl_paint_ctrl(p_scan_sig_bar, TRUE);
  ctrl_paint_ctrl(p_scan_sig_bar1, TRUE);
  return SUCCESS;
}

static void factory_update_progress(control_t *cont, u16 progress)
{
  control_t *p_container = ctrl_get_child_by_id(cont, IDC_FACTORY_DOWN_MENU_BG);
  control_t *bar = ctrl_get_child_by_id(p_container, IDC_SCAN_SEARCH_PROGRESS);
  if(ctrl_get_sts(bar) == OBJ_STS_HIDE)
  {
	ctrl_set_sts(bar, OBJ_STS_SHOW);
  }
  if(ctrl_get_sts(bar) != OBJ_STS_HIDE)
  {
    OS_PRINTF("UPDATE PROGRESS -> %d\n", progress);
    on_factory_test_bar_update(bar, progress, TRUE);
    on_factory_test_bar_paint(bar, TRUE);
  }
}

static RET_CODE on_factory_update_progress(control_t *cont, u16 msg,
                                   u32 para1, u32 para2)
{
  OS_PRINTF("PROCESS -> update progress\n");
  factory_update_progress(cont, (u16)para1);
  return SUCCESS;
}

void _add_prog_to_list(control_t *cont, clt_scan_pg_info_t *p_pg_info)
{
	u8 idx = 0;
   clt_scan_pg_info_t pg;
	memcpy(&pg, p_pg_info, sizeof(clt_scan_pg_info_t));
	idx = pg.video_pid != 0 ? 0 /* tv */ : 1 /* radio */;
	prog_curn[idx] ++;
	return ;
}

static RET_CODE on_factory_pg_found(control_t *cont, u16 msg,
                            u32 para1, u32 para2)
{
  OS_PRINTF("PROCESS -> add prog\n");
  _add_prog_to_list(cont, (clt_scan_pg_info_t *)para1);
  return SUCCESS;
}

static void _get_finish_str(u16 *str, u16 max_len)
{
  u16 uni_str[10], len;

  if(prog_curn[0] > 0
    || prog_curn[1] > 0)
  {
    len = 0, str[0] = '\0';
    gui_get_string(IDS_MSG_SEARCH_IS_END, str, max_len);

    convert_i_to_dec_str(uni_str, prog_curn[0]);
    ui_uni_strcat(str, uni_str, max_len);

    len = (u16)ui_uni_strlen(str);
    gui_get_string(IDS_MSG_N_TV, &str[len], (u16)(max_len - len));

    convert_i_to_dec_str(uni_str, prog_curn[1]);
    ui_uni_strcat(str, uni_str, max_len);

    len = (u16)ui_uni_strlen(str);
    gui_get_string(IDS_MSG_N_RADIO, &str[len], (u16)(max_len - len));
  }
  else
  {
    gui_get_string(IDS_MSG_NO_PROG_FOUND, str, max_len);
  }
}

static void _process_finish(void)
{
  comm_dlg_data_t dlg_data =
  {
    ROOT_ID_INVALID,
    DLG_FOR_CONFIRM | DLG_STR_MODE_EXTSTR,
    COMM_DLG_X, COMM_DLG_Y,
    COMM_DLG_W, COMM_DLG_H,
    0,
    30000,
  };

  u16 content[64 + 1];
  client_stop_scan(g_p_client);

  _get_finish_str(content, 64);
  dlg_data.content = (u32)content;
  ui_comm_dlg_open(&dlg_data);
}

static RET_CODE on_factory_finished(control_t *cont, u16 msg,
                            u32 para1, u32 para2)
{  
	_process_finish();
	return SUCCESS;
}

static RET_CODE power_led_light_off(control_t *cont, u16 msg,
                            u32 para1, u32 para2)
{
	clt_set_led led_setting;
	static BOOL light_flag = FALSE;
	if(!light_flag)
	{
		#ifndef WIN32
		led_setting.is_onoff = TRUE;
       led_setting.led_type = CLT_LED_LOCK;
       client_set_led_onoff(g_p_client, &led_setting);
		led_setting.is_onoff = FALSE;
       led_setting.led_type = CLT_LED_POWER;
       client_set_led_onoff(g_p_client, &led_setting);
		#endif
		light_flag = TRUE;
	}
	else
	{
		#ifndef WIN32
		led_setting.is_onoff = TRUE;
       led_setting.led_type = CLT_LED_POWER;
       client_set_led_onoff(g_p_client, &led_setting);
		led_setting.is_onoff = FALSE;
       led_setting.led_type = CLT_LED_LOCK;
       client_set_led_onoff(g_p_client, &led_setting);		
       #endif
		light_flag = FALSE;
	}
	return SUCCESS;
}

static RET_CODE on_exit_factory(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  clt_set_led led_setting;
  fw_tmr_destroy(ROOT_ID_FACTORY_MODE, MSG_HEART_BEAT);
  fw_tmr_destroy(ROOT_ID_FACTORY_MODE, MSG_UPDATE_MODE);

  #ifndef WIN32
  led_setting.is_onoff = TRUE;
  led_setting.led_type = CLT_LED_POWER;
  client_set_led_onoff(g_p_client, &led_setting);
  led_setting.is_onoff = FALSE;
  led_setting.led_type = CLT_LED_LOCK;
  client_set_led_onoff(g_p_client, &led_setting);		
  #endif
  client_channel_stop_play(g_p_client);
  client_setting_restore_factory(g_p_client);

  if(fw_find_root_by_id(ROOT_ID_INFOR_BAR)!=NULL)
  {
    fw_destroy_mainwin_by_id(ROOT_ID_INFOR_BAR);
  }
  manage_close_menu(ROOT_ID_FACTORY_MODE,0,0);

  ui_pop_msg_refresh_msg();

   return SUCCESS;
}

static RET_CODE on_factory_up_or_down_switch_focus(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
   control_t  *p_container,*parent, *ctrl_bg,* ctrl_list, *p_ctrl_tmp; //*p_down_container;
    u16 ctrl_id, cur_focus_pos , k = 0;
    ctrl_id = ctrl_get_ctrl_id(p_ctrl);
   parent = fw_find_root_by_id(ROOT_ID_FACTORY_MODE); 
   p_container = ctrl_get_child_by_id(parent, IDC_FACTORY_UP_CONTAINER);
    MT_ASSERT(parent!=NULL);
    switch(ctrl_id)
    {
      case IDC_FACTORY_UP_CONTAINER_LIST:
         ctrl_list = ctrl_get_child_by_id(p_container, IDC_FACTORY_UP_CONTAINER_LIST);
         cur_focus_pos = list_get_focus_pos(ctrl_list);

         ctrl_bg = ctrl_get_child_by_id(parent,IDC_FACTORY_DOWN_MENU_BG);
         for(k = 0; k < list_child_item[cur_focus_pos].child_total; ++k)
         {
            if(list_child_item[cur_focus_pos].child_style[k].is_relate == TRUE)
            {
                p_ctrl_tmp = ctrl_get_child_by_id(ctrl_bg, list_child_item[cur_focus_pos].child_style[k].id);
                if(p_ctrl!= NULL&& ctrl_get_sts(p_ctrl) == OBJ_STS_SHOW)
                {
                    list_select(p_ctrl);
                    ctrl_process_msg(p_ctrl, MSG_LOSTFOCUS, 0, 0);
                   ctrl_default_proc(p_ctrl_tmp,MSG_GETFOCUS, 0, 0);

                   break;   
                }
            }
         }
        OS_PRINTF("signal form IDC_FACTORY_UP_CONTAINER_LIST \n");
        break;
      case  IDC_FACTORY_DOWN_MENU_BG:
        ctrl_list = ctrl_get_child_by_id(p_container, IDC_FACTORY_UP_CONTAINER_LIST);
        MT_ASSERT(ctrl_list!= NULL);
        cur_focus_pos = list_get_focus_pos(ctrl_list);
        p_ctrl_tmp = ctrl_get_active_ctrl(p_ctrl);
        
        if((ctrl_list != NULL) && (p_ctrl_tmp != NULL) )
        {
          ctrl_process_msg(p_ctrl_tmp, MSG_LOSTFOCUS, 0, 0);
          list_unselect_item(ctrl_list, list_get_focus_pos(ctrl_list));
          ctrl_process_msg(ctrl_list, MSG_GETFOCUS, 0, 0);
          break;
        }
        break;
      default:
        OS_PRINTF("hava no signal ----\n");
        break;
    }
	ctrl_paint_ctrl(parent, TRUE);
    return SUCCESS;
}

void set_prog_name(int pg_pos)
{
  control_t *p_ctrol,*p_down,*p_pg_name;
  char sel_str[128];
  char fmt_str[64];
  BOOL ret = FALSE;
  u16 pgname[CLT_MAX_PROG_NAME_LENGTH];
  memset(pgname, 0, sizeof(pgname));
  p_ctrol = fw_find_root_by_id(ROOT_ID_FACTORY_MODE);
  p_down = ctrl_get_child_by_id(p_ctrol, IDC_FACTORY_DOWN_MENU_BG);
  p_pg_name = ctrl_get_child_by_id(p_down, IDC_DISPLAY_TV_SELECT);
  snprintf(sel_str, sizeof(sel_str), "select name from curn_view where limit 1 pos %d", pg_pos);
  snprintf(fmt_str, sizeof(fmt_str), "%%s:%d", CLT_MAX_PROG_NAME_LENGTH);
  ret = client_dbase_select(g_p_client, sel_str, fmt_str, pgname);
  if(ret == TRUE)
  on_factory_test_set_content_by_unistr(p_pg_name, pgname);
  ctrl_paint_ctrl(p_pg_name,TRUE);

}

void factory_scan_set_info(void)
{
	control_t *p_base, *p_cton, *p_ctrl;
	u32 freq, sym, qam;
    clt_nim_info_t para;

	p_cton = fw_find_root_by_id(ROOT_ID_FACTORY_MODE);
	p_ctrl = ctrl_get_child_by_id(p_cton, IDC_FACTORY_DOWN_MENU_BG);
	p_base = ctrl_get_child_by_id(p_ctrl, IDC_SCAN_FREQ_INFO);
	freq = on_factory_test_numedit_get_num(p_base);
	
	p_base = ctrl_get_child_by_id(p_ctrl, IDC_SCAN_SYMB_INFO);
	sym = on_factory_test_numedit_get_num(p_base);
	p_base = ctrl_get_child_by_id(p_ctrl, IDC_SCAN_QAM_INFO);
	qam = (u8)on_factory_test_select_get_focus(p_base);
	para.lockc.tp_freq = freq;
	para.lockc.tp_sym = sym;
	switch (qam)
	{
		case 0:
			para.lockc.nim_modulate  = CLT_NIM_MODULA_QAM16;
			break;
		case 1:
			para.lockc.nim_modulate  = CLT_NIM_MODULA_QAM32;
			break;
		case 2:
			para.lockc.nim_modulate  = CLT_NIM_MODULA_QAM64;
			break;
		case 3:
			para.lockc.nim_modulate  = CLT_NIM_MODULA_QAM128;
			break;
		case 4:
			para.lockc.nim_modulate  = CLT_NIM_MODULA_QAM256;
			break;
		default:
		break;
	}
 para.lock_mode = CLT_SYS_DVBC;

 client_set_signal_tp(g_p_client, &para);
	
}

static RET_CODE factory_list_move_update(control_t *p_list, u16 new_focus, u16 old_focus)
{
  control_t *p_bg, *p_maninwin, *p_temp;
  u16 i = 0;
  p_maninwin= fw_find_root_by_id(ROOT_ID_FACTORY_MODE);
  p_bg = ctrl_get_child_by_id(p_maninwin, IDC_FACTORY_DOWN_MENU_BG);
  MT_ASSERT(p_bg!= NULL);
  for(i = 0;i < list_child_item[old_focus].child_total; i++)
  {
    p_temp = ctrl_get_child_by_id(p_bg, list_child_item[old_focus].child_style[i].id);
    if(p_temp != NULL)
    {
      ctrl_set_sts(p_temp, OBJ_STS_HIDE);
    }
    else
    {
      UI_PRINTF("system_setting_content_update : error ctrl 11!!!!!\n");
    }
  }
   for(i = 0;i < list_child_item[new_focus].child_total; i++)
  {
    p_temp = ctrl_get_child_by_id(p_bg, list_child_item[new_focus].child_style[i].id);
    if(p_temp != NULL && list_child_item[new_focus].child_style[i].id != IDC_SCAN_SEARCH_PROGRESS)
    {
      	ctrl_set_sts(p_temp, OBJ_STS_SHOW);
    }
    else
    {
      UI_PRINTF("system_setting_content_update : error ctrl 11!!!!!\n");
    }
  }
  ctrl_paint_ctrl(p_maninwin, TRUE);
  return SUCCESS;
}

static RET_CODE on_factory_setting_change_focus(control_t *p_down_back, u16 msg, u32 para1, u32 para2)
{
  u16 new_focus = 0, old_focus = 0;
  
  control_t *p_ctrol, *p_up_container, *p_list;
  int cur_pg_index = client_dbase_get_pos(g_p_client,"curn_view");
  UI_PRINTF("system setting change focus\n");
  msg = MSG_FOCUS_RIGHT;   

  on_factory_up_or_down_switch_focus(p_down_back, 0,0, 0);
  p_ctrol = fw_find_root_by_id(ROOT_ID_FACTORY_MODE);
  p_up_container = ctrl_get_child_by_id(p_ctrol, IDC_FACTORY_UP_CONTAINER);
  p_list = ctrl_get_child_by_id(p_up_container, IDC_FACTORY_UP_CONTAINER_LIST);
  old_focus = list_get_focus_pos(p_list);
  
  list_class_proc(p_list, msg, para1, para2);
  new_focus = list_get_focus_pos(p_list);
  if(new_focus == 1 || new_focus == 2)
  {
	if(new_focus == 1)
	{
		factory_play_pg = TRUE;
    	ui_play_prog_by_pos(cur_pg_index);
		set_prog_name(cur_pg_index);
	}
	else if(new_focus == 2)
	{		
		factory_scan_set_info();
	}
  }
  if(old_focus == 1)
  {
  	factory_play_pg = FALSE;
	client_channel_stop_play(g_p_client);
  }
  list_set_focus_pos(p_list, new_focus);
  factory_list_move_update(p_list, new_focus, old_focus);
  on_factory_up_or_down_switch_focus(p_list, 0, 0, 0);
  return SUCCESS;
}

static void ui_shift_prog(int cur_pg_index, s16 offset, BOOL is_play)
{
  s32 dividend = 0;
  int pg_count = 0;
  clt_dbase_get_count_para_t para = {{0}};

  strncpy(para.view_name, "curn_view", sizeof(para.view_name));
  pg_count =  client_dbase_get_count(g_p_client,&para);

  if(pg_count == 0)
  {
    return;
  }
  dividend = cur_pg_index + offset; 
  if(dividend >= pg_count)
  {
    if(cur_pg_index == (pg_count - 1))
    {
      cur_pg_index = 0;
    }
    else
    {
      cur_pg_index = pg_count - 1;
    }
  }
  else
  {
    if(dividend <= 0)
    {
      if(cur_pg_index == 0)
      {
        cur_pg_index = pg_count - 1;
      }
      else
      {
        cur_pg_index = 0;
      }
    }
    else
    {
      cur_pg_index = (u16)(dividend) % pg_count;
    }
  }
  
  if(is_play)
  {
    ui_play_prog_by_pos(cur_pg_index);
  }
}

RET_CODE on_factctory_change_prog(control_t *cont, u16 msg,
                            u32 para1, u32 para2)
{
	control_t *p_ctrol,*plist_container, *plist, *p_down, *p_volume, *volume_num, *p_key;
	u16 cur_pg_index = 0, focus = 0;
	p_ctrol = fw_find_root_by_id(ROOT_ID_FACTORY_MODE);
	plist_container = ctrl_get_child_by_id(p_ctrol, IDC_FACTORY_UP_CONTAINER);
	plist = ctrl_get_child_by_id(plist_container, IDC_FACTORY_UP_CONTAINER_LIST);
	p_down = ctrl_get_child_by_id(p_ctrol, IDC_FACTORY_DOWN_MENU_BG);
	p_volume = ctrl_get_child_by_id(p_down, IDC_DISPLAY_VOLUME) ;
	volume_num = ctrl_get_child_by_id(p_down, IDC_DISPLAY_VOLUME_TEXT1);
	focus = list_get_focus_pos(plist);
	cur_pg_index = client_dbase_get_pos(g_p_client,"curn_view");
	if(focus == 1)
	{
		switch (msg)
		{
			case MSG_FOCUS_UP:
			{
				ui_shift_prog(cur_pg_index, 1, TRUE);
				cur_pg_index = client_dbase_get_pos(g_p_client,"curn_view");
			   ui_play_prog_by_pos(cur_pg_index);
				set_prog_name(cur_pg_index);
				break;
			}
			case MSG_FOCUS_DOWN:
			{
				ui_shift_prog(cur_pg_index, -1, TRUE);;
				cur_pg_index = client_dbase_get_pos(g_p_client,"curn_view");
			   ui_play_prog_by_pos(cur_pg_index);
				set_prog_name(cur_pg_index);
				break;
			}
			case MSG_FOCUS_LEFT:
				if(global_volue> 0)
		      {
		        global_volue--;
		        client_setting_set_volume(g_p_client, global_volue);
		      }
				text_set_content_by_dec(volume_num,global_volue);
				pbar_set_current(p_volume, global_volue);
				ctrl_paint_ctrl(p_down, TRUE);
				break;
			case MSG_FOCUS_RIGHT:
			  if(global_volue < MAX_FACTORY_VOLUME)
		      {
		        global_volue++;		        
               client_setting_set_volume(g_p_client, global_volue);
		      }
			  pbar_set_current(p_volume, global_volue);
			  text_set_content_by_dec(volume_num,global_volue);
			  ctrl_paint_ctrl(p_down, TRUE);
			break;
			default:
				break;
		}
	}
	else if (focus == 3)
	{
		switch (msg)
		{
			case MSG_POWER_OFF:
				p_key = ctrl_get_child_by_id(p_down, IDC_PORT_KEY_POWER);
				ctrl_set_rstyle(p_key, RSI_BLUE, RSI_BLUE, RSI_BLUE);
				break;
			case MSG_FOCUS_LEFT:
				p_key = ctrl_get_child_by_id(p_down, IDC_PORT_KEY_LEFT);
				ctrl_set_rstyle(p_key, RSI_BLUE, RSI_BLUE, RSI_BLUE);
				break;
			case MSG_FOCUS_RIGHT:
				p_key = ctrl_get_child_by_id(p_down, IDC_PORT_KEY_RIGHT);
				MT_ASSERT(p_key != NULL);
				ctrl_set_rstyle(p_key, RSI_BLUE, RSI_BLUE, RSI_BLUE);
				break;
			case MSG_FOCUS_UP:
				p_key = ctrl_get_child_by_id(p_down, IDC_PORT_KEY_UP);
				ctrl_set_rstyle(p_key, RSI_BLUE, RSI_BLUE, RSI_BLUE);
				break;
			case MSG_FOCUS_DOWN:
				p_key = ctrl_get_child_by_id(p_down, IDC_PORT_KEY_DOWN);
				ctrl_set_rstyle(p_key, RSI_BLUE, RSI_BLUE, RSI_BLUE);
				break;
			default:
				break;
		}
			ctrl_paint_ctrl(p_down, TRUE);
	}
	cont_class_proc(cont,  msg,  para1,  para2);
	return SUCCESS;
}

static RET_CODE on_factory_search_change_freq_symbol(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  ui_comm_num_proc(p_ctrl, msg, para1, para2);
  factory_scan_set_info();
  return SUCCESS;
}

static RET_CODE on_factory_manual_search_change_qam(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  u16 f_index;
  f_index = cbox_static_get_focus(p_ctrl);
  switch(msg)
  {
	case MSG_INCREASE:
		if(f_index == 4)
		{
			cbox_static_set_focus(p_ctrl,0);
		       ctrl_paint_ctrl(p_ctrl, FALSE);
		}
		else
		{
			 cbox_class_proc(p_ctrl, msg, para1, para2);
		}
		break;
	case MSG_DECREASE:
		if(f_index == 0)
		{
			cbox_static_set_focus(p_ctrl,4);
		       ctrl_paint_ctrl(p_ctrl, FALSE);
		}
		else
		{
			cbox_class_proc(p_ctrl, msg, para1, para2);
		}
		break;
	default:
		break;
  }
   factory_scan_set_info();
  return SUCCESS;
}

void factory_start_auto_scan(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
	control_t *p_base;
	u32 freq, sym, qam;
   clt_scan_input_para_t para;
   memset(prog_curn, 0, sizeof(prog_curn));

	p_base = ctrl_get_child_by_id(p_ctrl, IDC_SCAN_FREQ_INFO);
	freq = on_factory_test_numedit_get_num(p_base);
	
	p_base = ctrl_get_child_by_id(p_ctrl, IDC_SCAN_SYMB_INFO);
	sym = on_factory_test_numedit_get_num(p_base);
	p_base = ctrl_get_child_by_id(p_ctrl, IDC_SCAN_QAM_INFO);
	qam = (u8)on_factory_test_select_get_focus(p_base);
	para.tp.freq = freq;
	para.tp.sym = sym;
	switch (qam)
	{
		case 0:
			para.tp.mod = CLT_NIM_MODULA_QAM16;
			break;
		case 1:
			para.tp.mod = CLT_NIM_MODULA_QAM32;
			break;
		case 2:
			para.tp.mod = CLT_NIM_MODULA_QAM64;
			break;
		case 3:
			para.tp.mod = CLT_NIM_MODULA_QAM128;
			break;
		case 4:
			para.tp.mod = CLT_NIM_MODULA_QAM256;
			break;
		default:
		break;
	}
  para.free_only= 0;
  para.scan_type = CLT_SCAN_TYPE_MANUAL;
  para.chan_type = CLT_SCAN_CHAN_RADIO; //Raja changed for radio only
  para.nit_type = CLT_SCAN_NIT_SCAN_WITHOUT;

  client_start_scan(g_p_client, (clt_scan_input_para_t*)&para);
	
}

static RET_CODE on_factory_select_msg_start_search(control_t *p_ctrl, u16 msg, u32 para1, u32 para2)
{
  factory_start_auto_scan(p_ctrl->p_parent, msg, para1, para2);
  return SUCCESS;
}

static RET_CODE on_factory_ca_info_accept_notify(control_t *p_block, u16 msg, u32 para1, u32 para2)
{
   u32 event_id = para1;
   switch(msg)
  {
    case MSG_CA_INIT_OK:
	#ifndef WIN32
      fw_tmr_create(ROOT_ID_FACTORY_MODE, MSG_UPDATE_MODE, 500, FALSE);
      client_cas_select(g_p_client,"select "CLT_CAS_SN_DATA","CLT_CAS_CARD_VER_DATA","
        CLT_CAS_MANU_NAME" from "CLT_CAS_CARD_INFO_TAB,"%c:32,%c:32,%c:32");
	#endif
      break;

    case MSG_CA_EVT_NOTIFY:
      if(event_id == CLT_CAS_S_ADPT_CARD_REMOVE || event_id == CLT_CAS_C_DETITLE_ALL_READED)
      {
	  	#ifndef WIN32
        client_cas_select(g_p_client,"select "CLT_CAS_SN_DATA","CLT_CAS_CARD_VER_DATA","
          CLT_CAS_MANU_NAME" from "CLT_CAS_CARD_INFO_TAB,"%c:32,%c:32,%c:32");
		#endif
      }
      break;
  }

  return SUCCESS;
}

static RET_CODE timer_out_get_ca_info(control_t *p_block, u16 msg, u32 para1, u32 para2)
{
  client_cas_select(g_p_client,"select "CLT_CAS_SN_DATA","CLT_CAS_CARD_VER_DATA","
    CLT_CAS_MANU_NAME" from "CLT_CAS_CARD_INFO_TAB,"%c:32,%c:32,%c:32");

  return SUCCESS;
}

BEGIN_KEYMAP(factory_setting_cont_keymap, NULL)
  ON_EVENT(V_KEY_EXIT, MSG_EXIT)
END_KEYMAP(factory_setting_cont_keymap, NULL)

BEGIN_MSGPROC(factory_setting_cont_proc, cont_class_proc)
  ON_COMMAND(MSG_CA_EVT_NOTIFY, on_factory_ca_info_accept_notify)
  ON_COMMAND(MSG_CA_INIT_OK, on_factory_ca_info_accept_notify)
  ON_COMMAND(MSG_CA_CARD_INFO, on_factory_ca_info_update)
  ON_COMMAND(MSG_PLUG_IN, on_factory_usb_notify)
  ON_COMMAND(MSG_PLUG_OUT, on_factory_usb_notify)  
  ON_COMMAND(MSG_SIGNAL_UPDATE, on_factory_signal_update)
  
  ON_COMMAND(MSG_SCAN_PROGRESS, on_factory_update_progress)
  ON_COMMAND(MSG_SCAN_PG_FOUND, on_factory_pg_found)
  ON_COMMAND(MSG_SCAN_FINISHED, on_factory_finished)
  ON_COMMAND(MSG_SLEEP_TMROUT, power_led_light_off)
  ON_COMMAND(MSG_UPDATE_MODE, timer_out_get_ca_info)
  ON_COMMAND(MSG_EXIT, on_exit_factory)
END_MSGPROC(factory_setting_cont_proc, cont_class_proc)

BEGIN_KEYMAP(factory_setting_list_keymap, NULL)
  ON_EVENT(V_KEY_EXIT, MSG_EXIT)
END_KEYMAP(factory_setting_list_keymap, NULL)

BEGIN_MSGPROC(factory_setting_list_proc, list_class_proc)
  ON_COMMAND(MSG_EXIT, on_exit_factory)
END_MSGPROC(factory_setting_list_proc, list_class_proc)

BEGIN_KEYMAP(factory_down_bg_keymap, NULL)
  ON_EVENT(V_KEY_UP, MSG_FOCUS_UP)
  ON_EVENT(V_KEY_DOWN, MSG_FOCUS_DOWN)
    ON_EVENT(V_KEY_MENU,MSG_SWITCH_MENU)
   
  ON_EVENT(V_KEY_LEFT, MSG_FOCUS_LEFT)
  ON_EVENT(V_KEY_RIGHT, MSG_FOCUS_RIGHT)
  ON_EVENT(V_KEY_POWER, MSG_POWER_OFF)
END_KEYMAP(factory_down_bg_keymap,NULL)

BEGIN_MSGPROC(factory_down_bg_proc, cont_class_proc)
  ON_COMMAND(MSG_SWITCH_MENU, on_factory_setting_change_focus)
  ON_COMMAND(MSG_FOCUS_UP, on_factctory_change_prog)
  ON_COMMAND(MSG_FOCUS_DOWN, on_factctory_change_prog)
  ON_COMMAND(MSG_FOCUS_RIGHT, on_factctory_change_prog)
  ON_COMMAND(MSG_FOCUS_LEFT, on_factctory_change_prog)
  ON_COMMAND(MSG_POWER_OFF, on_factctory_change_prog)
END_MSGPROC(factory_down_bg_proc, cont_class_proc)
//scan info 
BEGIN_MSGPROC(factory_search_num_edit_proc, ui_comm_num_proc)
  ON_COMMAND(MSG_NUMBER, on_factory_search_change_freq_symbol)
END_MSGPROC(factory_search_num_edit_proc, ui_comm_num_proc)

BEGIN_MSGPROC(factory_search_qam_proc, cbox_class_proc)
  ON_COMMAND(MSG_INCREASE, on_factory_manual_search_change_qam)
  ON_COMMAND(MSG_DECREASE, on_factory_manual_search_change_qam)
END_MSGPROC(factory_search_qam_proc, cbox_class_proc)

BEGIN_KEYMAP(factory_manual_search_select_keymap, NULL)
  ON_EVENT(V_KEY_OK, MSG_SELECT)
END_KEYMAP(factory_manual_search_select_keymap, NULL)

BEGIN_MSGPROC(factory_manual_search_select_proc, text_class_proc)
  ON_COMMAND(MSG_SELECT, on_factory_select_msg_start_search)
END_MSGPROC(factory_manual_search_select_proc, text_class_proc)

BEGIN_MSGPROC(factory_comm_basic_info_select_proc, cbox_class_proc)
END_MSGPROC(factory_comm_basic_info_select_proc, cbox_class_proc)






