/********************************************************************************************/
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/* Montage Proprietary and Confidential                                                     */
/* Copyright (c) 2014 Montage Technology Group Limited and its affiliated companies         */
/********************************************************************************************/
#include "ui_common.h"
#include "ui_menu_data.h"
#include "ui_ca_menu_data.h"


#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

extern RET_CODE open_background (u32 para1, u32 para2);
extern RET_CODE open_mainmenu (u32 para1, u32 para2);
extern RET_CODE open_mainmenu_1 (u32 para1, u32 para2);
extern RET_CODE open_auto_search (u32 para1, u32 para2);
extern RET_CODE open_manual_search (u32 para1, u32 para2);
extern RET_CODE open_range_search (u32 para1, u32 para2);
extern RET_CODE open_ota (u32 para1, u32 para2);
extern RET_CODE open_upgrade_by_usb (u32 para1, u32 para2);
extern RET_CODE open_do_search (u32 para1, u32 para2);
extern RET_CODE open_small_list (u32 para1, u32 para2);
extern RET_CODE open_infor_bar (u32 para1, u32 para2);
extern RET_CODE open_volume (u32 para1, u32 para2);
extern RET_CODE open_language_setting (u32 para1, u32 para2);
extern RET_CODE open_av_setting (u32 para1, u32 para2);
extern RET_CODE open_time_setting (u32 para1, u32 para2);
extern RET_CODE open_book_manage (u32 para1, u32 para2);
extern RET_CODE open_epg (u32 para1, u32 para2);
extern RET_CODE open_osd_setting (u32 para1, u32 para2);
extern RET_CODE open_freq_setting (u32 para1, u32 para2);
extern RET_CODE open_stb_info (u32 para1, u32 para2);
extern RET_CODE open_network_config (u32 para1, u32 para2);
extern RET_CODE open_channel_edit (u32 para1, u32 para2);
extern RET_CODE open_channel_infor (u32 para1, u32 para2);
extern RET_CODE open_category (u32 para1, u32 para2);
extern RET_CODE open_fav_list (u32 para1, u32 para2);
extern RET_CODE open_fav_edit (u32 para1, u32 para2);
extern RET_CODE open_parental_lock (u32 para1, u32 para2);
extern RET_CODE open_music (u32 para1, u32 para2);
extern RET_CODE open_music_fullscreen (u32 para1, u32 para2);
extern RET_CODE open_picture (u32 para1, u32 para2);
extern RET_CODE open_pic_play_setup (u32 para1, u32 para2);
extern RET_CODE open_picture_fullscreen (u32 para1, u32 para2);
extern RET_CODE open_movie (u32 para1, u32 para2);
extern RET_CODE open_movie_fullscreen (u32 para1, u32 para2);
extern RET_CODE open_text_encode (u32 para1, u32 para2);
extern RET_CODE open_record_manager (u32 para1, u32 para2);
extern RET_CODE open_record_play_fullscreen (u32 para1, u32 para2);
extern RET_CODE open_hdd_infor (u32 para1, u32 para2);
extern RET_CODE open_storage_format (u32 para1, u32 para2);
extern RET_CODE open_dvr_config (u32 para1, u32 para2);
extern RET_CODE open_lan_setting (u32 para1, u32 para2);
extern RET_CODE open_ping_test (u32 para1, u32 para2);
extern RET_CODE open_wifi_manager (u32 para1, u32 para2);
extern RET_CODE open_wifi_setup (u32 para1, u32 para2);
extern RET_CODE open_online_app (u32 para1, u32 para2);
extern RET_CODE open_youtube (u32 para1, u32 para2);
extern RET_CODE open_online_play (u32 para1, u32 para2);
extern RET_CODE open_yahoo_news (u32 para1, u32 para2);
extern RET_CODE open_google_map (u32 para1, u32 para2);
extern RET_CODE open_news_detail (u32 para1, u32 para2);
extern RET_CODE open_weather (u32 para1, u32 para2);
extern RET_CODE open_keyboard (u32 para1, u32 para2);
extern RET_CODE open_timeshift (u32 para1, u32 para2);
extern RET_CODE open_pvr (u32 para1, u32 para2);
extern RET_CODE open_factory_set (u32 para1, u32 para2);
extern RET_CODE open_volume_usb (u32 para1, u32 para2);
extern RET_CODE open_epg_detail_info (u32 para1, u32 para2);
extern RET_CODE open_num_play (u32 para1, u32 para2);
extern RET_CODE open_mac_addr (u32 para1, u32 para2);
extern RET_CODE open_ota_search (u32 para1, u32 para2);
extern RET_CODE open_pvr_rec_bar (u32 para1, u32 para2);
extern RET_CODE open_mute (u32 para1, u32 para2);
extern RET_CODE open_audio_setting(u32 para1, u32 para2);
extern RET_CODE open_game_othello(u32 para1, u32 para2);
extern RET_CODE open_game_tetris(u32 para1, u32 para2);
extern RET_CODE open_game(u32 para1, u32 para2);
extern RET_CODE open_notify(u32 para1, u32 para2);
extern RET_CODE open_game_pushbox(u32 para1, u32 para2);
extern RET_CODE open_game_snake(u32 para1, u32 para2);
extern RET_CODE open_video_goto(u32 para1, u32 para2);
extern RET_CODE open_subt_language(u32 para1, u32 para2);
extern RET_CODE open_dvbs_biss_qrcode(u32 para1, u32 para2);
extern RET_CODE open_dvbs_installation_list(u32 para1, u32 para2);
extern RET_CODE open_dvbs_sat_search(u32 para1, u32 para2);
extern RET_CODE open_dvbs_edit_sat(u32 para1, u32 para2);
extern RET_CODE open_dvbs_edit_tp(u32 para1, u32 para2);
extern RET_CODE open_dvbs_tp_search(u32 para1, u32 para2);
extern RET_CODE open_dvbs_motor_setup(u32 para1, u32 para2);
extern RET_CODE open_dvbs_goto_x(u32 para1, u32 para2);
extern RET_CODE open_dvbs_limit_setup(u32 para1, u32 para2);
extern RET_CODE open_dvbs_motor_antenna(u32 para1, u32 para2);
extern RET_CODE open_dump_db (u32 para1, u32 para2);


extern RET_CODE open_dvbt_mainmenu(u32 para1, u32 para2);
extern RET_CODE open_dvbt_channel_search(u32 para1, u32 para2);
extern RET_CODE open_dvbt_language_setting(u32 para1, u32 para2);
extern RET_CODE open_dvbt_manual_search(u32 para1, u32 para2);
extern RET_CODE open_dvbt_channel_list(u32 para1, u32 para2);
extern RET_CODE open_dvbt_av_setting(u32 para1, u32 para2);
extern RET_CODE open_dvbt_system_setting(u32 para1, u32 para2);
extern RET_CODE open_dvbt_time_setting(u32 para1, u32 para2);
extern RET_CODE open_dvbt_media_center(u32 para1, u32 para2);
extern RET_CODE open_dvbs_ca_bw(u32 para1, u32 para2);
extern RET_CODE open_dvbt_search_list (u32 para1, u32 para2);
extern RET_CODE open_installation_guide (u32 para1, u32 para2);
extern RET_CODE open_dvbt_cable_search (u32 para1, u32 para2);
extern RET_CODE open_dvbt_t2_ota (u32 para1, u32 para2);
extern RET_CODE open_dvbt_ota_list (u32 para1, u32 para2);
extern RET_CODE open_dvbt_ota_upgrade (u32 para1, u32 para2);
extern RET_CODE open_dvbt_t2_search (u32 para1, u32 para2);
extern RET_CODE open_dvbs_ota(u32 para1, u32 para2);
extern RET_CODE open_dvbs_ota_upgrade (u32 para1, u32 para2);
extern RET_CODE open_dvbs_ota_search (u32 para1, u32 para2);
extern RET_CODE open_dvbt_ts_record (u32 para1, u32 para2);
extern RET_CODE open_factory_mode_ui(u32 para1, u32 para2);
extern RET_CODE open_ts_record(u32 para1, u32 para2);
extern RET_CODE open_push_media(u32 para1, u32 para2);
extern RET_CODE open_pushvod_cate(u32 para1, u32 para2);
extern RET_CODE open_pushvod_pg(u32 para1, u32 para2);
extern RET_CODE open_pushvod_pg_detail(u32 para1, u32 para2);
extern RET_CODE open_pushvod_hot_pg(u32 para1, u32 para2);

//extern RET_CODE open_dvbt_network(u32 para1, u32 para2);

static menu_attr_t all_menu_attr[] =
{
//  root_id,            play_state,   auto_close, signal_msg,   open_proc
//PS_PLAY
 {ROOT_ID_BACKGROUND, PS_PLAY, ON, SM_OFF, NULL},
 {ROOT_ID_MAINMENU, PS_PLAY, OFF, SM_OFF, open_mainmenu_1},
 {ROOT_ID_CHANNEL_EDIT, PS_PLAY, OFF, SM_OFF, open_channel_edit},
 {ROOT_ID_CHANNEL_INFOR, PS_PLAY, OFF, SM_OFF, open_channel_infor},
 {ROOT_ID_SMALL_LIST, PS_PLAY, OFF, SM_BAR, open_small_list},
 {ROOT_ID_INFOR_BAR, PS_PLAY, OFF, SM_BAR, open_infor_bar},
 {ROOT_ID_VOLUME, PS_PLAY, ON, SM_OFF, open_volume},
 {ROOT_ID_EPG, PS_PLAY, OFF, SM_OFF, open_epg},
 {ROOT_ID_FAV_EDIT, PS_PLAY, OFF, SM_OFF, open_fav_edit},
 {ROOT_ID_BOOK_MANAGE, PS_PLAY, OFF, SM_OFF, open_book_manage},
 {ROOT_ID_FAV_LIST, PS_PLAY, OFF, SM_OFF, open_fav_list},
 {ROOT_ID_PARENTAL_LOCK, PS_PLAY, OFF, SM_OFF, open_parental_lock},
 {ROOT_ID_EPG_DETAIL_INFO, PS_PLAY, OFF, SM_OFF, open_epg_detail_info},
 {ROOT_ID_CATEGORY, PS_PLAY, OFF, SM_OFF, open_category},
 {ROOT_ID_NUM_PLAY, PS_PLAY, OFF, SM_OFF, open_num_play},
 {ROOT_ID_PVR_REC_BAR, PS_PLAY, OFF, SM_OFF, open_pvr_rec_bar},

//PS_STOP
 {ROOT_ID_AUTO_SEARCH, PS_STOP, OFF, SM_BAR, open_auto_search},
 {ROOT_ID_MANUAL_SEARCH, PS_STOP, OFF, SM_BAR, open_manual_search},
 {ROOT_ID_RANGE_SEARCH, PS_STOP, OFF, SM_BAR, open_range_search},
 {ROOT_ID_OTA, PS_STOP, OFF, SM_BAR, open_ota},
 {ROOT_ID_UPGRADE_BY_USB, PS_STOP, OFF, SM_OFF, open_upgrade_by_usb},
 {ROOT_ID_DUMP_DB, PS_STOP, OFF, SM_OFF, open_dump_db},
 {ROOT_ID_DO_SEARCH, PS_STOP, OFF, SM_OFF, open_do_search},
 {ROOT_ID_LANGUAGE_SETTING, PS_STOP, OFF, SM_OFF, open_language_setting},
 {ROOT_ID_AV_SETTING, PS_STOP, OFF, SM_OFF, open_av_setting},
 {ROOT_ID_TIME_SETTING, PS_STOP, OFF, SM_OFF, open_time_setting},
 {ROOT_ID_OSD_SETTING, PS_STOP, OFF, SM_OFF, open_osd_setting},
 {ROOT_ID_MUSIC, PS_STOP, OFF, SM_OFF, open_music},
 {ROOT_ID_MUSIC_FULLSCREEN, PS_STOP, OFF, SM_OFF, open_music_fullscreen},
 {ROOT_ID_PICTURE, PS_STOP, OFF, SM_OFF, open_picture},
 {ROOT_ID_PIC_PLAY_SETUP, PS_STOP, OFF, SM_OFF, open_pic_play_setup},
 {ROOT_ID_PICTURE_FULLSCREEN, PS_STOP, OFF, SM_OFF, open_picture_fullscreen},
 {ROOT_ID_MOVIE, PS_STOP, OFF, SM_OFF, open_movie},
 {ROOT_ID_MOVIE_FULLSCREEN, PS_STOP, OFF, SM_OFF, open_movie_fullscreen},
 {ROOT_ID_TEXT_ENCODE, PS_STOP, ON, SM_OFF, open_text_encode},
 {ROOT_ID_LAN_SETTING, PS_STOP, OFF, SM_OFF, open_lan_setting},
 {ROOT_ID_PING_TEST, PS_STOP, OFF, SM_OFF, open_ping_test},
 {ROOT_ID_WIFI_MANAGER, PS_STOP, OFF, SM_OFF, open_wifi_manager},
 {ROOT_ID_WIFI_SETUP, PS_STOP, OFF, SM_OFF, open_wifi_setup},
 {ROOT_ID_FREQ_SETTING, PS_STOP, OFF, SM_OFF, open_freq_setting},
 {ROOT_ID_STB_INFO, PS_STOP, OFF, SM_OFF, open_stb_info},
 {ROOT_ID_NETWORK_CONFIG, PS_STOP, OFF, SM_OFF, open_network_config},
 {ROOT_ID_GOOGLE_MAP, PS_STOP, OFF, SM_OFF, open_google_map},
 {ROOT_ID_YAHOO_NEWS, PS_STOP, OFF, SM_OFF, open_yahoo_news},
 {ROOT_ID_NEWS_DETAIL, PS_STOP, OFF, SM_OFF, open_news_detail},
 {ROOT_ID_WEATHER, PS_STOP, OFF, SM_OFF, open_weather},
 {ROOT_ID_OTA_SEARCH, PS_STOP, OFF, SM_OFF, open_ota_search},
 {ROOT_ID_GAME, PS_STOP, OFF, SM_OFF, open_game},
 {ROOT_ID_TETRIS, PS_STOP, OFF, SM_OFF, open_game_tetris}, 
 {ROOT_ID_OTHELLO, PS_STOP, OFF, SM_OFF, open_game_othello}, 
 {ROOT_ID_GAME_PUSHBOX, PS_STOP, OFF, SM_OFF, open_game_pushbox}, 
 {ROOT_ID_GAME_SNAKE, PS_STOP, OFF, SM_OFF, open_game_snake}, 
 {ROOT_ID_ONLINE_APP, PS_STOP, OFF, SM_OFF, open_online_app},
 {ROOT_ID_MAC_ADDR, PS_STOP, OFF, SM_OFF, open_mac_addr},
 {ROOT_ID_FACTORY_SET, PS_STOP, OFF, SM_OFF, open_factory_set},
 {ROOT_ID_PVR, PS_STOP, OFF, SM_OFF, open_pvr},
 {ROOT_ID_DVBS_OTA, PS_STOP, OFF, SM_BAR, open_dvbs_ota},
 {ROOT_ID_DVBS_OTA_SEARCH, PS_STOP, OFF, SM_BAR, open_dvbs_ota_search},
 {ROOT_ID_DVBS_OTA_UPGRADE, PS_STOP, OFF, SM_BAR, open_dvbs_ota_upgrade},

//PS_KEEP
 {ROOT_ID_KEYBOARD, PS_KEEP, OFF, SM_OFF, open_keyboard},
 {ROOT_ID_HDD_INFOR, PS_KEEP, OFF, SM_OFF, open_hdd_infor},
 {ROOT_ID_STORAGE_FORMAT, PS_KEEP, OFF, SM_OFF, open_storage_format},
 {ROOT_ID_DVR_CONFIG, PS_KEEP, OFF, SM_OFF, open_dvr_config},
 {ROOT_ID_AUDIO_SETTING, PS_PLAY, OFF, SM_OFF, open_audio_setting}, 
 {ROOT_ID_TIMESHIFT, PS_KEEP, OFF, SM_OFF, open_timeshift},
 {ROOT_ID_VOLUME_USB, PS_KEEP, OFF, SM_OFF, open_volume_usb},
 {ROOT_ID_VIDEO_GOTO, PS_KEEP, OFF, SM_OFF, open_video_goto},
 {ROOT_ID_FACTORY_MODE, PS_STOP, OFF, SM_OFF, open_factory_mode_ui},
 {ROOT_ID_TS_RECORD, PS_STOP, OFF, SM_OFF, open_ts_record},
#ifdef PUSHVOD_SUPPORT
 {ROOT_ID_PUSH_MEDIA, PS_KEEP, OFF, SM_OFF, open_push_media},
 {ROOT_ID_PUSHVOD_CATE, PS_KEEP, OFF, SM_OFF, open_pushvod_cate},
 {ROOT_ID_PUSHVOD_PG, PS_KEEP, OFF, SM_OFF, open_pushvod_pg},
 {ROOT_ID_PUSHVOD_PG_DETAIL, PS_KEEP, OFF, SM_OFF, open_pushvod_pg_detail},
 {ROOT_ID_PUSHVOD_HOT_PG, PS_KEEP, OFF, SM_OFF, open_pushvod_hot_pg},
#endif
//PS_TS
 {ROOT_ID_RECORD_MANAGER, PS_TS, OFF, SM_OFF, open_record_manager},
 {ROOT_ID_RECORD_PLAY_FULLSCREEN, PS_TS, OFF, SM_OFF, open_record_play_fullscreen},
 {ROOT_ID_ONLINE_PLAY, PS_KEEP, OFF, SM_OFF, open_online_play},
 {ROOT_ID_YOUTUBE, PS_KEEP, OFF, SM_OFF, open_youtube},
  {ROOT_ID_SUBT_LANGUAGE, PS_KEEP, OFF, SM_OFF, open_subt_language},

//PS_PREV 
 {ROOT_ID_NOTIFY, PS_PREV, OFF, SM_OFF, open_notify}, 

 //s-scan
 {ROOT_ID_DVBS_INSTALLATION_LIST, PS_STOP, OFF, SM_BAR, open_dvbs_installation_list},
 {ROOT_ID_DVBS_SAT_SEARCH, PS_KEEP, OFF, SM_BAR, open_dvbs_sat_search},
 {ROOT_ID_DVBS_EDIT_SAT, PS_KEEP, OFF, SM_OFF, open_dvbs_edit_sat},
 {ROOT_ID_DVBS_EDIT_TP, PS_KEEP, OFF, SM_OFF, open_dvbs_edit_tp},
 {ROOT_ID_DVBS_TP_SEARCH, PS_STOP, OFF, SM_OFF, open_dvbs_tp_search},
 {ROOT_ID_DVBS_MOTOR_SETUP, PS_KEEP, OFF, SM_OFF, open_dvbs_motor_setup},
 {ROOT_ID_DVBS_GOTO_X, PS_STOP, OFF, SM_OFF, open_dvbs_goto_x},
 {ROOT_ID_DVBS_LIMIT_SETUP, PS_STOP, OFF, SM_OFF, open_dvbs_limit_setup},
 {ROOT_ID_DVBS_MOTOR_ANTENNA, PS_STOP, OFF, SM_OFF, open_dvbs_motor_antenna},
 {ROOT_ID_DVBS_CA_BW, PS_KEEP, OFF, SM_OFF, open_dvbs_ca_bw},
 {ROOT_ID_DVBS_BISS_QRCODE, PS_KEEP, OFF, SM_OFF, open_dvbs_biss_qrcode},
 
 //t-menu
 {ROOT_ID_DVBT_MAINMENU, PS_STOP, OFF, SM_OFF, open_dvbt_mainmenu},
 {ROOT_ID_DVBT_CHANNEL_SEARCH, PS_STOP, OFF, SM_OFF, open_dvbt_channel_search},
 {ROOT_ID_DVBT_LANGUAGE_SETTING, PS_STOP, OFF, SM_OFF, open_dvbt_language_setting},
 {ROOT_ID_DVBT_MANUAL_SEARCH, PS_STOP, OFF, SM_BAR, open_dvbt_manual_search},
 {ROOT_ID_DVBT_CHANNEL_LIST, PS_STOP, OFF, SM_OFF, open_dvbt_channel_list},
 {ROOT_ID_DVBT_AV_SETTING, PS_STOP, OFF, SM_OFF, open_dvbt_av_setting},
 {ROOT_ID_DVBT_TIME_SETTING, PS_STOP, OFF, SM_OFF, open_dvbt_time_setting},
 {ROOT_ID_DVBT_SYSTEM_SETTING, PS_STOP, OFF, SM_OFF, open_dvbt_system_setting},  
 {ROOT_ID_DVBT_MEDIA_CENTER, PS_STOP, OFF, SM_OFF, open_dvbt_media_center},
 {ROOT_ID_DVBT_SEARCH_LIST, PS_STOP, OFF, SM_OFF, open_dvbt_search_list}, 
 {ROOT_ID_INSTALLATION_GUIDE, PS_STOP, OFF, SM_OFF, open_installation_guide}, 
 {ROOT_ID_DVBT_CABLE_SEARCH, PS_STOP, OFF, SM_OFF, open_dvbt_cable_search}, 
 {ROOT_ID_DVBT_T2_SEARCH, PS_STOP, OFF, SM_OFF, open_dvbt_t2_search},
 {ROOT_ID_DVBT_OTA_UPGRADE, PS_STOP, OFF, SM_OFF, open_dvbt_ota_upgrade}, 
 {ROOT_ID_DVBT_OTA_LIST, PS_STOP, OFF, SM_OFF, open_dvbt_ota_list}, 
 {ROOT_ID_DVBT_T2_OTA, PS_STOP, OFF, SM_BAR, open_dvbt_t2_ota}, 
 {ROOT_ID_DVBT_TS_RECORD, PS_STOP, OFF, SM_BAR, open_dvbt_ts_record}, 
 {ROOT_ID_MAINMENU_1, PS_STOP, OFF, SM_BAR, open_mainmenu}, 
};

static logo_attr_t const all_logo_attr[] =
{
	{0, 0}
};

preview_attr_t const all_preview_attr[] =
{
  {
    0, {0},
  },
};

u16 const fullscreen_root[] =
{
  ROOT_ID_BACKGROUND,
  ROOT_ID_VOLUME,
  ROOT_ID_VOLUME_USB,
  ROOT_ID_TIMESHIFT,
  ROOT_ID_INFOR_BAR,
  ROOT_ID_PVR_REC_BAR,
  ROOT_ID_MUTE,
  ROOT_ID_TIMESHIFT,
  ROOT_ID_RECORD_PLAY_FULLSCREEN,
  ROOT_ID_FACTORY_MODE,
  ROOT_ID_CA_ROLLING_PREVIEW,
};

u16 const popup_root[] =
{
  ROOT_ID_COMM_DLG,
  ROOT_ID_COMM_PWDLG,
};

//可以显示pwd对话框的菜单(can do play channel menu)
u16 const pwd_dlg_root[] =
{
  ROOT_ID_BACKGROUND,
  ROOT_ID_INFOR_BAR,
  ROOT_ID_EPG,
};



menu_attr_t *_ui_menu_data_api_get_menu_by_id(u16 root_id)
{
  u8          i         = 0;
  //menu_attr_t *p_menu   = NULL;
  //u16          menu_cnt = 0;

  OS_PRINTF("[%s] [%d] root_id [%d]\n",__FUNCTION__,__LINE__,root_id);
  for(i = 0; i < ARRAY_SIZE(all_menu_attr); i++)
  {
    if(all_menu_attr[i].root_id == root_id)
    {
      return &all_menu_attr[i];
    }
  }

  for(i = 0; i < g_ca_menu_attr_count; i++)
  {
    if(ca_menu_attr[i].root_id == root_id)
    {
      return &ca_menu_attr[i];
    }
  }

  return NULL;
}

void _ui_menu_data_api_set_menu_by_id(u16 root_id, menu_attr_t *menu_attr_tmp)
{
  u8          i         = 0;
  
  for(i = 0; i < ARRAY_SIZE(all_menu_attr); i++)
  {
    if(all_menu_attr[i].root_id == root_id)
    {
      memcpy(&all_menu_attr[i], menu_attr_tmp, sizeof(menu_attr_t));
      return;
    }
  }

  for(i = 0; i < g_ca_menu_attr_count; i++)
  {
    if(ca_menu_attr[i].root_id == root_id)
    {
      memcpy(&ca_menu_attr[i], menu_attr_tmp, sizeof(menu_attr_t));
      return;
    }
  }

  return ;
}

/*
 * ui_menu_data interface all_menu_attr
 */
play_sts ui_menu_data_api_get_play_sts(u16 root_id)
{
  play_sts     ret    = PS_KEEP;
  menu_attr_t *p_menu = NULL;

  p_menu = _ui_menu_data_api_get_menu_by_id(root_id);

  if(NULL == p_menu)
  {
    return PS_KEEP;
  }
  
  ret = p_menu->play_state;
  
  return ret;
}


u8 ui_menu_data_api_get_auto_close(u16 root_id)
{
  menu_attr_t *p_menu = NULL;

  p_menu = _ui_menu_data_api_get_menu_by_id(root_id);
  if(NULL == p_menu)
  {
    return OFF;
  }
  
  return p_menu->auto_close;
}

OPEN_MENU ui_menu_data_api_get_open_func(u16 root_id)
{
  menu_attr_t *p_menu = NULL;

  p_menu = _ui_menu_data_api_get_menu_by_id(root_id);
  if(NULL == p_menu)
  {
    return NULL;
  }
  
  return p_menu->open_func;
}


/*
 * ui_menu_data interface all_logo_attr
 */
u16 ui_menu_data_api_get_logo_id(u16 root_id)
{
  u16 i = 0;

  for(i = 0; i < ARRAY_SIZE(all_logo_attr); i++)
  {
    if(all_logo_attr[i].root_id == root_id)
    {
      return all_logo_attr[i].logo_id;
    }
  }
  
  return 0;
}


/*
 * ui_menu_data interface all_preview_attr
 */
BOOL ui_menu_data_api_is_pview_menu(u16 root_id)
{
  u8 i = 0;

  for(i = 0; i < ARRAY_SIZE(all_preview_attr); i++)
  {
    if(all_preview_attr[i].root_id == root_id)
    {
      return TRUE;
    }
  }
  return FALSE;
}

rect_t * ui_menu_data_api_get_pview_position(u16 root_id)
{
  u8 i = 0;
  rect_t *p_rect = NULL;

  for(i = 0; i < ARRAY_SIZE(all_preview_attr); i++)
  {
    if(all_preview_attr[i].root_id == root_id)
    {
      p_rect = (rect_t *)((void *)&all_preview_attr[i].position);
    }
  }
  return p_rect;
}

BOOL ui_menu_data_api_get_pview_rect(u16 root_id, u16 *left, u16 *top, u16 *width, u16 *height)
{
  u8 i = 0;

  for(i = 0; i < ARRAY_SIZE(all_preview_attr); i++)
  {
    if(all_preview_attr[i].root_id == root_id)
    {
      *left = all_preview_attr[i].position.left;
      *top = all_preview_attr[i].position.top;
      *width = all_preview_attr[i].position.right - all_preview_attr[i].position.left;
      *height = all_preview_attr[i].position.bottom - all_preview_attr[i].position.top;
      return TRUE;
    }
  }
  return FALSE;
}


//Can show mute /signal window at the same time
//Other windows can be opened
BOOL ui_menu_data_api_is_fscreen_menu(u16 root_id)
{
  u8 i = 0;

  for(i = 0; i < ARRAY_SIZE(fullscreen_root); i++)
  {
    if(fullscreen_root[i] == root_id)
    {
      return TRUE;
    }
  }

  for(i = 0; i < g_ca_fullscreen_root_count; i++)
  {
    if(ca_fullscreen_root[i] == root_id)
    {
      return TRUE;
    }
  }

  return FALSE;
}


BOOL ui_menu_data_api_is_popup_menu(u16 root_id)
{
  u8 i = 0;

  for(i = 0; i < ARRAY_SIZE(popup_root); i++)
  {
    if(popup_root[i] == root_id)
    {
      return TRUE;
    }
  }
  return FALSE;
}

BOOL ui_menu_data_api_is_pwd_dlg_menu(u16 root_id)
{
  u8 i = 0;

  for(i = 0; i < ARRAY_SIZE(pwd_dlg_root); i++)
  {
    if(pwd_dlg_root[i] == root_id)
    {
      return TRUE;
    }
  }
  return FALSE;
}

BOOL ui_menu_data_api_is_usb_pview_menu(u16 root_id)
{ 
  if(ROOT_ID_MUSIC == root_id || ROOT_ID_PICTURE== root_id)
  {
    return TRUE;
  }
  return FALSE;
}

BOOL ui_menu_data_api_is_video_menu(u16 root_id)
{ 
  if(ROOT_ID_ONLINE_PLAY == root_id)
  {
    return TRUE;
  }
  return FALSE;
}


u8 ui_menu_data_api_get_menu_type(u16 root_id)
{
  if(ui_menu_data_api_is_fscreen_menu(root_id))
  {
    return MENU_TYPE_FULLSCREEN;
  }
  else if(ui_menu_data_api_is_pview_menu(root_id))
  {
    return MENU_TYPE_PREVIEW;
  }
  else if(ui_menu_data_api_is_popup_menu(root_id))
  {
    return MENU_TYPE_POPUP;
  }
  else if(ui_menu_data_api_is_usb_pview_menu(root_id))
  {
    return MENU_TYPE_USB_PREV;
  }
  else if(ui_menu_data_api_is_video_menu(root_id))
  {
    return MENU_TYPE_VIDEO;
  }
  else
  {
    return MENU_TYPE_NORMAL;
  }
}

