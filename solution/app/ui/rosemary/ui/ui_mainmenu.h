/********************************************************************************************/
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/* Montage Proprietary and Confidential                                                     */
/* Copyright (c) 2017 Montage Technology Group Limited and its affiliated companies         */
/********************************************************************************************/
#ifndef __UI_MAINMENU_H__
#define __UI_MAINMENU_H__

enum mainmenu_control_id
{
  IDC_MAINMENU_INVALID = 0,
  IDC_MAINMENU_MAIN_TITLE = 2,
  IDC_MAINMENU_AD_WIN = 3,
  IDC_MAINMENU_MAINMENU_MBOX = 4,
  IDC_MAINMENU_ITEM_CONT = 5,
  IDC_MAINMENU_ARROW_UP = 7,
  IDC_MAINMENU_ARROW_DOWN = 0,
  IDC_MAINMENU_LINE_1 = 8,
  IDC_MAINMENU_LINE_2 = 9,
  IDC_MAINMENU_LINE_3 = 10,
  IDC_MAINMENU_LINE_4 = 11,
  IDC_MAINMENU_LINE_5 = 12,
  IDC_MAINMENU_LINE_6 = 13,
  IDC_MAINMENU_LINE_7 = 14,
  IDC_MAINMENU_CHANNEL_LIST = 15,
  IDC_MAINMENU_INSTALLATION_LIST = 16,
  IDC_MAINMENU_SYSTEM_LIST = 17,
  IDC_MAINMENU_TOOLS_LIST = 18,
  IDC_MAINMENU_MIDEACENTER_LIST = 19,
  IDC_MAINMENU_CA_LIST = 20,
  IDC_MAINMENU_NETWORK_LIST = 21,
  IDC_MAINMENU_MAINMENU_TITLE_TEXT = 6,
};
#define MAINMENU_PWD_DLG_FOR_CHK_X ((SCREEN_WIDTH + 300 + 90 - PWDLG_W)/2) //((SCREEN_WIDTH-PWDLG_W)/2)
#define MAINMENU_PWD_DLG_FOR_CHK_Y ((SCREEN_HEIGHT-PWDLG_H)/2 + 50)
#define PWD_DLG_FOR_CHK_X ((SCREEN_WIDTH + 300 + 90 - PWDLG_W)/2) //((SCREEN_WIDTH-PWDLG_W)/2)
#define PWD_DLG_FOR_CHK_Y ((SCREEN_HEIGHT-PWDLG_H)/2 + 50)
#define USB_NOTIFY_W  (COMM_DLG_W)
#define USB_NOTIFY_H  (COMM_DLG_H)
#define USB_NOTIFY_L  PWD_DLG_FOR_CHK_X

#define USB_NOTIFY_T  PWD_DLG_FOR_CHK_Y

RET_CODE open_mainmenu(u32 para1, u32 para2);
RET_CODE open_mainmenu_1 (u32 para1, u32 para2);


#endif
