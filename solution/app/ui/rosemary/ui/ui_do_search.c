/********************************************************************************************/
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/* Montage Proprietary and Confidential                                                     */
/* Copyright (c) 2017 Montage Technology Group Limited and its affiliated companies         */
/********************************************************************************************/
#include "ui_common.h"
#include "ui_desc.h"
#include "ui_do_search.h"
#include "ui_infor_bar.h"
//search list 
#define DO_SEARCH_LIST_CNT              2
#define DO_SEARCH_LIST_PAGE              6//8

#define DO_SEARCH_TP_LIST_PAGE           3// 4
#define DO_SEARCH_TP_LIST_FIELD_CNT     2

enum do_search_local_msg
{
  MSG_DO_SEARCH_START = MSG_LOCAL_BEGIN + 180,
  MSG_STOP_SCAN,
  MSG_DO_SEARCH_END,
};

static u16 do_search_cont_keymap(u16 key);
static RET_CODE do_search_cont_proc(control_t *ctrl, u16 msg, u32 para1, u32 para2);
void display_on_oled_string( char *string,u8 line  );


#define PROG_NAME_STRLEN    24
#define LCN_NO_STRLEN      5 /* 999 */

static u16 prog_name_str[DO_SEARCH_LIST_CNT][DO_SEARCH_LIST_PAGE][PROG_NAME_STRLEN + 1];
static u16 *prog_name_str_addr[DO_SEARCH_LIST_CNT][DO_SEARCH_LIST_PAGE];
static u8 prog_scramble[DO_SEARCH_LIST_CNT][DO_SEARCH_LIST_PAGE];
static u16 prog_lcn_num[DO_SEARCH_LIST_CNT][DO_SEARCH_LIST_PAGE][LCN_NO_STRLEN + 1];
static u16 *prog_lcn_num_addr[DO_SEARCH_LIST_CNT][DO_SEARCH_LIST_PAGE];
static u16 prog_curn[DO_SEARCH_LIST_CNT];
static u16 first_id[DO_SEARCH_LIST_CNT];

static clt_scan_sat_info_t g_curn_sat_info;

#define TP_NO_STRLEN      4 /* 999 */
#define TP_INFO_STRLEN    64

static u16 tp_no_str[DO_SEARCH_TP_LIST_PAGE][TP_NO_STRLEN + 1];
static u16 tp_info_str[DO_SEARCH_TP_LIST_PAGE][TP_INFO_STRLEN + 1];
static u16 *tp_no_str_addr[DO_SEARCH_TP_LIST_PAGE];
static u16 *tp_info_str_addr[DO_SEARCH_TP_LIST_PAGE];
static u16 tp_curn;
static BOOL is_stop = TRUE;
static BOOL is_ask_for_cancel = FALSE;
static BOOL is_finished = TRUE;
static BOOL is_saved = TRUE;

BOOL do_search_is_finish(void)
{
  return (is_finished && is_saved) ;
}

BOOL do_search_is_stop(void)
{
  return is_stop;
}

static void init_static_data(void)
{
  u16 i, j, *p_str;

  for(i = 0; i < DO_SEARCH_LIST_CNT; i++)
  {
    for(j = 0; j < DO_SEARCH_LIST_PAGE; j++)
    {
      p_str = prog_name_str[i][j];
      prog_name_str_addr[i][j] = p_str;
      p_str[0] = '\0';

      p_str = prog_lcn_num[i][j];
      prog_lcn_num_addr[i][j] = p_str;
      p_str[0] = '\0';
    }
  }
  memset(prog_curn, 0, sizeof(prog_curn));

  for(i = 0; i < DO_SEARCH_TP_LIST_PAGE; i++)
  {
    p_str = tp_no_str[i];
    tp_no_str_addr[i] = p_str;
    p_str[0] = '\0';

    p_str = tp_info_str[i];
    tp_info_str_addr[i] = p_str;
    p_str[0] = '\0';
  }
  tp_curn = 0;

  for (i = 0; i < DO_SEARCH_LIST_CNT; i++)
  {
    first_id[i] = INVALIDID;
  }

  //first_pg_flag = 0;
}

RET_CODE open_do_search (u32 para1, u32 para2)
{
  control_t *p_cont;
  control_t *p_txt;
  clt_scan_input_para_t* data = (clt_scan_input_para_t*)para1;
  p_cont = init_desc(ROOT_ID_DO_SEARCH, para1, para2);
  if(p_cont == NULL)
  {
    return ERR_FAILURE;
  }

  ctrl_set_keymap(p_cont, do_search_cont_keymap);
  ctrl_set_proc(p_cont, do_search_cont_proc);
  p_txt = ctrl_get_ctrl_by_unique_id(p_cont, IDC_DO_SEARCH_TITLE);
  is_saved = is_stop = is_finished = FALSE;
  
  init_static_data();
  
  if(data->scan_type == CLT_SCAN_TYPE_AUTO || data->scan_type == CLT_SCAN_TYPE_DVBT_AUTO)
  {
    text_set_content_by_strid(p_txt, IDS_AUTO_SEARCH);
  }
  else if(data->scan_type == CLT_SCAN_TYPE_MANUAL || data->scan_type == CLT_SCAN_TYPE_DVBT_MANUAL)
  {
    text_set_content_by_strid(p_txt, IDS_MANUAL_SEARCH);
  }
  else if(data->scan_type == CLT_SCAN_TYPE_RANGE)
  {
    text_set_content_by_strid(p_txt, IDS_RANGE_SEARCH);
  }
  else if(data->scan_type == CLT_SCAN_TYPE_BLIND)
  {
    text_set_content_by_strid(p_txt, IDS_BLIND_SCAN);
  }
  else if(data->scan_type == CLT_SCAN_TYPE_PRESET)
  {
    text_set_content_by_strid(p_txt, IDS_FULL_SCAN);
  }
  else if((data->scan_type == CLT_SCAN_TYPE_TP)
          ||(data->scan_type == CLT_SCAN_TYPE_CUSTOMER_TP))
  {
    text_set_content_by_strid(p_txt, IDS_TP_SEARCH);
  }
  else if(data->scan_type == CLT_SCAN_TYPE_PID)
  {
    text_set_content_by_strid(p_txt, IDS_TP_SEARCH);
  }

  client_start_scan(g_p_client, (clt_scan_input_para_t*)para1);

  ctrl_paint_ctrl(p_cont, FALSE);
  return SUCCESS;
}


static BOOL have_logic_number(void)
{
  return FALSE;
}

static void init_prog_list_content(control_t *list)
{
  u8 i;
  u16 list_idx = ctrl_get_ctrl_id(list) == IDC_DO_SEARCH_LIST_TV ? 0 : 1;
  u8 asc_str[5] = {0};
  u16 uni_str[5] = {0};

  for(i = 0; i < DO_SEARCH_LIST_PAGE; i++)
  {
    if(have_logic_number())
    {
      list_set_field_content_by_unistr(list, i, 0, prog_lcn_num_addr[list_idx][i]);
    }
    else
    {
      if(prog_curn[list_idx] <= DO_SEARCH_LIST_PAGE)
      {
        if(i < prog_curn[list_idx])
        {
          sprintf((char*)asc_str, "%.4d", i + 1);
          ui_str_asc2uni(asc_str, uni_str);
          list_set_field_content_by_unistr(list, i, 0, uni_str);
        }
        else
        {
          list_set_field_content_by_unistr(list, i, 0, (u16*)" ");
        }
      }
      else
      {
        sprintf((char*)asc_str, "%.4d", prog_curn[list_idx] - (DO_SEARCH_LIST_PAGE - 1) + i);
        ui_str_asc2uni(asc_str, uni_str);
        list_set_field_content_by_unistr(list, i, 0, uni_str);
      }
    }
    list_set_field_content_by_extstr(list, i, 1, prog_name_str_addr[list_idx][i]);

    list_set_field_content_by_icon(list, i, 2, prog_scramble[list_idx][i]? IM_TV_MONEY:0);	
  } 
}


static void init_tp_list_content(control_t *list)
{
  u8 i;

  for(i = 0; i < DO_SEARCH_TP_LIST_PAGE; i++)
  {
    list_set_field_content_by_extstr(list, i, 0, tp_no_str_addr[i]);
    list_set_field_content_by_extstr(list, i, 1, tp_info_str_addr[i]);
  }
}


static void add_pro_info_to_data(u8 list, u16 *p_str, clt_scan_pg_info_t *p_pg)
{
  u16 i, curn = prog_curn[list] % DO_SEARCH_LIST_PAGE;
  u8 ascstr[LCN_NO_STRLEN + 1];
  prog_name_str[list][curn][0] = '\0';
  ui_uni_strcat(prog_name_str[list][curn], p_str, PROG_NAME_STRLEN);
  prog_scramble[list][curn] = (u8)p_pg->is_scrambled;
  if(have_logic_number())
  {
    ui_str_asc2uni(ascstr, prog_lcn_num[list][curn]);
    prog_lcn_num_addr[list][curn] = prog_lcn_num[list][curn];
  }
  prog_name_str_addr[list][curn] = prog_name_str[list][curn];
  prog_curn[list]++;
  if(prog_curn[list] >= DO_SEARCH_LIST_PAGE)
  {
    for(i = 0; i < DO_SEARCH_LIST_PAGE; i++)
    {
      prog_name_str_addr[list][i] =
        prog_name_str[list][(prog_curn[list] + i + 1) % DO_SEARCH_LIST_PAGE];
      prog_scramble[list][curn] = prog_scramble[list][(prog_curn[list] + i + 1) % DO_SEARCH_LIST_PAGE];
      if(have_logic_number())
        prog_lcn_num_addr[list][i] = prog_lcn_num[list][(prog_curn[list] + i + 1) % DO_SEARCH_LIST_PAGE];
    }
  }

}

static void add_info_to_data(u16 *p_str)
{
  u8 ascstr[TP_NO_STRLEN + 1];
  u16 i, curn = tp_curn % DO_SEARCH_TP_LIST_PAGE;

  sprintf((char *)ascstr, "%.4d", tp_curn + 1);
  ui_str_asc2uni(ascstr, tp_no_str[curn]);
  tp_no_str_addr[curn] = tp_no_str[curn];

  tp_info_str[curn][0] = '\0';
  ui_uni_strcat(tp_info_str[curn], p_str, TP_INFO_STRLEN);
  tp_info_str_addr[curn] = tp_info_str[curn];

  if(tp_curn >= DO_SEARCH_TP_LIST_PAGE)
  {
    for(i = 0; i < DO_SEARCH_TP_LIST_PAGE; i++)
    {
      tp_no_str_addr[i] =
        tp_no_str[(tp_curn + i + 1) % DO_SEARCH_TP_LIST_PAGE];
      tp_info_str_addr[i] =
        tp_info_str[(tp_curn + i + 1) % DO_SEARCH_TP_LIST_PAGE];
    }
  }

  tp_curn++;
}


static void add_prog_to_list(control_t *cont, clt_scan_pg_info_t *p_pg_info)
{
  control_t *frm, *icon, *list;
  clt_scan_pg_info_t pg;
  u8 idx = 0;
  u8 icon_num[10];
  u16 content[PROG_NAME_STRLEN + 1];
  u16 width = 0;
  u16 strbuf[3] = {'.', '.', '.'};
  OS_PRINTF("ADD PG -> name[%s]\n", p_pg_info->name);

  memcpy(&pg, p_pg_info, sizeof(clt_scan_pg_info_t));

  idx = pg.video_pid != 0 ? 0 /* tv */ : 1 /* radio */;

  /* NAME */
  memcpy(content, pg.name, sizeof(content));
  width = rsc_get_unistr_width(gui_get_rsc_handle(), content, FSI_WHITE_20);

  if(width > 200)
  {
    memcpy(&content[14], strbuf, sizeof(strbuf));
    content[17] = '\0';
  }
  add_pro_info_to_data(idx, content,&pg);

  frm = ctrl_get_child_by_id(cont, IDC_DO_SEARCH_PROG_INFO_FRM);
  icon = ctrl_get_ctrl_by_unique_id(cont, IDC_DO_SEARCH_NUM_TV + idx);

  sprintf((char *)icon_num, "[%d]", prog_curn[idx]);
  text_set_content_by_ascstr(icon, icon_num);
  ctrl_paint_ctrl(icon, TRUE);

  list = ctrl_get_child_by_id(frm, IDC_DO_SEARCH_LIST_TV + idx);
  init_prog_list_content(list);
  ctrl_paint_ctrl(list, TRUE);
  OS_PRINTF("END ADD PG -> name[%s]\n", p_pg_info->name);

  if(first_id[idx] == INVALIDID)
  {
    first_id[idx] = (u16)pg.id;
  }
}

static void add_tp_to_list(control_t *cont, clt_scan_tp_node_t *p_tp_info)
{
  control_t *list;
  clt_scan_tp_node_t tp;
  u16 str_id=IDS_QAM64;
  u8 asc_buf[TP_INFO_STRLEN + 1];
  u16 uni_buf[TP_INFO_STRLEN + 1];
  u8 ascstr[32];
  u16 unistr[64];
  //u8 scan_type = ui_scan_param_get_type();

  OS_PRINTF("ADD TP -> freq[%d]\n", p_tp_info->freq);
  memcpy(&tp, p_tp_info, sizeof(clt_scan_tp_node_t));

  asc_buf[0] = '\0';
  uni_buf[0] = '\0';

  if(g_curn_sat_info.sat_type == CLT_SAT_TYPE_DVBT)
  {
    sprintf((char *)asc_buf, " %.5d...", (int)tp.freq);
  }
  else if(g_curn_sat_info.sat_type == CLT_SAT_TYPE_DVBS)
  {
    sprintf((char *)asc_buf, " %.5d %s %.5d...", (int)tp.freq,
    (char *)(tp.polarity ? "V" : "H"), (int)tp.sym);
  }
  else if(g_curn_sat_info.sat_type == CLT_SAT_TYPE_DVBC)
  {
    switch(tp.mod)
    {
    case CLT_SCAN_NIM_MODULA_AUTO:
      str_id = IDS_AUTO;
      break;

    case CLT_SCAN_NIM_MODULA_BPSK:
      str_id =IDS_QPSK;
      break;

    case CLT_SCAN_NIM_MODULA_QPSK:
      str_id = IDS_BPSK;
      break;

    case CLT_SCAN_NIM_MODULA_8PSK:
      str_id = IDS_8PSK;
      break;

    case CLT_SCAN_NIM_MODULA_QAM16:
      str_id = IDS_QAM16;
      break;

    case CLT_SCAN_NIM_MODULA_QAM32:
      str_id = IDS_QAM32;
      break;

    case CLT_SCAN_NIM_MODULA_QAM64:
      str_id = IDS_QAM64;
      break;

    case CLT_SCAN_NIM_MODULA_QAM128:
      str_id = IDS_QAM128;
      break;

    case CLT_SCAN_NIM_MODULA_QAM256:
      str_id = IDS_QAM256;
      break;
    default:
      str_id = IDS_QAM64;
      break;
    }
    gui_get_string(str_id, unistr, 64);
    ui_str_uni2asc((u8 *)ascstr,(u16 *)unistr);
    sprintf((char *)asc_buf, "%.6d %s %.4d...", (int)tp.freq,
      (char *)ascstr, (int)tp.sym);
  }
  ui_str_nasc2uni(asc_buf, uni_buf, TP_INFO_STRLEN);

  add_info_to_data(uni_buf);

  list = ctrl_get_ctrl_by_unique_id(cont, IDC_DO_SEARCH_TP_LIST);
  init_tp_list_content(list);
  ctrl_paint_ctrl(list, TRUE);
  OS_PRINTF("END ADD TP ->freq[%d]\n", p_tp_info->freq);
}

static RET_CODE on_sat_found(control_t *cont, u16 msg,
                            u32 para1, u32 para2)
{
  clt_scan_sat_info_t *p_sat_info = (clt_scan_sat_info_t *)para1;
  
  OS_PRINTF("PROCESS -> add sat\n");
  
  memcpy(&g_curn_sat_info, p_sat_info, sizeof(clt_scan_sat_info_t));
  return SUCCESS;
}

static RET_CODE on_pg_found(control_t *cont, u16 msg,
                            u32 para1, u32 para2)
{
  OS_PRINTF("PROCESS -> add prog\n");
  add_prog_to_list(cont, (clt_scan_pg_info_t *)para1);
  return SUCCESS;
}


static RET_CODE on_tp_found(control_t *cont, u16 msg,
                            u32 para1, u32 para2)
{
  OS_PRINTF("PROCESS -> add/change tp\n");
  add_tp_to_list(cont, (clt_scan_tp_node_t *)para1);
  return SUCCESS;
}

static RET_CODE on_update_progress(control_t *p_cont, u16 msg,
                                   u32 para1, u32 para2)
{
	u16 progress = (u16)para1;
	control_t *p_bar = ctrl_get_ctrl_by_unique_id(p_cont, IDC_DO_SEARCH_PBAR);
	control_t *p_ctrl;
	u8 str_buf[10];
  if(ctrl_get_sts(p_bar) != OBJ_STS_HIDE)
  {
    OS_PRINTF("UPDATE PROGRESS -> %d\n", progress);
    if(pbar_get_current(p_bar) != progress)
    {
      pbar_set_current(p_bar, progress);
      ctrl_paint_ctrl(p_bar, TRUE);
	    p_ctrl = ctrl_get_ctrl_by_unique_id(p_cont, IDC_DO_SEARCH_PERCENT);
      sprintf((char *)str_buf, "%d%%", progress);
      text_set_content_by_ascstr(p_ctrl, str_buf);
      ctrl_paint_ctrl(p_ctrl, TRUE);      
    }
  }

  return SUCCESS;
}

static void get_finish_str(u16 *str, u16 max_len)
{
  u16 uni_str[10], len;

  if(prog_curn[0] > 0
    || prog_curn[1] > 0)
  {
    len = 0, str[0] = '\0';
    gui_get_string(IDS_MSG_SEARCH_IS_END, str, max_len);

    convert_i_to_dec_str(uni_str, prog_curn[0]);
    ui_uni_strcat(str, uni_str, max_len);

    len = (u16)ui_uni_strlen(str);
    gui_get_string(IDS_MSG_N_TV, &str[len], (u16)(max_len - len));

    convert_i_to_dec_str(uni_str, prog_curn[1]);
    ui_uni_strcat(str, uni_str, max_len);

    len = (u16)ui_uni_strlen(str);
    gui_get_string(IDS_MSG_N_RADIO, &str[len], (u16)(max_len - len));
  }
  else
  {
    gui_get_string(IDS_MSG_NO_PROG_FOUND, str, max_len);	
  }
}

static void process_finish(void)
{
  comm_dlg_data_t dlg_data =
  {
    ROOT_ID_INVALID,
    DLG_FOR_CONFIRM | DLG_STR_MODE_EXTSTR,
    COMM_DLG_X, COMM_DLG_Y,
    COMM_DLG_W, COMM_DLG_H,
    0,
    15000,
  };

  u16 content[64 + 1];

  get_finish_str(content, 64);
  dlg_data.content = (u32)content;

  is_stop = TRUE;
  is_finished = TRUE;

  if(is_ask_for_cancel)
  {
    client_resume_scan(g_p_client);
  }

  ui_comm_dlg_open(&dlg_data);
}

static RET_CODE on_finished(control_t *cont, u16 msg,
                            u32 para1, u32 para2)
{  
  OS_PRINTF("[%s] [%d] \n",__FUNCTION__,__LINE__);

  display_on_oled_string( "   Scan Done    ",0  );
  display_on_oled_string( "    Press Ok    ",1	);
  
  if(!is_finished)
  {
    process_finish();
    is_finished = TRUE;

    manage_close_all_menus();
	 //open_infor_bar(0, 0);
  }
  return SUCCESS;
}

static void do_cancel(void)
{
  if(!is_stop)
  {
    OS_PRINTF("DO_SEARCH: cancel scan!\n");
    is_stop = TRUE;
    client_stop_scan(g_p_client);
  }
}

static void undo_cancel(void)
{
  OS_PRINTF("DO_SEARCH: resume scan!\n");
  client_resume_scan(g_p_client);
}

static RET_CODE on_stop_scan(control_t *cont, u16 msg,
                             u32 para1, u32 para2)
{
	if(!is_stop)
  {
    // pause scanning, at first
    OS_PRINTF("DO_SEARCH: pause scan!\n");
    client_pause_scan(g_p_client);
    is_ask_for_cancel = TRUE;
    ui_comm_ask_for_dodlg_open(NULL, IDS_MSG_ASK_FOR_EXIT_SCAN,
                               do_cancel, undo_cancel, 0);
    is_ask_for_cancel = FALSE;
  }
  return SUCCESS;
}

static RET_CODE on_save_do_search(control_t *cont, u16 msg,
                                  u32 para1, u32 para2)
{
  comm_dlg_data_t saving_data =
  {
    ROOT_ID_INVALID,
    DLG_FOR_SHOW | DLG_STR_MODE_STATIC,
    COMM_DLG_X, COMM_DLG_Y,
    COMM_DLG_W, COMM_DLG_H,
    IDS_MSG_SAVING,
    8000,
  };

  if((prog_curn[0] > 0)
    || (prog_curn[1] > 0))
  {
    ui_comm_dlg_open(&saving_data);
  }

  client_stop_scan(g_p_client);
  client_epg_start_policy(g_p_client, CLT_EPG_START_POLICY_ALLPF);

  is_stop = is_finished = TRUE;

  if((prog_curn[0] > 0)
    || (prog_curn[1] > 0))
  {
    ui_comm_dlg_close();
  }
  return SUCCESS;
}
  
static RET_CODE on_db_is_full(control_t *cont, u16 msg,
                              u32 para1, u32 para2)
{
  
  client_pause_scan(g_p_client);

  // ask for cancel
  ui_comm_cfmdlg_open(NULL, IDS_MSG_SPACE_IS_FULL, do_cancel, 0);
  return SUCCESS;
}

BEGIN_KEYMAP(do_search_cont_keymap, NULL)
ON_EVENT(V_KEY_CANCEL, MSG_STOP_SCAN)
ON_EVENT(V_KEY_EXIT, MSG_STOP_SCAN)
ON_EVENT(V_KEY_BACK, MSG_STOP_SCAN)
ON_EVENT(V_KEY_MENU, MSG_STOP_SCAN)
END_KEYMAP(do_search_cont_keymap, NULL)

BEGIN_MSGPROC(do_search_cont_proc, cont_class_proc)
ON_COMMAND(MSG_SCAN_SAT_FOUND, on_sat_found)
ON_COMMAND(MSG_SCAN_PG_FOUND, on_pg_found)
ON_COMMAND(MSG_SCAN_TP_FOUND, on_tp_found)
ON_COMMAND(MSG_SCAN_PROGRESS, on_update_progress)
ON_COMMAND(MSG_SCAN_FINISHED, on_finished)
ON_COMMAND(MSG_STOP_SCAN, on_stop_scan)
ON_COMMAND(MSG_SAVE, on_save_do_search)
ON_COMMAND(MSG_SCAN_DB_FULL, on_db_is_full)
END_MSGPROC(do_search_cont_proc, cont_class_proc)

