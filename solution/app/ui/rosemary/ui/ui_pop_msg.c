/********************************************************************************************/
/* Copyright (c) 2014 Montage Technology Group Limited and its affiliated companies         */
/* Montage Proprietary and Confidential                                                     */
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/********************************************************************************************/
#include "ui_common.h"
#include "ui_pop_msg.h"
#include "ui_menu_data.h"

extern BOOL ui_is_desktop_start(void);


enum control_id
{
  IDC_INVALID = 0,
  IDC_CONTENT,
};

void display_on_oled_string( char *string,u8 line  );

//Attention g_msg_strID just for ca or other module setting
//stb msg: no signal, lock, enctry is check every time
static u16 g_msg_strID = RSC_INVALID_ID;

void ui_pop_msg_clear_ca_msg(void)
{
  //except other module's msg
  if((g_msg_strID != IDS_MSG_NO_SIGNAL)
         &&(g_msg_strID != IDS_MSG_NO_PROG)
         &&(g_msg_strID != IDS_LOCK)
         &&(g_msg_strID != IDS_PARENTAL_LOCK))
  {
    g_msg_strID  = RSC_INVALID_ID;
    OS_PRINTF("func[%s] line[%d] str_id[%d]\n", __FUNCTION__, __LINE__, g_msg_strID);   
  }
}

void ui_pop_msg_clear_signal_msg(void)
{
  if(g_msg_strID == IDS_MSG_NO_SIGNAL)
  {
    g_msg_strID  = RSC_INVALID_ID;
    OS_PRINTF("func[%s] line[%d] str_id[%d]\n", __FUNCTION__, __LINE__, g_msg_strID);   
  }
}

void ui_pop_msg_set_msg_strid(u16 str_id)
{
  OS_PRINTF("func[%s] line[%d] old_id[%d], new_id[%d]\n", 
    __FUNCTION__, __LINE__, g_msg_strID, str_id);
  
  g_msg_strID = str_id;
}

u16 ui_pop_msg_get_msg_strid(void)
{  
  return g_msg_strID;
}

void ui_pop_msg_open(void)
{
  control_t *p_txt, *p_cont = NULL;
  u16 txt_x, txt_y, txt_w, txt_h;
  u16 focus_id = fw_get_focus_id();

  if(!ui_menu_data_api_is_fscreen_menu(focus_id))
  {
    return;
  }

  p_cont = fw_create_mainwin(ROOT_ID_POP_MSG,
                             SIGNAL_CONT_FULL_X, SIGNAL_CONT_FULL_Y,
                             SIGNAL_CONT_FULL_W, SIGNAL_CONT_FULL_H,
                             ROOT_ID_INVALID, 0,
                             OBJ_ATTR_INACTIVE, 0);
  if(p_cont == NULL)
  {
    return;
  }

  ctrl_set_rstyle(p_cont, RSI_COMMON_RECT1, RSI_COMMON_RECT1, RSI_COMMON_RECT1);

  {
    txt_x = SIGNAL_TXT_FULL_X;
    txt_y = SIGNAL_TXT_FULL_Y;
    txt_w = SIGNAL_TXT_FULL_W;
    txt_h = SIGNAL_TXT_FULL_H;
  }

  p_txt = ctrl_create_ctrl(CTRL_TEXT, IDC_CONTENT,
                           txt_x, txt_y, txt_w, txt_h,
                           p_cont, 0);
  ctrl_set_rstyle(p_txt, RSI_IGNORE, RSI_IGNORE, RSI_IGNORE);

  text_set_align_type(p_txt, STL_CENTER | STL_VCENTER);
  text_set_font_style(p_txt, FSI_WHITE_22, FSI_WHITE_22, FSI_WHITE_22);
  text_set_content_type(p_txt, TEXT_STRTYPE_STRID);
  if(g_msg_strID != RSC_INVALID_ID)
  {
    text_set_content_by_strid(p_txt, g_msg_strID);
  }

  ctrl_paint_ctrl(p_cont, FALSE);
}

void ui_pop_msg_close(void)
{
  control_t *p_msg_win = NULL;

  p_msg_win = fw_find_root_by_id(ROOT_ID_POP_MSG);
  if(p_msg_win != NULL)
  {
    fw_destroy_mainwin_by_id(ROOT_ID_POP_MSG);
  }
}

/*
 * skip stb local msg check, force to show pop msg by param str_id
 */
void ui_pop_msg_refresh_msg_force(u16 str_id)
{
  control_t *p_msg_win = NULL;
  control_t *p_txt = NULL;

  if(!ui_is_desktop_start())
  {
    return;
  }
  
  g_msg_strID = str_id;
  
  //refresh msg window
  if(g_msg_strID != RSC_INVALID_ID)
  {
    p_msg_win = fw_find_root_by_id(ROOT_ID_POP_MSG);
    
    if(p_msg_win != NULL)
    {
      p_txt = ctrl_get_child_by_id(p_msg_win, IDC_CONTENT);
      
      if((u16)text_get_content(p_txt) != g_msg_strID)
      {
        text_set_content_by_strid(p_txt, g_msg_strID);
        ctrl_paint_ctrl(p_txt, TRUE);
      }
    }
    else
    {
      ui_pop_msg_open();
    }
  }
  else
  {
    p_msg_win = fw_find_root_by_id(ROOT_ID_POP_MSG);
    if(p_msg_win != NULL)
    {
      fw_destroy_mainwin_by_id(ROOT_ID_POP_MSG);
    }
  }
}

/*
 *根据优先级刷新
 *优先级为 no prog >no signal > lock > ca or other moduel
 *encrypt has move to on_playback_descramble
*/
void ui_pop_msg_refresh_msg(void)
{
  clt_system_status_t system_status = {0};
  u16 str_id = RSC_INVALID_ID;
  int pg_cnt = 0;
  clt_dbase_get_count_para_t para = {{0}};

  if(!ui_is_desktop_start())
  {
    return;
  }
  

  strncpy(para.view_name, "curn_view", sizeof(para.view_name));
  pg_cnt =  client_dbase_get_count(g_p_client,&para);
  client_get_system_status(g_p_client, &system_status);

  OS_PRINTF("pg_cnt[%d] signal[%d] is_chkpwd[%d] is_pass_chkpwd[%d] is_age[%d] is_encrypt[%d]\n",
    pg_cnt, system_status.is_signal_lock, system_status.is_chkpwd, system_status.is_pass_chkpwd, system_status.is_encrypt);

  //if has pg
  if(pg_cnt <= 0)
  {
    str_id = IDS_MSG_NO_PROG;
	display_on_oled_string("   No Program   ",0	);
	display_on_oled_string("                ",1 );
  }
  //second check if usr lock
  else if(system_status.is_chkpwd && (!system_status.is_pass_chkpwd))
  {
    str_id = IDS_LOCK;
  }
  else if(system_status.is_age_lock && (!system_status.is_pass_chkpwd))
  {
    str_id = IDS_PARENTAL_LOCK;
  }
  else if (! system_status.is_signal_lock)
  {
    str_id = IDS_MSG_NO_SIGNAL;
	display_on_oled_string("   No Signal    ",0	);
	display_on_oled_string("  Check cable   ",1 );
  }
  //maybe set by ca or other module
  else if((g_msg_strID != IDS_MSG_NO_PROG)
         &&(g_msg_strID != IDS_LOCK)
         &&(g_msg_strID != IDS_PARENTAL_LOCK))
  {
    str_id = g_msg_strID;
    OS_PRINTF("func[%s] line[%d] str_id[%d]\n", __FUNCTION__, __LINE__, g_msg_strID);   
  }
  
  //refresh msg window
  ui_pop_msg_refresh_msg_force(str_id);
}





