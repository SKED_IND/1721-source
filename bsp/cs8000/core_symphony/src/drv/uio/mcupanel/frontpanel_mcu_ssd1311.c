/********************************************************************************************/
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/* Montage Proprietary and Confidential                                                     */
/* Copyright (c) 2014 Montage Technology Group Limited and its affiliated companies         */
/********************************************************************************************/
#include <string.h>
#include "sys_types.h"
#include "sys_define.h"
#include "sys_regs_magic.h"
#include "mtos_fifo.h"
#include "mtos_int.h"
#include "mtos_printk.h"
#include "mtos_mem.h"
#include "mtos_task.h"
#include "mtos_misc.h"
#include "drv_dev.h"
#include "hal_concerto_irq.h"
#include "hal_timer.h"
#include "hal_base.h"
#include "hal_misc.h"
#include "hal_gpio.h"
#include "uio.h"
#include "i2c.h"
#include "../uio_priv.h"


#define SSD1311_WRITE_ADDR (0x78)
#define FP_MAX_LED_NUM  16

typedef struct
{
  u16 dis_buff[FP_MAX_LED_NUM];    /* Buffer for LED display scan */
  u32 timer_id;
  void *p_bus_dev;
  fp_cfg_t config;
}fp_priv_t;


//static unsigned char  MSG1    [2][16]={"  Winstar-OLED  ","   WEO001602G   "};


static RET_CODE SSD1311_WRITE_CMD(u8 cmd)
{
	RET_CODE ret = -1;
	u8 tmp[2] = {0};
	uio_device_t  *p_uio_dev = NULL;
	void *p_mcu_dev = NULL;

	p_uio_dev = (uio_device_t *)dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_UIO);
	lld_uio_t *p_lld = (lld_uio_t *)p_uio_dev->p_priv;
	fp_priv_t *p_fp = (fp_priv_t *)p_lld->p_fp;
	p_mcu_dev = p_fp->p_bus_dev;
	//mtos_printk("Fun[%s] Line[%u] p_mcu_dev = 0x%x\n", __FUNCTION__, __LINE__, p_mcu_dev);
		
	tmp[0] = 0;
	tmp[1] = cmd;
	ret = i2c_write(p_mcu_dev, SSD1311_WRITE_ADDR, tmp, 2, 0);
	
	return ret;
}


static RET_CODE SSD1311_WRITE_DATA(u8 cmd)
{
	RET_CODE ret = -1;
	u8 tmp[2] = {0};
	uio_device_t  *p_uio_dev = NULL;
	void *p_mcu_dev = NULL;

	p_uio_dev = (uio_device_t *)dev_find_identifier(NULL, DEV_IDT_TYPE, SYS_DEV_TYPE_UIO);
	lld_uio_t *p_lld = (lld_uio_t *)p_uio_dev->p_priv;
	fp_priv_t *p_fp = (fp_priv_t *)p_lld->p_fp;
	p_mcu_dev = p_fp->p_bus_dev;
	//mtos_printk("Fun[%s] Line[%u] p_mcu_dev = 0x%x\n", __FUNCTION__, __LINE__, p_mcu_dev);
		
	tmp[0] = 0x40;
	tmp[1] = cmd;
	ret = i2c_write(p_mcu_dev, SSD1311_WRITE_ADDR, tmp, 2, 0);
	
	return ret;
}

static RET_CODE mcu_ssd1311_open(lld_uio_t *p_lld, fp_cfg_t *p_cfg)
{
	RET_CODE ret = SUCCESS;
	fp_priv_t *p_panel = NULL;

	if(NULL != p_lld->p_fp)
	{
		return SUCCESS;
	}

	mtos_printk("Fun[%s] Line[%u] p_bus_dev = 0x%x,  reg[0xbf15b400] = 0x%x\n", __FUNCTION__, __LINE__, p_cfg->p_bus_dev, hal_get_u32((u32 *)0xbf15b400));

	p_panel = (fp_priv_t *)mtos_malloc(sizeof(fp_priv_t));
	MT_ASSERT(NULL != p_panel);
	
	memset(p_panel, 0, sizeof(fp_priv_t));
	p_lld->p_fp = p_panel;
	p_panel->p_bus_dev = p_cfg->p_bus_dev;

	#if 0
	if((NULL != p_cfg) && (NULL != p_cfg->p_info))
	{ 
		p_panel->config.p_info = (pan_hw_info_t *)mtos_malloc(sizeof(pan_hw_info_t));
		memset(p_panel->config.p_info, 0, sizeof(pan_hw_info_t));
		memcpy(p_panel->config.p_info, p_cfg->p_info, sizeof(pan_hw_info_t));
	}
	#endif


	ret = SSD1311_WRITE_CMD(0x08);  

	//Function Set
	//00101010 ==0x2A
	SSD1311_WRITE_CMD(0x2A);	
	//RE =1,SD=0,IS = 0

	//Function Selection A
	//Internal VDD Regulator ON
	SSD1311_WRITE_CMD(0x71);
	SSD1311_WRITE_DATA(0x4C);  // 00  disable; 4c enable
	mtos_task_delay_ms(10);

	//Function Selection B
	//Select Font table
	SSD1311_WRITE_CMD(0x72);   
	SSD1311_WRITE_DATA(0x01); 

	//OLED characterization	ON	
	SSD1311_WRITE_CMD(0x79);	   	

	//RE =1,SD=1,IS = 0

	//Set Contrast		   ----**********		  NON Default
	SSD1311_WRITE_CMD(0x81);
	SSD1311_WRITE_CMD(0xFF);

	//Set dispaly clock divide Ratio		   *******************************
	SSD1311_WRITE_CMD(0xD5);
	SSD1311_WRITE_CMD(0x70);	  //default


	//Set phase Length	  ***************** Non Default
	SSD1311_WRITE_CMD(0xD9);
	SSD1311_WRITE_CMD(0x78);		//

	//Set SEG pin Hardware Config.	   (POR)
	SSD1311_WRITE_CMD(0xDA);
	SSD1311_WRITE_CMD(0x10);	   //Default value

	//Set VcomH Deselect Level		 *******************limit
	SSD1311_WRITE_CMD(0xDB);
	SSD1311_WRITE_CMD(0x40);			//maxumun  Vcomh

	// Funciton Selection C 	(GPIO output High)		  */************************
	SSD1311_WRITE_CMD(0xDC);
	SSD1311_WRITE_CMD(0x03);		  //enable boost
	mtos_task_delay_ms(10);

	//OLED characterization	OFF
	SSD1311_WRITE_CMD(0x78);	

	//RE = 1 ,SD=0,IS = 0

	// Extended funcion Set 00001000
	SSD1311_WRITE_CMD(0x08);
	// scan direction 
	SSD1311_WRITE_CMD(0x06);
	//Extended command Set ending
	//Function Set
	SSD1311_WRITE_CMD(0x28);
	//RE = 0,SD=0,IS = 0

	//clear display
	SSD1311_WRITE_CMD(0x01);
	//Return home
	SSD1311_WRITE_CMD(0x02);
	//entry mode Set
	SSD1311_WRITE_CMD(0x06);
	//display on
	SSD1311_WRITE_CMD(0x0C);

	SSD1311_WRITE_CMD(0x02);

	mtos_task_delay_ms(10);
	
	return ret;
}

static RET_CODE mcu_ssd1311_display(lld_uio_t *p_lld,u8 *p_data, u32 param)
{
	
	unsigned char k=0; 

	mtos_printk("Fun[%s] Line[%u] \n", __FUNCTION__, __LINE__);

	#if 0
	SSD1311_WRITE_CMD(0x80);

	for(k=0;k<16;k++)
		SSD1311_WRITE_DATA(MSG1[0][k]);
			
	SSD1311_WRITE_CMD(0xC0);

	for(k=0;k<16;k++)
		SSD1311_WRITE_DATA(MSG1[1][k]);	
	#endif

	SSD1311_T *ptr = (SSD1311_T *)param;
	
	u8 len = ptr->len;
	u8 dis_fp = ptr->disfp;


	if(len > FP_MAX_LED_NUM)
		return ERR_FAILURE;

	if(dis_fp)
	{
		SSD1311_WRITE_CMD(0xC0);

		for(k=0;k<len;k++)
			SSD1311_WRITE_DATA(p_data[k]);	
	}
	else
	{
		SSD1311_WRITE_CMD(0x80);

		for(k=0;k<len;k++)
			SSD1311_WRITE_DATA(p_data[k]);	
	}

	return SUCCESS;
}

static RET_CODE mcu_ssd1311_io_ctrl(lld_uio_t *p_lld,u32 cmd, u32 param)
{
	RET_CODE ret = SUCCESS;

	return ret;
}

static RET_CODE mcu_ssd1311_stop(lld_uio_t *p_lld)
{
	fp_priv_t *p_fp = (fp_priv_t *)p_lld->p_fp;

	if(NULL != p_fp)
	{
		if (NULL != p_fp->config.p_info)
		{
			mtos_free(p_fp->config.p_info);
			p_fp->config.p_info = NULL;
		}
	
		mtos_free(p_fp);
		p_fp = NULL;
	}
	return SUCCESS;
}

void mcu_ssd1311_attach(lld_uio_t *p_lld)
{
	p_lld->fp_open = mcu_ssd1311_open;
	p_lld->fp_stop = mcu_ssd1311_stop;
	p_lld->fp_io_ctrl = mcu_ssd1311_io_ctrl;
	p_lld->display = mcu_ssd1311_display;
}

