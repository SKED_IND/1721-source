/********************************************************************************************/
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/* Montage Proprietary and Confidential                                                     */
/* Copyright (c) 2014 Montage Technology Group Limited and its affiliated companies         */
/********************************************************************************************/
#include <string.h>
#include "sys_types.h"
#include "sys_define.h"
#include "sys_regs_symphony.h"
#include "mtos_fifo.h"
#include "mtos_int.h"
#include "mtos_misc.h"
#include "mtos_mem.h"
#include "mtos_printk.h"
#include "hal_concerto_irq.h"
#include "drv_dev.h"
#include "hal_base.h"
#include "hal_misc.h"
#include "uio.h"
#include "../uio_priv.h"

#define IRDA_DEBUG 0
#if IRDA_DEBUG
#define IRDA_PRINT OS_PRINTF
#else
#define IRDA_PRINT(...)     do {} while(0)
#endif

#define CPU_BYTE_TEST 0
/************************************************************************
 * IrDA Controller
 ************************************************************************/
/*!
  comments
  */
#define SYMPHONY_IR_BASE_ADDR      0xBF151000
/*!
  comments
  */
#define SYMPHONY_IR_NEC_DATA      SYMPHONY_IR_BASE_ADDR
/*!
  comments
  */
#define SYMPHONY_IR_COM_DATA     (SYMPHONY_IR_BASE_ADDR + 0x04)
/*!
  comments
  */
#define SYMPHONY_IR_GLOBAL_CTRL  (SYMPHONY_IR_BASE_ADDR + 0x08)
/*!
  comments
  */
#define SYMPHONY_IR_GLOBAL_STA   (SYMPHONY_IR_BASE_ADDR + 0x0C)
/*!
  comments
  */
#define SYMPHONY_IR_INT_CFG      (SYMPHONY_IR_BASE_ADDR + 0x10)
/*!
  comments
  */
#define SYMPHONY_IR_INT_RAWSTA   (SYMPHONY_IR_BASE_ADDR + 0x14)
/*!
  comments
  */
#define SYMPHONY_IR_NEC_FILT     (SYMPHONY_IR_BASE_ADDR + 0x18)
/*!
  comments
  */
#define SYMPHONY_IR_COMBUF_POP   (SYMPHONY_IR_BASE_ADDR + 0x1C)
/*!
  comments
  */
#define SYMPHONY_IR_NEC_START_ONTSET   (SYMPHONY_IR_BASE_ADDR + 0x40)
/*!
  comments
  */
#define SYMPHONY_IR_NEC_START_PRDSET   (SYMPHONY_IR_BASE_ADDR + 0x44)
/*!
  comments
  */
#define SYMPHONY_IR_NEC_REPEAT_ONTSET  (SYMPHONY_IR_BASE_ADDR + 0x48)
/*!
  comments
  */
#define SYMPHONY_IR_NEC_REPEAT_PRDSET  (SYMPHONY_IR_BASE_ADDR + 0x4C)
/*!
  comments
  */
#define SYMPHONY_IR_NEC_BIT1_ONTSET    (SYMPHONY_IR_BASE_ADDR + 0x50)
/*!
  comments
  */
#define SYMPHONY_IR_NEC_BIT1_PRDSET    (SYMPHONY_IR_BASE_ADDR + 0x54)
/*!
  comments
  */
#define SYMPHONY_IR_NEC_BIT0_ONTSET    (SYMPHONY_IR_BASE_ADDR + 0x58)
/*!
  comments
  */
#define SYMPHONY_IR_NEC_BIT0_PRDSET    (SYMPHONY_IR_BASE_ADDR + 0x5C)
/*!
  comments
  */
#define SYMPHONY_IR_WAVEFILT_CFG0  (SYMPHONY_IR_BASE_ADDR + 0x80)
/*!
  comments
  */
#define SYMPHONY_IR_WAVEFILT_CFG1  (SYMPHONY_IR_BASE_ADDR + 0x84)
/*!
  comments
  */
#define SYMPHONY_IR_WAVEFILT_CFG2  (SYMPHONY_IR_BASE_ADDR + 0x88)
/*!
  comments
  */
#define SYMPHONY_IR_WAVEFILT_CFG3  (SYMPHONY_IR_BASE_ADDR + 0x8C)
/*!
  comments
  */
#define SYMPHONY_IR_WAVEFILT_MASK  (SYMPHONY_IR_BASE_ADDR + 0x90)
/*!
  comments
  */
#define SYMPHONY_IR_WAVEFILT_MASK1  (SYMPHONY_IR_BASE_ADDR + 0x9C)
/*!
  comments
  */
#define SYMPHONY_IR_WFN_ONTSET  (SYMPHONY_IR_BASE_ADDR + 0x400)
/*!
  comments
  */
#define SYMPHONY_IR_WFN_PRDSET  (SYMPHONY_IR_BASE_ADDR + 0x404)

#define DEFAULT_SAMPLE_FREQ 1 // 187.5KHz by default
#define RC5_WF_IGNORE_T_BIT 1 // ignore T bit for RC5 wave-filter

#define IRDA_SYMPHONY_CHK_RPTKEY() (hal_get_u32((volatile u32 *)SYMPHONY_IR_GLOBAL_STA) & 0x01)
#define IRDA_SYMPHONY_CHK_OVERFLOW() ((hal_get_u32((volatile u32 *)SYMPHONY_IR_GLOBAL_STA) >> 1) & 0x01)
#define IRDA_SYMPHONY_CHK_KUPSTA() ((hal_get_u32((volatile u32 *)SYMPHONY_IR_INT_RAWSTA) >> 1) & 0x1)
#define IRDA_SYMPHONY_CHK_KDNSTA() (hal_get_u32((volatile u32 *)SYMPHONY_IR_INT_RAWSTA) & 0x1)
#define IRDA_SYMPHONY_CHK_WFSTA() ((hal_get_u32((volatile u32 *)SYMPHONY_IR_INT_RAWSTA) >> 4) & 0xF)

#define GET_SYMPHONY_IR_WAVEFILT_MASK(n) (SYMPHONY_IR_BASE_ADDR + 0x90 + (4 * n))
#define GET_SYMPHONY_IR_WFN_ONTSET(n) (SYMPHONY_IR_BASE_ADDR + 0x400 + (8 * n))
#define GET_SYMPHONY_IR_WFN_PRDSET(n) (SYMPHONY_IR_BASE_ADDR + 0x404 + (8 * n))

#define IRDA_RPT_TIME_MS 300

/*****************************************************************
* IRDA external APIs declaration
*****************************************************************/
//Attention !!!
//it shoud be replace to uio_check_rpt_key_symphony after ledkb verify done
extern BOOL uio_check_rpt_key_concerto(uio_type_t uio, u8 code, u8 ir_index);

/*****************************************************************
* IRDA internal APIs declaration
*****************************************************************/
void irda_symphony_soft_reset(void);
//static void irda_symphony_idle_set(u8 idle);
//static void irda_symphony_sample_clk_set(u8 sample_clk);
static void irda_symphony_wave_len_set(u8 channel, u8 len);
static void irda_symphony_wave_add_set(u8 channel, u8 addr);
static void irda_symphony_wave_mask_bit_set(u32 bit, u32 value);
static void irda_symphony_wave_channel_set(u8 channel, u8 is_en);
static void irda_symphony_wave_channel_int_set(u8 channel_int, u8 is_en);
static void irda_symphony_wave_wfn_set(u8 n, u16 ontime_l, u16 ontime_h, u16 period_l, u16 period_h);


/*****************************************************************
* IRDA internal variables
*****************************************************************/
static BOOL is_ir_intialized = 0;

typedef struct
{
  BOOL print_user_code;
  u16 sw_user_code[IRDA_MAX_USER];
  u8 user_code_set;
  u8 irda_protocol;
  u16 user_code;
  u32 irda_nec_repeat_time;
  u8 irda_sw_recv_decimate;
  u8 irda_wfilt_channel;
  u32 irda_flag;
  irda_wfilt_cfg_t irda_wfilt_channel_cfg[IRDA_MAX_CHANNEL];
}irda_priv_t;

typedef struct
{
  u16 ontime;
  u16 period;
  u8 protocol;
} irda_dec_para_t;

typedef struct
{
  union
  {
    struct
    {
      u16 usercode;
      u16 code;
    } nec;
    struct
    {
      u8 ucode;
      u8 kcode;
    } konka;
    struct
    {
      u8 ucode;
      u8 kcode;
    } rc5;
  } r;
  u32 state;
  u16 *p_sigtbl;
  u8 counter;
  u8 repeat_interval;
} irda_dec_result_t;

#if 0
/*!
  IRDA Protocol
*/
typedef enum
{
  IRDA_SW_NEC = 1,
  IRDA_SW_KONKA,
  IRDA_SW_RC5
} irda_sw_pro_t;
#endif

/*!
  IRDA NEC Decoder State
*/
typedef enum
{
  E_NEC_STA_IDLE = 0,
  E_NEC_STA_START,
  E_NEC_STA_USRL,
  E_NEC_STA_USRH,
  E_NEC_STA_KEY,
  E_NEC_STA_YEK,
  E_NEC_STA_REPEAT,
  E_NEC_STA_FINISH
} irda_pro_nec_status_t;

/*!
  IRDA NEC Decoder Signal
*/
typedef enum
{
  E_NEC_SIG_START = 0,
  E_NEC_SIG_0,
  E_NEC_SIG_1,
  E_NEC_SIG_REPEAT,
  E_NEC_SIG_INVALID
} irda_pro_nec_sig_t;

#if DEFAULT_SAMPLE_FREQ //  Sample freq 187.5 KHz
	static u16 nec_sigtbl[16] = {1180, 2192, 2380, 3288, 70, 180, 160, 270, 70, 180, 300, 540, 1180,
	        2192, 1476, 2380};//2280
	static u16 nec_sigtbl_24m[16] = {1048, 1948, 2030, 2922, 62, 160, 142, 240, 62, 160, 266, 480, 1048,
	        1948, 1312, 2026};
#else //  Sample freq 46.875 KHz
	static u16 nec_sigtbl[16] = {295, 548, 571, 822, 17, 50, 20, 68, 17, 50, 75, 135, 295,
		548, 369, 570};
	static u16 nec_sigtbl_24m[16] = {262, 487, 507, 730, 15, 44, 15, 60, 15, 44, 66, 120, 262,
		487, 328, 506};
#endif

#if IRDA_DEBUG
static const char * nec_sig_strs[5] = {
"E_NEC_SIG_START","E_NEC_SIG_0","E_NEC_SIG_1",
"E_NEC_SIG_REPEAT", "E_NEC_SIG_INVALID"
};

static const char * nec_sta_strs[8] = {
"E_NEC_STA_IDLE","E_NEC_STA_START","E_NEC_STA_USRL",
"E_NEC_STA_USRH", "E_NEC_STA_KEY","E_NEC_STA_YEK",
"E_NEC_STA_REPEAT", "E_NEC_STA_FINISH"
};
#endif

/*!
  IRDA Konka Decoder State
  */
typedef enum
{
  E_KK_STA_IDLE = 0,
  E_KK_STA_START,
  E_KK_STA_USR,
  E_KK_STA_KEY,
  E_KK_STA_SYNC,
  E_KK_STA_FINISH
} irda_pro_kk_status_t;

/*!
  IRDA Konka Decoder Signal
  */
typedef enum
{
  E_KK_SIG_START = 0,
  E_KK_SIG_BIT0,
  E_KK_SIG_BIT1,
  E_KK_SIG_SYNC,
  E_KK_SIG_INVALID
}  irda_pro_kk_sig_t;

#if DEFAULT_SAMPLE_FREQ //  Sample freq 187.5 KHz
static u16 konka_sigtbl[16] = {489, 646, 978, 1293, 65, 135, 327, 480, 65, 135, 489, 646, 65,
        135, 653, 1000};
#else //  Sample freq 46.875 KHz
static u16 konka_sigtbl[16] = {130, 160, 260, 300, 20, 40, 87, 100, 20, 40, 135, 148, 20,
      40, 187, 213};
#endif

#if IRDA_DEBUG
static const char * kk_sig_strs[5] = {
"E_KK_SIG_START","E_KK_SIG_BIT0","E_KK_SIG_BIT1",
"E_KK_SIG_SYNC", "E_KK_SIG_INVALID"
};

static const char * kk_sta_strs[6] = {
"E_KK_STA_IDLE","E_KK_STA_START","E_KK_STA_USR",
"E_KK_STA_KEY","E_KK_STA_SYNC","E_KK_STA_FINISH"
};
#endif

#if DEFAULT_SAMPLE_FREQ //  Sample freq 187.5 KHz
static u16 rc5_sigtbl[8] = {133, 266, 266, 433, 433, 600, 533, 800};
#else //  Sample freq 46.875 KHz
static u16 rc5_sigtbl[8] = {34, 67, 67, 109, 109, 150, 134, 200};
#endif

/*!
  IRDA RC5 Decoder State
*/
typedef enum
{
  E_RC5_STA_IDLE = 0,
  E_RC5_STA_S1,
  E_RC5_STA_S2,
  E_RC5_STA_T,
  E_RC5_STA_USR,
  E_RC5_STA_KEY,
  E_RC5_STA_FINISH,
  E_RC5_STA_INVALID
} irda_pro_rc5_status_t;


#define IRDA_TEST 1
#define IRDA_NEC_TEST 1
#define IRDA_SFT_NEC_TEST 0

#ifdef IRDA_TEST

#define IR_BASE  0xbf151000

#ifdef IRDA_WAVE_FILT_TEST
	#define IR_BASE_DEBUG_OFFSET 0x18
#else
	#define IR_BASE_DEBUG_OFFSET 0x90
#endif
#define IRDA_CLK 100
#define SPEED_MULT 10

struct Irdacfg_info_t
{
	unsigned int	idle_ot				;
	unsigned int	repeat_ot			;
	unsigned char	clk_sample			;
	unsigned int	repeat_smp_ctrl		;
	unsigned int	repeat_accept_ctrl	;
	unsigned int	chk_en	;
	unsigned char	user_filter_en		;
	unsigned int	user_filter_data	;
	unsigned char	key_filter_en		;
	unsigned int	key_filter_data		;
	unsigned char	idle_state			;
	unsigned int	phy_period_noise	;
	unsigned char	int_frq				;
	unsigned char	wf_en				;
};
typedef struct Irdacfg_info_t s_irdacfg_info;

unsigned int irda_wave_data[]=//----------FPGA
{
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x06b50695,0x0a0409e4,
0x00750050,0x00e400c4,
0x00750050,0x00e400c4,
0x00750050,0x00e400c4,
0x00750050,0x01ba019a,
0x00750050,0x00e400c4,
0x00750050,0x00e400c4,
0x00750050,0x01ba019a,
0x00750050,0x00e400c4,
0x00750050,0x01ba019a,
0x00750050,0x01ba019a,
0x00750050,0x01ba019a,
0x00750050,0x00e400c4,
0x00750050,0x01ba019a,
0x00750050,0x01ba019a,
0x00750050,0x01ba019a,
0x00750050,0x00e400c4,
0x00750050,0x00e400c4,
0x00750050,0x01ba019a,
0x00750050,0x00e400c4,
0x00750050,0x01ba019a,
0x00750050,0x00e400c4,
0x00750050,0x00e400c4,
0x00750050,0x00e400c4,
0x00750050,0x00e400c4,
0x00750050,0x01ba019a,
0x00750050,0x00e400c4,
0x00750050,0x01ba019a,
0x00750050,0x00e400c4,
0x00750050,0x01ba019a,
0x00750050,0x01ba019a,
0x00750050,0x01ba019a,
0x00750050,0x01ba019a,
0x00750050,0x0fff0ff0,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00ad008d,0x01490120,
0x01490120,0x01e501c5,
0x00ad008d,0x01e501c5,
0x01490120,0x02820262,
0x00ad008d,0x01490120,
0x00ad008d,0x01490120,
0x00ad008d,0x01490120,
0x00ad008d,0x01490120,
0x01490120,0x02820262,
0x01490120,0x0fff0ff0,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff,
0x00000000,0xffffffff

};

unsigned int irda_wave_mask[]=//----------FPGA
{
	0xffffffff,
	0xffffffff,
	0xffffffff,
	0xffffffff
};

#ifdef IRDA_SFT_NEC_TEST
unsigned int irda_sft_signal[]=//----------SIM
{
//(/10/0.00128)=/0.0128

0x02c60290,0x044c03fc,//{9,8.58},{13.5,13.5}	//(9,13.5)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x02c60290,0x044c03fc,//{9,8.58},{13.5,13.5}	//(9,13.5)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x02c60290,0x044c03fc,//{9,8.58},{13.5,13.5}	//(9,13.5)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x02c60290,0x044c03fc,//{9,8.58},{13.5,13.5}	//(9,13.5)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x00d200aa,//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x007d0055,//{0.56,0.14},{1.125,1.125}	//(0.56,1.125)
0x002d000a,0x00d200aa//{0.56,0.14},{2.25,2.25}	//(0.56,2.25)
};
#else
unsigned int irda_sft_signal[]=//----------SIM
{
0x0046001e,0x00aa0080,	//{0.839,0.419},{1.674,1.674}	//(0.839,1.674)
0x0046001e,0x00aa0080,	//{0.839,0.419},{1.674,1.674}	//(0.839,1.674)
0x0087005f,0x01090102,	//{1.674,1.254},{3.348,3.348}	//(1.674,3.348)
0x0087005f,0x01090102,	//{1.674,1.254},{3.348,3.348}	//(1.674,3.348)
0x0087005f,0x00df00be,	//{1.674,1.254},{2.509,2.509}	//(1.674,2.509)
0x0046001e,0x00aa0080,	//{0.839,0.419},{1.674,1.674}	//(0.839,1.674)
0x0046001e,0x00df00be,	//{0.839,0.419},{2.509,2.509}	//(0.839,2.509)
0x0087005f,0x01090102,	//{1.674,1.254},{3.348,3.348}	//(1.674,3.348)
0x0087005f,0x00df00be,	//{1.674,1.254},{2.509,2.509}	//(1.674,2.509)
0x0046001e,0x00aa0080,	//{0.839,0.419},{1.674,1.674}	//(0.839,1.674)
0x0046001e,0x00aa0080,	//{0.839,0.419},{1.674,1.674}	//(0.839,1.674)
0x0087005f,0x01090102,	//{1.674,1.254},{3.348,3.348}	//(1.674,3.348)
0x0087005f,0x01090102,	//{1.674,1.254},{3.348,3.348}	//(1.674,3.348)
0x0087005f,0x00df00be,	//{1.674,1.254},{2.509,2.509}	//(1.674,2.509)
0x0046001e,0x00aa0080,	//{0.839,0.419},{1.674,1.674}	//(0.839,1.674)
0x0046001e,0x00df00be,	//{0.839,0.419},{2.509,2.509}	//(0.839,2.509)
0x0087005f,0x01090102,	//{1.674,1.254},{3.348,3.348}	//(1.674,3.348)
0x0087005f,0x00df00be,	//{1.674,1.254},{2.509,2.509}	//(1.674,2.509)
0x0046001e,0x00aa0080,	//{0.839,0.419},{1.674,1.674}	//(0.839,1.674)
0x0046001e,0x00aa0080,	//{0.839,0.419},{1.674,1.674}	//(0.839,1.674)
0x0087005f,0x01090102,	//{1.674,1.254},{3.348,3.348}	//(1.674,3.348)
0x0087005f,0x01090102,	//{1.674,1.254},{3.348,3.348}	//(1.674,3.348)
0x0087005f,0x00df00be,	//{1.674,1.254},{2.509,2.509}	//(1.674,2.509)
0x0046001e,0x00aa0080,	//{0.839,0.419},{1.674,1.674}	//(0.839,1.674)
0x0046001e,0x00df00be,	//{0.839,0.419},{2.509,2.509}	//(0.839,2.509)
0x0087005f,0x01090102,	//{1.674,1.254},{3.348,3.348}	//(1.674,3.348)
0x0087005f,0x00df00be,	//{1.674,1.254},{2.509,2.509}	//(1.674,2.509)
0x0046001e,0x00aa0080,	//{0.839,0.419},{1.674,1.674}	//(0.839,1.674)
0x0046001e,0x00aa0080,	//{0.839,0.419},{1.674,1.674}	//(0.839,1.674)
0x0087005f,0x01090102,	//{1.674,1.254},{3.348,3.348}	//(1.674,3.348)
0x0087005f,0x01090102,	//{1.674,1.254},{3.348,3.348}	//(1.674,3.348)
0x0087005f,0x00df00be,	//{1.674,1.254},{2.509,2.509}	//(1.674,2.509)
0x0046001e,0x00aa0080,	//{0.839,0.419},{1.674,1.674}	//(0.839,1.674)
0x0046001e,0x00df00be,	//{0.839,0.419},{2.509,2.509}	//(0.839,2.509)
0x0087005f,0x01090102,	//{1.674,1.254},{3.348,3.348}	//(1.674,3.348)
0x0087005f,0x00df00be,	//{1.674,1.254},{2.509,2.509}	//(1.674,2.509)

};//RC5
#endif

#endif

/*****************************************************************
* IRDA internal APIs declaration
*****************************************************************/
void irda_nec_test(s_irdacfg_info irdacfg);
void irda_nec_check(s_irdacfg_info irdacfg);
void irda_soft_test(s_irdacfg_info irdacfg);
void irda_sft_check();

#ifdef IRDA_TEST
void ot_cfg(s_irdacfg_info irdacfg)
{
	unsigned int read_temp;
	//unsigned int a,b,c,d,e;

//	//108/SPEED_MULT/t_smp_ms/0xfff=2				//-------------SIM
//	unsigned int repeat_ot = 2;

	//108/SPEED_MULT/t_smp_ms/0xfff=2				//-------------FPGA
	unsigned int repeat_ot = 15;

	read_temp = *((volatile unsigned int *)( IR_BASE + 0x8 ));
	*((volatile unsigned int *)( IR_BASE + 0x8 )) = ((read_temp & 0xff80ffff) | (irdacfg.idle_ot<<20) | (repeat_ot<<16));

}

void nec_time_cfg(irdacfg)
{
	//unsigned int t_smp_ms,clk_div,cfg_mult,cfg_mult_min,cfg_mult_max;

	//187.5k~~0.0053ms
	//90/0.053/4 = 9/0.0053/4

	//(187.5k/24M)*100M=781k~~~~~~~~0.00128ms
	//90/0.128/4 = 0.9/0.00128/4

	//////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~cfg_mult~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~				//-------------SIM
	////if((irdacfg.clk_sample == 0)) {clk_div = 8;		}
	////if((irdacfg.clk_sample == 1)) {clk_div = 32;    }
	////if((irdacfg.clk_sample == 2)) {clk_div = 128;   }
	////if((irdacfg.clk_sample == 3)) {clk_div = 512;   }
	////
	////t_smp_ms = clk_div/IRDA_CLK/1000;//0.00128ms
	////cfg_mult = 1/SPEED_MULT/t_smp_ms/4;
	////cfg_mult = 19.53;
	//////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~cfg_mult~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//
	//unsigned int start_on_h = 190;//9 * cfg_mult;			//175.2
	//unsigned int start_on_l = 160;//(9-0.42) * cfg_mult;	//167.5
	//unsigned int start_pd_h = 275;//13.5 * cfg_mult;		//261.6
	//unsigned int start_pd_l = 250;//13.5 * cfg_mult;
	//unsigned int repeat_on_h = 190;//9 * cfg_mult;			//175.2
	//unsigned int repeat_on_l = 160;//(9-0.42) * cfg_mult;	//167.5
	//unsigned int repeat_pd_h = 235;//11.25 * cfg_mult;		//219.7
	//unsigned int repeat_pd_l = 205;//11.25 * cfg_mult;
	//unsigned int data1_on_h = 18;//0.56 * cfg_mult;			//10.92
	//unsigned int data1_on_l = 2;//(0.56-0.42) * cfg_mult;	//2.73
	//unsigned int data1_pd_h = 58;//2.25 * cfg_mult;			//43.92
	//unsigned int data1_pd_l = 32;//2.25 * cfg_mult;
	//unsigned int data0_on_h = 20;//0.56 * cfg_mult;			//10.92
	//unsigned int data0_on_l = 2;//(0.56-0.42) * cfg_mult;	//2.73
	//unsigned int data0_pd_h = 32;//1.125 * cfg_mult;		//21.6
	//unsigned int data0_pd_l = 10;//1.125 * cfg_mult;


	////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~cfg_mult~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~				//-------------FPGA
	//if((irdacfg.clk_sample == 0)) {clk_div = 8;		}
	//if((irdacfg.clk_sample == 1)) {clk_div = 32;    }
	//if((irdacfg.clk_sample == 2)) {clk_div = 128;   }
	//if((irdacfg.clk_sample == 3)) {clk_div = 512;   }
	//
	//t_smp_ms = clk_div/IRDA_CLK/1000;//0.0053ms
	//cfg_mult = 1/SPEED_MULT/t_smp_ms/4;
	//cfg_mult = 47.1;
	////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~cfg_mult~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	/*
	*	default 27M param
	*/
	unsigned int start_on_h = 548;//9 * cfg_mult;			//175.2
	unsigned int start_on_l = 295;//(9-0.42) * cfg_mult;	//167.5
	unsigned int start_pd_h = 822;//13.5 * cfg_mult;		//261.6
	unsigned int start_pd_l = 571;//13.5 * cfg_mult;
	unsigned int repeat_on_h = 548;//9 * cfg_mult;			//175.2
	unsigned int repeat_on_l = 295;//(9-0.42) * cfg_mult;	//167.5
	unsigned int repeat_pd_h = 822;//11.25 * cfg_mult;		//219.7
	unsigned int repeat_pd_l = 512;//11.25 * cfg_mult;
	unsigned int data1_on_h = 40;//0.56 * cfg_mult;			//10.92
	unsigned int data1_on_l = 5;//(0.56-0.42) * cfg_mult;	//2.73
	unsigned int data1_pd_h = 130;//2.25 * cfg_mult;			//43.92
	unsigned int data1_pd_l = 75;//2.25 * cfg_mult;
	unsigned int data0_on_h = 40;//0.56 * cfg_mult;			//10.92
	unsigned int data0_on_l = 5;//(0.56-0.42) * cfg_mult;	//2.73
	unsigned int data0_pd_h = 65;//1.125 * cfg_mult;		//21.6
	unsigned int data0_pd_l = 40;//1.125 * cfg_mult;

	/*
	*	24M param
	*/
	unsigned int start_on_h_24m = 487;//9 * cfg_mult;			//175.2
	unsigned int start_on_l_24m = 262;//(9-0.42) * cfg_mult;	//167.5
	unsigned int start_pd_h_24m = 730;//13.5 * cfg_mult;		//261.6
	unsigned int start_pd_l_24m = 507;//13.5 * cfg_mult;
	unsigned int repeat_on_h_24m = 548;//9 * cfg_mult;			//175.2
	unsigned int repeat_on_l_24m = 262;//(9-0.42) * cfg_mult;	//167.5
	unsigned int repeat_pd_h_24m = 730;//11.25 * cfg_mult;		//219.7
	unsigned int repeat_pd_l_24m = 455;//11.25 * cfg_mult;
	unsigned int data1_on_h_24m = 35;//0.56 * cfg_mult;			//10.92
	unsigned int data1_on_l_24m = 4;//(0.56-0.42) * cfg_mult;	//2.73
	unsigned int data1_pd_h_24m = 115;//2.25 * cfg_mult;			//43.92
	unsigned int data1_pd_l_24m = 66;//2.25 * cfg_mult;
	unsigned int data0_on_h_24m = 35;//0.56 * cfg_mult;			//10.92
	unsigned int data0_on_l_24m = 4;//(0.56-0.42) * cfg_mult;	//2.73
	unsigned int data0_pd_h_24m = 57;//1.125 * cfg_mult;		//21.6
	unsigned int data0_pd_l_24m = 35;//1.125 * cfg_mult;

	if(((*(volatile unsigned int *)0xBF140020)&(1<<30))==0)
	{
		*((volatile unsigned int *)( IR_BASE + 0x40 ))= (start_on_h<<16) | start_on_l;//	start on time
		*((volatile unsigned int *)( IR_BASE + 0x44 ))= (start_pd_h<<16) | start_pd_l;//	start period
		*((volatile unsigned int *)( IR_BASE + 0x48 ))= (repeat_on_h<<16) | repeat_on_l;//	repeat on time
		*((volatile unsigned int *)( IR_BASE + 0x4c ))= (repeat_pd_h<<16) | repeat_pd_l;//	repeat period
		*((volatile unsigned int *)( IR_BASE + 0x50 ))= (data1_on_h<<16) | data1_on_l;//	data1 on time
		*((volatile unsigned int *)( IR_BASE + 0x54 ))= (data1_pd_h<<16) | data1_pd_l;//	data1 on time
		*((volatile unsigned int *)( IR_BASE + 0x58 ))= (data0_on_h<<16) | data0_on_l;//	data0 on time
		*((volatile unsigned int *)( IR_BASE + 0x5c ))= (data0_pd_h<<16) | data0_pd_l;//	data0 on time
	}
	else
	{
		*((volatile unsigned int *)( IR_BASE + 0x40 ))= (start_on_h_24m<<16) | start_on_l_24m;//	start on time
		*((volatile unsigned int *)( IR_BASE + 0x44 ))= (start_pd_h_24m<<16) | start_pd_l_24m;//	start period
		*((volatile unsigned int *)( IR_BASE + 0x48 ))= (repeat_on_h_24m<<16) | repeat_on_l_24m;//	repeat on time
		*((volatile unsigned int *)( IR_BASE + 0x4c ))= (repeat_pd_h_24m<<16) | repeat_pd_l_24m;//	repeat period
		*((volatile unsigned int *)( IR_BASE + 0x50 ))= (data1_on_h_24m<<16) | data1_on_l_24m;//	data1 on time
		*((volatile unsigned int *)( IR_BASE + 0x54 ))= (data1_pd_h_24m<<16) | data1_pd_l_24m;//	data1 on time
		*((volatile unsigned int *)( IR_BASE + 0x58 ))= (data0_on_h_24m<<16) | data0_on_l_24m;//	data0 on time
		*((volatile unsigned int *)( IR_BASE + 0x5c ))= (data0_pd_h_24m<<16) | data0_pd_l_24m;//	data0 on time
	}


}

void clksmp_cfg(s_irdacfg_info irdacfg)
{
	unsigned int read_temp;
	read_temp = *((volatile unsigned int *)( IR_BASE + 0x8 ));
	*((volatile unsigned int *)( IR_BASE + 0x8 )) = ((read_temp & 0xffffffe7) | (irdacfg.clk_sample<<3));

}

void user_filter(s_irdacfg_info irdacfg)
{
	unsigned int read_temp;
	read_temp = *((volatile unsigned int *)( IR_BASE + 0x18 ));
	*((volatile unsigned int *)( IR_BASE + 0x18 )) = ((read_temp & 0x7f0000ff) | (irdacfg.user_filter_en<<31) | (irdacfg.user_filter_data<<8));
}

void key_filter(s_irdacfg_info irdacfg)
{
	unsigned int read_temp;
	read_temp = *((volatile unsigned int *)( IR_BASE + 0x18 ));
	*((volatile unsigned int *)( IR_BASE + 0x18 )) = ((read_temp & 0xbfffff00) | (irdacfg.key_filter_en<<30) | (irdacfg.key_filter_data));
}

void rpt_smp_ctrl(s_irdacfg_info irdacfg)
{
	unsigned int read_temp;
	read_temp = *((volatile unsigned int *)( IR_BASE + 0x8 ));
	*((volatile unsigned int *)( IR_BASE + 0x8 )) = ((read_temp & 0xfffff0bf) | (irdacfg.repeat_smp_ctrl<<8) | (irdacfg.chk_en<<6));
}

void rpt_accept_mode(s_irdacfg_info irdacfg)
{
	unsigned int read_temp;
	read_temp = *((volatile unsigned int *)( IR_BASE + 0x8 ));
	*((volatile unsigned int *)( IR_BASE + 0x8 )) = ((read_temp & 0xffffffdf) | (irdacfg.repeat_accept_ctrl<<5));
}

void idle_state(s_irdacfg_info irdacfg)
{
	unsigned int read_temp;
	read_temp = *((volatile unsigned int *)( IR_BASE + 0x8 ));
	*((volatile unsigned int *)( IR_BASE + 0x8 )) = ((read_temp & 0xfffffffb) | (irdacfg.idle_state<<2));
}

void noise_set(s_irdacfg_info irdacfg)
{
	unsigned int read_temp;

	read_temp = *((volatile unsigned int *)( IR_BASE + 0x8 ));
	*((volatile unsigned int *)( IR_BASE + 0x8 )) = ((read_temp & 0x00ffffff) | (irdacfg.phy_period_noise<<24));
}

void com_int_frq(s_irdacfg_info irdacfg)
{
	unsigned int read_temp;
	read_temp = *((volatile unsigned int *)( IR_BASE + 0x10 ));
	*((volatile unsigned int *)( IR_BASE + 0x10 )) = ((read_temp & 0xffffc0ff) | (irdacfg.int_frq<<8));
}

void nec_en()
{
	unsigned int read_temp;
	read_temp = *((volatile unsigned int *)( IR_BASE + 0x8 ));
	*((volatile unsigned int *)( IR_BASE + 0x8 )) = ((read_temp & 0xfffffffc) | 0x1);
	read_temp = *((volatile unsigned int *)( IR_BASE + 0x10 ));
	*((volatile unsigned int *)( IR_BASE + 0x10 )) = ((read_temp & 0xffffff00) | 0x3);
}

void soft_en()
{
	unsigned int read_temp;
	read_temp = *((volatile unsigned int *)( IR_BASE + 0x8 ));
	*((volatile unsigned int *)( IR_BASE + 0x8 )) = ((read_temp & 0xfffffffc) | 0x3);
	read_temp = *((volatile unsigned int *)( IR_BASE + 0x10 ));
	*((volatile unsigned int *)( IR_BASE + 0x10 )) = ((read_temp & 0xffffff00) | 0xc);
}

void filt_info_set()//----------FPGA
{
	unsigned int read_temp;
	unsigned long i=0, len=0;
	//unsigned long addr_cnt=0;

	read_temp = *((volatile unsigned int *)( IR_BASE + 0x80 ));
	*((volatile unsigned int *)( IR_BASE + 0x80 )) = ((read_temp & 0x0) | 0x210b);
	read_temp = *((volatile unsigned int *)( IR_BASE + 0x84 ));
	*((volatile unsigned int *)( IR_BASE + 0x84 )) = ((read_temp & 0x0) | 0x0943);
	read_temp = *((volatile unsigned int *)( IR_BASE + 0x88 ));
	*((volatile unsigned int *)( IR_BASE + 0x88 )) = ((read_temp & 0x0) | 0x0000);
	read_temp = *((volatile unsigned int *)( IR_BASE + 0x8c ));
	*((volatile unsigned int *)( IR_BASE + 0x8c )) = ((read_temp & 0x0) | 0x0000);

	len		= (sizeof(irda_wave_data)/8);
	for ( i=0; i<len; i++ )
	{
		*((volatile unsigned int *)( IR_BASE + 0x400 + i*8 )) = irda_wave_data[i*2];
		*((volatile unsigned int *)( IR_BASE + 0x404 + i*8 )) = irda_wave_data[i*2+1];
	}

	len		= (sizeof(irda_wave_mask)/4);
	for ( i=0; i<len; i++ )
	{
		*((volatile unsigned int *)( IR_BASE + 4*i +0x90 )) = irda_wave_mask[i];
	}

}


void filt_en(s_irdacfg_info irdacfg)
{
	unsigned int read_temp;
	read_temp = *((volatile unsigned int *)( IR_BASE + 0x8 ));
	*((volatile unsigned int *)( IR_BASE + 0x8 )) = ((read_temp & 0xfffffffc) | 0x1);
	read_temp = *((volatile unsigned int *)( IR_BASE + 0x8 ));
	*((volatile unsigned int *)( IR_BASE + 0x8 )) = ((read_temp & 0xfffff0ff) | (irdacfg.wf_en<<12));
	read_temp = *((volatile unsigned int *)( IR_BASE + 0x10 ));
	*((volatile unsigned int *)( IR_BASE + 0x10 )) = ((read_temp & 0xffffff00) | (irdacfg.wf_en<<4));
}

void check_fifo_overflow()
{
	while( (*((volatile unsigned int *)( IR_BASE + 0x0c )) & 0x2 ) != 0x2 );
}

void check_fifo_num_36()
{
	while( (*((volatile unsigned int *)( IR_BASE + 0x0c )) & 0xff00 ) != 0x2400 );//0x24~~36
}

void check_sft_down_int()
{
	while( (*((volatile unsigned int *)( IR_BASE + 0x14 )) & 0x4 ) != 0x4 );
}

void check_nec_down_int()
{
	while( (*((volatile unsigned int *)( IR_BASE + 0x14 )) & 0x1 ) != 0x1 );
	OS_PRINTF("nec_down!\n");
}

void check_nec_up_int()
{
	while( (*((volatile unsigned int *)( IR_BASE + 0x14 )) & 0x2 ) != 0x2 );
	OS_PRINTF("nec_up!\n");
}

void check_nec_data(s_irdacfg_info irdacfg)
{
	unsigned int read_temp;

	read_temp = *((volatile unsigned int *)( IR_BASE + 0x00 ));
	OS_PRINTF("NEC_DATA = %x\n",read_temp);
}

void check_repeat_flag()
{
	while( (*((volatile unsigned int *)( IR_BASE + 0xc )) & 0x1 ) != 0x1 );
	OS_PRINTF("repeat!\n");
}

void check_filt_int(unsigned int filt_int_expect)
{
	while( (*((volatile unsigned int *)( IR_BASE + 0x14 )) & 0x000000f0 ) != (filt_int_expect<<4) );
	while( (*((volatile unsigned int *)( IR_BASE + 0x14 )) & 0x000000f0 ) != 0x0 );

	OS_PRINTF("FILT_CH = %x\n",filt_int_expect);

}

void check_sft_signal()
{
	unsigned int read_temp;
	unsigned long i=0, len=0;
	unsigned long read_period,read_on;
	unsigned long /*ontime,offtime,pdtime,*/pd_cnt_min,pd_cnt_max,on_cnt_min,on_cnt_max;

	//1/10/SPEED_MULT/t_smp_ms/=7.8125
	//unsigned long mux_min=7,mux_max=8;


	len		= (sizeof(irda_sft_signal)/8);
	for ( i=0; i<len; i++ )
	{

		on_cnt_max = (irda_sft_signal[i*2] & 0xffff0000) >>16;
		on_cnt_min = irda_sft_signal[i*2] & 0x0000ffff;
		pd_cnt_max = (irda_sft_signal[i*2+1] & 0xffff0000) >>16;
		pd_cnt_min = irda_sft_signal[i*2+1] & 0x0000ffff;


		//offtime = irda_sft_signal[i*2+1];
		//pdtime = ontime + offtime;
		//pd_cnt_min = pdtime*mux_min;
		//pd_cnt_max = pdtime*mux_max;
		//on_cnt_min = ontime*mux_min;
		//on_cnt_max = ontime*mux_max;

		read_temp = *((volatile unsigned int *)( IR_BASE + 0x1c ));
		*((volatile unsigned int *)( IR_BASE + 0x1c )) = ((read_temp & 0xfffffffe) | 0x1);//buf_pop

		read_temp = *((volatile unsigned int *)( IR_BASE + 0x04 ));
		read_period = ((read_temp & 0xffff0000)>>16);
		read_on = read_temp & 0x0000ffff;

		while((read_period<pd_cnt_min)|(read_period>pd_cnt_max)|(read_on<on_cnt_min)|(read_on>on_cnt_max));
	}
}

void read_sft_signal()
{
	unsigned int num=0,/*flow=0,*/read_temp,i;

	//num = (*((volatile unsigned int *)( IR_BASE + 0x0c )) & 0xff00 )>>8;
	//if(num == 0) {OS_PRINTF("initial:fifo is empty~~~please send!!!\n\n\n");}
	//else
	//	{
	//		for ( i=0; i<num; i++ )
	//		{
	//			read_temp = *((volatile unsigned int *)( IR_BASE + 0x1c ));
	//			*((volatile unsigned int *)( IR_BASE + 0x1c )) = (read_temp & 0xfffffffe | 0x1);//buf_pop
	//			read_temp = *((volatile unsigned int *)( IR_BASE + 0x04 ));
	//		}
	//		{OS_PRINTF("initial:fifo is cleared~~~please send!!!\n\n\n");}
	//	}
	//
	//while(flow == 0)
	//{
	//	flow = (*((volatile unsigned int *)( IR_BASE + 0x0c )) & 0x2 )>>1;
	//}
	//OS_PRINTF("fifo is flow!\n");

	while(num < 34)
	{
	num = (*((volatile unsigned int *)( IR_BASE + 0x0c )) & 0xff00 )>>8;
	}
	OS_PRINTF("fifo_num = %d\n",num);

	for ( i=0; i<num; i++ )
	{
		read_temp = *((volatile unsigned int *)( IR_BASE + 0x1c ));
		*((volatile unsigned int *)( IR_BASE + 0x1c )) = ((read_temp & 0xfffffffe) | 0x1);//buf_pop
		read_temp = *((volatile unsigned int *)( IR_BASE + 0x04 ));
		OS_PRINTF("sft_signal = %x\n",read_temp);
	}

}

#endif


#ifdef IRDA_TEST
void irda_test()
{
	unsigned int i;
	s_irdacfg_info irdacfg;

#ifdef IRDA_NEC_TEST

	//*((volatile unsigned int *)( IR_BASE + IR_BASE_DEBUG_OFFSET )) = 0xffff0000;//debug

	OS_PRINTF("IRDA_NEC_TEST!\n");
	irdacfg.idle_ot				=7;
	irdacfg.clk_sample			=2;
	irdacfg.repeat_smp_ctrl		=2;
	irdacfg.repeat_accept_ctrl	=1;
	irdacfg.chk_en						=0;
	irdacfg.user_filter_en		=0;
	irdacfg.user_filter_data	=0x5678;
	irdacfg.key_filter_en		=0;
	irdacfg.key_filter_data		=0x34;
	irdacfg.idle_state			=1;
	irdacfg.phy_period_noise	=20;	//420us/10/0.00128ms/4=8.2~~~~~~~~~~~~1/SPEED_MULT/t_smp_ms/4;	//----------SIM
																//420us/1/0.00533ms/4=19.8~~~~~~~~~~~~1/SPEED_MULT/t_smp_ms/4;			//----------FPGA

	irda_nec_test(irdacfg);

	for ( i=0; i<5; i++ )
	{
	OS_PRINTF("round = %d:\n",i);
	OS_PRINTF("reg(0xBF0A0000) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF0A0000));
	OS_PRINTF("reg(0xBF0A0004) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF0A0004));
	OS_PRINTF("reg(0xBF0A0008) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF0A0008));
	OS_PRINTF("reg(0xBF0A000c) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF0A000c));
	OS_PRINTF("reg(0xBF0A0010) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF0A0010));
	OS_PRINTF("reg(0xBF0A0014) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF0A0014));
	OS_PRINTF("reg(0xBF0A0018) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF0A0018));
	irda_nec_check(irdacfg);
	}

	*((volatile unsigned int *)( IR_BASE + IR_BASE_DEBUG_OFFSET )) = 0xffff0fff;//debug
#endif

#ifdef IRDA_SFT_NEC_TEST

	*((volatile unsigned int *)( IR_BASE + IR_BASE_DEBUG_OFFSET )) = 0xffff0000;//debug

	OS_PRINTF("IRDA_SFT_NEC_TEST!\n");

	irdacfg.idle_ot				=7;
	irdacfg.clk_sample			=2;
	irdacfg.idle_state			=1;
	irdacfg.phy_period_noise	=16;	//420us/10/0.00128ms/4=8.2~~~~~~~~~~~~1/SPEED_MULT/t_smp_ms/4;	//----------SIM
																//420us/1/0.00533ms/4=19.8~~~~~~~~~~~~1/SPEED_MULT/t_smp_ms/4;			//----------FPGA

	irda_soft_test(irdacfg);
	OS_PRINTF("reg(0xBF0A0008) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF0A0008));
	for ( i=0; i<5; i++ )
	{
	OS_PRINTF("round = %d:\n",i);
	irda_sft_check();
	}

	*((volatile unsigned int *)( IR_BASE + IR_BASE_DEBUG_OFFSET )) = 0xffff1fff;//debug
#endif

#ifdef IRDA_SFT_RC5_TEST
	*((volatile unsigned int *)( IR_BASE + IR_BASE_DEBUG_OFFSET )) = 0xffff0000;//debug

	OS_PRINTF("IRDA_SFT_RC5_TEST!\n");

	irdacfg.idle_ot				=7;
	irdacfg.clk_sample			=2;
	irdacfg.idle_state			=1;
	irdacfg.phy_period_noise	=28;	//600us/10/0.00128ms/4=11.7~~~~~~~~~~~~1/SPEED_MULT/t_smp_ms/4;	//----------SIM
																//600us/1/0.00533ms/4=28.1~~~~~~~~~~~~1/SPEED_MULT/t_smp_ms/4;			//----------FPGA

	irda_soft_test(irdacfg);

	for ( i=0; i<5; i++ )
	{
	OS_PRINTF("round = %d:\n",i);
	irda_sft_check();
	}

	*((volatile unsigned int *)( IR_BASE + IR_BASE_DEBUG_OFFSET )) = 0xffff2fff;//debug
#endif


#ifdef IRDA_WAVE_FILT_TEST
	*((volatile unsigned int *)( IR_BASE + IR_BASE_DEBUG_OFFSET )) = 0xffff0000;//debug

	OS_PRINTF("IRDA_WAVE_FILT_TEST!\n");

	irdacfg.idle_ot				=7;
	irdacfg.clk_sample			=2;
	irdacfg.idle_state			=1;
	irdacfg.phy_period_noise	=20;	//150us/10/0.00128ms/4=2.9~~~~~~~~~~~~1/SPEED_MULT/t_smp_ms/4;	//----------SIM
																//420us/1/0.00533ms/4=19.8~~~~~~~~~~~~1/SPEED_MULT/t_smp_ms/4;			//----------FPGA

	irdacfg.wf_en				=0x3;

	irda_filt_test(irdacfg);

	for ( i=0; i<5; i++ )
	{
	OS_PRINTF("round = %d:\n",i);
	irda_filt_check(irdacfg);
	}

	*((volatile unsigned int *)( IR_BASE + IR_BASE_DEBUG_OFFSET )) = 0xffff3fff;//debug
#endif

}

void irda_nec_test(s_irdacfg_info irdacfg)
{
	ot_cfg(irdacfg);
	clksmp_cfg(irdacfg);
	idle_state(irdacfg);
	noise_set(irdacfg);

	nec_time_cfg(irdacfg);
	rpt_smp_ctrl(irdacfg);
	user_filter(irdacfg);
	key_filter(irdacfg);
	rpt_accept_mode(irdacfg);//1:accept all simple-code

	nec_en();//mode=0;IE_NEC_DN;IE_NEC_UP
}

void irda_soft_test(s_irdacfg_info irdacfg)
{
	ot_cfg(irdacfg);
	clksmp_cfg(irdacfg);
	idle_state(irdacfg);
	noise_set(irdacfg);

	soft_en();//mode=1;IE_NEC_DN;IE_NEC_UP
}

void irda_filt_test(s_irdacfg_info irdacfg)//pay attention to ir_debuf_offset
{
	ot_cfg(irdacfg);
	clksmp_cfg(irdacfg);
	idle_state(irdacfg);
	noise_set(irdacfg);

	filt_info_set();//parameter,wf_len,wf_staddr
	filt_en(irdacfg);//mode=0;WF_EN;IE_WF
}

void irda_nec_check(s_irdacfg_info irdacfg)
{
	check_nec_down_int();
	check_repeat_flag();
	check_nec_data(irdacfg);
	check_nec_up_int();
}

void irda_sft_check()
{
	read_sft_signal();
}

void irda_sft_nec_check(s_irdacfg_info irdacfg)
{
	check_fifo_overflow();
	check_sft_signal();
}

void irda_sft_rc5_check(s_irdacfg_info irdacfg)
{
	check_fifo_num_36();
	check_sft_signal();
}

void irda_filt_check(s_irdacfg_info irdacfg)
{
	check_filt_int(0x2);
	check_filt_int(0x1);
}
#endif

/*****************************************************************
* IRDA internal functions
*****************************************************************/
static void irda_symphony_get_ontime_and_period(u16 * p_ontime, u16 * p_period)
{
  u32 ptmp = 0;
  u32 dtmp = 0;

  u16 m1dat_ont;
  u16 m1dat_prd;

  // Data FIFO Pop
  ptmp = SYMPHONY_IR_COMBUF_POP;
  dtmp = hal_get_u32((volatile u32 *)ptmp);
  dtmp |= 0x1;
  hal_put_u32((volatile u32 *)ptmp, dtmp);

  // pop 2nd to confirm pop success
  ptmp = SYMPHONY_IR_COMBUF_POP;
  dtmp = hal_get_u32((volatile u32 *)ptmp);
  dtmp |= 0x1;
  //hal_put_u32((volatile u32 *)ptmp, dtmp);

  // Data FIFO fetch
  ptmp = SYMPHONY_IR_COM_DATA;
  dtmp = hal_get_u32((volatile u32 *)ptmp);
  IRDA_PRINT("(line %d)dtmp [0x%x]\n",__LINE__,dtmp);

  m1dat_ont = dtmp & 0xFFFF;
  m1dat_prd = (dtmp & 0xFFFF0000) >> 16;

  *p_ontime = m1dat_ont;
  *p_period = m1dat_prd;
}

static u8 irda_symphony_get_recv_num(void)
{
  return ((hal_get_u32((volatile u32 *)SYMPHONY_IR_GLOBAL_STA) >> 8) & 0xFF);
}

static u8 irda_symphony_nec_decoder_parse_sig(u16 ontime, u16 period, u16 * sigtbl)
{
  if((ontime >= sigtbl[0]) && (ontime <= sigtbl[1]) &&
      (period >= sigtbl[2]) && (period <= sigtbl[3]))
  {
    return E_NEC_SIG_START;
  }
  else if((ontime >= sigtbl[4]) && (ontime <= sigtbl[5]) &&
          (period >= sigtbl[6]) && (period <= sigtbl[7]))
  {
    return E_NEC_SIG_0;
  }
  else if((ontime >= sigtbl[8]) && (ontime <= sigtbl[9]) &&
          (period >= sigtbl[10]) && (period <= sigtbl[11]))
  {
    return E_NEC_SIG_1;
  }
  else if((ontime >= sigtbl[12]) && (ontime <= sigtbl[13]) &&
          (period >= sigtbl[14]) && (period <= sigtbl[15]))
  {
    return E_NEC_SIG_REPEAT;
  }
  else
  {
    return E_NEC_SIG_INVALID;
  }
}

inline static BOOL irda_symphony_nec_decode_check_keycode(u16 code)
{
  u8 ch = (code >> 8) & 0xff;
  u8 cl = code & 0xff;
  if((ch + cl) == 0xff)
    return TRUE;
  else
    return FALSE;
}

inline static void irda_symphony_nec_reset_decoder(irda_dec_result_t *p_result)
{
  p_result->state = E_NEC_STA_IDLE;
  p_result->counter = 0;
  p_result->r.nec.code = 0;
  p_result->r.nec.usercode = 0;
}

/*!
  IRDA NEC Decoder
*/
static void irda_symphony_nec_decoder(irda_dec_para_t *p_para, irda_dec_result_t *p_result)
{
  u8 sig = irda_symphony_nec_decoder_parse_sig(p_para->ontime, p_para->period, p_result->p_sigtbl);
  IRDA_PRINT("\nnec irda_decoder sig=%s, state=%s\n", nec_sig_strs[sig],nec_sta_strs[p_result->state]);
  mtos_printk(" BKS [%s] %d \n",__FUNCTION__, __LINE__ );
  if(p_result->state == E_NEC_STA_IDLE)
  {
    if(sig == E_NEC_SIG_START)
    {
      p_result->counter = 0;
      p_result->state = E_NEC_STA_START;
    }
    else if(sig == E_NEC_SIG_REPEAT)
    {
	 p_result->state = E_NEC_STA_REPEAT;
    }
    else
    {
      p_result->counter = 0;
    }
  }
  else if(p_result->state == E_NEC_STA_START)
  {
    if(sig == E_NEC_SIG_0 || sig == E_NEC_SIG_1)
    {
      p_result->state = E_NEC_STA_USRL;
      if(sig == E_NEC_SIG_0)
      {
        p_result->r.nec.usercode &= ~(1 << p_result->counter);
      }
      else
      {
        p_result->r.nec.usercode |= (1 << p_result->counter);
      }
    }
    else
    {
      irda_symphony_nec_reset_decoder(p_result);
    }
  }
  else if(p_result->state == E_NEC_STA_USRL)
  {
    if(sig == E_NEC_SIG_0 || sig == E_NEC_SIG_1)
    {
      p_result->counter ++;
      if(p_result->counter < 8)
      {
        if(sig == E_NEC_SIG_0)
        {
          p_result->r.nec.usercode &= ~(1 << p_result->counter);
        }
        else
        {
          p_result->r.nec.usercode |= (1 << p_result->counter);
        }
      }
      else
      {
        p_result->counter = 0;
        p_result->state = E_NEC_STA_USRH;
        if(sig == E_NEC_SIG_0)
        {
          p_result->r.nec.usercode &= ~(1 << (p_result->counter + 8));
        }
        else
        {
          p_result->r.nec.usercode |= (1 << (p_result->counter + 8));
        }
      }
    }
    else
    {
      irda_symphony_nec_reset_decoder(p_result);
    }
  }
  else if(p_result->state == E_NEC_STA_USRH)
  {
    if(sig == E_NEC_SIG_0 || sig == E_NEC_SIG_1)
    {
      p_result->counter ++;
      if(p_result->counter < 8)
      {
        if(sig == E_NEC_SIG_0)
        {
          p_result->r.nec.usercode &= ~(1 << (p_result->counter + 8));
        }
        else
        {
          p_result->r.nec.usercode |= (1 << (p_result->counter + 8));
        }
      }
      else
      {
        p_result->counter = 0;
        p_result->state = E_NEC_STA_KEY;
        if(sig == E_NEC_SIG_0)
        {
          p_result->r.nec.code &= ~(1 << p_result->counter);
        }
        else
        {
          p_result->r.nec.code |= (1 << p_result->counter);
        }
      }
    }
    else
    {
      irda_symphony_nec_reset_decoder(p_result);
    }
  }
  else if(p_result->state == E_NEC_STA_KEY)
  {
    if(sig == E_NEC_SIG_0 || sig == E_NEC_SIG_1)
    {
      p_result->counter ++;
      if(p_result->counter < 8)
      {
        if(sig == E_NEC_SIG_0)
        {
          p_result->r.nec.code &= ~(1 << p_result->counter);
        }
        else
        {
          p_result->r.nec.code |= (1 << p_result->counter);
        }
      }
      else
      {
        p_result->counter = 0;
        p_result->state = E_NEC_STA_YEK;
        if(sig == E_NEC_SIG_0)
        {
          p_result->r.nec.code &= ~(1 << (p_result->counter + 8));
        }
        else
        {
          p_result->r.nec.code |= (1 << (p_result->counter + 8));
        }
      }
    }
    else
    {
      irda_symphony_nec_reset_decoder(p_result);
    }
  }
  else if(p_result->state == E_NEC_STA_YEK)
  {
    if(sig == E_NEC_SIG_0 || sig == E_NEC_SIG_1)
    {
      p_result->counter ++;
      if(p_result->counter < 8)
      {
        if(sig == E_NEC_SIG_0)
        {
          p_result->r.nec.code &= ~(1 << (p_result->counter + 8));
        }
        else
        {
          p_result->r.nec.code |= (1 << (p_result->counter + 8));
        }
        if(p_result->counter == 7)
        {
          p_result->state = E_NEC_STA_FINISH;
        }
      }
    }
    else
    {
      irda_symphony_nec_reset_decoder(p_result);
    }
  }
  else if(p_result->state == E_NEC_STA_FINISH)
  {
    if(sig == E_NEC_SIG_START)
    {
      p_result->state = E_NEC_STA_START;
      p_result->counter = 0;
      p_result->r.nec.code = 0;
      p_result->r.nec.usercode = 0;
    }
    else if(sig == E_NEC_SIG_REPEAT)
    {
      p_result->counter = 0;
      if(p_result->repeat_interval == 0)
      {
        p_result->state = E_NEC_STA_FINISH;
      }
      else
      {
        p_result->state = E_NEC_STA_REPEAT;
      }
    }
    else
    {
      irda_symphony_nec_reset_decoder(p_result);
    }
  }
  else if(p_result->state == E_NEC_STA_REPEAT)
  {
    if(sig == E_NEC_SIG_START)
    {
      p_result->state = E_NEC_STA_START;
      p_result->counter = 0;
      p_result->r.nec.code = 0;
      p_result->r.nec.usercode = 0;
    }
    else if(sig == E_NEC_SIG_REPEAT)
    {
      p_result->counter ++;
      if(p_result->counter >= p_result->repeat_interval)
      {
        p_result->state = E_NEC_STA_FINISH;
        p_result->counter = 0;
      }
    }
    else
    {
      irda_symphony_nec_reset_decoder(p_result);
    }
  }

  if(p_result->state == E_NEC_STA_FINISH)
  {
    if(!irda_symphony_nec_decode_check_keycode(p_result->r.nec.code))
    {
      irda_symphony_nec_reset_decoder(p_result);
    }
  }
}/*IRDA NEC Decoder END*/

static u8 irda_symphony_konka_decoder_parse_sig(u16 ontime, u16 period, u16 * sigtbl)
{
  if((ontime >= sigtbl[0]) && (ontime <= sigtbl[1]) &&
      (period >= sigtbl[2]) && (period <= sigtbl[3]))
  {
    return E_KK_SIG_START;
  }
  else if((ontime >= sigtbl[4]) && (ontime <= sigtbl[5]) &&
             (period >= sigtbl[6]) && (period <= sigtbl[7]))
  {
    return E_KK_SIG_BIT0;
  }
  else if((ontime >= sigtbl[8]) && (ontime <= sigtbl[9]) &&
             (period >= sigtbl[10]) && (period <= sigtbl[11]))
  {
    return E_KK_SIG_BIT1;
  }
  else if((ontime >= sigtbl[12]) && (ontime <= sigtbl[13]) &&
             (period >= sigtbl[14]) && (period <= sigtbl[15]))
  {
    return E_KK_SIG_SYNC;
  }
  else
  {
    return E_KK_SIG_INVALID;
  }
}

inline static void irda_symphony_konka_reset_decoder(irda_dec_result_t *p_result)
{
  p_result->state = E_KK_STA_IDLE;
  p_result->counter = 0;
  p_result->r.konka.ucode = 0;
  p_result->r.konka.kcode = 0;
}

/*IRDA Konka Decoder */
static void irda_symphony_konka_decoder(irda_dec_para_t *p_para, irda_dec_result_t *p_result)
{
  u8 sig = irda_symphony_konka_decoder_parse_sig(p_para->ontime, p_para->period, p_result->p_sigtbl);
  IRDA_PRINT("\nkonka irda_decoder sig=%s, state=%s\n", kk_sig_strs[sig], kk_sta_strs[p_result->state]);
  if(p_result->state == E_KK_STA_IDLE)
  {
    if(sig == E_KK_SIG_START)
    {
      p_result->counter = 0;
      p_result->state = E_KK_STA_START;
    }
    else
    {
      p_result->counter = 0;
    }
  }
  else if(p_result->state == E_KK_STA_START)
  {
    if(sig == E_KK_SIG_BIT0 || sig == E_KK_SIG_BIT1)
    {
      p_result->state = E_KK_STA_USR;
      p_result->counter = 0;
      if(sig == E_KK_SIG_BIT0)
      {
        p_result->r.konka.ucode &= ~(1 << (7-p_result->counter));
      }
      else
      {
        p_result->r.konka.ucode |= (1 << (7-p_result->counter));
      }
    }
    else
    {
      irda_symphony_konka_reset_decoder(p_result);
    }
  }
  else if(p_result->state == E_KK_STA_USR)
  {
    if(sig == E_KK_SIG_BIT0 || sig == E_KK_SIG_BIT1)
    {
      p_result->counter ++;
      if(p_result->counter < 8)
      {
        if(sig == E_KK_SIG_BIT0)
        {
          p_result->r.konka.ucode &= ~(1 << (7-p_result->counter));
        }
        else
        {
          p_result->r.konka.ucode |= (1 << (7-p_result->counter));
        }
      }
      else
      {
        p_result->state = E_KK_STA_KEY;
        p_result->counter = 0;
        if(sig == E_KK_SIG_BIT0)
        {
          p_result->r.konka.kcode &= ~(1 << (7-p_result->counter));
        }
        else
        {
          p_result->r.konka.kcode |= (1 << (7-p_result->counter));
        }
      }
    }
    else
    {
      irda_symphony_konka_reset_decoder(p_result);
    }
  }
  else if(p_result->state == E_KK_STA_KEY)
  {
    if(sig == E_KK_SIG_BIT0 || sig == E_KK_SIG_BIT1)
    {
      p_result->counter ++;
      if(p_result->counter < 8)
      {
        if(sig == E_KK_SIG_BIT0)
        {
          p_result->r.konka.kcode &= ~(1 << (7-p_result->counter));
        }
        else
        {
          p_result->r.konka.kcode |= (1 << (7-p_result->counter));
        }
        if(p_result->counter == 7)
        {
          p_result->state = E_KK_STA_SYNC;
          p_result->counter = 0;
        }
      }
    }
    else
    {
      irda_symphony_konka_reset_decoder(p_result);
    }
  }
  else if(p_result->state == E_KK_STA_SYNC)
  {
    if(sig == E_KK_SIG_SYNC)
    {
      p_result->state = E_KK_STA_FINISH;
    }
    else
    {
      irda_symphony_konka_reset_decoder(p_result);
    }
  }
}/*IRDA Konka Decoder END */

/*IRDA Soft Decoder*/
static RET_CODE irda_symphony_decoder(irda_dec_para_t *p_para, irda_dec_result_t *p_result)
{
  if(p_para == NULL || p_result == NULL)
  {
    return ERR_PARAM;
  }

  switch(p_para->protocol)
  {
    case IRDA_NEC:
      return ERR_NOFEATURE;

    case IRDA_SW_NEC:
      irda_symphony_nec_decoder(p_para, p_result);
      return SUCCESS;

    case IRDA_SW_KONKA:
      irda_symphony_konka_decoder(p_para, p_result);
      return SUCCESS;

    default:
      return ERR_NOFEATURE;
    }
}/*IRDA Soft Decoder END*/

/*!
  IRDA RC5 decoder
*/
static void irda_symphony_rc5_decoder(uio_priv_t *p_priv,
  irda_priv_t *p_irda, irda_dec_para_t *p_para, irda_dec_result_t *p_result)
{
  //u16 overtime = ((*((volatile u32 *)SYMPHONY_IR_GLOBAL_CTRL) >> 20) & 0xFF) << 8;
  u16 recv_num = 0;
  u16 irda_code = 0;

  static u8 bit_val[14] = { 0 };
  static u8 half_bit1 = 0;
  static s8 bit_T = -1;

  recv_num = irda_symphony_get_recv_num();
  while(recv_num)
  {
    irda_symphony_get_ontime_and_period(&p_para->ontime, &p_para->period);
    IRDA_PRINT("(%d)#ontime:%d, period:%d\n", recv_num, p_para->ontime, p_para->period);

    if ((p_para->ontime >= p_result->p_sigtbl[0] && p_para->ontime <= p_result->p_sigtbl[1]) &&
          (p_para->period >= p_result->p_sigtbl[2] && p_para->period <= p_result->p_sigtbl[3]))
    {
      if (p_result->state <= E_RC5_STA_S2)
      {
        if (p_result->state > E_RC5_STA_IDLE)
        {
          p_result->counter ++;
        }
        bit_val[p_result->counter] = 1;
        half_bit1 = 1;
        IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
          __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
        p_result->state ++;
      }
      else
      {
        bit_val[++ p_result->counter] = half_bit1;
        IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
          __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
        if ((p_result->state == E_RC5_STA_T) ||
            ((p_result->state == E_RC5_STA_USR) && (p_result->counter >= 8)))
        {
          p_result->state ++;
        }
        else if ((p_result->state == E_RC5_STA_KEY) && (p_result->counter == 13))
        {
          OS_PRINTF("line %d set state INVALID\n",__LINE__);
          p_result->state = E_RC5_STA_INVALID;
        }
      }
    }
    else if ((p_para->ontime >= p_result->p_sigtbl[2] && p_para->ontime <= p_result->p_sigtbl[3]) &&
                 (p_para->period >= p_result->p_sigtbl[4] && p_para->period <= p_result->p_sigtbl[5]))
    {
#if 0 //Raja   
      if (!half_bit1)
      {
        OS_PRINTF("line %d set state INVALID\n",__LINE__);
        p_result->state = E_RC5_STA_FINISH;
      }
      else
#endif
      {
        bit_val[++ p_result->counter] = 1;
        half_bit1 = 0;
        IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
          __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
        if (((p_result->state >= E_RC5_STA_S1) && (p_result->state <= E_RC5_STA_T)) ||
             ((p_result->state == E_RC5_STA_USR) && (p_result->counter >= 8)))
        {
          p_result->state ++;
        }

        bit_val[++ p_result->counter] = 0;
        IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
          __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
        if (((p_result->state >= E_RC5_STA_S1) && (p_result->state <= E_RC5_STA_T)) ||
            ((p_result->state == E_RC5_STA_USR) && (p_result->counter >= 8)))
        {
          p_result->state ++;
        }
        else if ((p_result->state == E_RC5_STA_KEY) && (p_result->counter == 13))
        {
          OS_PRINTF("line %d set state INVALID\n",__LINE__);
          p_result->state = E_RC5_STA_INVALID;
        }
      }
    }
    else if ((p_para->ontime >= p_result->p_sigtbl[0] && p_para->ontime <= p_result->p_sigtbl[1]) &&
                 (p_para->period >= p_result->p_sigtbl[4] && p_para->period <= p_result->p_sigtbl[5]))
    {
#if 0 //Raja
      if (half_bit1)
      {
        OS_PRINTF("line %d set state INVALID\n",__LINE__);
        p_result->state = E_RC5_STA_FINISH;
      }
      else
#endif	  	
      {
        bit_val[++ p_result->counter] = 0;
        half_bit1= 1;
        IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
          __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
        if ((p_result->state == E_RC5_STA_T) ||
            ((p_result->state == E_RC5_STA_USR) && (p_result->counter >= 8)))
        {
          p_result->state ++;
        }
        else if ((p_result->state == E_RC5_STA_KEY) && (p_result->counter == 13))
        {
          OS_PRINTF("line %d set state INVALID\n",__LINE__);
          p_result->state = E_RC5_STA_INVALID;
        }
      }
    }
    else if ((p_para->ontime >= p_result->p_sigtbl[2] && p_para->ontime <= p_result->p_sigtbl[3]) &&
                 (p_para->period >= p_result->p_sigtbl[6] && p_para->period <= p_result->p_sigtbl[7]))
    {
#ifdef NEVER
      if (!half_bit1)
      {
        OS_PRINTF("line %d set state INVALID\n",__LINE__);
        p_result->state = E_RC5_STA_FINISH;
      }
      else
#endif /* NEVER */
      {
        bit_val[++ p_result->counter] = 1;
        half_bit1 = 0;
        IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
          __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
        if (((p_result->state >= E_RC5_STA_S1) && (p_result->state <= E_RC5_STA_T)) ||
             ((p_result->state == E_RC5_STA_USR) && (p_result->counter >= 8)))
        {
          p_result->state ++;
       	}

        bit_val[++ p_result->counter] = 0;
        half_bit1= 1;
        IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
          __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
        if ((p_result->state == E_RC5_STA_S2) ||
             (p_result->state == E_RC5_STA_T) ||
             ((p_result->state == E_RC5_STA_USR) && (p_result->counter >= 8)))
        {
          p_result->state ++;
        }
        else if ((p_result->state == E_RC5_STA_KEY) && (p_result->counter == 13))
        {
          OS_PRINTF("line %d set state INVALID\n",__LINE__);
          p_result->state = E_RC5_STA_INVALID;
        }
      }
    }
    else if ((p_para->ontime >= p_result->p_sigtbl[0] && p_para->ontime <= p_result->p_sigtbl[1]) &&
		   (p_para->period == 0xFFF))
    {
      bit_val[++ p_result->counter] = half_bit1;
      if ((p_result->state == E_RC5_STA_KEY) && (p_result->counter == 13))
      {
        IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
          __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
        p_result->state = E_RC5_STA_FINISH;
      }
    }
    else if ((p_para->ontime >= p_result->p_sigtbl[2] && p_para->ontime <= p_result->p_sigtbl[3]) &&
		(p_para->period == 0xFFF))
    {
      bit_val[++ p_result->counter] = half_bit1;
      half_bit1 = 0;

      IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
        __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);

      bit_val[++ p_result->counter] = 0;
      if ((p_result->state == E_RC5_STA_KEY) && (p_result->counter == 13))
      {
        IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
          __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
        p_result->state = E_RC5_STA_FINISH;
      }
    }
    else
    {
      OS_PRINTF("line %d set state INVALID\n",__LINE__);
      p_result->state = E_RC5_STA_FINISH;
    }

    recv_num --;

    if (p_result->state == E_RC5_STA_FINISH)
    {
      p_result->r.rc5.ucode = 0;
      p_result->r.rc5.ucode |= (bit_val[3] & 0x1) << 4;
      p_result->r.rc5.ucode |= (bit_val[4] & 0x1) << 3;
      p_result->r.rc5.ucode |= (bit_val[5] & 0x1) << 2;
      p_result->r.rc5.ucode |= (bit_val[6] & 0x1) << 1;
      p_result->r.rc5.ucode |= bit_val[7] & 0x1;

      p_result->r.rc5.kcode = 0;
      p_result->r.rc5.kcode |= (bit_val[8] & 0x1) << 5;
      p_result->r.rc5.kcode |= (bit_val[9] & 0x1) << 4;
      p_result->r.rc5.kcode |= (bit_val[10] & 0x1) << 3;
      p_result->r.rc5.kcode |= (bit_val[11] & 0x1) << 2;
      p_result->r.rc5.kcode |= (bit_val[12] & 0x1) << 1;
      p_result->r.rc5.kcode |= bit_val[13] & 0x1;

      p_irda->user_code = p_result->r.rc5.ucode;
      irda_code = p_result->r.rc5.kcode | (UIO_IRDA << 8);
      if (bit_T == bit_val[2])
      {
        irda_code |= 1 << 15;
      }
      else
      {
        bit_T = bit_val[2];
      }

      OS_PRINTF("   user code: %08x\n", p_result->r.rc5.ucode);
      OS_PRINTF("   irda code: %08x, %x\n", p_result->r.rc5.kcode, irda_code);

	  
#ifdef NEVER
	  if (  p_result->r.rc5.ucode == 0x0F  )
  	  {	  
		switch( p_result->r.rc5.kcode )	
		{
			case 0x10: //down
				irda_code = 0x8010;				
			break;

			default :
				break;
		}
	  }
	  else
#endif /* NEVER */

	  if ( ( p_result->r.rc5.ucode == 0x1F ) || (  p_result->r.rc5.ucode == 0x0F ) )
      {
      	 switch ( p_result->r.rc5.kcode  ) 
   	 	{

			case 0x12: //up
				irda_code = 0x8052;				
			break;

			 case 0x16: //right
				 irda_code = 0x8056;			 
			 break;
			 
			 case 0x17: //left
				 irda_code = 0x8057;			 
			 break;
			
			 case 0x15: //ok
				 irda_code = 0x8055;				 
			 break;

			case 0x10 : //menu
				irda_code = 0x8050;
			break;	

			case 0x39: //zero
				irda_code = 0x8079;				
			break;

			case 0x2e: //ch-
					irda_code = 0x806e;				
			break;
				
			case 0x2d: //ch+
					irda_code = 0x806d;				
			break;


			case 0x2c: //vol-
					irda_code = 0x806c;				
			break;
				
			case 0x2b: //vol+
					irda_code = 0x806b;				
			break;
	
			default :
				break;

   	 	}
      }
		

      OS_PRINTF("new irda code: %08x, %x\n", p_result->r.rc5.kcode, irda_code);



	  mtos_fifo_put(&p_priv->fifo, irda_code);


      half_bit1 = 0;
      p_result->counter = 0;
      memset(bit_val, 0x0, 14);
      p_result->state = E_RC5_STA_IDLE;
    }
    else if (p_result->state == E_RC5_STA_INVALID)
    {
      OS_PRINTF("STATE E_RC5_STA_INVALID\n");
      break;
    }
  }

  if (p_result->state == E_RC5_STA_INVALID)
  {
    half_bit1 = 0;
    p_result->counter = 0;
    memset(bit_val, 0, 14);
    p_result->state = E_RC5_STA_IDLE;

    while(recv_num)
    {
      irda_symphony_get_ontime_and_period(&p_para->ontime, &p_para->period);
      recv_num --;
    }
  }
}


#if 0 //defined but not used
/*!
  IRDA RC5 decoder handling tail bit(s) loss
*/
static void irda_symphony_rc5_decoder_bit_loss(uio_priv_t *p_priv,
  irda_priv_t *p_irda, irda_dec_para_t *p_para, irda_dec_result_t *p_result)
{
  u32 interrupt_interval = 0;
  u16 recv_num = 0;
  u16 irda_code = 0;

  static u32 hw_ticks_start = 0;
  static u8 bit_val[14] = { 0 };
  static u8 half_bit1 = 0;
  static s8 bit_T = -1;

  static u8 prev_lost_2bits = 0;
  static u8 prev_usrcode = 0;
  static u8 prev_keycode = 0;
  static s8 prev_bit_T = -1;

 OS_PRINTF("irda_symphony_rc5_decoder_bit_loss enter\n");
  interrupt_interval = mtos_hw_ticks_get() - hw_ticks_start;
  if ((mtos_hw_ticks_get() - hw_ticks_start) > 16000000)
  {
    IRDA_PRINT("interrput interval: %d\n", interrupt_interval);
    if ((p_result->state== E_RC5_STA_KEY) && (p_result->counter== 11) && half_bit1)
    {
      prev_lost_2bits = 1;
      prev_bit_T = bit_val[2];

      prev_usrcode = 0;
      prev_usrcode |= (bit_val[3] & 0x1) << 4;
      prev_usrcode |= (bit_val[4] & 0x1) << 3;
      prev_usrcode |= (bit_val[5] & 0x1) << 2;
      prev_usrcode |= (bit_val[6] & 0x1) << 1;
      prev_usrcode |= bit_val[7] & 0x1;

      prev_keycode = 0;
      prev_keycode |= (bit_val[8] & 0x1) << 5;
      prev_keycode |= (bit_val[9] & 0x1) << 4;
      prev_keycode |= (bit_val[10] & 0x1) << 3;
      prev_keycode |= (bit_val[11] & 0x1) << 2;
      prev_keycode |= 0x2;
    }

    half_bit1 = 0;
    p_result->counter = 0;
    memset(bit_val, 0, 14);
    p_result->state = E_RC5_STA_IDLE;
  }
  hw_ticks_start = mtos_hw_ticks_get();


  recv_num = irda_symphony_get_recv_num();
  OS_PRINTF("irda_symphony_rc5_decoder_bit_loss = 0x%x\n",recv_num);
  while(recv_num)
  {
    irda_symphony_get_ontime_and_period(&p_para->ontime, &p_para->period);
    IRDA_PRINT("(%d)#ontime:%d, period:%d\n", recv_num, p_para->ontime, p_para->period);

    if ((p_para->ontime >= p_result->p_sigtbl[0] && p_para->ontime <= p_result->p_sigtbl[1]) &&
          (p_para->period >= p_result->p_sigtbl[2] && p_para->period <= p_result->p_sigtbl[3]))
    {
      if (p_result->state <= E_RC5_STA_S2)
      {
        if (p_result->state > E_RC5_STA_IDLE)
        {
          p_result->counter ++;
        }
        bit_val[p_result->counter] = 1;
        half_bit1 = 1;
        IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
          __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
        p_result->state ++;
      }
      else
      {
        bit_val[++ p_result->counter] = half_bit1;
        IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
          __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
        if ((p_result->state == E_RC5_STA_T) ||
            ((p_result->state == E_RC5_STA_USR) && (p_result->counter >= 8)))
        {
          p_result->state ++;
        }
        else if ((p_result->state == E_RC5_STA_KEY) && (p_result->counter == 12))
        {
          bit_val[++ p_result->counter] = half_bit1;
          half_bit1 = 0;
          IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
            __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
          p_result->state = E_RC5_STA_FINISH;
        }
      }
    }
    else if ((p_para->ontime >= p_result->p_sigtbl[2] && p_para->ontime <= p_result->p_sigtbl[3]) &&
                 (p_para->period >= p_result->p_sigtbl[4] && p_para->period <= p_result->p_sigtbl[5]))
    {
      if (!half_bit1)
      {
        p_result->state = E_RC5_STA_INVALID;
      }
      else
      {
        bit_val[++ p_result->counter] = 1;
        half_bit1 = 0;
        IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
          __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
        if (((p_result->state >= E_RC5_STA_S1) && (p_result->state <= E_RC5_STA_T)) ||
             ((p_result->state == E_RC5_STA_USR) && (p_result->counter >= 8)))
        {
          p_result->state ++;
        }

        bit_val[++ p_result->counter] = 0;
        IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
          __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
        if (((p_result->state >= E_RC5_STA_S1) && (p_result->state <= E_RC5_STA_T)) ||
            ((p_result->state == E_RC5_STA_USR) && (p_result->counter >= 8)))
        {
          p_result->state ++;
        }
        else if ((p_result->state == E_RC5_STA_KEY) && (p_result->counter == 12))
        {
          bit_val[++ p_result->counter] = 0;
          IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
            __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
          p_result->state = E_RC5_STA_FINISH;
        }
      }
    }
    else if ((p_para->ontime >= p_result->p_sigtbl[0] && p_para->ontime <= p_result->p_sigtbl[1]) &&
                 (p_para->period >= p_result->p_sigtbl[4] && p_para->period <= p_result->p_sigtbl[5]))
    {
      if (half_bit1)
      {
        p_result->state = E_RC5_STA_INVALID;
      }
      else
      {
        bit_val[++ p_result->counter] = 0;
        half_bit1= 1;
        IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
          __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
        if ((p_result->state == E_RC5_STA_T) ||
            ((p_result->state == E_RC5_STA_USR) && (p_result->counter >= 8)))
        {
          p_result->state ++;
        }
        else if ((p_result->state == E_RC5_STA_KEY) && (p_result->counter == 12))
        {
          bit_val[++ p_result->counter] = 1;
          half_bit1 = 0;
          IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
            __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
          p_result->state = E_RC5_STA_FINISH;
        }
      }
    }
    else if ((p_para->ontime >= p_result->p_sigtbl[2] && p_para->ontime <= p_result->p_sigtbl[3]) &&
                 (p_para->period >= p_result->p_sigtbl[6] && p_para->period <= p_result->p_sigtbl[7]))
    {
      if (!half_bit1)
      {
        p_result->state = E_RC5_STA_INVALID;
      }
      else
      {
        bit_val[++ p_result->counter] = 1;
        half_bit1 = 0;
        IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
          __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
        if (((p_result->state >= E_RC5_STA_S1) && (p_result->state <= E_RC5_STA_T)) ||
             ((p_result->state == E_RC5_STA_USR) && (p_result->counter >= 8)))
        {
          p_result->state ++;
        }

        bit_val[++ p_result->counter] = 0;
        half_bit1= 1;
        IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
          __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
        if ((p_result->state == E_RC5_STA_S2) ||
             (p_result->state == E_RC5_STA_T) ||
             ((p_result->state == E_RC5_STA_USR) && (p_result->counter >= 8)))
        {
          p_result->state ++;
        }
        else if ((p_result->state == E_RC5_STA_KEY) && (p_result->counter == 12))
        {
          bit_val[++ p_result->counter] = half_bit1;
          half_bit1 = 0;
          IRDA_PRINT("line: %d, state: %d, bit_val[%d]: %d, half_bit1: %d\n",
            __LINE__, p_result->state, p_result->counter, bit_val[p_result->counter], half_bit1);
          p_result->state = E_RC5_STA_FINISH;
        }
      }
    }
    else
    {
      p_result->state = E_RC5_STA_INVALID;
    }

    recv_num --;

    if ((p_result->state == E_RC5_STA_FINISH) || (p_result->state == E_RC5_STA_INVALID))
    {
      break;
    }
  }

  if (p_result->state <= E_RC5_STA_FINISH)
  {
    if (prev_lost_2bits && half_bit1 && (prev_bit_T == bit_val[2]))
    {
      p_irda->user_code = prev_usrcode;
      irda_code = prev_keycode | (UIO_IRDA << 8);
      if (prev_bit_T == bit_T)
      {
        irda_code |= 1 << 15;
      }
      else
      {
        bit_T = prev_bit_T;
      }

      OS_PRINTF("   user code: %08x\n", prev_usrcode);
      OS_PRINTF("   irda code: %08x, %x\n", prev_keycode, irda_code);

      mtos_fifo_put(&p_priv->fifo, irda_code);

      prev_lost_2bits = 0;
      prev_bit_T = -1;
      prev_usrcode = 0;
      prev_keycode = 0;
    }
  }

  switch (p_result->state)
  {
    case E_RC5_STA_FINISH:
      p_result->r.rc5.ucode = 0;
      p_result->r.rc5.ucode |= (bit_val[3] & 0x1) << 4;
      p_result->r.rc5.ucode |= (bit_val[4] & 0x1) << 3;
      p_result->r.rc5.ucode |= (bit_val[5] & 0x1) << 2;
      p_result->r.rc5.ucode |= (bit_val[6] & 0x1) << 1;
      p_result->r.rc5.ucode |= bit_val[7] & 0x1;

      p_result->r.rc5.kcode = 0;
      p_result->r.rc5.kcode |= (bit_val[8] & 0x1) << 5;
      p_result->r.rc5.kcode |= (bit_val[9] & 0x1) << 4;
      p_result->r.rc5.kcode |= (bit_val[10] & 0x1) << 3;
      p_result->r.rc5.kcode |= (bit_val[11] & 0x1) << 2;
      p_result->r.rc5.kcode |= (bit_val[12] & 0x1) << 1;
      p_result->r.rc5.kcode |= bit_val[13] & 0x1;

      p_irda->user_code = p_result->r.rc5.ucode;
      irda_code = p_result->r.rc5.kcode | (UIO_IRDA << 8);
      if (bit_T == bit_val[2])
      {
        irda_code |= 1 << 15;
      }
      else
      {
        bit_T = bit_val[2];
      }

      OS_PRINTF("   user code: %08x\n", p_result->r.rc5.ucode);
      OS_PRINTF("   irda code: %08x, %x\n", p_result->r.rc5.kcode, irda_code);

      mtos_fifo_put(&p_priv->fifo, irda_code);

    case E_RC5_STA_INVALID:
      prev_lost_2bits = 0;
      prev_bit_T = -1;
      prev_usrcode = 0;
      prev_keycode = 0;

      half_bit1 = 0;
      p_result->counter = 0;
      memset(bit_val, 0, 14);
      p_result->state = E_RC5_STA_IDLE;

      while(recv_num)
      {
        irda_symphony_get_ontime_and_period(&p_para->ontime, &p_para->period);
        recv_num --;
      }
      break;

    default:
      break;
  }
}
#endif

static void irda_symphony_rc5_wfilt_set(u8 *bit_val, u8 wfilt_add_len ,u8 * p_channel_len)
{
  //u16 overtime = ((*((volatile u32 *)SYMPHONY_IR_GLOBAL_CTRL) >> 16) & 0xFF) << 4;
  u8 j = 0, k = 0;

  while (j <= 13)
  {
    if (j == 13)
    {
      irda_symphony_wave_wfn_set(wfilt_add_len + k, rc5_sigtbl[0], rc5_sigtbl[1], 0, 4095);
      IRDA_PRINT("%d, bit_val[%d]: %d\n", __LINE__, j, bit_val[j]);
      j ++;
    }
    else if ((bit_val[j] && bit_val[j+1]) || (!bit_val[j] && !bit_val[j+1])) // 11b or 00b
    {
      if (j < 13)
      {
        irda_symphony_wave_wfn_set(wfilt_add_len + k, rc5_sigtbl[0], rc5_sigtbl[1], rc5_sigtbl[2], rc5_sigtbl[3]);
        IRDA_PRINT("%d, bit_val[%d]: %d\n", __LINE__, j, bit_val[j]);
        j ++;
      }
    }
    else if (bit_val[j] && !bit_val[j+1]) // 10b
    {
      if (j < 12)
      {
        if (bit_val[j+2]) // 101b
          irda_symphony_wave_wfn_set(wfilt_add_len + k, rc5_sigtbl[2], rc5_sigtbl[3], rc5_sigtbl[6], rc5_sigtbl[7]);
        else if (!bit_val[j+2]) // 100b
          irda_symphony_wave_wfn_set(wfilt_add_len + k, rc5_sigtbl[2], rc5_sigtbl[3], rc5_sigtbl[4], rc5_sigtbl[5]);

        IRDA_PRINT("%d, bit_val[%d]: %d\n", __LINE__, j, bit_val[j]);
        IRDA_PRINT("%d, bit_val[%d]: %d\n", __LINE__, j + 1, bit_val[j+1]);
        j += 2;
      }
      else if (j == 12) // 10b
      {
	 irda_symphony_wave_wfn_set(wfilt_add_len + k, rc5_sigtbl[2], rc5_sigtbl[3], 0, 4095);

        IRDA_PRINT("%d, bit_val[%d]: %d\n", __LINE__, j, bit_val[j]);
        IRDA_PRINT("%d, bit_val[%d]: %d\n", __LINE__, j + 1, bit_val[j+1]);
        j += 2;
      }
    }
    else if (!bit_val[j] && bit_val[j+1]) // 01b
    {
      irda_symphony_wave_wfn_set(wfilt_add_len + k, rc5_sigtbl[0], rc5_sigtbl[1], rc5_sigtbl[4], rc5_sigtbl[5]);
      IRDA_PRINT("%d, bit_val[%d]: %d\n", __LINE__, j, bit_val[j]);
      j ++;
    }

     if(k == 1)
    {
	    irda_symphony_wave_mask_bit_set(wfilt_add_len +k, 0);
    }
    else
    {
	    irda_symphony_wave_mask_bit_set(wfilt_add_len +k, 1);
    }


    k++;
  }

  *p_channel_len = k;
}

#if 0 //defined but not used
static void irda_symphony_rc5_wfilt_set_bit_loss(u8 *bit_val, u8 * p_channel_len)
{
  u8 j = 0, k = 0;

  while (j <= 12)
  {
    if ((bit_val[j] && bit_val[j+1]) || (!bit_val[j] && !bit_val[j+1])) // 11b or 00b
    {
      irda_symphony_wave_wfn_set(k, rc5_sigtbl[0], rc5_sigtbl[1], rc5_sigtbl[2], rc5_sigtbl[3]);
      IRDA_PRINT("%d, bit_val[%d]: %d\n", __LINE__, j, bit_val[j]);
      j ++;
    }
    else if (bit_val[j] && !bit_val[j+1]) // 10b
    {
      if (j < 12)
      {
        if (bit_val[j+2]) // 101b
          irda_symphony_wave_wfn_set(k, rc5_sigtbl[2], rc5_sigtbl[3], rc5_sigtbl[6], rc5_sigtbl[7]);
        else if (!bit_val[j+2]) // 100b
          irda_symphony_wave_wfn_set(k, rc5_sigtbl[2], rc5_sigtbl[3], rc5_sigtbl[4], rc5_sigtbl[5]);

        IRDA_PRINT("%d, bit_val[%d]: %d\n", __LINE__, j, bit_val[j]);
        IRDA_PRINT("%d, bit_val[%d]: %d\n", __LINE__, j + 1, bit_val[j+1]);

        j += 2;
      }
      else if (j == 12)
        break;
    }
    else if (!bit_val[j] && bit_val[j+1]) // 01b
    {
      irda_symphony_wave_wfn_set(k, rc5_sigtbl[0], rc5_sigtbl[1], rc5_sigtbl[4], rc5_sigtbl[5]);
      IRDA_PRINT("%d, bit_val[%d]: %d\n", __LINE__, j, bit_val[j]);
      j ++;
    }

    irda_symphony_wave_mask_bit_set(k, 1);

    k++;
  }

#if RC5_WF_IGNORE_T_BIT
  // ignore samples relevant to T bit
  irda_symphony_wave_mask_bit_set(1, 0);
  irda_symphony_wave_mask_bit_set(2, 0);
#endif

  *p_channel_len = k;
}
#endif

static RET_CODE irda_symphony_set_wfilt(irda_priv_t *p_ir)
{
  u8 i = 0;
  u8 j = 0;
  u8 channel_num = 0;
  u8 wfilt_add_len = 0;
  u8 channel_len = 0;
  u8 irda_protocol = 0;
  u32 code = 0;
  u32 mask_code = 0;
  u32 ptmp = 0,dtmp =0;

  OS_PRINTF("enter wflit\n");
  //irda_symphony_soft_reset();
//  irda_symphony_idle_set(1);
#if DEFAULT_SAMPLE_FREQ //  Sample freq 187.5 KHz
 // irda_symphony_sample_clk_set(2);
#else //  Sample freq 46.875 KHz
  //irda_symphony_sample_clk_set(3);
#endif

  ptmp = SYMPHONY_IR_INT_CFG;
  dtmp = hal_get_u32((volatile u32 *)ptmp);
  dtmp |= (0xf<<4);
  hal_put_u32((volatile u32 *)ptmp, dtmp);

  irda_symphony_wave_channel_set(0xf, FALSE);
  irda_symphony_wave_channel_int_set(0xf , FALSE);
  channel_num = p_ir->irda_wfilt_channel;

  OS_PRINTF("wflit channel_num = %d\n",channel_num);
  switch(channel_num)
  {
    case 4:
      irda_symphony_wave_channel_set(0xf, TRUE);
      irda_symphony_wave_channel_int_set(0xf , TRUE);
	  break;
    case 3:
      irda_symphony_wave_channel_set(0x7, TRUE);
      irda_symphony_wave_channel_int_set(0x7, TRUE);
	  break;
    case 2:
      irda_symphony_wave_channel_set(0x3, TRUE);
      irda_symphony_wave_channel_int_set(0x3, TRUE);
	  break;
    case 1:
      irda_symphony_wave_channel_set(0x1, TRUE);
      irda_symphony_wave_channel_int_set(0x1, TRUE);
      break;
    default :
      return ERR_PARAM;
  }

  #if 0
  OS_PRINTF("%s line: %d\n",__func__,__LINE__);
  OS_PRINTF("reg(0xBF0A0090) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF0A0090));
  OS_PRINTF("reg(0xBF0A0094) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF0A0094));
  OS_PRINTF("reg(0xBF0A0098) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF0A0098));
  OS_PRINTF("reg(0xBF0A009c) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF0A009c));
  #endif
  for(i = 0; i < channel_num; i++)
  {
    //int tmpd = 0;
    //channel_len = p_ir->irda_wfilt_channel_cfg[i].addr_len;
    irda_symphony_wave_add_set(i, wfilt_add_len);
    irda_protocol = p_ir->irda_wfilt_channel_cfg[i].protocol;
	
    if ((irda_protocol == IRDA_NEC) || (irda_protocol == IRDA_SW_NEC))
    { 
    	/* IrDA NEC protocol */
      channel_len = p_ir->irda_wfilt_channel_cfg[i].addr_len;
      mask_code = p_ir->irda_wfilt_channel_cfg[i].wfilt_mask;
      code = p_ir->irda_wfilt_channel_cfg[i].wfilt_code;
      // 16 bit usercode | 8 bit keycode | 8 bit reversed keycode
      // --->
      // 8 bit reversed keycode | 8 bit keycode | 16 bit usercode
      code = ((code & 0xFFFF0000) >> 16) |
		     ((code & 0xFF00) << 8) |
             ((code & 0xFF) << 24);

      for(j = 0; j < channel_len; j++)
      {
        if(mask_code == 0)
        {
          irda_symphony_wave_mask_bit_set(j + wfilt_add_len, 1);
        }
        else
        {
          if((mask_code >> j) & 0x1)
          {
            irda_symphony_wave_mask_bit_set(j + wfilt_add_len, 1);
          }
          else
          {
            irda_symphony_wave_mask_bit_set(j + wfilt_add_len, 0);
          }
        }
        if((code >> j) & 0x1) // Timing of NEC bit 1 code
        {
		if(((*(volatile unsigned int *)0xBF140020)&(1<<30))==0)
		{
			irda_symphony_wave_wfn_set(j + wfilt_add_len ,
			nec_sigtbl[8], nec_sigtbl[9], nec_sigtbl[10], nec_sigtbl[11]);
		}
		 else
		{
			irda_symphony_wave_wfn_set(j + wfilt_add_len ,
			nec_sigtbl_24m[8], nec_sigtbl_24m[9], nec_sigtbl_24m[10], nec_sigtbl_24m[11]);
		}
        }
        else // Timing of NEC bit 0 code
        {
		if(((*(volatile unsigned int *)0xBF140020)&(1<<30))==0)
		{
			irda_symphony_wave_wfn_set(j + wfilt_add_len ,
			nec_sigtbl[4], nec_sigtbl[5], nec_sigtbl[6], nec_sigtbl[7]);
		}
		else
		{
			irda_symphony_wave_wfn_set(j + wfilt_add_len ,
			nec_sigtbl_24m[4], nec_sigtbl_24m[5], nec_sigtbl_24m[6], nec_sigtbl_24m[7]);
		}
        }
      }
      wfilt_add_len += channel_len;
    }
    else if (irda_protocol == IRDA_SW_KONKA)
    { 
       /* IrDA Konka protocol */
	channel_len = p_ir->irda_wfilt_channel_cfg[i].addr_len;
      mask_code = p_ir->irda_wfilt_channel_cfg[i].wfilt_mask;
      code = p_ir->irda_wfilt_channel_cfg[i].wfilt_code;
      for(j = 0; j < channel_len; j++)
      {
        if(mask_code == 0)
        {
          irda_symphony_wave_mask_bit_set(j + wfilt_add_len, 1);
        }
        else
        {
          if((mask_code << j) & 0x8000)
          {
            irda_symphony_wave_mask_bit_set(j + wfilt_add_len, 1);
          }
          else
          {
            irda_symphony_wave_mask_bit_set(j + wfilt_add_len, 0);
          }
        }
        if((code << j) & 0x8000) // Timing of Konka bit 1 code
        {
          irda_symphony_wave_wfn_set(j + wfilt_add_len,
            konka_sigtbl[8], konka_sigtbl[9], konka_sigtbl[10], konka_sigtbl[11]);
        }
        else // Timing of Konka bit 0 code
        {
          irda_symphony_wave_wfn_set(j + wfilt_add_len,
            konka_sigtbl[4], konka_sigtbl[5], konka_sigtbl[6], konka_sigtbl[7]);
        }
      }
      wfilt_add_len += channel_len;
    }
    else if (irda_protocol == IRDA_SW_RC5)
    { 
    	/* IrDA RC5 protocol */
      u8 ucode = 0;
      u8 bit_val[14] = { 0 };

      code = p_ir->irda_wfilt_channel_cfg[i].wfilt_code;
      #if 0
      OS_PRINTF("#### %s line: %d code = 0x%x ####\n",__func__,__LINE__,code);
      #endif
      ucode = (code >> 8) & 0x1F;
      code &= 0x3F;
      code |= (ucode << 6) | (0x3 << 12);
      for (j = 13; j > 0; j --)
      {
        bit_val[13 - j] = (code >> j) & 0x1;
	 //OS_PRINTF("bit%d: %x\n",(13 - j),bit_val[13 - j]);
      }
	 bit_val[13] = code & 0x1;
	 //OS_PRINTF("bit13: %x\n",bit_val[13]);

#if !RC5_WF_IGNORE_T_BIT
      bit_val[2] = i & 0x1; // the 3rd bit T
#endif

	irda_symphony_rc5_wfilt_set(bit_val,  wfilt_add_len, &channel_len);

	#if 0
	OS_PRINTF("reg(0xBF151090) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151090));
	OS_PRINTF("reg(0xBF151094) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151094));
	OS_PRINTF("reg(0xBF151098) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151098));
	OS_PRINTF("reg(0xBF15109c) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF15109c));
	#endif
	wfilt_add_len += channel_len;
    }

    irda_symphony_wave_len_set(i, channel_len - 1);

    OS_PRINTF("channel len: %d, wflit_add_len: %d\n", channel_len, wfilt_add_len);
    OS_PRINTF("wfilt code: 0x%x\n", code);
  }

  #if 0
  OS_PRINTF("reg(0xBF151000) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151000));
  OS_PRINTF("reg(0xBF151004) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151004));
  OS_PRINTF("reg(0xBF151008) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151008));
  OS_PRINTF("reg(0xBF15100c) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF15100c));
  OS_PRINTF("reg(0xBF151010) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151010));
  OS_PRINTF("reg(0xBF151014) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151014));
  OS_PRINTF("reg(0xBF151018) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151018));
  OS_PRINTF("reg(0xBF151080) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151080));
  OS_PRINTF("reg(0xBF151084) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151084));
  OS_PRINTF("reg(0xBF151088) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151088));
  OS_PRINTF("reg(0xBF15108c) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF15108c));
  OS_PRINTF("reg(0xBF151090) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151090));
  OS_PRINTF("reg(0xBF151094) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151094));
  OS_PRINTF("reg(0xBF151098) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151098));
  OS_PRINTF("reg(0xBF15109c) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF15109c));

  for(i=0;i<34;i++)
  {
	OS_PRINTF("reg(0x%x) = 0x%x\n",(0xBF151400+i*4),hal_get_u32((volatile u32 *)(0xBF151400+i*4)));
  }
  #endif

  return SUCCESS;
}

#define SYM_RST_ALLOW_AO   0xbf15300c
#define SYM_RST_REQ_AO   0xbf153008
#define SYM_RST_CTRL_AO   0xbf153004
void irda_symphony_glb_rst(void)
{
  u32 ptmp = 0;
  u32 dtmp = 0;
  u8 reset_bit = 3;

  ptmp = SYM_RST_REQ_AO;
  dtmp = hal_get_u32((volatile u32 *)ptmp);
  dtmp |= 1 << reset_bit;
  hal_put_u32((volatile u32 *)ptmp, dtmp);

  ptmp = SYM_RST_ALLOW_AO;
  do
  {
    dtmp = hal_get_u32((volatile u32 *)ptmp);
  }
  while (!((dtmp >> reset_bit) & 0x1));

  ptmp = SYM_RST_CTRL_AO;
  dtmp = hal_get_u32((volatile u32 *)ptmp);
  dtmp &= ~ (1<< reset_bit);
  hal_put_u32((volatile u32 *)ptmp, dtmp);

  dtmp |= 1 << reset_bit;
  hal_put_u32((volatile u32 *)ptmp, dtmp);

  ptmp = SYM_RST_REQ_AO;
  dtmp = hal_get_u32((volatile u32 *)ptmp);
  dtmp &= ~ (1 << reset_bit);
  hal_put_u32((volatile u32 *)ptmp, dtmp);
}

void irda_symphony_soft_reset(void)
{
	u16 ontime=0;
	u16 period=0;
	u16 recv_num = 0;

	recv_num = irda_symphony_get_recv_num();

	while(recv_num--)
	{
		irda_symphony_get_ontime_and_period(&ontime, &period);
	  	IRDA_PRINT("(%d)#ontime:%d, period:%d\n", recv_num, ontime, period);
	}
	OS_PRINTF("symphony irda clr buffer\n");
}

static void irda_symphony_wave_channel_set(u8 channel, u8 is_en)
{
  u32 ptmp = 0;
  u32 dtmp = 0;

 // chn_bit = 24 + channel;

  ptmp = SYMPHONY_IR_GLOBAL_CTRL;
  dtmp = hal_get_u32((volatile u32 *)ptmp);

  if (is_en)
  {
  //  dtmp |= 1<<chn_bit;
   dtmp |= channel << 12;
  }
  else
  {
    //dtmp &= ~(1<<chn_bit);
    dtmp &= ~(channel << 12);
  }

  hal_put_u32((volatile u32 *)ptmp, dtmp);
}

static void irda_symphony_wave_channel_int_set(u8 channel_int, u8 is_en)
{
  u32 ptmp = 0;
  u32 dtmp = 0;

  ptmp = SYMPHONY_IR_INT_CFG;
  dtmp = hal_get_u32((volatile u32 *)ptmp);
  if (is_en)
  {
   dtmp |= channel_int << 4;
  }
  else
  {
    dtmp &= ~(channel_int << 4);
  }
  hal_put_u32((volatile u32 *)ptmp, dtmp);
}

static void irda_symphony_wave_mask_bit_set(u32 bit, u32 value)
{
  u8 position = 0;
  u8 field = 0;
  u32 dtmp = 0;
  u32 ptmp = 0;

  position = bit / 32;
  field = bit % 32;

  ptmp = GET_SYMPHONY_IR_WAVEFILT_MASK(position);
  dtmp = hal_get_u32((volatile u32 *)ptmp);
  dtmp &= ~(1 << field);
  dtmp |=(value << field);
  //mtos_printk("%s  reg = 0x%x, val = 0x%x\n",__func__,ptmp,dtmp);
  #if CPU_BYTE_TEST
  hal_put_u8((volatile u8 *)ptmp, (u8)(dtmp&0xff));
  hal_put_u8(((volatile u8 *)ptmp+1), (u8)((dtmp>>8)&0xff));
  hal_put_u8(((volatile u8 *)ptmp+2), (u8)((dtmp>>16)&0xff));
  hal_put_u8(((volatile u8 *)ptmp+3), (u8)((dtmp>>24)&0xff));
  #else
  hal_put_u32((volatile u32 *)ptmp, dtmp);
  #endif
}

static void irda_symphony_wave_len_set(u8 channel, u8 len)
{
  u32 ptmp = 0;
  u32 dtmp = 0;

  switch (channel)
  {
    case 0:
      ptmp = SYMPHONY_IR_WAVEFILT_CFG0;
      break;
    case 1:
      ptmp = SYMPHONY_IR_WAVEFILT_CFG1;
      break;
    case 2:
      ptmp = SYMPHONY_IR_WAVEFILT_CFG2;
      break;
    case 3:
      ptmp = SYMPHONY_IR_WAVEFILT_CFG3;
      break;
    default:
      break;
  }

  dtmp = hal_get_u32((volatile u32 *)ptmp);
  dtmp &= 0xFFFF80FF;
  dtmp |= len << 8;
  hal_put_u32((volatile u32 *)ptmp, dtmp);
}

static void irda_symphony_wave_add_set(u8 channel, u8 addr)
{
  u32 ptmp = 0;
  u32 dtmp = 0;

  switch (channel)
  {
    case 0:
      ptmp = SYMPHONY_IR_WAVEFILT_CFG0;
      break;
    case 1:
      ptmp = SYMPHONY_IR_WAVEFILT_CFG1;
      break;
    case 2:
      ptmp = SYMPHONY_IR_WAVEFILT_CFG2;
      break;
    case 3:
      ptmp = SYMPHONY_IR_WAVEFILT_CFG3;
      break;
    default:
      break;
  }

  dtmp = hal_get_u32((volatile u32 *)ptmp);
  dtmp &= 0xFFFFFF80;
  dtmp |= addr;
  #if CPU_BYTE_TEST
  hal_put_u8((volatile u8 *)ptmp, (u8)(dtmp&0xff));
  hal_put_u8(((volatile u8 *)ptmp+1), (u8)((dtmp>>8)&0xff));
  hal_put_u8(((volatile u8 *)ptmp+2), (u8)((dtmp>>16)&0xff));
  hal_put_u8(((volatile u8 *)ptmp+3), (u8)((dtmp>>24)&0xff));
  #else
  hal_put_u32((volatile u32 *)ptmp, dtmp);
  #endif

}

 static void irda_symphony_wave_wfn_set(u8 n, u16 ontime_l, u16 ontime_h, u16 period_l, u16 period_h)
{
  u32 ptmp = 0;
  u32 dtmp = 0;

  IRDA_PRINT("%s, %d, n: %d, ontime_l: %d, ontime_h: %d, period_l: %d, period_h: %d\n",
    __FUNCTION__, __LINE__, n, ontime_l, ontime_h, period_l, period_h);

  ptmp = GET_SYMPHONY_IR_WFN_ONTSET(n);
  IRDA_PRINT("ontime_reg_add = 0x%x\n",ptmp);
  //dtmp = hal_get_u32((volatile u32 *)ptmp);
  //dtmp &= 0x0;
  dtmp = (ontime_h << 16) | ontime_l;
  #if CPU_BYTE_TEST
  hal_put_u8((volatile u8 *)ptmp, (u8)(dtmp&0xff));
  hal_put_u8(((volatile u8 *)ptmp+1), (u8)((dtmp>>8)&0xff));
  hal_put_u8(((volatile u8 *)ptmp+2), (u8)((dtmp>>16)&0xff));
  hal_put_u8(((volatile u8 *)ptmp+3), (u8)((dtmp>>24)&0xff));
  #else
  hal_put_u32((volatile u32 *)ptmp, dtmp);
  #endif
  ptmp = GET_SYMPHONY_IR_WFN_PRDSET(n);
  IRDA_PRINT("petime_reg_add = 0x%x\n",ptmp);
  //dtmp = hal_get_u32((volatile u32 *)ptmp);
  //dtmp &= 0x0;
  dtmp = (period_h << 16) | period_l;
  #if CPU_BYTE_TEST
  hal_put_u8((volatile u8 *)ptmp, (u8)(dtmp&0xff));
  hal_put_u8(((volatile u8 *)ptmp+1), (u8)((dtmp>>8)&0xff));
  hal_put_u8(((volatile u8 *)ptmp+2), (u8)((dtmp>>16)&0xff));
  hal_put_u8(((volatile u8 *)ptmp+3), (u8)((dtmp>>24)&0xff));
  #else
  hal_put_u32((volatile u32 *)ptmp, dtmp);
  #endif
  /*
  *	一组ontime和period寄存器都被设置完之后，才会被真正写进去，
  *	这个时候读出来的数据才是正确的
  */
  IRDA_PRINT("reg(0x%x) = 0x%x\n",ptmp,hal_get_u32((volatile u32 *)ptmp));
  IRDA_PRINT("reg(0x%x) = 0x%x\n",(ptmp-4),hal_get_u32((volatile u32 *)(ptmp-4)));
  IRDA_PRINT("reg(0x%x) = 0x%x\n",(ptmp-4),hal_get_u32((volatile u32 *)(ptmp-4)));
}

#if 0 //defined but not used
static void irda_symphony_idle_set(u8 idle)
{
  u32 ptmp = 0;
  u32 dtmp = 0;

  ptmp = SYMPHONY_IR_GLOBAL_CTRL;
  dtmp = hal_get_u32((volatile u32 *)ptmp);

  if (idle)
  {
    dtmp |= 0x4;	// Set idle state as High
  }
  else
  {
    dtmp &= ~0x4;	// Set idle state as Low
  }

  hal_put_u32((volatile u32 *)ptmp, dtmp);
}

static void irda_symphony_sample_clk_set(u8 sample_clk)
{
  u32 ptmp = 0;
  u32 dtmp = 0;

  ptmp = SYMPHONY_IR_GLOBAL_CTRL;
  dtmp = hal_get_u32((volatile u32 *)ptmp);

  switch (sample_clk)
  {
    case 0: // 3MHz
      dtmp &= 0xFFFFFF0F;
      break;
    case 1: // 750KHz
      dtmp &= 0xFFFFFF0F;
      dtmp |= 0x10;
      break;
    case 2:	// 187.5HKz (mode 0 default value)
      dtmp &= 0xFFFFFF0F;
      dtmp |= 0x20;
      break;
    case 3:	// 46.875KHz
       dtmp &= 0xFFFFFF0F;
       dtmp |= 0x30;
       break;
    default:
       break;
  }

  hal_put_u32((volatile u32 *)ptmp, dtmp);
}
#endif


 void irda_symphony_handler_for_smc(u8 ucode_l, u8 ucode_h, u8 kcode, u8 rep)
{
  u16 ucode=0;
  //u8 kcode=0;
  u16 irda_code = 0;
  u8 i = 0, irh = 0;
  //u8 rep = 0;

  uio_device_t *p_dev = (uio_device_t *)dev_find_identifier(NULL,
                                        DEV_IDT_TYPE, SYS_DEV_TYPE_UIO);
  lld_uio_t *p_lld = (lld_uio_t *) p_dev->p_priv;
  uio_priv_t *p_priv = (uio_priv_t *)p_lld->p_priv;
  irda_priv_t *p_irda = (irda_priv_t *) p_lld->p_irda;

  OS_PRINTF("#####irda_symphony_handler_for_smc######\n");

OS_PRINTF("ucode_l = 0x%x, ucode_h = 0x%x, kcode = 0x%x, rep = 0x%x\n", ucode_l, ucode_h, kcode, rep);
  p_irda->irda_flag = 1;


  ucode =   (ucode_h << 8 ) |ucode_l;

  if(TRUE == p_irda->user_code_set)
  {
    if (p_irda->print_user_code)
    {
      OS_PRINTF("\nReceive irda data code %d", kcode);
      OS_PRINTF("   user code high: %d, user code low: %d\n",
                  ((ucode & 0xFF00) >> 8), (ucode & 0xFF));
    }

    for(i = 0; i < IRDA_MAX_USER; i++)
    {
      if(p_irda->sw_user_code[i] == ucode)
      {
        break;
      }
    }
    if(i >= IRDA_MAX_USER)
    {
      return;
    }
  }

  //if(IRDA_SYMPHONY_CHK_RPTKEY())
  if(rep)
  {
    //Attention !!!
    //it shoud be replace to uio_check_rpt_key_symphony after ledkb verify done
    if(FALSE == uio_check_rpt_key_concerto(UIO_IRDA, kcode, i))
    {
      return;
    }
  }

  irh = UIO_IRDA | ((i) << UIO_USR_CODE_SHIFT);
  irda_code = kcode | (irh << 8);
  if (p_irda->irda_protocol == IRDA_NEC)
  {
   // if(IRDA_SYMPHONY_CHK_RPTKEY())
   if(rep)
    {
      irda_code |= (1 << 15);
    }
  }
  else
  {
    p_irda->user_code = ucode;
  }

  OS_PRINTF("***user code high: %08x, user code low: %08x\n",
              ((ucode & 0xFF00) >> 8), (ucode & 0xFF));
  OS_PRINTF("***irda code: %08x, %x\n", kcode, irda_code);
  mtos_fifo_put(&p_priv->fifo, irda_code);
}

static void irda_symphony_int_handler(void)
{
  u16 ucode=0;
  u8 kcode=0;
  u16 irda_code = 0;
  u8 i = 0, irh = 0;
  static u32 save_ircode = 0;
  u8 repeat_status = 0;

  uio_device_t *p_dev = (uio_device_t *)dev_find_identifier(NULL,
                                        DEV_IDT_TYPE, SYS_DEV_TYPE_UIO);
  lld_uio_t *p_lld = (lld_uio_t *) p_dev->p_priv;
  uio_priv_t *p_priv = (uio_priv_t *)p_lld->p_priv;
  irda_priv_t *p_irda = (irda_priv_t *) p_lld->p_irda;

  //OS_PRINTF("#####irda_symphony_int_handler######\n");
 // OS_PRINTF("reg SYMPHONY_IR_INT_RAWSTA = 0x%x\n",hal_get_u32((volatile u32 *)SYMPHONY_IR_INT_RAWSTA));
 // return ;
  mtos_printk("\irda_symphony_int_handler [%d] irda_protocol [%d]\n",__LINE__,p_irda->irda_protocol);
  p_irda->irda_flag = 1;

  if(p_irda->irda_protocol == IRDA_NEC)  /* hardware decoding */
  {
    if (IRDA_SYMPHONY_CHK_KDNSTA()) /*key down*/
    {
      u32 ptmp = 0;
      u32 dtmp = 0;

      ptmp = SYMPHONY_IR_NEC_DATA;
      dtmp = hal_get_u32((volatile u32 *)ptmp);

	//OS_PRINTF("dtmp = 0x%x\n",dtmp);
      ucode = dtmp & 0xFFFF;
      kcode = (dtmp >> 16) & 0xFF;
    }
    if (IRDA_SYMPHONY_CHK_KUPSTA()) /*key up*/
    {
      IRDA_PRINT("key up interrupt\n");
      return;
    }
  }
  else /* software decoding */
  {
    u16 ontime=0;
    u16 period=0;
    u16 recv_num = 0;

    static irda_dec_para_t dec_para = { 0 };
    static irda_dec_result_t dec_result = {{{0}}};

    if (IRDA_SYMPHONY_CHK_OVERFLOW())
    {  // buffer overflow
      IRDA_PRINT("IRDA buffer overflow\n");
      irda_symphony_soft_reset();
      return;
    }

    if(!is_ir_intialized)
    {
      is_ir_intialized = TRUE;
      if (p_irda->irda_protocol == IRDA_SW_NEC)
      {
        dec_result.state = E_NEC_STA_IDLE;
	if(((*(volatile unsigned int *)0xBF140020)&(1<<30))==0)
	{
		dec_result.p_sigtbl = nec_sigtbl;
	}
	else
	{
		dec_result.p_sigtbl = nec_sigtbl_24m;
	}
        dec_result.repeat_interval = 2;
      }
      else if (p_irda->irda_protocol == IRDA_SW_KONKA)
      {
        dec_result.state = E_KK_STA_IDLE;
        dec_result.p_sigtbl = konka_sigtbl;
      }
      else if (p_irda->irda_protocol == IRDA_SW_RC5)
      {
        dec_result.state = E_RC5_STA_IDLE;
        dec_result.p_sigtbl = rc5_sigtbl;
      }
    }

    dec_para.protocol = p_irda->irda_protocol;

    if (dec_para.protocol == IRDA_SW_RC5)
    {
	irda_symphony_rc5_decoder(p_priv, p_irda, &dec_para, &dec_result);
	return;
    }
    
    recv_num = irda_symphony_get_recv_num();
	mtos_printk("\irda_symphony_int_handler [%d] [%d]\n",__LINE__, recv_num);
    while(recv_num)
    {
      irda_symphony_get_ontime_and_period(&ontime, &period);
      IRDA_PRINT("(%d)#ontime:%d, period:%d\n", recv_num, ontime, period);

      dec_para.ontime = ontime;
      dec_para.period = period;
      irda_symphony_decoder(&dec_para, &dec_result);
      if ((p_irda->irda_protocol == IRDA_SW_NEC) &&
	      (dec_result.state == E_NEC_STA_FINISH))
      {
        kcode = dec_result.r.nec.code & 0xff;
        ucode = dec_result.r.nec.usercode;

        save_ircode = (ucode<<8) | kcode;
        break;
      }
	else if ((p_irda->irda_protocol == IRDA_SW_NEC) &&
			(dec_result.state == E_NEC_STA_REPEAT))
	{
	       OS_PRINTF("Fun[%s] Line[%u]  save_ircode = 0x%x,  reg[SYMPHONY_IR_GLOBAL_STA] = 0x%x\n", 
		   			__FUNCTION__,__LINE__, save_ircode, hal_get_u32((volatile u32 *)SYMPHONY_IR_GLOBAL_STA));
		kcode = save_ircode & 0xff;
		ucode = (save_ircode >> 8) & 0xffff;
		dec_result.state = E_NEC_STA_FINISH;
		repeat_status = TRUE;
		break;
	}
      else if ((p_irda->irda_protocol == IRDA_SW_KONKA) &&
               (dec_result.state == E_KK_STA_FINISH))
      {
        kcode = dec_result.r.konka.kcode;
        ucode = dec_result.r.konka.ucode;
        break;
      }
      recv_num --;
    }

    while(recv_num)
    {
      irda_symphony_get_ontime_and_period(&ontime, &period);
      recv_num --;
    }

    if (p_irda->irda_protocol == IRDA_SW_NEC)
    {
      if (dec_result.state != E_NEC_STA_FINISH)
      {
        return;
      }
      else
      {
        // Keep E_NEC_STA_FINISH state for repeated keycode
      }
    }
    else if (p_irda->irda_protocol == IRDA_SW_KONKA)
    {
      if (dec_result.state != E_KK_STA_FINISH)
      {
        return;
      }
      else
      {
        dec_result.state = E_KK_STA_IDLE;
      }
    }
  }

  if(TRUE == p_irda->user_code_set)
  {
    if (p_irda->print_user_code)
    {
      OS_PRINTF("\nReceive irda data code %d", kcode);
      OS_PRINTF("   user code high: %d, user code low: %d\n",
                  ((ucode & 0xFF00) >> 8), (ucode & 0xFF));
    }

    for(i = 0; i < IRDA_MAX_USER; i++)
    {
      if(p_irda->sw_user_code[i] == ucode)
      {
        break;
      }
    }
    if(i >= IRDA_MAX_USER)
    {
      return;
    }
  }

  if((p_irda->irda_protocol == IRDA_NEC) && IRDA_SYMPHONY_CHK_RPTKEY())
  {
    //Attention !!!
    //it shoud be replace to uio_check_rpt_key_symphony after ledkb verify done
    if(FALSE == uio_check_rpt_key_concerto(UIO_IRDA, kcode, i))
    {
      return;
    }
  }
  else if((p_irda->irda_protocol == IRDA_SW_NEC) && (repeat_status == TRUE))
  {
	if(FALSE == uio_check_rpt_key_concerto(UIO_IRDA, kcode, i))
	{
		return;
	}
  }

  irh = UIO_IRDA | ((i) << UIO_USR_CODE_SHIFT);
  irda_code = kcode | (irh << 8);
  if (p_irda->irda_protocol == IRDA_NEC)
  {
    if(IRDA_SYMPHONY_CHK_RPTKEY())
    {
      irda_code |= (1 << 15);
    }
  }
  else if(p_irda->irda_protocol == IRDA_SW_NEC)
  {
	if(repeat_status == TRUE)
	{
		irda_code |= (1 << 15);
	}
  }
  else 
  {
    p_irda->user_code = ucode;
  }

  OS_PRINTF("   user code high: %08x, user code low: %08x\n",
              ((ucode & 0xFF00) >> 8), (ucode & 0xFF));
  OS_PRINTF("   irda code: %08x, %x\n", kcode, irda_code);
  mtos_fifo_put(&p_priv->fifo, irda_code);
}

static u8 irda_symphony_nec_set_rpt_speed(u32 speed_ms)
{
  u32 ot_set = 0;

  /*
   * NEC ir protocol: interval approximately is 110ms between repeat and next repeat.
   * IRDA controller supports a maximum of do once sampling while 15 repeat codes have passed.
  */
  ot_set = (u32)((15 * speed_ms) / 1650);
  ot_set = (ot_set > 0xF)? 0xF : ot_set;

  return ot_set;
}

static RET_CODE irda_symphony_set_hw_usercode(u32 en, u32 usercode)
{
  u32 ptmp = 0;
  u32 dtmp = 0;
  OS_PRINTF("ybian: driver set user code\n");
  ptmp = SYMPHONY_IR_NEC_FILT;
  dtmp = hal_get_u32((volatile u32 *)ptmp);

  if (en)
  {
    //enable usercode filtering
    //only specific usercode acceptable
    dtmp |= 1 << 31;
    dtmp &= ~(0xFFFF << 8);
    dtmp |= (usercode & 0xFFFF)<<8;
    hal_put_u32((volatile u32 *)ptmp, dtmp);
  }
  else
  {
    //disable usercode filtering
    //all usercode acceptable
    dtmp &= ~ (1 << 31);
    dtmp &= ~(0xFFFF << 8);
    hal_put_u32((volatile u32 *)ptmp, dtmp);
  }

  OS_PRINTF("reg SYMPHONY_IR_NEC_FILT = 0x%x\n",hal_get_u32((volatile u32 *)SYMPHONY_IR_NEC_FILT));
  return SUCCESS;
}

static RET_CODE irda_symphony_set_hw_keycode(u32 en, u32 keycode)
{
  u32 ptmp = 0;
  u32 dtmp = 0;

  ptmp = SYMPHONY_IR_NEC_FILT;
  dtmp = hal_get_u32((volatile u32 *)ptmp);

  if (en)
  {
    //enable keycode filtering
    //only specific keycode acceptable
    dtmp |= 1 << 30;
    hal_put_u32((volatile u32 *)ptmp, dtmp);

    dtmp &= ~0xFF;
    dtmp |= keycode & 0xFF;
    hal_put_u32((volatile u32 *)ptmp, dtmp);
  }
  else
  {
    //disable keycode filtering
    //all keycode acceptable
    dtmp &= ~(1 << 30);
    dtmp &= ~0xFF;
    hal_put_u32((volatile u32 *)ptmp, dtmp);
  }

  return SUCCESS;
}

static RET_CODE irda_symphony_get_usercode(lld_uio_t *p_lld, u16 *p_usercode)
{
  irda_priv_t *p_ir = (irda_priv_t *)p_lld->p_irda;
  u16 ucode = 0;
  u32 ptmp = 0;
  u32 dtmp = 0;

  if(p_usercode == NULL)
  {
    IRDA_PRINT("Get IRDA user code fail\n");
    return ERR_PARAM;
  }

  if(p_ir->irda_protocol != IRDA_NEC)
  {
    *p_usercode = p_ir->user_code;
  }
  else
  {
    ptmp = SYMPHONY_IR_NEC_DATA;
    dtmp = hal_get_u32((volatile u32 *)ptmp);
    ucode = dtmp & 0xFFFF;

    *p_usercode = ucode;
  }

  return SUCCESS;
}

static RET_CODE irda_symphony_io_ctrl(lld_uio_t * p_lld, u32 cmd, u32 param)
{
  RET_CODE ret = SUCCESS;
  irda_priv_t *p_ir = (irda_priv_t *) p_lld->p_irda;
  int i = 0;
  
  mtos_printk(" BKS irda_symphony_io_ctrl [0x%x] \n", cmd );

  switch (cmd)
  {
    case UIO_IR_SET_HW_USERCODE:
      ret = irda_symphony_set_hw_usercode(param >> 16, param & 0xffff);
      break;
    case UIO_IR_SET_HW_KEYCODE:
      ret = irda_symphony_set_hw_keycode(param >> 16, param & 0xffff);
      break;
    case UIO_IR_GET_USERCODE:
      ret = irda_symphony_get_usercode(p_lld, (u16 *)param);
      break;
    case UIO_IR_DISABLE_SW_USERCODE:
      p_ir->user_code_set = FALSE;
      for (i = 0; i < IRDA_MAX_USER; i++)
      {
        p_ir->sw_user_code[i] = 0x0;
      }
      break;
    case UIO_IR_PRINT_SW_USERCODE:
      if (param)
      {
        p_ir->print_user_code = TRUE;
      }
      else
      {
        p_ir->print_user_code = FALSE;
      }
      break;
    case UIO_IR_SET_WAVEFILT:
      ret = irda_symphony_set_wfilt(p_ir);
      break;
    case UIO_IR_CHECK_KEY:
      if(p_ir->irda_flag)
      {
        ret = SUCCESS;//irda key pressed
        *((u32 *)param) = p_ir->irda_flag;
      }
      else
      {
        ret = ERR_FAILURE;//no key pressed
        *((u32 *)param) = p_ir->irda_flag;
      }
      break;
    case UIO_IR_CLEAR_KEY_FLAG:
      p_ir->irda_flag = 0;//set the key flag 0
      break;
    default:
      ret = ERR_NOFEATURE;
      break;
  }

  return ret;
}

static RET_CODE irda_symphony_set_sw_usercode(lld_uio_t * p_lld, u8 num, u16 * p_array)
{
  u8 i = 0;
  irda_priv_t *p_ir = (irda_priv_t *) p_lld->p_irda;

  if (num > IRDA_MAX_USER)
  {
    return ERR_PARAM;
  }
  for (i = 0; i < num; i++)
  {
    p_ir->sw_user_code[i] = p_array[i];
  }
  p_ir->user_code_set = TRUE;

  return SUCCESS;
}

/*!
   Irda soft decoding initialization
*/
void irda_symphony_swdec_init(lld_uio_t *p_lld)
{
  irda_priv_t *p_ir = (irda_priv_t *)p_lld->p_irda;

  u32 ptmp = 0;
  u32 dtmp = 0;
  u8 sw_int_freq = 0;

 //symphony      soft nec
   /*
  *	PERIOD_NOISE	31:24		   0x14
  *	IDLE_OT			22:20		   0x7
  *   REPEAT_OT		19:16		   0xF
  *	WF_EN			15:12		   0x0  disable
  *   REPEAT_CTRL		11:8			   0x2
  *   CHK_EN			6			   1
  *   RPTMODE			5			   0   //not accept
  *   SAMPLE_FRQ		4:3			   2   //750K
  *   IDLE_STA			2			   1
  *   MODE			1			   0    //hw nec en
  *   SMP_EN			0			   1	  //
  */

  ptmp = SYMPHONY_IR_GLOBAL_CTRL;
  dtmp = hal_get_u32((volatile u32 *)ptmp);
  dtmp &= 0x0;
  #if DEFAULT_SAMPLE_FREQ
  dtmp |= (0x5 << 24);//period noise set as 0x14
  if ((p_ir->irda_protocol == IRDA_SW_RC5))
  {
  	dtmp |= (0x19 << 24);//period noise set as 0x14
  	dtmp |= (0x5 << 20);//set idle overtime set as 7
  }
  else
  {
  	dtmp |= (0x7 << 20);//set idle overtime set as 7
  }
  dtmp |= (0xF << 16); // set repeat overtime set as 0xF
  #else
  dtmp |= (0x1 << 24);
  dtmp |= (0x1 << 20);
  dtmp |= (0x4 << 16); // set repeat overtime set as 0xF
  #endif
  //dtmp |= (0x3 << 20);//set idle overtime set as 7
  dtmp &= ~(0xF << 12);//Wave filter channel disabled
  dtmp &= ~(0x1 << 6);//set check enable
  dtmp &= ~(1 << 5); // Discard timeout repeated code (~108ms fixed);
  #if DEFAULT_SAMPLE_FREQ
  dtmp |= (0x2 << 3);//sample_clk 187.5
  #else
  dtmp |= (0x3 << 3);//sample_clk 46.875
  #endif
  dtmp |= (0x1 << 2);//idle sta
  dtmp |= (0x1 << 1);//soft protocol mode
  dtmp |= 0x1 ;//sample en

  hal_put_u32((volatile u32 *)ptmp, dtmp);
  //OS_PRINTF("reg(0xBF0A0008) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF0A0008));

  /*
  --IC decoding key down interrupt disabled;
  --IC decoding key up interrupt disabled;
  --Soft decoding Key interrupt enabled;
  --Wave filter interrupt disabled;
  --Soft decoding interrupt every 1 valid sample
  */
  ptmp = SYMPHONY_IR_INT_CFG;
  dtmp = hal_get_u32((volatile u32 *)ptmp);
  dtmp &= (~0xF); // Wave filter interrupt disabled
               // hw nec decoding key up interrupt disabled
               // hw nec decoding key down interrupt disabled
  //if (p_ir->irda_protocol == IRDA_SW_RC5)
  {
  	dtmp |= 0x1 << 2; //soft protocol key down int enable
  }
			   
  dtmp |= 0x1 << 3; //soft protocol overtime enable
  /* Repeat sample interrupt frequency
     1. For IRDA protocol with repeated code (i.e. NEC),
        leave INT_FRQ as zero, generate int for each valid sample
	 2. For IRDA protocol without repeated code,
	    configure INT_FRQ as 1 tipically, generate int every 2 valid samples
  */
  if (!p_ir->irda_sw_recv_decimate)
  {
    sw_int_freq = 0;
  }
  else
  {
    sw_int_freq = (p_ir->irda_sw_recv_decimate - 1) & 0x1F;
  }
  dtmp &= ~(0x1F << 8);
  dtmp |= sw_int_freq << 8;
  hal_put_u32((volatile u32 *)ptmp, dtmp);

  #if 0
  OS_PRINTF("reg(SYMPHONY_IR_INT_CFG) = 0x%x\n",hal_get_u32((volatile u32 *)ptmp));
  OS_PRINTF("reg(0xBF151000) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151000));
  OS_PRINTF("reg(0xBF151004) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151004));
  OS_PRINTF("reg(0xBF151008) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151008));
  OS_PRINTF("reg(0xBF15100c) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF15100c));
  OS_PRINTF("reg(0xBF151010) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151010));
  OS_PRINTF("reg(0xBF151014) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151014));
  OS_PRINTF("reg(0xBF151018) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151018));
  #endif

}

/*!
   Irda hardware decoding initialization
   Irda controller only supports NEC protocol
*/
void irda_symphony_hwdec_init(lld_uio_t *p_lld)
{
  irda_priv_t *p_ir = (irda_priv_t *)p_lld->p_irda;

  u32 ptmp = 0;
  u32 dtmp = 0;
  u8 rpt_ctrl= 0;

  ptmp = SYMPHONY_IR_NEC_FILT;
  dtmp = hal_get_u32((volatile u32 *)ptmp);
  dtmp &= ~(1 << 31); // usercode filter enable
  dtmp &= ~(1 << 30); // keycode filter enable
  hal_put_u32((volatile u32 *)ptmp, dtmp);

  /*
  *	PERIOD_NOISE	31:24		   0x14
  *	IDLE_OT			22:20		   0x7
  *   REPEAT_OT		19:16		   0xF
  *	WF_EN			15:12		   0x0  disable
  *   REPEAT_CTRL		11:8			   0x2
  *   CHK_EN			6			   1
  *   RPTMODE			5			   0   //not accept
  *   SAMPLE_FRQ		4:3			   2   //187.5K
  *   IDLE_STA			2			   1
  *   MODE			1			   0    //hw nec en
  *   SMP_EN			0			   1	  //
  */

  ptmp = SYMPHONY_IR_GLOBAL_CTRL;
  dtmp = hal_get_u32((volatile u32 *)ptmp);
  dtmp &= 0x0;
  dtmp |= 0x5 << 24;//period noise set as 0x14
  dtmp |= 0x7 << 20;//set idle overtime set as 7
  dtmp |= 0x4 << 16; // set repeat overtime set as 0xF
  dtmp &= ~(0xF << 12);//Wave filter channel disabled
  dtmp |= 0x2 << 8;//set repeat ctrl num
  OS_PRINTF("p_ir->irda_nec_repeat_time = %d\n",p_ir->irda_nec_repeat_time);
  if((p_ir->irda_nec_repeat_time == 0) || (p_ir->irda_nec_repeat_time > 1650))
  {
    rpt_ctrl = irda_symphony_nec_set_rpt_speed(IRDA_RPT_TIME_MS);
  }
  else
  {
    rpt_ctrl = irda_symphony_nec_set_rpt_speed(p_ir->irda_nec_repeat_time);
  }
  dtmp |= rpt_ctrl << 8;//set repeat ctrl num
  dtmp &= ~(0x1 << 6);//set check enable
  dtmp &= ~(0x1 << 5);
  //dtmp |= (0x1 << 5);
  //dtmp &= ~(0x1 << 5);//set RPTMODE 0简码超时丢弃
  dtmp |= 0x2 << 3;
  dtmp |= 0x1<<2 ;//idle sta
  dtmp &= ~(0x1 << 1);//hw nec en
  dtmp |= 0x1 ;//sample en
  hal_put_u32((volatile u32 *)ptmp, dtmp);
  //OS_PRINTF("reg(SYMPHONY_IR_GLOBAL_CTRL) = 0x%x\n",hal_get_u32((volatile u32 *)ptmp));
  ptmp = SYMPHONY_IR_INT_CFG;
  dtmp = hal_get_u32((volatile u32 *)ptmp);
  dtmp &= ~0xF; // IC decoding key down interrupt enabled;
  dtmp |= 0x1; // Wave filter interrupt disabled
               // Soft decoding key interrupt disabled
               // IC decoding key up interrupt disabled
  hal_put_u32((volatile u32 *)ptmp, dtmp);
  #if 0
  OS_PRINTF("reg(SYMPHONY_IR_INT_CFG) = 0x%x\n",hal_get_u32((volatile u32 *)ptmp));
  OS_PRINTF("reg(0xBF151000) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151000));
  OS_PRINTF("reg(0xBF151004) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151004));
  OS_PRINTF("reg(0xBF151008) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151008));
  OS_PRINTF("reg(0xBF15100c) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF15100c));
  OS_PRINTF("reg(0xBF151010) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151010));
  OS_PRINTF("reg(0xBF151014) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151014));
  OS_PRINTF("reg(0xBF151018) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151018));
  //OS_PRINTF("reg(0xBF15101c) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF15101c));
  //OS_PRINTF("reg(0xBF151020) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151020));
 // OS_PRINTF("reg(0xBF151024) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151024));
  //OS_PRINTF("reg(0xBF151028) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF151028));
  //OS_PRINTF("reg(0xBF15102c) = 0x%x\n",hal_get_u32((volatile u32 *)0xBF15102c));
  #endif
  nec_time_cfg();

}

static RET_CODE irda_symphony_open(lld_uio_t * p_lld, irda_cfg_t * p_cfg)
{
  irda_priv_t *p_ir = NULL;

  /* this func is for debug*/
  //irda_test();

  IRDA_PRINT("#### symphony irda_symphony_open ####\n");
  if (NULL != p_lld->p_irda)
  {
    IRDA_PRINT("\nIRDA has already been initialized\n");
    return SUCCESS;
  }

  p_lld->p_irda = (irda_priv_t *) mtos_align_malloc(sizeof(irda_priv_t), 8);
  MT_ASSERT(NULL != p_lld->p_irda);
  memset(p_lld->p_irda, 0, sizeof(irda_priv_t));
  p_ir = p_lld->p_irda;
  p_ir->irda_protocol = p_cfg->protocol;
  p_ir->irda_nec_repeat_time = p_cfg->irda_repeat_time;
  p_ir->irda_sw_recv_decimate = p_cfg->irda_sw_recv_decimate;
  p_ir->irda_wfilt_channel = p_cfg->irda_wfilt_channel;
  p_ir->irda_wfilt_channel_cfg[0] = p_cfg->irda_wfilt_channel_cfg[0];
  p_ir->irda_wfilt_channel_cfg[1] = p_cfg->irda_wfilt_channel_cfg[1];
  p_ir->irda_wfilt_channel_cfg[2] = p_cfg->irda_wfilt_channel_cfg[2];
  p_ir->irda_wfilt_channel_cfg[3] = p_cfg->irda_wfilt_channel_cfg[3];

  is_ir_intialized = FALSE;
  irda_symphony_soft_reset();
  if (p_ir->irda_protocol == IRDA_NEC)
  {
    irda_symphony_hwdec_init(p_lld);
    IRDA_PRINT("IRDA mode 0 initialized\n");
  }
  else
  {
    irda_symphony_swdec_init(p_lld);
    IRDA_PRINT("IRDA mode 1 initialized\n");
  }

  IRDA_PRINT("\ BKS nirda open  p_ir->irda_protocol [%d]\n", p_ir->irda_protocol);
  mtos_irq_request(IRQ_IRDA_ID, irda_symphony_int_handler, RISE_EDGE_TRIGGER);
  OS_PRINTF("\nirda open\n");

  return SUCCESS;
}

static RET_CODE irda_symphony_stop(lld_uio_t * p_lld)
{
  mtos_irq_release(IRQ_IRDA_ID, irda_symphony_int_handler);
  if (NULL != p_lld->p_irda)
  {
    mtos_align_free(p_lld->p_irda);
    p_lld->p_irda = NULL;
  }
  is_ir_intialized = FALSE;
  OS_PRINTF("\nirda closed\n");

  return SUCCESS;
}

void irda_attach(lld_uio_t * p_lld)
{
  mtos_printk(" BKS irda_attach %d \n", __LINE__ );
  p_lld->irda_open = irda_symphony_open;
  p_lld->irda_stop = irda_symphony_stop;
  p_lld->irda_io_ctrl = irda_symphony_io_ctrl;
  p_lld->set_sw_usrcode = irda_symphony_set_sw_usercode;
}
