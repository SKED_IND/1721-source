/********************************************************************************************/
/* Montage Technology (Shanghai) Co., Ltd.                                                  */
/* Montage Proprietary and Confidential                                                     */
/* Copyright (c) 2014 Montage Technology Group Limited and its affiliated companies         */
/********************************************************************************************/

#include "sys_types.h"
#include "sys_define.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "lib_util.h"
#include "hal_gpio.h"
#include "hal_misc.h"
#include "mtos_misc.h"
#include "mtos_task.h"
#include "mtos_sem.h"
#include "mtos_printk.h"
#include "mtos_mem.h"
#include "mtos_msg.h"
#include "drv_dev.h"
#include "uio.h"
#include "hal_misc.h"
#include "hal_concerto_irq.h"
#include "hal_watchdog.h"
#include "ipc.h"
#include "common.h"
//#include "../../drvbase/drv_dev_priv.h"
//#include "sys_regs_concerto.h"
#include "lpower.h"
//#include "../lpower_priv.h"
//#include "concerto_standby_drv.h"
#include "standby.h"
#include "sys_regs_symphony.h"
#include "../../drv/power/symphony/standby_img/concerto_lpm.h"
#include "i2c.h"
#include "mips.h"

extern void concerto_standby_entry(standby_config_t * info);

#define RST_REQ0   0xBF510000
#define RST_REQ1   0xBF510004
#define RST_REQ2   0xBF510008
#define RST_REQ3   0xBF51000C
#define RST_CTRL0   0xBF510010
#define RST_CTRL1   0xBF510014
#define RST_CTRL2   0xBF510018
#define RST_CTRL3   0xBF51001C
#define RST_ALLOW0   0xBF510020
#define RST_ALLOW1   0xBF510024
#define RST_ALLOW2   0xBF510028
#define RST_ALLOW3   0xBF51002C

#define cpu0_mask 0<<1
#define cpu1_mask 1<<1
#define cpu2_mask 2<<1

#define CPU0_UPDATE_BUSY 28<<1
#define CPU1_UPDATE_BUSY 29<<1
#define CPU2_UPDATE_BUSY 30<<1
#define BUS_UPDATE_BUSY 31<<1

/*
*	CLK_SYS_CFG
*/
#define CPU0_CLK_SEL 		0
#define CPU1_CLK_SEL 		1
#define CPU2_CLK_SEL 		2
#define APB_CLK_SEL  		4
#define AHB_CLK_SEL  		5
#define SYS_CLK_0_SEL 		8
#define SYS_CLK_1_SEL 		12
#define SYS_CLK_2_SEL 		16
#define AHB_DIV_NUM 		20 
#define APB_DIV_NUM 		24 

/*
*	CLK_TRANSPORT_CFG
*/
#define SECURE_HS_CLK_SEL  16
#define SECURE_HS_CLK_DIV  18
#define SECURE_CLK_SWITCH 19

#define CLK_SYS_CFG   0xBF500000
#define CLK_UPDATE_CFG  0xBF500008
#define CLK_TRANSPORT_CFG  0xBF500024


static u16 *p_standby_fw;

static u32 standby_fw_size = 0;

#define reg32(addr)  (*(volatile unsigned int *)(addr))

RET_CODE standby_cpu_attach_fw_fd650_symphony(void)
{
    #if 0
	static u16 standby_buff_fd650[] =
	{
		#include "aomcu_fd650.rom"
	};

	p_standby_fw = standby_buff_fd650;
	standby_fw_size = sizeof(standby_buff_fd650)/sizeof(u16);
    #endif
	return SUCCESS;
}

RET_CODE standby_cpu_attach_fw_ct1642_symphony(void)
{
    #if 0
	static u16 standby_buff_ct1642[] =
	{
		#include "aomcu_ct1642.rom"
	};

	p_standby_fw = standby_buff_ct1642;
	standby_fw_size = sizeof(standby_buff_ct1642)/sizeof(u16);
    #endif
	return SUCCESS;
}


RET_CODE standby_cpu_attach_fw_no_fp_symphony(void)
{
    #if 0
	static u16 standby_buff_nofp[] =
	{
		#include "aomcu_nofp.rom"
	};

	p_standby_fw = standby_buff_nofp;
	standby_fw_size = sizeof(standby_buff_nofp)/sizeof(u16);
    #endif
	return SUCCESS;
}

RET_CODE standby_cpu_attach_fw_fd650_original_symphony(void)
{
    #if 0
	static u16 standby_buff_fd650[] =
	{
		#include "aomcu_fd650_original.rom"
	};

	p_standby_fw = standby_buff_fd650;
	standby_fw_size = sizeof(standby_buff_fd650)/sizeof(u16);
    #endif
	return SUCCESS;
}

RET_CODE standby_cpu_attach_fw_ct1642_original_symphony(void)
{
    #if 0
	static u16 standby_buff_ct1642[] =
	{
		#include "aomcu_ct1642_original.rom"
	};

	p_standby_fw = standby_buff_ct1642;
	standby_fw_size = sizeof(standby_buff_ct1642)/sizeof(u16);
    #endif
	return SUCCESS;
}

RET_CODE standby_cpu_attach_fw_aogpio_symphony(void)
{
    #if 0
	static u16 standby_buff_gpio[] =
	{
		#include "aomcu_gpio.rom"
	};

	p_standby_fw = standby_buff_gpio;
	standby_fw_size = sizeof(standby_buff_gpio)/sizeof(u16);
    #endif
	return SUCCESS;
}

RET_CODE standby_cpu_attach_fw_ssd1311_symphony(void)
{
	static u16 standby_buff_ssd1311[] =
	{
		#include "aomcu_ssd1311.rom"
	};

	p_standby_fw = standby_buff_ssd1311;
	standby_fw_size = sizeof(standby_buff_ssd1311)/sizeof(u16);
	return SUCCESS;
}

void symphony_dump(int count)
{
#if 0
	u16* base_addr;
	int i ;

	mtos_open_printk();
	base_addr = (u16*)((u32)AOMCU_RAM_ADDR);
	for(i = 0 ; i < count ; i++)
	{
	   OS_PRINTF("[addr:%x, 0x%x] ",base_addr,*base_addr++);
	}
#endif
	OS_PRINTF("\n ****ok ***\n ");
}



/*!
  * we close all the interrupts, and also make use of WII feature
  */
static void stdby_disable_all_intrs(void)
{
  unsigned int g_int_mask0 = 0, g_int_mask1 = 0;
  unsigned int g_cp0_cause = 0, g_cp0_status = 0;
  unsigned int g_cp0_count = 0, g_cp0_compare = 0;

  g_int_mask0 = reg32(R_M_PIC_INT_MASK0);
  g_int_mask1 = reg32(R_M_PIC_INT_MASK1);

  reg32(R_M_PIC_INT_MASK0) = 0xFFFFFFFF;
  reg32(R_M_PIC_INT_MASK1) = 0xFFFFFFFF;

  OS_PRINTF("stdby_disable_all_intrs 0: c0_compare=0x%x, c0_counter=0x%x\n", \
  read_c0_compare(), read_c0_count());
  OS_PRINTF("stdby_disable_all_intrs 0: c0_cause=0x%x, c0_status=0x%x\n", \
  read_c0_cause(), read_c0_status());

  g_cp0_count = read_c0_count();
  g_cp0_compare = read_c0_compare();
  g_cp0_cause = read_c0_cause();
  g_cp0_status = read_c0_status();
  /*!
    *  disable builtin timer counting
    */
  write_c0_cause(read_c0_cause() | (0x1 << 27));
  write_c0_compare(read_c0_compare());
  write_c0_count(0);

  /*!
    *  'Cause we have WI feature indicated by bit31 in CONFIG7,
    *  So we use it
    */
  write_c0_status((read_c0_status() & (~0xFF01)) | (0x10 << 8));

  OS_PRINTF("stdby_disable_all_intrs 1: c0_compare=0x%x, c0_counter=0x%x\n", \
  read_c0_compare(), read_c0_count());
  OS_PRINTF("stdby_disable_all_intrs 1: c0_cause=0x%x, c0_status=0x%x\n", \
  read_c0_cause(), read_c0_status());
}

static void symphony_sty_in_sram(u8 fp_type)
{
	standby_config_t styconf = {0};

	//a temp patch for watchdog hardware bug
	unsigned int temp = 0;
	temp = reg32(0xBF15601C);
	temp &= ~(3 << 24);
	temp |= (1 << 24);
	reg32(0xBF15601C) = temp;

	temp = reg32(0xBF156308);
	temp |= (1 << 19);
	reg32(0xBF156308) = temp;

	temp = reg32(0xBF156000);
	temp &= ~0x3;
	temp |= 0x1;
	reg32(0xBF156000) = temp;

	temp = reg32(0xBF15630C);
	temp |= (1 << 23);
	reg32(0xBF15630C) = temp;

	temp = reg32(0xBF156004);
	temp &= ~(0xff << 16);
	temp |= (0x55 << 16);
	reg32(0xBF156004) = temp;

	temp = reg32(0xBF156300);
	temp |= (0xf << 26);
	reg32(0xBF156300) = temp;
	mtos_task_delay_ms(10);

	/*!
	*  disable all intrs first
	*/
	stdby_disable_all_intrs();

#if 1
	ipc_msg_t msg_send;
	msg_send.time_out_ms = 200;
	{
	/*sleep av cpu*/
	msg_send.msg_id = IPC_MSG_AUD_FW_CMD_SLEEP;
	ap_send_to_av(AP_SYS_DEV_AUD, &msg_send, 1);
	}
#endif

      OS_PRINTF("2DO concerto_standby_entry!\n");
	
	styconf.wakeup_src_mask = fp_type;
	concerto_standby_entry(&styconf);
}


static void symphony_switch_to_high_clk()
{
	unsigned int  temp = 0;

	//check cpu2
	if(reg32(RST_CTRL0)&(1<<12))
	{//bit12 == 1
		reg32(CLK_UPDATE_CFG)|=cpu2_mask;
	}
	else
	{
		reg32(CLK_UPDATE_CFG)&=(~(cpu2_mask));
	}


	temp = reg32(CLK_TRANSPORT_CFG)|(1<<SECURE_HS_CLK_DIV)|(1<<SECURE_HS_CLK_SEL);  
	reg32(CLK_TRANSPORT_CFG) = temp; 

	/*
		step 3. SECURE_CLK_SWITCH = 1 
	*/
	temp = reg32(CLK_TRANSPORT_CFG)|(1<<SECURE_CLK_SWITCH); 
	reg32(CLK_TRANSPORT_CFG) = temp;



	//CPU2_CLK_SEL swtich
	temp = reg32(CLK_SYS_CFG)|(1<<CPU2_CLK_SEL);
	reg32(CLK_SYS_CFG) = temp;

	mtos_task_delay_ms(10);

        while(1)
        {
                if(reg32(CLK_UPDATE_CFG)>>CPU2_UPDATE_BUSY == 0x0){
                        break;
                }
        }

}




u32 load_mcu(enum fp_type type,int need_dump)
{
  int i =0 ;
  volatile u32* base_addr = (volatile u32*)((u32)AOMCU_RAM_ADDR);
  u32 size = standby_fw_size;
  u16 *p_data = p_standby_fw ;


     if(standby_fw_size == 0)
     		MT_ASSERT(0);

	if(p_data != NULL)
	{

		for(i =0;i<size;i++)
		{
			volatile u32*temp = base_addr;

			*(volatile u32*)base_addr++ = (u32)p_data[i];

			//OS_PRINTF("standby_buff_aomcu %x :  %x\n",i,p_data[i]);

			if(((u32)p_data[i]) != *temp)
			{
				OS_PRINTF("0x%x ,0x%x,0x%x\n",i,p_data[i],*temp);
				return ERR_FAILURE;
			}
	 	}
	}
	else{
	   OS_PRINTF("download.....unsupport bin\n");
	   return ERR_FAILURE;
	}

	if(need_dump == 1){
		symphony_dump(size);
	}

	return SUCCESS;
}


u32 symphony_sram_aomcu(struct standby_cpu *cmd)
{
	OS_PRINTF("***StandbyCPU:irda_key[0x%x] fp_key[0x%x] cur_time[%d]:[%d] reset_min[%d]\n",
				cmd->pd_key0,cmd->pd_key_fp,cmd->hour,cmd->minute,cmd->auto_power_up_system_minute);

	u8 wake_up_days = 0;
	trans_info_standby_t standby_info;
	u32 *p_standby_info = (u32*)&standby_info;
	u32 dtmp = 0;


	if(SUCCESS != load_mcu(cmd->type, 0))
	{
	    OS_PRINTF("load standby bin error ............\n");
	    return -1;
	}

	if(cmd->p_raw_map)
	{
		u8 *map = cmd->p_raw_map;
		standby_info.bitmap[0] =((map[1]<<4)|map[0]);
		standby_info.bitmap[1]= ((map[3]<<4)|map[2]);
		standby_info.bitmap[2]= ((map[5]<<4)|map[4]);
		standby_info.bitmap[3]= ((map[7]<<4)|map[6]);

		OS_PRINTF("standby_info.bitmap[0x%x][0x%x][0x%x][0x%x][0x%x][0x%x][0x%x][0x%x]\n\n",
																		map[0], map[1],
																		map[2], map[3],
																		map[4], map[5],
																		map[6], map[7]);
	}

	standby_info.cur_hour = cmd->hour;
	standby_info.cur_min = cmd->minute;
	u32 tmp_hours = cmd->auto_power_up_system_minute/60;

	if(tmp_hours >= 24)
	    wake_up_days = tmp_hours/24;

	if(wake_up_days > 254)
	{
	       OS_PRINTF("set days[%d]over flow \n",wake_up_days);
	       wake_up_days = 254;
	}

	standby_info.wake_day = wake_up_days;
	standby_info.wake_hour = tmp_hours%24;
	standby_info.wake_min = cmd->auto_power_up_system_minute%60;
	standby_info.wake_up_key = cmd->pd_key_fp;
	OS_PRINTF("wakeup time:  day = %u, hour = %d, min = %d\n",standby_info.wake_day,standby_info.wake_hour,standby_info.wake_min);

	//standby_info.standby_config_info = 0 ;

      standby_info.standby_config_info = cmd->standby_config;

	if(cmd->en_led_disp_time)
	{
		standby_info.standby_config_info |= (0x1 << 6);
		OS_PRINTF("#1config_info 0x%x\n",standby_info.standby_config_info);
	}
	if(cmd->en_led_disp_char)
	{
		standby_info.standby_config_info |= (0x1 << 5);
		OS_PRINTF("#2config_info 0x%x\n",standby_info.standby_config_info);
	}

	if(cmd->auto_time_wake_up)
	{
		standby_info.standby_config_info |= (0x1 << 7);
		OS_PRINTF("#3config_info 0x%x\n",standby_info.standby_config_info);
	}

	OS_PRINTF("#5 0x%x\n",standby_info.standby_config_info);


	standby_info.standby_config_info2 = 0;
	if(cmd->led_infor.config_led_en){

	    standby_info.standby_config_info2 |= (0x1 << 1);
	    standby_info.led_pos= (cmd->led_infor.led_pos[0] & 0x3) + ((cmd->led_infor.led_pos[1] & 0x3) << 2) +      \
			               ((cmd->led_infor.led_pos[2] & 0x3) << 4) + ((cmd->led_infor.led_pos[3] & 0x3) << 6);
	    OS_PRINTF("#config_inf3 0x%x,led_pos=0x%x\n",standby_info.standby_config_info2,standby_info.led_pos);
	}
	if(cmd->led_infor.config_lock_en){

	    standby_info.standby_config_info2 |= (0x1 << 2);
	    standby_info.standby_config_info2 |= ((cmd->led_infor.lock_pos & 0x3) << 3);
	    OS_PRINTF("#config_inf4 0x%x,standby_config_info2=0x%x\n",standby_info.standby_config_info2,standby_info.standby_config_info2);
	}
	 if(cmd->led_infor.config_colon_en){

	    standby_info.standby_config_info2 |= (0x1 << 5);
	    standby_info.standby_config_info2 |= ((cmd->led_infor.colon_pos & 0x3) << 6);
	    OS_PRINTF("#config_inf5 0x%x,standby_config_info2=0x%x\n",standby_info.standby_config_info2,standby_info.standby_config_info2);
	}

	*((volatile u32 *)LPM_MESSAGE2AO_0) =  *p_standby_info++;
	*((volatile u32 *)LPM_MESSAGE2AO_1) =  *p_standby_info++;
	*((volatile u32 *)LPM_MESSAGE2AO_2) =  *p_standby_info++;
	*((volatile u32 *)LPM_MESSAGE2AO_3) =  *p_standby_info;


	OS_PRINTF("\n data register  0xbfedc014 0x%x",*((volatile u32 *)LPM_MESSAGE2AO_0));
	OS_PRINTF("\n data register  0xbfedc018 0x%x",*((volatile u32 *)LPM_MESSAGE2AO_1));
	OS_PRINTF("\n data register  0xbfedc01c 0x%x",*((volatile u32 *)LPM_MESSAGE2AO_2));
	OS_PRINTF("\n data register  0xbfedc020 0x%x\n\n",*((volatile u32 *)LPM_MESSAGE2AO_3));

	dtmp = hal_get_u32((volatile u32 *)R_IR_GLOBAL_CTRL);
	dtmp &= ~(0x1 << 1);//hw nec en for ir wavefilter
	hal_put_u32((volatile u32 *)R_IR_GLOBAL_CTRL, dtmp);


	mtos_task_delay_ms(5);

	symphony_switch_to_high_clk();
	symphony_sty_in_sram(cmd->type);
	

	return -1;  // can not go here....

}



u32 symphony_standbycpu(struct standby_cpu *cmd)
{
	OS_PRINTF("***StandbyCPU:irda_key[0x%x] fp_key[0x%x] cur_time[%d]:[%d] reset_min[%d]\n",
				cmd->pd_key0,cmd->pd_key_fp,cmd->hour,cmd->minute,cmd->auto_power_up_system_minute);

	u8 wake_up_days = 0;
	trans_info_standby_t standby_info;
	u32 *p_standby_info = (u32*)&standby_info;
	u32 dtmp = 0;

	if(SUCCESS != load_mcu(cmd->type, 0))
	{
	    OS_PRINTF("load standby bin error ............\n");
	    return -1;
	}

	if(cmd->p_raw_map)
	{
		u8 *map = cmd->p_raw_map;
		standby_info.bitmap[0] =((map[1]<<4)|map[0]);
		standby_info.bitmap[1]= ((map[3]<<4)|map[2]);
		standby_info.bitmap[2]= ((map[5]<<4)|map[4]);
		standby_info.bitmap[3]= ((map[7]<<4)|map[6]);

		OS_PRINTF("standby_info.bitmap[0x%x][0x%x][0x%x][0x%x][0x%x][0x%x][0x%x][0x%x]\n\n",
																		map[0], map[1],
																		map[2], map[3],
																		map[4], map[5],
																		map[6], map[7]);
	}

	standby_info.cur_hour = cmd->hour;
	standby_info.cur_min = cmd->minute;
	u32 tmp_hours = cmd->auto_power_up_system_minute/60;

	if(tmp_hours >= 24)
	    wake_up_days = tmp_hours/24;

	if(wake_up_days > 254)
	{
	       OS_PRINTF("set days[%d]over flow \n",wake_up_days);
	       wake_up_days = 254;
	}

	standby_info.wake_day = wake_up_days;
	standby_info.wake_hour = tmp_hours%24;
	standby_info.wake_min = cmd->auto_power_up_system_minute%60;
	standby_info.wake_up_key = cmd->pd_key_fp;
	OS_PRINTF("wakeup time:  day = %u, hour = %d, min = %d\n",standby_info.wake_day,standby_info.wake_hour,standby_info.wake_min);

	//standby_info.standby_config_info = 0 ;

      standby_info.standby_config_info = cmd->standby_config;

	if(cmd->en_led_disp_time)
	{
		standby_info.standby_config_info |= (0x1 << 6);
		OS_PRINTF("#1config_info 0x%x\n",standby_info.standby_config_info);
	}
	if(cmd->en_led_disp_char)
	{
		standby_info.standby_config_info |= (0x1 << 5);
		OS_PRINTF("#2config_info 0x%x\n",standby_info.standby_config_info);
	}

	if(cmd->auto_time_wake_up)
	{
		standby_info.standby_config_info |= (0x1 << 7);
		OS_PRINTF("#3config_info 0x%x\n",standby_info.standby_config_info);
	}

	OS_PRINTF("#5 0x%x\n",standby_info.standby_config_info);


	standby_info.standby_config_info2 = 0;
	if(cmd->led_infor.config_led_en){

	    standby_info.standby_config_info2 |= (0x1 << 1);
	    standby_info.led_pos= (cmd->led_infor.led_pos[0] & 0x3) + ((cmd->led_infor.led_pos[1] & 0x3) << 2) +      \
			               ((cmd->led_infor.led_pos[2] & 0x3) << 4) + ((cmd->led_infor.led_pos[3] & 0x3) << 6);
	    OS_PRINTF("#config_inf3 0x%x,led_pos=0x%x\n",standby_info.standby_config_info2,standby_info.led_pos);
	}
	if(cmd->led_infor.config_lock_en){

	    standby_info.standby_config_info2 |= (0x1 << 2);
	    standby_info.standby_config_info2 |= ((cmd->led_infor.lock_pos & 0x3) << 3);
	    OS_PRINTF("#config_inf4 0x%x,standby_config_info2=0x%x\n",standby_info.standby_config_info2,standby_info.standby_config_info2);
	}
	 if(cmd->led_infor.config_colon_en){

	    standby_info.standby_config_info2 |= (0x1 << 5);
	    standby_info.standby_config_info2 |= ((cmd->led_infor.colon_pos & 0x3) << 6);
	    OS_PRINTF("#config_inf5 0x%x,standby_config_info2=0x%x\n",standby_info.standby_config_info2,standby_info.standby_config_info2);
	}

	*((volatile u32 *)LPM_MESSAGE2AO_0) =  *p_standby_info++;
	*((volatile u32 *)LPM_MESSAGE2AO_1) =  *p_standby_info++;
	*((volatile u32 *)LPM_MESSAGE2AO_2) =  *p_standby_info++;
	*((volatile u32 *)LPM_MESSAGE2AO_3) =  *p_standby_info;


	OS_PRINTF("\n data register  0xbfedc014 0x%x",*((volatile u32 *)LPM_MESSAGE2AO_0));
	OS_PRINTF("\n data register  0xbfedc018 0x%x",*((volatile u32 *)LPM_MESSAGE2AO_1));
	OS_PRINTF("\n data register  0xbfedc01c 0x%x",*((volatile u32 *)LPM_MESSAGE2AO_2));
	OS_PRINTF("\n data register  0xbfedc020 0x%x\n\n",*((volatile u32 *)LPM_MESSAGE2AO_3));

	dtmp = hal_get_u32((volatile u32 *)R_IR_GLOBAL_CTRL);
	dtmp &= ~(0x1 << 1);//hw nec en for ir wavefilter
	hal_put_u32((volatile u32 *)R_IR_GLOBAL_CTRL, dtmp);

	mtos_task_delay_ms(1);

	*((volatile u32 *)LPM_AOMCU_EN) = 1;   // standby cpu work@@@

	return -1;  // can not go here....

}




